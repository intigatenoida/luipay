package com.app.luipay.response.passwordResponse

import com.app.luipay.response.BaseResponse

data class CreatePasswordResponse(
    val Response: Response,
    val ResponseCollection: List<Any>
):BaseResponse()