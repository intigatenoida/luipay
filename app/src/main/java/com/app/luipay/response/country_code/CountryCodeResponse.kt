package com.app.luipay.response.AppStatusResponse.country_code

import com.app.luipay.response.BaseResponse

data class CountryCodeResponse(
    val Response: Any,
    val ResponseCollection: List<ResponseCollection>
) : BaseResponse()