package com.app.luipay.response.profileImageResponse

data class Response(
    val ProfilePictureUrl: String
)