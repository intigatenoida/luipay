package com.app.luipay.response

import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName

@Keep
open class BaseResponse(
    @SerializedName("AppVersion")
    val AppVersion: Any = "",
    @SerializedName("MessageKey")
    val MessageKey: String = "",
    @SerializedName("ResponseStatus")
    val ResponseStatus: String = ""
)