package com.app.luipay.response.login_response

data class AccessToken(
    val access_token: String,
    val expires_in: Int,
    val token_type: Any
)