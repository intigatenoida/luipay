package com.app.luipay.response.login_response

data class Response(
    val AccessToken: AccessToken,
    val AuthenticationKey: Any,
    val IsDefaultDevice: Boolean,
    val IsKycPending: Boolean,
    val IsOtpRequired: Boolean
)