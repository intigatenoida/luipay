package com.app.luipay.response.AppStatusResponse

data class ResponseCollection(
    val MessageDescription: String,
    val StatusCode: Int
)