package com.app.luipay.response.login_response

import com.app.luipay.response.BaseResponse

data class LoginResponse(
    val Response: Response,
    val ResponseCollection: List<Any>
):BaseResponse()