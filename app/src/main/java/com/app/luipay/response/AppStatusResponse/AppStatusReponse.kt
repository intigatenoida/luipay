package com.app.luipay.response.AppStatusResponse

import androidx.annotation.Keep
import com.app.luipay.response.BaseResponse
import com.google.gson.annotations.SerializedName

@Keep
data class AppStatusReponse(
    @SerializedName("Response")
    val Response: Any,
    @SerializedName("ResponseCollection")
    val ResponseCollection: List<ResponseCollection>
) : BaseResponse()