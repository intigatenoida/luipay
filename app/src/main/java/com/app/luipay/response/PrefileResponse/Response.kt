package com.app.luipay.response.PrefileResponse

data class Response(
    var Address: String,
    var Dob: String,
    var Email: String,
    var Gender: String,
    var IsKycPending: Boolean,
    var ProfilePicUrl: String,
    var TransactionLimit: String,
    var CountryCodeId: Int,
    var CountryCode: String,
    var MobileNo: String,
    var Name: String


)