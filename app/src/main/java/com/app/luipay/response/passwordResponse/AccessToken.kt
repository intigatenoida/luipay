package com.app.luipay.response.passwordResponse

data class AccessToken(
    val access_token: String,
    val expires_in: Int,
    val token_type: Any
)