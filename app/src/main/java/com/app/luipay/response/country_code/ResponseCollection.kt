package com.app.luipay.response.AppStatusResponse.country_code

data class ResponseCollection(
    val Text: String,
    val Value: Int
)