package com.app.luipay.response.kyc_list_response

import com.app.luipay.response.BaseResponse

data class KYCListResponse(
    val Response: Any,
    val ResponseCollection: List<ResponseCollection>
):BaseResponse()