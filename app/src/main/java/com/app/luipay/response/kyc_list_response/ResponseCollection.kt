package com.app.luipay.response.kyc_list_response

data class ResponseCollection(
    val Text: String,
    val Value: Int
)