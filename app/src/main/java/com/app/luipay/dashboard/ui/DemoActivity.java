package com.app.luipay.dashboard.ui;

import android.animation.ValueAnimator;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.app.luipay.R;
import com.app.luipay.util.BaseActivity;

public class DemoActivity extends BaseActivity {
    boolean isShow = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.animation_demo);
        ConstraintLayout header = findViewById(R.id.hedaer);
        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        float dpWidth = displayMetrics.widthPixels ;

        ((Button) findViewById(R.id.submit)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isShow) {
                    isShow = false;
                    ValueAnimator va = ValueAnimator.ofInt(100, 0);
                    va.setDuration(400);
                    va.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                        public void onAnimationUpdate(ValueAnimator animation) {
                            Integer value = (Integer) animation.getAnimatedValue();
                            Log.e("*******************min","width"+value);
                            header.getLayoutParams().height = value.intValue();
                            header.getLayoutParams().width = (int) ((value.intValue() * dpWidth) / 100);
                            header.requestLayout();
                        }
                    });
                    va.start();
                } else {
                    ValueAnimator va = ValueAnimator.ofInt(0, 100);
                    va.setDuration(400);
                    va.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                        public void onAnimationUpdate(ValueAnimator animation) {
                            Integer value = (Integer) animation.getAnimatedValue();
                            Log.e("*******************max","width"+value);
                            header.getLayoutParams().height = value.intValue();
                            header.getLayoutParams().width = (int) ((value.intValue() * dpWidth) / 100);
                            header.requestLayout();
                        }
                    });
                    va.start();
                    isShow = true;
                }
            }
        });
    }
}
