package com.app.luipay.dashboard.ui.fragment

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import com.app.luipay.R
import com.app.luipay.giftcard.activity.GiftCardActivity
import com.app.luipay.link_bank.activity.CardBankAccountACtivity
import com.app.luipay.network.ApiAuthHelper
import com.app.luipay.network.ApiClient
import com.app.luipay.network.CallBackManager
import com.app.luipay.network.RetroError
import com.app.luipay.profile.activity.ProfileActivity
import com.app.luipay.request.GiftListRequest
import com.app.luipay.response.BaseResponse
import com.app.luipay.setting.SettingActivity
import com.app.luipay.util.Appconstant
import com.app.luipay.util.BaseFragment
import com.app.luipay.util.CommonUtils
import com.app.luipay.util.Prefutils
import kotlinx.android.synthetic.main.fragment_my_wallet.*

class MyWalletFragment : BaseFragment(), View.OnClickListener {

    companion object {
        val TAG: String = "MyWalletFragment"
        fun getInstance(): MyWalletFragment {
            val fragment = MyWalletFragment()
            return fragment
        }
    }

    private var rootView: View? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        rootView = inflater.inflate(R.layout.fragment_my_wallet, container, false)
        rootView!!.findViewById<ImageView>(R.id.imgBack).visibility = View.GONE
        rootView!!.findViewById<TextView>(R.id.headerTitle).text = getString(R.string.my_wallet)



        rootView!!.findViewById<ConstraintLayout>(R.id.settinglayout).setOnClickListener(this)
        rootView!!.findViewById<ConstraintLayout>(R.id.profileLayout).setOnClickListener(this)
        rootView!!.findViewById<ConstraintLayout>(R.id.cardlayout).setOnClickListener(this)
        rootView!!.findViewById<ConstraintLayout>(R.id.giftCardlayout).setOnClickListener(this)


        return rootView
    }

    override fun onResume() {
        super.onResume()
        getGiftCount()
        setProfileData()
    }

    private fun setProfileData() {
        val profileData = Prefutils.getProfileData(activity!!)
        profileData?.let {
            txtProfileName.text = it.Name
            txtProfileNumber.text = it.MobileNo
        }
        CommonUtils.loadProfilePic(activity!!, imgProfile)
    }

    private fun getGiftCount() {
        if (!isOnline())
            return
        val helper = ApiClient.getClient(activity!!).create(ApiAuthHelper::class.java)
        val request = GiftListRequest()
        request.LanguageTypeId =
            Prefutils.getFromPrefsInt(activity, Prefutils.API_LANGUAGE)
        request.ServiceAuthenticationToken = Appconstant.API_KEY
        val call = helper.getGiftList(request, Appconstant.API_HEADER)
        call.enqueue(object : CallBackManager<BaseResponse>() {
            override fun onSuccess(any: BaseResponse?, message: String) {


            }

            override fun onFailure(message: String) {
//                CommonUtils.showToast(activity!!, message)
                giftCard.text = "0 ${getString(R.string.gift_card)}"
            }

            override fun onError(error: RetroError) {

            }

        })
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.settinglayout -> {
                startActivity(Intent(activity, SettingActivity::class.java))
            }
            R.id.profileLayout -> {
                startActivity(Intent(activity, ProfileActivity::class.java))
            }
            R.id.cardlayout -> {
                startActivity(Intent(activity, CardBankAccountACtivity::class.java))
            }
            R.id.giftCardlayout -> {
                startActivity(Intent(activity, GiftCardActivity::class.java))
            }
        }
    }
}