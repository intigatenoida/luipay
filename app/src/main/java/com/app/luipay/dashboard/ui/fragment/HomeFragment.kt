package com.app.luipay.dashboard.ui.fragment

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.viewpager.widget.ViewPager
import com.app.luipay.R
import com.app.luipay.dashboard.ui.adapter.HeaderPagerAdapter
import com.app.luipay.deposite.activity.DepositeBankListActivity
import com.app.luipay.link_bank.activity.LinkBankActivity
import com.app.luipay.loginuser.LoginSelectionActivity
import com.app.luipay.notification.activity.NotificationActivity
import com.app.luipay.payment_code.activity.PaymentCodeActivity
import com.app.luipay.request_transfer.activity.RequestMoneyActivity
import com.app.luipay.transfer.activity.TransferActivity
import com.app.luipay.util.BaseFragment

class HomeFragment : BaseFragment(), View.OnClickListener {


    companion object {
        val TAG: String = "HomeFragment"
        public fun getInstance(): HomeFragment {
            var fragment = HomeFragment()
            return fragment;
        }
    }

    private lateinit var rootView: View

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        rootView = inflater.inflate(R.layout.fragment_home, container, false)
        var viewPagerHeader = rootView.findViewById<ViewPager>(R.id.headerViewPager)
        viewPagerHeader.adapter = activity?.let { HeaderPagerAdapter(it) }


        var viewPagerFooter = rootView.findViewById<ViewPager>(R.id.bottomViewPager)
        viewPagerFooter.adapter = activity?.let { HeaderPagerAdapter(it) }
        rootView.findViewById<TextView>(R.id.transfer).setOnClickListener(this)
        rootView.findViewById<TextView>(R.id.requestMoneyTransfer).setOnClickListener(this)
        rootView.findViewById<TextView>(R.id.linkAccount).setOnClickListener(this)
        rootView.findViewById<TextView>(R.id.paymentCode).setOnClickListener(this)
        rootView.findViewById<ImageView>(R.id.notification).setOnClickListener(this)
        rootView.findViewById<TextView>(R.id.dipositeIntoWallet).setOnClickListener(this)
        rootView.findViewById<ImageView>(R.id.logout).setOnClickListener(this)


        return rootView
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.transfer -> {
                startActivity(Intent(activity, TransferActivity::class.java))
            }
            R.id.notification -> {
                startActivity(Intent(activity, NotificationActivity::class.java))
            }
            R.id.requestMoneyTransfer -> {
                startActivity(Intent(activity, RequestMoneyActivity::class.java))
            }
            R.id.linkAccount -> {
                startActivity(Intent(activity, LinkBankActivity::class.java))
            }
            R.id.paymentCode -> {
                startActivity(Intent(activity, PaymentCodeActivity::class.java))
            }
            R.id.dipositeIntoWallet -> {
                startActivity(Intent(activity, DepositeBankListActivity::class.java))
            }
            R.id.logout -> {
                val intent = Intent(activity, LoginSelectionActivity::class.java)
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                startActivity(intent)
                activity!!.finish()
            }


        }
    }
}