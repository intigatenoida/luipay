package com.app.luipay.dashboard.ui

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.transition.TransitionManager
import com.app.luipay.R
import com.app.luipay.dashboard.ui.fragment.HomeFragment
import com.app.luipay.dashboard.ui.fragment.MyWalletFragment
import com.app.luipay.network.ApiAuthHelper
import com.app.luipay.network.ApiClient
import com.app.luipay.network.CallBackManager
import com.app.luipay.network.RetroError
import com.app.luipay.notification.activity.NotificationActivity
import com.app.luipay.request.ProfileRequest
import com.app.luipay.response.PrefileResponse.ProfileResponse
import com.app.luipay.util.Appconstant
import com.app.luipay.util.BaseActivity
import com.app.luipay.util.CommonUtils
import com.app.luipay.util.Prefutils
import com.google.gson.GsonBuilder
import kotlinx.android.synthetic.main.layout_bottom_navigation.*


class DashboardActivity :
    BaseActivity(),
    View.OnClickListener {

    var lastActionId = -1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dashboard)
        LoadFragment(HomeFragment.getInstance(), HomeFragment.TAG)
        initListener()
        getProfileData()
    }

    private fun initListener() {
        homeActionParent.setOnClickListener(this)
        hotActionParent.setOnClickListener(this)
        historyActionParent.setOnClickListener(this)
        walletActionParent.setOnClickListener(this)
    }

    private fun getProfileData() {
        showProcessDialog()
        val helper =
            ApiClient.getClient(this@DashboardActivity).create(ApiAuthHelper::class.java)
        val profileRequest = ProfileRequest()

        profileRequest.LanguageTypeId = Appconstant.API_LANGUAGE
        profileRequest.ServiceAuthenticationToken = Appconstant.API_KEY
        val call = helper.getProfile(profileRequest, Appconstant.API_HEADER)
        call.enqueue(object : CallBackManager<ProfileResponse>() {
            override fun onSuccess(any: ProfileResponse?, message: String) {
                val respone = (any as ProfileResponse)
                var tempResponse =
                    GsonBuilder().disableHtmlEscaping().create().toJson(respone.Response)
                Prefutils.saveToPrefs(
                    this@DashboardActivity,
                    Prefutils.PROFILE,
                    tempResponse
                )

                hideProcessDailog()
            }

            override fun onFailure(message: String) {
                hideProcessDailog()
            }

            override fun onError(error: RetroError) {
                hideProcessDailog()
            }

        })

    }


    private fun LoadFragment(
        instance: Fragment,
        tag: String
    ) {
        val transition = supportFragmentManager.beginTransaction()
        transition.replace(R.id.container_home, instance, tag)
        transition.addToBackStack(null)
        transition.commit()
    }


    override fun onBackPressed() {
        if (lastActionId == R.id.walletAction) {
            homeAction.performClick()
        } else {
            finish()
        }
    }

    override fun onClick(v: View?) {

        if (lastActionId == v!!.id) return
        disableAll()

        when (v.id) {
            R.id.homeActionParent -> {
                lastActionId = R.id.homeActionParent
                homeAction.background = ContextCompat.getDrawable(this, R.drawable.nav_rectangle)
                home_icon.setImageResource(R.drawable.home_nav_new)
                home_icon_text.visibility = View.VISIBLE
                LoadFragment(HomeFragment.getInstance(), HomeFragment.TAG)

            }
            R.id.hotActionParent -> {
                lastActionId = R.id.hotActionParent
                CommonUtils.showToast(this, "coming soon")
                homeActionParent.performClick()
            }
            R.id.historyActionParent -> {
                lastActionId = R.id.historyActionParent
                startActivity(Intent(this, NotificationActivity::class.java))
                homeActionParent.performClick()
            }
            R.id.walletActionParent -> {
                lastActionId = R.id.walletActionParent
                walletAction.background = ContextCompat.getDrawable(this, R.drawable.nav_rectangle)
                wallet_icon.setImageResource(R.drawable.wallet_selected)
                wallet_icon_text.visibility = View.VISIBLE
                LoadFragment(MyWalletFragment.getInstance(), MyWalletFragment.TAG)

            }
        }

        TransitionManager.beginDelayedTransition(bottomBar)
    }

    private fun disableAll() {
        home_icon_text.visibility = View.GONE
        hot_icon_text.visibility = View.GONE
        hist_icon_text.visibility = View.GONE
        wallet_icon_text.visibility = View.GONE
        home_icon.setImageResource(R.drawable.nav_home_unselect)
        wallet_icon.setImageResource(R.drawable.wallet_nav)
        homeAction.setBackgroundColor(ContextCompat.getColor(this, R.color.colorWhite))
        hotAction.setBackgroundColor(ContextCompat.getColor(this, R.color.colorWhite))
        historyAction.setBackgroundColor(ContextCompat.getColor(this, R.color.colorWhite))
        walletAction.setBackgroundColor(ContextCompat.getColor(this, R.color.colorWhite))
    }


}
