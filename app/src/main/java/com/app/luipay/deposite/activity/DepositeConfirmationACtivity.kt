package com.app.luipay.deposite.activity

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import com.app.luipay.R
import com.app.luipay.request_transfer.activity.TransferResultActivity
import com.app.luipay.transfer.dailog.FingerprintDailog
import com.app.luipay.transfer.model.ContactNumberModel
import com.app.luipay.util.BaseActivity
import kotlinx.android.synthetic.main.activity_deposite_confirmation.*
import kotlinx.android.synthetic.main.header_center_text_left_arrow.*

class DepositeConfirmationACtivity : BaseActivity(), FingerprintDailog.onCancelListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_deposite_confirmation)
        initHeader()
    }

    private fun initHeader() {
        imgBack.setOnClickListener { finish() }
        headerTitle.text = getString(R.string.deposite_into_wallet)
        nextButton.setOnClickListener {
            openFingerprintDialog()

        }

    }

    private fun startConfirmationActivity() {
        val selectedContactList=ArrayList<ContactNumberModel>()
        val contactNumberModel=ContactNumberModel()
        contactNumberModel.amount="4325 tk"
        contactNumberModel.name="Balance in the wallet:"
        selectedContactList.add(contactNumberModel)

         val contactNumberModel1=ContactNumberModel()
        contactNumberModel1.amount="2784100995"
        contactNumberModel1.name="Trading code:"
        selectedContactList.add(contactNumberModel1)

        var bundle = Bundle()
        bundle.putParcelableArrayList("contact", selectedContactList)
        val intent = Intent(
            this@DepositeConfirmationACtivity,
            TransferResultActivity::class.java
        )
        intent.putExtras(bundle)
        startActivityForResult(intent,100)
    }


    private fun openFingerprintDialog() {
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        val prev = supportFragmentManager.findFragmentByTag("dialog")
        if (prev != null) {
            fragmentTransaction.remove(prev)
        }
        fragmentTransaction.addToBackStack(null)
        val dialogFragment = FingerprintDailog.getInstance()
        dialogFragment.show(fragmentTransaction, "dialog")
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 100 && resultCode == Activity.RESULT_OK) {
            val resultIntent = Intent()
            setResult(Activity.RESULT_OK, resultIntent)
            finish()
        }
    }

    override fun onCancel() {
        startConfirmationActivity()
    }
}