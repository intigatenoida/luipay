package com.app.luipay.deposite.activity

import android.app.Activity
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.text.Editable
import android.text.Selection
import android.text.TextWatcher
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.luipay.R
import com.app.luipay.deposite.adapter.DepositeBankListAapter
import com.app.luipay.deposite.model.BankListModel
import com.app.luipay.util.BaseActivity
import com.app.luipay.util.CommonUtils
import kotlinx.android.synthetic.main.activity_deposite_bank_list.*
import kotlinx.android.synthetic.main.header_center_text_left_arrow.*


class DepositeBankListActivity : BaseActivity(), DepositeBankListAapter.onBankSelectListener {

    var bankList = ArrayList<BankListModel>()
    lateinit var bankListAdapter: DepositeBankListAapter
    lateinit var selectedBankData: BankListModel
    var lastIndex: Int = -1
    private val plus91 = "tk "


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_deposite_bank_list)
        initListener()
        accountRecyclerView.layoutManager = LinearLayoutManager(this)
        val verticalDecoration = DividerItemDecoration(
            this,
            DividerItemDecoration.VERTICAL
        )
        val verticalDivider =
            ContextCompat.getDrawable(this, R.drawable.divider_16)
        verticalDecoration.setDrawable(verticalDivider!!)
        accountRecyclerView.addItemDecoration(verticalDecoration)
        bankListAdapter = DepositeBankListAapter(this, getBankData(), this)
        accountRecyclerView.adapter = bankListAdapter
        addTextWatcher()

//        processTesting()
    }

    private fun processTesting() {
        showProcessDialog()
        Handler().postDelayed(Runnable { hideProcessDailog() }, 5000)
    }


    private fun addTextWatcher() {

        amountEdt.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                enableButton()
/*//                manageCurrency(s.toString())
//                if (s.toString().trim().length==0){
//                    amountEdt.setTypeface(
//                        Typeface.createFromAsset(
//                            assets,
//                            "poppins_regular.ttf"
//                        )
//                    )
//                }else{
//                    amountEdt.setTypeface(
//                        Typeface.createFromAsset(
//                            assets,
//                            "poppins_medium.ttf"
//                        )
//                    )
//                }*/
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

        })
    }

    private fun manageCurrency(str: String) {

        if (!str.startsWith(plus91)) {
            if (str.contains(" ")) {
                val strSplit = str.split(" ").toTypedArray()
                if (strSplit.size == 2) {
                    amountEdt.setText(plus91 + strSplit[1])
                    val position: Int = amountEdt.length()
                    val etext: Editable = amountEdt.text!!
                    Selection.setSelection(etext, position)
                } else {
                    amountEdt.setText(plus91)
                    val position: Int = amountEdt.length()
                    val etext: Editable = amountEdt.text!!
                    Selection.setSelection(etext, position)
                }
            } else {
                val length: Int = amountEdt.getText().toString().length
                if (length > 2) {
                    amountEdt.setText(
                        plus91 + amountEdt.getText().toString().substring(
                            2,
                            amountEdt.getText().toString().trim({ it <= ' ' }).length
                        )
                    )
                } else {
                    amountEdt.setText(plus91)
                }
                val position: Int = amountEdt.length()
                val etext: Editable = amountEdt.text!!
                Selection.setSelection(etext, position)
            }
        }
    }

    private fun enableButton() {
        if ((::selectedBankData.isInitialized)
            and (((amountEdt.text.toString().trim().isNotEmpty())) and (!amountEdt.text.toString().trim().equals(
                "tk"
            )))
        ) {
            nextButton.setTextColor(Color.parseColor("#092058"))
            nextButton.background =
                ContextCompat.getDrawable(this, R.drawable.rectangle_white_back_theme_border)
            nextButton.isEnabled = true
        } else {
            nextButton.setTextColor(Color.parseColor("#AABDD5"))
            nextButton.background =
                ContextCompat.getDrawable(this, R.drawable.rectangle_white_background_no_border)
            nextButton.isEnabled = false
        }
    }

    private fun getBankData(): java.util.ArrayList<BankListModel> {

        var bankListModel = BankListModel()
        bankListModel.bankname = "IBB Ltd"
        bankListModel.isLast = false
        bankListModel.bankIconUrl = R.drawable.temp_bank
        bankList.add(bankListModel)

        var bankListModel1 = BankListModel()
        bankListModel1.bankname = "Prime Bank Ltd"
        bankListModel1.isLast = false
        bankListModel1.bankIconUrl = R.drawable.temp_bank1
        bankList.add(bankListModel1)

//        bankListModel = BankListModel()
//        bankListModel.bankname = "IBB Ltd"
//        bankListModel.isLast = false
//        bankListModel.bankIconUrl = R.drawable.temp_bank
//        bankList.add(bankListModel)

//        bankListModel1 = BankListModel()
//        bankListModel1.bankname = "Prime Bank Ltd"
//        bankListModel1.isLast = false
//        bankListModel1.bankIconUrl = R.drawable.temp_bank1
//        bankList.add(bankListModel1)
//
//        bankListModel = BankListModel()
//        bankListModel.bankname = "IBB Ltd"
//        bankListModel.isLast = false
//        bankListModel.bankIconUrl = R.drawable.temp_bank
//        bankList.add(bankListModel)
//
//        bankListModel1 = BankListModel()
//        bankListModel1.bankname = "Prime Bank Ltd"
//        bankListModel1.isLast = false
//        bankListModel1.bankIconUrl = R.drawable.temp_bank1
//        bankList.add(bankListModel1)


        val bankListModel2 = BankListModel()
        bankListModel2.bankname = getString(R.string.other_funds)
        bankListModel2.isLast = true
        bankListModel2.bankIconUrl = R.drawable.other_funds
        bankList.add(bankListModel2)
        return bankList
    }


    private fun initListener() {
        nextButton.setOnClickListener {
            startActivityForResult(Intent(this, DepositeConfirmationACtivity::class.java), 100)
        }
        imgBack.setOnClickListener { finish() }
        headerTitle.text = getString(R.string.deposite_into_wallet)


    }

    override fun onBankSelected(bankListModel: BankListModel, position: Int) {
        if (position == bankList.size - 1) {
            CommonUtils.showToast(this, "coming soon")
        } else {
            if (lastIndex == -1) {
                lastIndex = position
                selectedBankData = bankListModel
                bankList[position].isSelected = !bankList[position].isSelected
            } else if (lastIndex != position) {
                bankList[lastIndex].isSelected = !bankList[lastIndex].isSelected
                if (::bankListAdapter.isInitialized) {
                    bankListAdapter.notifyItemChanged(lastIndex)
                }
                lastIndex = position
                selectedBankData = bankListModel
                bankList[position].isSelected = !bankList[position].isSelected

            }
            if (::bankListAdapter.isInitialized) {
                bankListAdapter.notifyItemChanged(position)
            }
        }
        enableButton()
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 100 && resultCode == Activity.RESULT_OK) {
            finish()
        }
    }
}