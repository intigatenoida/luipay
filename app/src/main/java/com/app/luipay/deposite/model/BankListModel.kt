package com.app.luipay.deposite.model

import android.os.Parcel
import android.os.Parcelable

class BankListModel() : Parcelable {

    var bankname: String = ""
    var bankIconUrl: Int = 0
    var isSelected: Boolean = false
    var isLast: Boolean = false



    constructor(parcel: Parcel) : this() {
        bankname = parcel.readString().toString()
        bankIconUrl = parcel.readInt()
        isSelected = parcel.readByte() != 0.toByte()
        isLast = parcel.readByte() != 0.toByte()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(bankname)
        parcel.writeInt(bankIconUrl)
        parcel.writeByte(if (isSelected) 1 else 0)
        parcel.writeByte(if (isLast) 1 else 0)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<BankListModel> {
        override fun createFromParcel(parcel: Parcel): BankListModel {
            return BankListModel(parcel)
        }

        override fun newArray(size: Int): Array<BankListModel?> {
            return arrayOfNulls(size)
        }
    }

}