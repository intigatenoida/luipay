package com.app.luipay.deposite.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.app.luipay.R
import com.app.luipay.deposite.model.BankListModel
import kotlinx.android.synthetic.main.layout_bank_link.view.*

class DepositeBankListAapter(
    val context: Context,
    val bankList: ArrayList<BankListModel>,
    val listener: onBankSelectListener
) :
    RecyclerView.Adapter<DepositeBankListAapter.ViewHolder>() {

    interface onBankSelectListener {
        fun onBankSelected(bankListModel: BankListModel, position: Int)
    }


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {}

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.layout_bank_link,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return bankList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val bankData = bankList[position]
        holder.itemView.bankName.text = bankData.bankname
        holder.itemView.bankIcon.setImageResource(bankData.bankIconUrl)
        if (bankData.isLast) {
            holder.itemView.righticon.setImageDrawable(
                ContextCompat.getDrawable(
                    context,
                    R.drawable.arraw_right
                )
            )
            holder.itemView.righticon.visibility = View.VISIBLE
        } else {
            if (bankData.isSelected) {
                holder.itemView.righticon.setImageDrawable(
                    ContextCompat.getDrawable(
                        context,
                        R.drawable.right_checked_arrow
                    )
                )
                holder.itemView.righticon.visibility = View.VISIBLE
            } else {
                holder.itemView.righticon.visibility = View.GONE
            }
        }



        holder.itemView.container.setOnClickListener {
            listener?.let {
                listener.onBankSelected(bankData, position)
            }
        }
    }

}