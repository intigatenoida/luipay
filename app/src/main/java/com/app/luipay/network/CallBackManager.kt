package com.app.luipay.network

import com.app.luipay.response.BaseResponse
import com.app.luipay.util.CommonUtils
import com.google.gson.GsonBuilder
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.BufferedReader
import java.io.File
import java.io.FileReader
import java.io.IOException
import java.net.UnknownHostException

abstract class CallBackManager<T> : Callback<T> {

    override fun onFailure(call: Call<T>, t: Throwable) {
        if (t is UnknownHostException) {
            onError(RetroError(RetroError.Kind.NETWORK, "Unable to connect to server.", -999))
        } else {
            onError(
                RetroError(
                    RetroError.Kind.UNEXPECTED,
                    "Unable to connect to server. Try after sometime.",
                    -999
                )
            )
        }
    }

    override fun onResponse(call: Call<T>, response: Response<T>) {
        if (response.isSuccessful) {
            val responseNew = response.body() as BaseResponse
            CommonUtils.checkStatusMessage(responseNew.MessageKey)
            try {
                val jsonObject = readStatusCodeFromFile(responseNew.ResponseStatus.toInt())
                if (jsonObject != null) {
                    if (jsonObject.getInt("StatusCode") == 1) {
                        onSuccess(response.body(), "")
                    } else {
                        onFailure(jsonObject.getString("MessageDescription"))
                    }
                } else {
                    onError(RetroError(RetroError.Kind.HTTP, "", -999))
                }
            } catch (e: Exception) {
                onSuccess(response.body(), "")
            }
        } else {
            val strTemp = response.errorBody()!!.string()
            val responseNew = GsonBuilder().create().fromJson(strTemp,BaseResponse::class.java)
            CommonUtils.checkStatusMessage(responseNew.MessageKey)
            try {
                val jsonObject = readStatusCodeFromFile(responseNew.ResponseStatus.toInt())
                if (jsonObject != null) {
                    onFailure(jsonObject.getString("MessageDescription"))
                } else {
                    onFailure(response.message())
                }
            } catch (e: Exception) {
                onFailure(response.message())
            }

        }
    }

    @Throws(IOException::class)
    open fun readStatusCodeFromFile(status: Int): JSONObject? {
        var br: BufferedReader? = null
        val file =
            File("data/data/com.app.luipay/responsecode/rescode.text")
        val data = StringBuilder()
        try {
            br = BufferedReader(FileReader(file))
            var line: String?
            while (br.readLine().also { line = it } != null) {
                data.append(line)
                data.append('\n')
            }
        } catch (e: IOException) {
            e.printStackTrace()
        } finally {
            br!!.close()
        }
        return try {
            val jsonObject = JSONObject(data.toString())
            val jsonArray = jsonObject.getJSONArray("ResponseCollection")
            for (i in 0 until jsonArray.length()) {
                val statusCode = jsonArray.getJSONObject(i).getInt("StatusCode")
                if (status == statusCode) {
                    return jsonArray.getJSONObject(i)
                }
            }
            null
        } catch (e: JSONException) {
            CommonUtils.showLog("error at parsing key", e.message)
            null
        }
    }


    abstract fun onSuccess(any: T?, message: String)
    abstract fun onFailure(message: String)
    abstract fun onError(error: RetroError)
}