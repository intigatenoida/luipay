package com.app.luipay.network

import com.app.luipay.request.*
import com.app.luipay.response.AppStatusResponse.AppStatusReponse
import com.app.luipay.response.AppStatusResponse.country_code.CountryCodeResponse
import com.app.luipay.response.BaseResponse
import com.app.luipay.response.PrefileResponse.ProfileResponse
import com.app.luipay.response.kyc_list_response.KYCListResponse
import com.app.luipay.response.login_response.LoginResponse
import com.app.luipay.response.passwordResponse.CreatePasswordResponse
import com.app.luipay.response.profileImageResponse.ProfilePictureImage
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.http.*

interface ApiAuthHelper {

    @POST("api/account/GetResponseMessages")
    fun getAppResponse(@Body appStatusRequest: AppStatusRequest, @Header("Content-Type") contentType: String): Call<AppStatusReponse>


    @POST("api/account/GetDropDownList")
    fun getCountryCode(@Body appStatusRequest: CountryCodeRequest, @Header("Content-Type") contentType: String): Call<CountryCodeResponse>


    @POST("api/account/UserRegistration")
    fun getuserRegistration(@Body appStatusRequest: UserRegistrationRequest, @Header("Content-Type") contentType: String): Call<BaseResponse>

    @POST("api/account/CheckSignUpOtp")
    fun checkOTPUserRegistration(@Body appStatusRequest: OtpUserRegistration, @Header("Content-Type") contentType: String): Call<BaseResponse>

    @POST("api/account/UpdateName")
    fun updateName(@Body appStatusRequest: UpdateName, @Header("Content-Type") contentType: String): Call<BaseResponse>

    @POST("api/account/MakeUserAccounPassword")
    fun updatePassword(@Body appStatusRequest: UpdatePassword, @Header("Content-Type") contentType: String): Call<CreatePasswordResponse>

    @POST("api/account/ResendOtpForSignUp")
    fun resendOtpSignUp(@Body appStatusRequest: ResendOtpSignUp, @Header("Content-Type") contentType: String): Call<BaseResponse>

    @POST("profile/UserProfile/SetUserEmailAddress")
    fun updateEmailAddress(@Body appStatusRequest: EmailRequest, @Header("Content-Type") contentType: String): Call<BaseResponse>

    @POST("profile/UserProfile/SetUserIdentification")
    fun updateUserIdentification(@Body appStatusRequest: UpdateUserIdentificationRequest, @Header("Content-Type") contentType: String): Call<BaseResponse>

    @POST("profile/UserProfile/SetUserAddress")
    fun updateAddress(@Body appStatusRequest: UpdateAddressRequest, @Header("Content-Type") contentType: String): Call<BaseResponse>

    @POST("api/account/LoginWithEmail")
    fun loginWithEmail(@Body appStatusRequest: LoginWithEmail, @Header("Content-Type") contentType: String): Call<BaseResponse>

    @POST("api/account/LoginWithMobileNo")
    fun loginWithMobileNumber(@Body appStatusRequest: LoginWithMobileNumber, @Header("Content-Type") contentType: String): Call<LoginResponse>

    @POST("profile/UserProfile/SetUserDateOfBirth")
    fun updateDOB(@Body appStatusRequest: UpdateDOBRequest, @Header("Content-Type") contentType: String): Call<BaseResponse>

    @POST("profile/UserProfile/GetPersonalInformation")
    fun getProfile(@Body appStatusRequest: ProfileRequest, @Header("Content-Type") contentType: String): Call<ProfileResponse>

    @POST("api/account/GetDropDownList")
    fun getKycList(@Body appStatusRequest: CountryCodeRequest, @Header("Content-Type") contentType: String): Call<KYCListResponse>

    @POST("api/account/UserAuthentication")
    fun validateEmailNumber(@Body appStatusRequest: EmailNumberValidationRequest, @Header("Content-Type") contentType: String): Call<BaseResponse>

    @Multipart
    @POST("upload/UploadKycDocument")
    fun uploadKyC(
        @Part image: MultipartBody.Part, @Part("LanguageTypeId") LanguageTypeId: RequestBody, @Part(
            "UserIdentityProofId"
        ) UserIdentityProofId: RequestBody, @Part("ServiceAuthenticationToken") ServiceAuthenticationToken: RequestBody
    ): Call<BaseResponse>

    @Multipart
    @POST("upload/UploadProfilePicture")
    fun uploadProfilePic(
        @Part image: MultipartBody.Part, @Part("LanguageTypeId") LanguageTypeId: RequestBody, @Part("ServiceAuthenticationToken") ServiceAuthenticationToken: RequestBody
    ): Call<ProfilePictureImage>


    @POST("profile/Setting/ChangePassword")
    fun changePassword(@Body appStatusRequest: ChangePasswordRequest, @Header("Content-Type") contentType: String): Call<BaseResponse>


    @POST("profile/Setting/GetGiftCardList")
    fun getGiftList(@Body appStatusRequest: GiftListRequest, @Header("Content-Type") contentType: String): Call<BaseResponse>


    @POST("profile/Setting/GetNotificationList")
    fun getNotificationList(@Body appStatusRequest: GetNotoficationListRequest, @Header("Content-Type") contentType: String): Call<BaseResponse>

    @Multipart
    @POST("bank/AddMoney")
    fun addMoney(  @PartMap() Map<String, RequestBody> partMap,
                   @Part MultipartBody.Part file): Call<ProfilePictureImage>


    @POST("trans/BankAccount/GetUserCardList")
    fun getUserCardList(@Body appStatusRequest: UserCardListRequest, @Header("Content-Type") contentType: String): Call<BaseResponse>
    trans/BankAccount/GetUserCardList


}