package com.app.luipay.network

import android.content.Context
import com.app.luipay.util.CommonUtils
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.security.SecureRandom
import java.security.cert.CertificateException
import java.security.cert.X509Certificate
import java.util.concurrent.TimeUnit
import javax.net.ssl.*


class ApiClient {


    companion object {
        // lateinit var customHeader: CustomHeader
        val BASE_UEL = "https://taka.onead.in/"

        lateinit var retrofit: Retrofit
        lateinit var retrofit1: Retrofit

        fun getClient(): Retrofit {
            if (!::retrofit.isInitialized) {
                try {
                    val okHttpClient = getUnsafeOkHttpClient()
                    val customHeader = CustomHeader(null)
                    okHttpClient!!.addInterceptor(customHeader)
                    if (CommonUtils.isDebug()) {
                        val loggingIntercepter = HttpLoggingInterceptor()
                        loggingIntercepter.level = HttpLoggingInterceptor.Level.BODY
                        okHttpClient!!.addInterceptor(loggingIntercepter)
                    }
                    okHttpClient!!.connectTimeout(15, TimeUnit.MINUTES)
                    okHttpClient.readTimeout(15, TimeUnit.MINUTES)
                    retrofit = Retrofit.Builder()
                        .addConverterFactory(GsonConverterFactory.create())
                        .baseUrl(BASE_UEL).client(okHttpClient.build()).build()
                } catch (e: java.lang.Exception) {
                }
            }
            return retrofit
        }


        fun getClient(context: Context): Retrofit {
            if (!::retrofit1.isInitialized) {
                try {
                    val okHttpClient = getUnsafeOkHttpClient()
                    val customHeader = CustomHeader(context)
                    okHttpClient!!.addInterceptor(customHeader)
                    if (CommonUtils.isDebug()) {
                        val loggingIntercepter = HttpLoggingInterceptor()
                        loggingIntercepter.level = HttpLoggingInterceptor.Level.BODY
                        okHttpClient.addInterceptor(loggingIntercepter)
                    }
                    okHttpClient.connectTimeout(15, TimeUnit.MINUTES)
                    okHttpClient.readTimeout(15, TimeUnit.MINUTES)
                    retrofit1 = Retrofit.Builder()
                        .addConverterFactory(GsonConverterFactory.create())
                        .baseUrl(BASE_UEL).client(okHttpClient.build()).build()
                } catch (e: java.lang.Exception) {
                }
            }
            return retrofit1
        }


        fun getUnsafeOkHttpClient(): OkHttpClient.Builder? {
            return try { // Create a trust manager that does not validate certificate chains
                val trustAllCerts: Array<TrustManager> = arrayOf<TrustManager>(
                    object : X509TrustManager {
                        @Throws(CertificateException::class)
                        override fun checkClientTrusted(
                            chain: Array<X509Certificate?>?,
                            authType: String?
                        ) {
                        }

                        @Throws(CertificateException::class)
                        override fun checkServerTrusted(
                            chain: Array<X509Certificate?>?,
                            authType: String?
                        ) {
                        }

                        override fun getAcceptedIssuers(): Array<X509Certificate> {
                            return arrayOf()
                        }

                    }
                )
                // Install the all-trusting trust manager
                val sslContext: SSLContext = SSLContext.getInstance("SSL")
                sslContext.init(null, trustAllCerts, SecureRandom())
                // Create an ssl socket factory with our all-trusting manager
                val sslSocketFactory: SSLSocketFactory = sslContext.getSocketFactory()
                val builder = OkHttpClient.Builder()
                builder.sslSocketFactory(
                    sslSocketFactory,
                    trustAllCerts[0] as X509TrustManager
                )
                builder.hostnameVerifier(object : HostnameVerifier {
                    override fun verify(
                        hostname: String?,
                        session: SSLSession?
                    ): Boolean {
                        return true
                    }
                })
                builder
            } catch (e: Exception) {
                throw RuntimeException(e)
            }
        }

    }

}