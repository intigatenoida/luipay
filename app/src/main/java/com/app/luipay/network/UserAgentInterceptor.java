package com.app.luipay.network;

import android.content.Context;
import android.text.TextUtils;

import com.app.luipay.util.CommonUtils;
import com.app.luipay.util.Prefutils;

import java.io.EOFException;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.concurrent.TimeUnit;

import okhttp3.Authenticator;
import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.Route;
import okhttp3.logging.HttpLoggingInterceptor;
import okio.Buffer;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;



/**
 *
 */
public class UserAgentInterceptor implements Interceptor, Authenticator {
    private final String TAG = "UserAgentInterceptor";
    private final String userAgent;
    private Context context;
    private static final Charset UTF8 = Charset.forName("UTF-8");

    private static boolean isRunning = false;

    //    private int chk = 0;
    boolean chk = true;

    Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public UserAgentInterceptor(String userAgent, Context context, OkHttpClient.Builder builder) {
        this.userAgent = userAgent;
        this.context = context;
        builder.authenticator(this);
    }

    public Chain chain;

    @Override
    public Response intercept(Chain chain) throws IOException {

        this.chain = chain;
        Request originalRequest = chain.request();
        Request.Builder requestWithUserAgent = originalRequest.newBuilder();

//        if (context != null && PrefUtilsOneAd.getUserModel(context) != null && PrefUtilsOneAd.getUserModel(context).getResponse() != null) {
//            requestWithUserAgent.header("User-Agent", PrefUtilsOneAd.getUserModel(context).getResponse().getId() + "_" + userAgent.replace(":", "-"));
//        } else {
//            requestWithUserAgent.header("User-Agent", userAgent.replace(":", "-"));
//        }

        if (context != null) {


            requestWithUserAgent.removeHeader("AUTHORIZATION");
            requestWithUserAgent.addHeader("AUTHORIZATION", Prefutils.Companion.getFromPrefs(context, Prefutils.Companion.getACCESS_TOKEN()));

        }
//        requestWithUserAgent.header("LangId", PrefUtilsOneAd.getFromPrefs(context, PrefUtilsOneAd.SELECT_LANGUAGE));

        if (originalRequest != null) {
            RequestBody requestBody = originalRequest.body();
            if (requestBody != null) {


                Buffer buffer = new Buffer();
                requestBody.writeTo(buffer);

                Charset charset = UTF8;
                MediaType contentType = requestBody.contentType();
                if (contentType != null) {
                    charset = contentType.charset(UTF8);
                }


                if (isPlaintext(buffer)) {
                    String sig;
                    SyncData syncData = new SyncData();
                    syncData.init();
                    sig = syncData.invokeNativeFunction(buffer.clone().readString(charset));

                    requestWithUserAgent.header("Signature", sig);

                }
            }
        }
        okhttp3.Request request = requestWithUserAgent
                .build();
        Response response = null;
        try {
            response = chain.proceed(request);
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }


        return response;
    }

    private static boolean isPlaintext(Buffer buffer) {
        try {
            Buffer prefix = new Buffer();
            long byteCount = buffer.size() < 64 ? buffer.size() : 64;
            buffer.copyTo(prefix, 0, byteCount);
            for (int i = 0; i < 16; i++) {
                if (prefix.exhausted()) {
                    break;
                }
                int codePoint = prefix.readUtf8CodePoint();
                if (Character.isISOControl(codePoint) && !Character.isWhitespace(codePoint)) {
                    return false;
                }
            }
            return true;
        } catch (EOFException e) {
            return false;
        }
    }


    @Override
    public Request authenticate(Route route, Response response) throws IOException {


//        CommonUtils.showLog(TAG, "authenticate");

        // Add new header to rejected request and retry it


        String token = Prefutils.Companion.getFromPrefs(context, Prefutils.Companion.getACCESS_TOKEN());
        if (!isRunning && !TextUtils.isEmpty(token)) {
            isRunning = true;
            String refreshToken = refreshToken();
            isRunning = false;
            if (refreshToken == null) {
                refreshToken = "";
                return null;
            }

            return response.request().newBuilder()
                    .removeHeader("AUTHORIZATION")
                    .addHeader("AUTHORIZATION", refreshToken)
                    .build();
        } else {
            return null;
        }

    }


    synchronized private String refreshToken() {

        OkHttpClient.Builder httpClient;
        httpClient = new OkHttpClient.Builder();
        httpClient.connectTimeout(15, TimeUnit.MINUTES);
        httpClient.readTimeout(15, TimeUnit.MINUTES);

        // httpClient.addInterceptor( new UserAgentInterceptor(OneAdSdk.getFun1990(context),context));
        if (CommonUtils.Companion.isDebug()) {
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);
            httpClient.addInterceptor(logging);

        }

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ApiClient.Companion.getBASE_UEL())
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient.build())
                .build();


        ApiAuthHelper authApiHelper = retrofit.create(ApiAuthHelper.class);

//        Reftr reftr = new Reftr();
//        reftr.setReqBody(PrefUtilsOneAd.getFromPrefs(context, PrefUtilsOneAd.ACCESS_TOKEN_REF));
//
//
//        SyncData syncData = new SyncData();
//        syncData.init();
//
//
//        JSONObject jsonObject = new JSONObject();
//        try {
//            jsonObject.put("reqBody", PrefUtilsOneAd.getFromPrefs(context, PrefUtilsOneAd.ACCESS_TOKEN_REF));
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//
//        String sig = syncData.invokeNativeFunction(jsonObject.toString());
//        Reft response = null;
//        retrofit2.Call<Reft> call = authApiHelper.refT(reftr, "application/json", PrefUtilsOneAd.getFromPrefs(context, PrefUtilsOneAd.SELECT_LANGUAGE), sig);
//        try {
//            response = call.execute().body();
//
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//
//        if (response == null || response.getError() != null) {
//            return null;
//        } else {
//            CommonUtils.commonMethodToSaveToken(context, response.getAccessToken(), response.getRefreshToken());
//            return "Bearer " + response.getAccessToken();
//        }
        return "";

    }


}

