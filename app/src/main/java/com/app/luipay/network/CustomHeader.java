package com.app.luipay.network;

import android.content.Context;

import com.app.luipay.util.Prefutils;

import org.jetbrains.annotations.NotNull;

import java.io.EOFException;
import java.io.IOException;
import java.nio.charset.Charset;

import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okio.Buffer;

public class CustomHeader implements Interceptor {

    private Context mContext;
    private static final Charset UTF8 = Charset.forName("UTF-8");

    public CustomHeader(Context mContext) {
        this.mContext = mContext;
    }


    @NotNull
    @Override
    public Response intercept(@NotNull Chain chain) throws IOException {

        Request originalRequest = chain.request();
        Request.Builder requestWithUserAgent = originalRequest.newBuilder();
        if (mContext != null) {
            requestWithUserAgent.addHeader("Authorization", "Bearer " + Prefutils.Companion.getFromPrefs(mContext, Prefutils.Companion.getACCESS_TOKEN()));
        }


        if (originalRequest != null) {
            RequestBody requestBody = originalRequest.body();
            if (requestBody != null) {


                Buffer buffer = new Buffer();
                requestBody.writeTo(buffer);

                Charset charset = UTF8;
                MediaType contentType = requestBody.contentType();
                if (contentType != null) {
                    charset = contentType.charset(UTF8);
                }


                if (isPlaintext(buffer)) {
                    String sig;
                    SyncData syncData = new SyncData();
                    syncData.init();
                    sig = syncData.invokeNativeFunction(buffer.clone().readString(charset));
//                    PrefUtilsOneAd.sigDecrement(context);
                    requestWithUserAgent.header("Signature", sig);
//                    if (PrefUtilsOneAd.sigGet(context) < 30000) {
//                        PrefUtilsOneAd.sigIncrement(context);
//                        String sig;
//                        SyncData syncData = new SyncData();
//                        syncData.init();
//                        sig = syncData.invokeNativeFunction(buffer.clone().readString(charset));
//                        PrefUtilsOneAd.sigDecrement(context);
//                        requestWithUserAgent.header("Signature", sig);
//                    } else {
//                        Bundle bundle = new Bundle();
//                        bundle.putString("ndkerror", "NDK ERROR");
//                        CommonUtils.logEvent(context, bundle, EventFunction.NDK_CLASS);
//                        CommonUtils.showLog(TAG, "NDK ERROR");
//                    }
//                    String sig;
//                    SyncData syncData = new SyncData();
//                    syncData.init();
//                    sig = syncData.invokeNativeFunction(buffer.clone().readString(charset));
//                    requestWithUserAgent.header("Signature", sig);
                }
            }
        }
        okhttp3.Request request = requestWithUserAgent
                .build();
        Response response = null;
        try {
            response = chain.proceed(request);
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
//        CacheControl cacheControl = new CacheControl.Builder()
//                .maxAge(2, TimeUnit.MINUTES)
//                .build();
//        if (request.url().toString().contains("secure/Recharge/")) {
//            return response.newBuilder()
//                    .header(CACHE_CONTROL, cacheControl.toString()).build();
//        }

        return response;
    }


    private static boolean isPlaintext(Buffer buffer) {
        try {
            Buffer prefix = new Buffer();
            long byteCount = buffer.size() < 64 ? buffer.size() : 64;
            buffer.copyTo(prefix, 0, byteCount);
            for (int i = 0; i < 16; i++) {
                if (prefix.exhausted()) {
                    break;
                }
                int codePoint = prefix.readUtf8CodePoint();
                if (Character.isISOControl(codePoint) && !Character.isWhitespace(codePoint)) {
                    return false;
                }
            }
            return true;
        } catch (EOFException e) {
            return false;
        }
    }


}
