
package com.app.luipay.transfer.dailog

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.app.luipay.R
import com.app.luipay.util.BaseDialogFragment
import kotlinx.android.synthetic.main.layout_dialog_fingerprint.view.*

class FingerprintDailog : BaseDialogFragment() {

    interface onCancelListener {
        fun onCancel()
    }

    lateinit var listener: onCancelListener

    companion object {
        fun getInstance(): FingerprintDailog {
            val fragment = FingerprintDailog()
            return fragment
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        listener = context as onCancelListener
    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val rootView = inflater.inflate(R.layout.layout_dialog_fingerprint, null)

        rootView.txtCancel.setOnClickListener {
            listener?.let {
                listener.onCancel()
            }
        }
        dialog!!.cancel()

        return rootView
    }


}