package com.app.luipay.transfer.activity

import android.app.Activity
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.core.content.ContextCompat
import com.app.luipay.R
import com.app.luipay.transfer.model.ContactNumberModel
import com.app.luipay.util.BaseActivity
import kotlinx.android.synthetic.main.activity_enter_money_transfer.*
import kotlinx.android.synthetic.main.header_center_text_left_arrow.*
import kotlinx.android.synthetic.main.layout_item_transfer_number.*

class EnterMoneyTransferActivity : BaseActivity() {

    lateinit var contact: ContactNumberModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_enter_money_transfer)
        initListener()
        initIntentData()
    }

    private fun initIntentData() {
        contact = intent.getParcelableExtra<ContactNumberModel>("contact")
        txtAlphabet1.text = contact.name.toCharArray()[0].toString().toUpperCase()
        txtName.text = contact.name
        txtNumber.text = contact.number
    }

    private fun initListener() {
        imgBack.setOnClickListener {
            onBackPressed()
        }
        headerTitle.text = getString(R.string.money_transfer)

        next.isEnabled = false
        edtAmount.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                changeNextButtonState(s.toString())
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

        })

        next.setOnClickListener() {
            var bundle = Bundle()
            bundle.putParcelable("contact", contact)
            bundle.putString("amount",edtAmount.text.toString())
            bundle.putString("msg",edtMessage.text.toString())
            val intent = Intent(
                this@EnterMoneyTransferActivity,
                MoneyTransferConfirmationActivity::class.java
            )
            intent.putExtras(bundle)
            startActivityForResult(intent, 100)
        }
    }


    private fun changeNextButtonState(otp: String) {
        if ((otp.isEmpty())) {
            next.setTextColor(Color.parseColor("#AABDD5"))
            next.background =
                ContextCompat.getDrawable(this, R.drawable.rectangle_white_background_no_border)
            next.isEnabled = false
        } else {
            if ((otp.toInt() < 50) or (otp.toInt() > 10000)) {
                next.setTextColor(Color.parseColor("#AABDD5"))
                next.background =
                    ContextCompat.getDrawable(this, R.drawable.rectangle_white_background_no_border)
                next.isEnabled = false
            } else {
                next.setTextColor(Color.parseColor("#092058"))
                next.background =
                    ContextCompat.getDrawable(this, R.drawable.rectangle_white_back_theme_border)
                next.isEnabled = true
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 100 && resultCode == Activity.RESULT_OK) {
            val resultIntent = Intent()
            setResult(Activity.RESULT_OK, resultIntent)
            finish()
        }
    }
}