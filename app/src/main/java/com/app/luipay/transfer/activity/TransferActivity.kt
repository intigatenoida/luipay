package com.app.luipay.transfer.activity

import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.database.Cursor
import android.graphics.Color
import android.os.Bundle
import android.provider.ContactsContract
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.luipay.R
import com.app.luipay.transfer.adapter.PhoneNumberAdapter
import com.app.luipay.transfer.model.ContactNumberModel
import com.app.luipay.util.BaseActivity
import com.app.luipay.util.EtakaPermissiom
import kotlinx.android.synthetic.main.activity_transfer.*
import kotlinx.android.synthetic.main.header_center_text_left_arrow.*


class TransferActivity : BaseActivity(), View.OnClickListener,
    PhoneNumberAdapter.setOnNumberSelectListener {

    private lateinit var adapter: PhoneNumberAdapter
    var filteredList = ArrayList<ContactNumberModel>()
    var contactList = ArrayList<ContactNumberModel>()
    //    var lastPosition = -1
    var lastSelection: ContactNumberModel? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_transfer)
        checkContactPermission()
        initItems()
    }


    private fun checkContactPermission() {
        if (EtakaPermissiom.checkAppPermission(
                this@TransferActivity,
                EtakaPermissiom.CONTACT_CODE
            )
        ) {
            getContactListFromDevice()
        }
    }

    private fun initItems() {
        searchTransfer.isEnabled = true
        imgBack.setOnClickListener(this)
        numberList.layoutManager = LinearLayoutManager(this)
        adapter = PhoneNumberAdapter(this@TransferActivity, filteredList, this@TransferActivity)
        val verticalDecoration = DividerItemDecoration(
            this,
            DividerItemDecoration.VERTICAL
        )
        val verticalDivider =
            ContextCompat.getDrawable(this, R.drawable.divider_4)
        verticalDecoration.setDrawable(verticalDivider!!)
        numberList.addItemDecoration(verticalDecoration)
        numberList.adapter = adapter

        searchTransfer.isEnabled = false
        searchTransfer.setOnClickListener() {
            var bundle = Bundle()
            bundle.putParcelable("contact", lastSelection)
            val intent = Intent(this@TransferActivity, EnterMoneyTransferActivity::class.java)
            intent.putExtras(bundle)
            startActivityForResult(intent, 100)
        }


        edtSearch.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                s?.let {
                    applyContactFilter(s.toString())
                }
            }

            override fun beforeTextChanged(
                s: CharSequence?,
                start: Int,
                count: Int,
                after: Int
            ) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

        })
    }

    private fun applyContactFilter(finder: String): Any {
        filteredList.clear()
        adapter.notifyDataSetChanged()
        if (finder.isEmpty()) {
            filteredList.addAll(contactList)
        } else {
            contactList.forEach {
                if ((it.name.contains(finder, true)) or (it.number.contains(finder, true))) {
                    filteredList.add(it)
                }
            }
        }
        adapter.notifyDataSetChanged()
        return false
    }

    private fun getContactListFromDevice() {


        var lastNumber = ""
        val cursor: Cursor? = contentResolver.query(
            ContactsContract.CommonDataKinds.Phone.CONTENT_URI, arrayOf(
                ContactsContract.Contacts._ID,
                ContactsContract.Contacts.DISPLAY_NAME,
                ContactsContract.CommonDataKinds.Phone.NUMBER/*,
                ContactsContract.RawContacts.ACCOUNT_TYPE*/
            ),
            /*ContactsContract.RawContacts.ACCOUNT_TYPE + " <> 'google' "*/null,
            null, ContactsContract.Contacts.DISPLAY_NAME + " ASC"
        )
        cursor?.let {
            if (cursor.count > 0) {

                while (cursor.moveToNext()) {
                    val model = ContactNumberModel()
                    var number =
                        cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER))
                    if (number.contains("+91")) {
                        number = number.removePrefix("+91").trim()
                    }
                    if (number.contains("+880")) {
                        number = number.removePrefix("+880").trim()
                    }

                    if (lastNumber.equals(number.trim())) {
                        continue
                    } else {
                        model.id =
                            cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID))
                        model.name =
                            cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME))

                        model.number = number
                        contactList.add(model)
                        lastNumber = number
                    }

                }
            }
        }
        filteredList.addAll(contactList)
        if (::adapter.isInitialized) {
            adapter.notifyDataSetChanged()
        } else {
            initItems()
        }
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.imgBack -> {
                onBackPressed()
            }
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }

    override fun onNumberSelect(data: ContactNumberModel, position: Int) {
//        if ((lastPosition != -1) and (lastPosition != position)) {
//            contactList[lastPosition].isSelected = !contactList[lastPosition].isSelected
//            adapter.notifyItemChanged(lastPosition)
//        }
//        lastPosition = position
        lastSelection?.let {
            val index = filteredList.indexOf(lastSelection!!)
            filteredList[index].isSelected = !filteredList[index].isSelected
            adapter.notifyDataSetChanged()
        }

        lastSelection = data

        adapter.notifyItemChanged(position)
        if (data.isSelected) {
            searchTransfer.setTextColor(Color.parseColor("#092058"))
            searchTransfer.background =
                ContextCompat.getDrawable(this, R.drawable.rectangle_white_back_theme_border)
            searchTransfer.isEnabled = true
        } else {
            searchTransfer.setTextColor(Color.parseColor("#AABDD5"))
            searchTransfer.background =
                ContextCompat.getDrawable(this, R.drawable.rectangle_white_background_no_border)
            searchTransfer.isEnabled = false
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 100 && resultCode == Activity.RESULT_OK) {
            val resultIntent = Intent()
            setResult(Activity.RESULT_OK, resultIntent)
            finish()
        } else if ((requestCode == EtakaPermissiom.SETTING_CODE)) {
            checkContactPermission()
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if ((requestCode == EtakaPermissiom.CONTACT_CODE)) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                getContactListFromDevice()
            } else {
                EtakaPermissiom.CheckForNeverAsk(this, EtakaPermissiom.CONTACT_CODE)
            }
        }
    }
}