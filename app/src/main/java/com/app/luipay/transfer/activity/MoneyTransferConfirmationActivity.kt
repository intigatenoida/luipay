package com.app.luipay.transfer.activity

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import com.app.luipay.R
import com.app.luipay.request_transfer.activity.TransferResultActivity
import com.app.luipay.transfer.dailog.FingerprintDailog
import com.app.luipay.transfer.model.ContactNumberModel
import com.app.luipay.util.BaseActivity
import kotlinx.android.synthetic.main.activity_money_transfer_confirmation.*
import kotlinx.android.synthetic.main.header_center_text_left_arrow.*
import java.util.*

class MoneyTransferConfirmationActivity : BaseActivity(),FingerprintDailog.onCancelListener {

    lateinit var contact: ContactNumberModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_money_transfer_confirmation)
        initListener()
        initIntentData()
    }

    private fun initIntentData() {
        txtTotalamount.text = "tk ${intent.extras!!.getString("amount")}"
        txtMessage.text = "${intent.extras!!.getString("msg")}"
        txtAmount.text = "tk ${intent.extras!!.getString("amount")}"
        contact = intent.extras!!.getParcelable<ContactNumberModel>("contact")!!
        txtAlphabet1.text = contact.name.toCharArray()[0].toString().toUpperCase()
        txtName.text = contact.name
        txtNumber.text = contact.number
        txtPeopleRequired.text = getString(R.string.people_are_required).replace(
            "#",
            "1",
            false
        )
    }

    private fun initListener() {
        headerTitle.text = getString(R.string.money_transfer)
        imgBack.setOnClickListener {
            onBackPressed()
        }
        next.setOnClickListener {
            openFingerprintDialog()

        }
    }


    private fun openResultActivity() {
        var bundle = Bundle()
        var arraylist = ArrayList<ContactNumberModel>()
        arraylist.add(contact)
        contact.amount = txtAmount.text.toString()
        bundle.putParcelableArrayList("contact", arraylist)
        val intent = Intent(
            this@MoneyTransferConfirmationActivity,
            TransferResultActivity::class.java
        )
        intent.putExtras(bundle)
        startActivityForResult(intent, 100)
    }

    private fun openFingerprintDialog() {
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        val prev = supportFragmentManager.findFragmentByTag("dialog")
        if (prev != null) {
            fragmentTransaction.remove(prev)
        }
        fragmentTransaction.addToBackStack(null)
        val dialogFragment = FingerprintDailog.getInstance()
        dialogFragment.show(fragmentTransaction, "dialog")
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 100 && resultCode == Activity.RESULT_OK) {
            val resultIntent = Intent()
            setResult(Activity.RESULT_OK, resultIntent)
            finish()
        }
    }

    override fun onCancel() {
        openResultActivity()
    }
}