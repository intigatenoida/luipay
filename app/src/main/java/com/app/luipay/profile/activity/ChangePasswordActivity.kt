package com.app.luipay.profile.activity

import android.graphics.Color
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import androidx.core.content.ContextCompat
import com.app.luipay.R
import com.app.luipay.network.ApiAuthHelper
import com.app.luipay.network.ApiClient
import com.app.luipay.network.CallBackManager
import com.app.luipay.network.RetroError
import com.app.luipay.request.ChangePasswordRequest
import com.app.luipay.response.BaseResponse
import com.app.luipay.util.*
import kotlinx.android.synthetic.main.activity_change_password.*
import kotlinx.android.synthetic.main.header_center_text_left_arrow.*

class ChangePasswordActivity : BaseActivity() {

    var showPasswordOld = true
    var showPasswordNew = true
    var showPasswordNewC = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_change_password)
        setHeader()
        initListener()

    }

    private fun setHeader() {
        imgBack.setOnClickListener { finish() }
        headerTitle.text = getString(R.string.change_password)
    }

    private fun initListener() {
        oldPassword.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                if (s!!.isNotEmpty()) {
                    imgShowPasswordOld.visibility = View.VISIBLE
                } else {
                    imgShowPasswordOld.visibility = View.GONE
                }
                changeSignUpButtonState()
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

        })
        newPassword.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                if (s!!.isNotEmpty()) {
                    imgShowPasswordNew.visibility = View.VISIBLE
                } else {
                    imgShowPasswordNew.visibility = View.GONE
                }
                changeSignUpButtonState()
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

        })
        newPasswordConf.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                if (s!!.isNotEmpty()) {
                    imgShowPasswordNewC.visibility = View.VISIBLE
                } else {
                    imgShowPasswordNewC.visibility = View.GONE
                }
                changeSignUpButtonState()
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

        })
        imgShowPasswordOld.setOnClickListener {
            CommonUtils.changePasswordFormat(showPasswordOld, oldPassword, imgShowPasswordOld)
            showPasswordOld = !showPasswordOld
        }
        imgShowPasswordNewC.setOnClickListener {
            CommonUtils.changePasswordFormat(showPasswordNewC, newPasswordConf, imgShowPasswordNewC)
            showPasswordNewC = !showPasswordNewC
        }
        imgShowPasswordNew.setOnClickListener {
            CommonUtils.changePasswordFormat(showPasswordNew, newPassword, imgShowPasswordNew)
            showPasswordNew = !showPasswordNew
        }
        save.setOnClickListener {
            if (isValidate())
                updatePassword()
        }
    }

    private fun updatePassword() {

        if (!isOnline()) {
            CommonUtils.showToast(
                this@ChangePasswordActivity,
                getString(R.string.no_internet_connection)
            )
            return
        }

        showProcessDialog()
        val helper = ApiClient.getClient(this@ChangePasswordActivity).create(ApiAuthHelper::class.java)
        val request = ChangePasswordRequest()
        request.LanguageTypeId =
            Prefutils.getFromPrefsInt(this@ChangePasswordActivity, Prefutils.API_LANGUAGE)
        request.ServiceAuthenticationToken = Appconstant.API_KEY
        request.OldPassword = CustomPassword.encrypt(oldPassword.text.toString().trim())
        request.NewPassword = CustomPassword.encrypt(newPassword.text.toString().trim())
        val call = helper.changePassword(request, Appconstant.API_HEADER)
        call.enqueue(object : CallBackManager<BaseResponse>() {
            override fun onSuccess(any: BaseResponse?, message: String) {
                hideProcessDailog()
                finish()
            }

            override fun onFailure(message: String) {
                hideProcessDailog()
                CommonUtils.showToast(this@ChangePasswordActivity, message)
            }

            override fun onError(error: RetroError) {
                hideProcessDailog()
            }

        })


    }


    private fun changeSignUpButtonState() {
        if (shouldEnable()) {
            save.setTextColor(Color.parseColor("#092058"))
            save.background =
                ContextCompat.getDrawable(this, R.drawable.rectangle_white_back_theme_border)
            save.isEnabled = true

        } else {
            save.setTextColor(Color.parseColor("#AABDD5"))
            save.background =
                ContextCompat.getDrawable(this, R.drawable.rectangle_white_background_no_border)
            save.isEnabled = false
        }
    }

    private fun shouldEnable(): Boolean {
        if ((oldPassword.text.toString().trim().length < 6)) {
            return false
        }
        if ((newPassword.text.toString().trim().length < 6)) {
            return false
        }
        if ((newPasswordConf.text.toString().trim().length < 6)) {
            return false
        }
        return true
    }

    fun isValidate(): Boolean {
        if ((oldPassword.text.toString().trim().length < 6)) {
            CommonUtils.showToast(
                this@ChangePasswordActivity,
                getString(R.string.enter_old_password)
            )
            return false
        }
        if ((newPassword.text.toString().trim().length < 6)) {
            CommonUtils.showToast(
                this@ChangePasswordActivity,
                getString(R.string.enter_new_password)
            )
            return false
        }
        if ((newPasswordConf.text.toString().trim().length < 6)) {
            CommonUtils.showToast(
                this@ChangePasswordActivity,
                getString(R.string.enter_new_password_conf)
            )
            return false
        }
        if (!CommonUtils.checkPasswordPattern(oldPassword.text.toString())) {
            CommonUtils.showToast(
                this@ChangePasswordActivity,
                getString(R.string.old_password_pattern)
            )
            return false
        }
        if (!CommonUtils.checkPasswordPattern(newPassword.text.toString())) {
            CommonUtils.showToast(
                this@ChangePasswordActivity,
                getString(R.string.new_password_pattern)
            )
            return false
        }
        if (!CommonUtils.checkPasswordPattern(newPasswordConf.text.toString())) {
            CommonUtils.showToast(
                this@ChangePasswordActivity,
                getString(R.string.new_conf_password_pattern)
            )
            return false
        }
        if (!newPasswordConf.text.toString().equals(newPassword.text.toString())) {
            CommonUtils.showToast(
                this@ChangePasswordActivity,
                getString(R.string.new_conf_password_match)
            )
            return false
        }

        return true
    }
}