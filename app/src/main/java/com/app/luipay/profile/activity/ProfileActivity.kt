package com.app.luipay.profile.activity

import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.view.View
import com.app.luipay.R
import com.app.luipay.network.ApiAuthHelper
import com.app.luipay.network.ApiClient
import com.app.luipay.network.CallBackManager
import com.app.luipay.network.RetroError
import com.app.luipay.profile.dialog_fragment.ChangeGenderDialog
import com.app.luipay.profile.dialog_fragment.ChangeImageDialog
import com.app.luipay.request.UpdateUserIdentificationRequest
import com.app.luipay.response.BaseResponse
import com.app.luipay.response.profileImageResponse.ProfilePictureImage
import com.app.luipay.util.*
import com.google.gson.GsonBuilder
import id.zelory.compressor.Compressor
import kotlinx.android.synthetic.main.activity_profile.*
import kotlinx.android.synthetic.main.header_center_text_left_arrow.*
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import java.io.File


class ProfileActivity : BaseActivity(), View.OnClickListener,
    ChangeImageDialog.OnImageSelectionListener, ChangeGenderDialog.OnGenderChangeListener {

    private lateinit var imageUri: Uri
    val SELECT_IMAGE = 100
    val CAPTURE_IMAGE = 101
    val ID = 1001
    val ADDRESS = 1002
    val DOB = 1003

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)
        initHeader()

    }

    override fun onResume() {
        super.onResume()
        setProfileData()
    }

    private fun setProfileData() {
        val profileData = Prefutils.getProfileData(this@ProfileActivity)
        profileData?.let {
            txtDob.text = it.Dob
            emailTxt.text = it.Email
            txtGender.text = it.Gender
            address.text = it.Address
            limit.text = it.TransactionLimit
            CommonUtils.loadImageCircular(this@ProfileActivity,it.ProfilePicUrl,imgProfile)
        }
    }

    private fun initHeader() {
        imgBack.setOnClickListener(this)
        headerTitle.text = getString(R.string.personal_information)
        qrCodeLayout.setOnClickListener(this)
        profileLayout.setOnClickListener(this)
        genderLayout.setOnClickListener(this)
        idLayout.setOnClickListener(this)
        DOBLayout.setOnClickListener(this)
        adressLayout.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.qrCodeLayout -> {
                startActivity(Intent(this, MyQRCodeActivity::class.java))
            }
            R.id.imgBack -> {
                finish()
            }
            R.id.profileLayout -> {
                changeProfileImageDialog()
            }
            R.id.genderLayout -> {
                changeGenderDialog()
            }
            R.id.idLayout -> {
                val intent = Intent(this@ProfileActivity, CommonUsesActivity::class.java)
                intent.putExtra("type", Appconstant.TYPE_ID)
                startActivityForResult(intent, ID)
            }
            R.id.DOBLayout -> {
                val intent = Intent(this@ProfileActivity, CommonUsesActivity::class.java)
                intent.putExtra("type", Appconstant.TYPE_DOB)
                startActivityForResult(intent, DOB)
            }
            R.id.adressLayout -> {
                val intent = Intent(this@ProfileActivity, CommonUsesActivity::class.java)
                intent.putExtra("type", Appconstant.TYPE_ADDRESS)
                startActivityForResult(intent, ADDRESS)
            }
        }
    }

    private fun changeGenderDialog() {

        val fragmentTransaction = supportFragmentManager.beginTransaction()
        val prev = supportFragmentManager.findFragmentByTag("dialog")
        if (prev != null) {
            fragmentTransaction.remove(prev)
        }
        fragmentTransaction.addToBackStack(null)
        val dialogFragment = ChangeGenderDialog.getInstance()
        dialogFragment.show(fragmentTransaction, "dialog")
    }


    private fun changeProfileImageDialog() {
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        val prev = supportFragmentManager.findFragmentByTag("dialog")
        if (prev != null) {
            fragmentTransaction.remove(prev)
        }
        fragmentTransaction.addToBackStack(null)
        val dialogFragment = ChangeImageDialog.getInstance(false)
        dialogFragment.show(fragmentTransaction, "dialog")
    }

    fun openGallery() {
        val intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), SELECT_IMAGE)
    }

    override fun onGallerySelectionListener() {
        openGallery()
    }

    override fun onCameraSelectionListener() {
        if (EtakaPermissiom.checkAppPermission(this@ProfileActivity, EtakaPermissiom.CAMERA_CODE)) {
            openCamera()
        }
    }

    override fun onPDFSelectionListener() {
        //Not required here
    }

    private fun openCamera() {
        Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
            imageUri = CommonUtils.getCameraUri(this@ProfileActivity)
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri)
            takePictureIntent.resolveActivity(packageManager)?.also {
                startActivityForResult(takePictureIntent, CAPTURE_IMAGE)
            }
        }

    }

    override fun onMaleSelectionListener() {
        upadateGander(1)
    }

    private fun upadateGander(selectGender: Int) {
        if (!isOnline()) {
            CommonUtils.showToast(
                this@ProfileActivity,
                getString(R.string.no_internet_connection)
            )
        } else {
            showProcessDialog()
            val helper =
                ApiClient.getClient(this@ProfileActivity).create(ApiAuthHelper::class.java)
            val otpUserRegistration = UpdateUserIdentificationRequest()

            otpUserRegistration.LanguageTypeId = Appconstant.API_LANGUAGE
            otpUserRegistration.ServiceAuthenticationToken = Appconstant.API_KEY
            otpUserRegistration.GenderId = selectGender
            val call =
                helper.updateUserIdentification(otpUserRegistration, Appconstant.API_HEADER)
            call.enqueue(object : CallBackManager<BaseResponse>() {
                override fun onSuccess(any: BaseResponse?, message: String) {
                    hideProcessDailog()
                    txtGender.text = if (selectGender == 1) {
                        getString(R.string.male)
                    } else {
                        getString(R.string.female)
                    }
                    val profileData = Prefutils.getProfileData(this@ProfileActivity)
                    profileData?.let {
                        it.Gender = txtGender.text.toString()
                    }
                    val tempResponse =
                        GsonBuilder().disableHtmlEscaping().create().toJson(profileData)
                    Prefutils.saveToPrefs(
                        this@ProfileActivity,
                        Prefutils.PROFILE,
                        tempResponse
                    )

                }

                override fun onFailure(message: String) {
                    CommonUtils.showToast(this@ProfileActivity, message)
                    hideProcessDailog()
                }

                override fun onError(error: RetroError) {
                    hideProcessDailog()
                }
            })
        }

    }

    override fun onFemaleSelectionListener() {
        upadateGander(2)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if ((requestCode == EtakaPermissiom.CAMERA_CODE)) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                openCamera()
            } else {
                EtakaPermissiom.CheckForNeverAsk(this, EtakaPermissiom.CAMERA_CODE)
            }
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                SELECT_IMAGE -> {
                    imageUri = data!!.data!!
                }
            }
            var file: File = FileUtils.from(this@ProfileActivity, imageUri)
            file = Compressor(this).compressToFile(file)
            uploadProfilePicture(file)
        }
    }

    private fun uploadProfilePicture(file: File) {
        if (!isOnline()) {
            CommonUtils.showToast(this, getString(R.string.no_internet_connection))
            return
        }
        try {
            showProcessDialog()

            val LanguageTypeId: RequestBody =
                "${Prefutils.getFromPrefsInt(this@ProfileActivity, Prefutils.API_LANGUAGE)}"
                    .toRequestBody("multipart/form-data".toMediaTypeOrNull())
//
            val ServiceAuthenticationToken: RequestBody = Appconstant.API_KEY
                .toRequestBody("multipart/form-data".toMediaTypeOrNull())

            val requestFile: RequestBody = RequestBody.create(
                contentResolver.getType(imageUri!!)!!.toMediaTypeOrNull(),
                file
            )

            val body: MultipartBody.Part =
                MultipartBody.Part.createFormData("File", file.getName(), requestFile)

            val helper = ApiClient.getClient(this@ProfileActivity).create(ApiAuthHelper::class.java)
            val call =
                helper.uploadProfilePic(body, LanguageTypeId, ServiceAuthenticationToken)
            call.enqueue(object : CallBackManager<ProfilePictureImage>() {
                override fun onSuccess(any: ProfilePictureImage?, message: String) {
                    val profileRespone = any as ProfilePictureImage
                    changeProfileImage(profileRespone)
                    hideProcessDailog()
                }

                override fun onFailure(message: String) {
                    hideProcessDailog()
                }

                override fun onError(error: RetroError) {
                    hideProcessDailog()
                }

            })
        } catch (e: Exception) {
            CommonUtils.showLog("0000", e.localizedMessage)
            hideProcessDailog()
        }

    }

    private fun changeProfileImage(profileRespone: ProfilePictureImage) {

        val profileData = Prefutils.getProfileData(this@ProfileActivity)
        profileData!!.ProfilePicUrl = profileRespone?.Response?.ProfilePictureUrl ?: ""
        val tempResponse =
            GsonBuilder().disableHtmlEscaping().create().toJson(profileData)
        Prefutils.saveToPrefs(
            this@ProfileActivity,
            Prefutils.PROFILE,
            tempResponse
        )
    }
}