package com.app.luipay.profile.dialog_fragment

import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.app.luipay.R
import com.app.luipay.util.BaseDialogFragment
import com.app.luipay.util.Prefutils

class ChangeGenderDialog : BaseDialogFragment() {

    companion object {
        fun getInstance(): ChangeGenderDialog {
            val fragment = ChangeGenderDialog()
            return fragment
        }
    }

    public interface OnGenderChangeListener {
        fun onMaleSelectionListener()
        fun onFemaleSelectionListener()
    }

    lateinit var listener: OnGenderChangeListener

    override fun onAttach(context: Context) {
        super.onAttach(context)
        listener = context as OnGenderChangeListener
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val rootView = inflater.inflate(R.layout.layout_dialog_gender, null)
        updateView(rootView)
        rootView.findViewById<TextView>(R.id.male).setOnClickListener {
            dialog!!.cancel()
            if (::listener.isInitialized) {
                listener.onMaleSelectionListener()
            }
        }
        rootView.findViewById<TextView>(R.id.female).setOnClickListener {
            dialog!!.cancel()
            if (::listener.isInitialized) {
                listener.onFemaleSelectionListener()
            }
        }

        return rootView
    }

    private fun updateView(rootView: View?) {
        val profileData = Prefutils.getProfileData(activity!!)
        profileData?.let {
            if (it.Gender.isNotEmpty()) {
                if (it.Gender.equals("male", true)) {
                    rootView!!.findViewById<TextView>(R.id.male).setTextColor(Color.parseColor("#092058"))
                    rootView!!.findViewById<TextView>(R.id.female).setTextColor(Color.parseColor("#4F6C8D"))
                } else {
                    rootView!!.findViewById<TextView>(R.id.female).setTextColor(Color.parseColor("#092058"))
                    rootView!!.findViewById<TextView>(R.id.male).setTextColor(Color.parseColor("#4F6C8D"))
                }
            }
        }
    }
}