package com.app.luipay.profile.activity

import android.app.DatePickerDialog
import android.graphics.Color
import android.os.Bundle
import android.text.Editable
import android.text.InputType
import android.text.TextWatcher
import android.widget.DatePicker
import androidx.core.content.ContextCompat
import com.app.luipay.R
import com.app.luipay.network.ApiAuthHelper
import com.app.luipay.network.ApiClient
import com.app.luipay.network.CallBackManager
import com.app.luipay.network.RetroError
import com.app.luipay.request.UpdateAddressRequest
import com.app.luipay.request.UpdateDOBRequest
import com.app.luipay.response.BaseResponse
import com.app.luipay.util.Appconstant
import com.app.luipay.util.BaseActivity
import com.app.luipay.util.CommonUtils
import com.app.luipay.util.Prefutils
import com.google.gson.GsonBuilder
import kotlinx.android.synthetic.main.activity_common_uses.*
import kotlinx.android.synthetic.main.header_center_text_left_arrow.*
import java.util.*

class CommonUsesActivity : BaseActivity() {

    var headerType = -1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_common_uses)
        headerType = intent.getIntExtra("type", 0)
        initHeader()
        initListener()
    }

    private fun initListener() {
        imgBack.setOnClickListener { finish() }
        edtCommon.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                notifyButton(s.toString())
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

        })
        cancel.setOnClickListener { finish() }
        save.setOnClickListener { validateData() }
    }

    private fun validateData() {
        var textData = edtCommon.text.toString().trim()
        if (textData.isEmpty()) {
            when (headerType) {
                Appconstant.TYPE_DOB -> {
                    CommonUtils.showToast(
                        this@CommonUsesActivity,
                        getString(R.string.please_select_date_of_birth)
                    )
                }
                Appconstant.TYPE_ADDRESS -> {
                    CommonUtils.showToast(
                        this@CommonUsesActivity,
                        getString(R.string.please_enter_address)
                    )
                }
                Appconstant.TYPE_ID -> {
                    CommonUtils.showToast(
                        this@CommonUsesActivity,
                        getString(R.string.please_enter_id)
                    )
                }
            }
        } else {
            when (headerType) {
                Appconstant.TYPE_DOB -> {
                    updateDOB(textData)
                }
                Appconstant.TYPE_ADDRESS -> {
                    updateAddress(textData)
                }
                Appconstant.TYPE_ID -> {
                    updateID(textData)
                }
            }
        }

    }

    private fun updateID(id: String) {

    }

    private fun updateAddress(address: String) {
        if (!isOnline()) {
            CommonUtils.showToast(
                this@CommonUsesActivity,
                getString(R.string.no_internet_connection)
            )
            return
        }

        showProcessDialog()
        val helper = ApiClient.getClient(this@CommonUsesActivity).create(ApiAuthHelper::class.java)
        val request = UpdateAddressRequest()
        request.Address = address
        request.LanguageTypeId =
            Prefutils.getFromPrefsInt(this@CommonUsesActivity, Prefutils.API_LANGUAGE)
        request.ServiceAuthenticationToken = Appconstant.API_KEY
        val call = helper.updateAddress(request, Appconstant.API_HEADER)
        call.enqueue(object : CallBackManager<BaseResponse>() {
            override fun onSuccess(any: BaseResponse?, message: String) {
                hideProcessDailog()
                updateProfileData(address)
            }

            override fun onFailure(message: String) {
                CommonUtils.showToast(this@CommonUsesActivity, message)
                hideProcessDailog()
            }

            override fun onError(error: RetroError) {
                hideProcessDailog()
            }

        })

    }

    private fun updateDOB(dob: String) {
        if (!isOnline()) {
            CommonUtils.showToast(
                this@CommonUsesActivity,
                getString(R.string.no_internet_connection)
            )
            return
        }

        showProcessDialog()
        val helper = ApiClient.getClient(this@CommonUsesActivity).create(ApiAuthHelper::class.java)
        val request = UpdateDOBRequest()
        request.DateOfBirth = dob
        request.LanguageTypeId =
            Prefutils.getFromPrefsInt(this@CommonUsesActivity, Prefutils.API_LANGUAGE)
        request.ServiceAuthenticationToken = Appconstant.API_KEY
        val call = helper.updateDOB(request, Appconstant.API_HEADER)
        call.enqueue(object : CallBackManager<BaseResponse>() {
            override fun onSuccess(any: BaseResponse?, message: String) {
                hideProcessDailog()
                updateProfileData(dob)
            }

            override fun onFailure(message: String) {
                CommonUtils.showToast(this@CommonUsesActivity, message)
                hideProcessDailog()
            }

            override fun onError(error: RetroError) {
                hideProcessDailog()
            }

        })

    }

    private fun updateProfileData(data: String) {
        val profileData = Prefutils.getProfileData(this@CommonUsesActivity)
        profileData?.let {
            when (headerType) {
                Appconstant.TYPE_DOB -> {
                    it.Dob = data
                }
                Appconstant.TYPE_ADDRESS -> {
                    it.Address = data
                }
            }
        }
        val tempResponse =
            GsonBuilder().disableHtmlEscaping().create().toJson(profileData)
        Prefutils.saveToPrefs(this@CommonUsesActivity, Prefutils.PROFILE, tempResponse)
        finish()
    }

    private fun notifyButton(data: String) {
        if (data.length > 0) {
            save.setTextColor(Color.parseColor("#092058"))
            save.background =
                ContextCompat.getDrawable(this, R.drawable.rectangle_white_back_theme_border)
            save.isEnabled = true

        } else {
            save.setTextColor(Color.parseColor("#AABDD5"))
            save.background =
                ContextCompat.getDrawable(this, R.drawable.rectangle_white_background_no_border)
            save.isEnabled = false
        }
    }

    private fun initHeader() {


        when (headerType) {
            Appconstant.TYPE_ID -> {
                headerTitle.text = getString(R.string.change_id)
                edtCommon.hint = getString(R.string.enter_your_id)
            }
            Appconstant.TYPE_DOB -> {
                headerTitle.text = getString(R.string.change_dob)
                edtCommon.hint = getString(R.string.enter_your_dob)
                edtCommon.inputType = InputType.TYPE_NULL
                edtCommon.setOnClickListener {
                    openDatePickerDialog()
                }
            }
            Appconstant.TYPE_ADDRESS -> {
                headerTitle.text = getString(R.string.change_address)
                edtCommon.hint = getString(R.string.enter_your_address)
                edtCommon.inputType = InputType.TYPE_TEXT_VARIATION_POSTAL_ADDRESS
            }
        }
        edtCommon.maxLines = 1
    }

    private fun openDatePickerDialog() {
        val calender = Calendar.getInstance(Locale.US)

        var year = calender.get(Calendar.YEAR)
        var month = calender.get(Calendar.MONTH)
        var day = calender.get(Calendar.DAY_OF_MONTH)
        if (edtCommon.text.toString().trim().length > 0) {
            val dateTemp = edtCommon.text.toString().trim().split("-")
            if (dateTemp.size == 3) {
                year = dateTemp[0].toInt()
                month = dateTemp[1].toInt() - 1
                day = dateTemp[2].toInt()
            }
        }


        val dialog =
            DatePickerDialog(
                this@CommonUsesActivity,/*R.style.DatePickerDialogTheme ,*/
                object : DatePickerDialog.OnDateSetListener {
                    override fun onDateSet(
                        view: DatePicker?,
                        year: Int,
                        month: Int,
                        dayOfMonth: Int
                    ) {
                        val dayOfMonthStr = if (dayOfMonth < 10) {
                            "0$dayOfMonth"
                        } else {
                            "$dayOfMonth"
                        }

                        val monthStr = if (month < 10) {
                            "0${month + 1}"
                        } else {
                            "${month + 1}"
                        }
                        edtCommon.setText("$year-$monthStr-$dayOfMonthStr")
                    }
                },
                year,
                month,
                day
            )

        dialog.datePicker.maxDate = Date().time
        dialog.show()

    }
}