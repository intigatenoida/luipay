package com.app.luipay.profile.activity

import android.os.Bundle
import com.app.luipay.R
import com.app.luipay.util.BaseActivity
import com.app.luipay.util.CommonUtils
import com.app.luipay.util.Prefutils
import kotlinx.android.synthetic.main.activity_my_qr_code.*
import kotlinx.android.synthetic.main.header_center_text_left_arrow.*

class MyQRCodeActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_qr_code)
        initHeadaer()
        setProfileData()
    }

    private fun initHeadaer() {
        imgBack.setOnClickListener { finish() }
        headerTitle.text = getString(R.string.my_qr_code)
        giveupMoney.isEnabled = true
        saveImage.isEnabled = true
    }

    private fun setProfileData() {
        val profileData = Prefutils.getProfileData(this@MyQRCodeActivity)
        profileData?.let {
            name.text = it.Name
            number.text = it.MobileNo
        }
        CommonUtils.loadProfilePic(this@MyQRCodeActivity, profilePic)
    }
}