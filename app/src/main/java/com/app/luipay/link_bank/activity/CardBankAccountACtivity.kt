package com.app.luipay.link_bank.activity

import android.os.Bundle
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.luipay.R
import com.app.luipay.deposite.model.BankListModel
import com.app.luipay.link_bank.adapter.CardBankAdapter
import com.app.luipay.util.BaseActivity
import kotlinx.android.synthetic.main.acticvity_card_bank_account.*
import kotlinx.android.synthetic.main.header_center_text_left_arrow.*

class CardBankAccountACtivity : BaseActivity() {

    var bankList = ArrayList<BankListModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.acticvity_card_bank_account)
        intiHeader()
    }

    private fun intiHeader() {


        imgBack.setOnClickListener { finish() }
        headerTitle.text = getString(R.string.cards_bank_accounts)
        cardBankRecycler.layoutManager = LinearLayoutManager(this)
        val verticalDecoration = DividerItemDecoration(
            this,
            DividerItemDecoration.VERTICAL
        )
        val verticalDivider =
            ContextCompat.getDrawable(this, R.drawable.divider_16)
        verticalDecoration.setDrawable(verticalDivider!!)
        cardBankRecycler.addItemDecoration(verticalDecoration)
        cardBankRecycler.adapter = CardBankAdapter(this,getBankData())

    }

    fun getBankData(): ArrayList<BankListModel> {
        var bankListModel = BankListModel()
        bankListModel.bankname = "AB Bank Ltd"
        bankListModel.isLast = false
        bankListModel.bankIconUrl = R.drawable.temp_bank
        bankList.add(bankListModel)

        var bankListModel1 = BankListModel()
        bankListModel1.bankname = "Agrani Bank Ltd"
        bankListModel1.isLast = false
        bankListModel1.bankIconUrl = R.drawable.temp_bank1
        bankList.add(bankListModel1)

        bankListModel = BankListModel()
        bankListModel.bankname = "IBB Ltd"
        bankListModel.isLast = false
        bankListModel.bankIconUrl = R.drawable.temp_bank3
        bankList.add(bankListModel)

        return bankList
    }
}