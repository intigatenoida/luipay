package com.app.luipay.link_bank.activity

import android.os.Bundle
import com.app.luipay.R
import com.app.luipay.deposite.model.BankListModel
import com.app.luipay.util.BaseActivity
import kotlinx.android.synthetic.main.activity_unlink_account.*
import kotlinx.android.synthetic.main.header_center_text_left_arrow.*

class UnlinkCardbankActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_unlink_account)
        initHeader()
    }

    private fun initHeader() {
        imgBack.setOnClickListener { finish() }
        headerTitle.text = getString(R.string.account_information)
        unlink.setOnClickListener { finish() }

        var bankData = intent.getParcelableExtra<BankListModel>("bank")
        bankIcon.setImageResource(bankData.bankIconUrl)
        bankName.text=bankData.bankname


    }
}