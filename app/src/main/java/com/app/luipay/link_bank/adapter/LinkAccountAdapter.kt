package com.app.luipay.link_bank.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.app.luipay.R
import com.app.luipay.deposite.model.BankListModel
import com.app.luipay.link_bank.activity.EnterCardDetailActivity
import kotlinx.android.synthetic.main.layout_bank_link_item.view.*
import java.util.ArrayList

class LinkAccountAdapter(
    val context: Context,
    val bankData: ArrayList<BankListModel>
) :
    RecyclerView.Adapter<LinkAccountAdapter.ViewHolder>() {


    public class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {}

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return LinkAccountAdapter.ViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.layout_bank_link_item,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return bankData.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val bankData = bankData[position]
        holder.itemView.txtBankName.text = bankData.bankname
        holder.itemView.bankImg.setImageResource(bankData.bankIconUrl)
        holder.itemView.container_link.setOnClickListener {
            val callIntent = Intent(context, EnterCardDetailActivity::class.java)
            callIntent.putExtra("isfrombank",true)
            context.startActivity(callIntent)
        }
    }

}