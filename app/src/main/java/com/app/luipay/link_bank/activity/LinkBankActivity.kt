package com.app.luipay.link_bank.activity

import android.content.Intent
import android.os.Bundle
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.GridLayoutManager
import com.app.luipay.R
import com.app.luipay.deposite.model.BankListModel
import com.app.luipay.link_bank.adapter.LinkAccountAdapter
import com.app.luipay.util.BaseActivity
import kotlinx.android.synthetic.main.activity_link_account.*
import kotlinx.android.synthetic.main.header_center_text_left_arrow.*

class LinkBankActivity : BaseActivity() {

    var bankList = ArrayList<BankListModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_link_account)
        initHeader()
        initBankGridView()
    }

    private fun initBankGridView() {
        bankGrid.layoutManager = GridLayoutManager(this, 3)
        val verticalDecoration = DividerItemDecoration(
            this,
            DividerItemDecoration.VERTICAL
        )
        val verticalDivider =
            ContextCompat.getDrawable(this, R.drawable.divider_4)
        verticalDecoration.setDrawable(verticalDivider!!)
        bankGrid.addItemDecoration(verticalDecoration)
        val horizontalDecoration = DividerItemDecoration(
            this,
            DividerItemDecoration.HORIZONTAL
        )
        val horizontalDivider =
            ContextCompat.getDrawable(this, R.drawable.divider_4_horizontal)
        horizontalDecoration.setDrawable(horizontalDivider!!)
        bankGrid.addItemDecoration(horizontalDecoration)
        bankGrid.adapter = LinkAccountAdapter(this,getBankData())
    }


    private fun getBankData(): java.util.ArrayList<BankListModel> {

        var bankListModel = BankListModel()
        bankListModel.bankname = "AB Bank Ltd"
        bankListModel.isLast = false
        bankListModel.bankIconUrl = R.drawable.temp_bank
        bankList.add(bankListModel)

        var bankListModel1 = BankListModel()
        bankListModel1.bankname = "Agrani Bank Ltd"
        bankListModel1.isLast = false
        bankListModel1.bankIconUrl = R.drawable.temp_bank1
        bankList.add(bankListModel1)

        bankListModel = BankListModel()
        bankListModel.bankname = "IBB Ltd"
        bankListModel.isLast = false
        bankListModel.bankIconUrl = R.drawable.temp_bank3
        bankList.add(bankListModel)

        bankListModel1 = BankListModel()
        bankListModel1.bankname = "Prime Bank Ltd"
        bankListModel1.isLast = false
        bankListModel1.bankIconUrl = R.drawable.temp_bank4
        bankList.add(bankListModel1)

        bankListModel = BankListModel()
        bankListModel.bankname = "AB Bank Ltd"
        bankListModel.isLast = false
        bankListModel.bankIconUrl = R.drawable.temp_bank
        bankList.add(bankListModel)

        bankListModel1 = BankListModel()
        bankListModel1.bankname = "Agrani Bank Ltd"
        bankListModel1.isLast = false
        bankListModel1.bankIconUrl = R.drawable.temp_bank1
        bankList.add(bankListModel1)

        bankListModel = BankListModel()
        bankListModel.bankname = "IBB Ltd"
        bankListModel.isLast = false
        bankListModel.bankIconUrl = R.drawable.temp_bank3
        bankList.add(bankListModel)

        bankListModel1 = BankListModel()
        bankListModel1.bankname = "Prime Bank Ltd"
        bankListModel1.isLast = false
        bankListModel1.bankIconUrl = R.drawable.temp_bank4
        bankList.add(bankListModel1)
        
        bankListModel = BankListModel()
        bankListModel.bankname = "AB Bank Ltd"
        bankListModel.isLast = false
        bankListModel.bankIconUrl = R.drawable.temp_bank
        bankList.add(bankListModel)
        bankListModel1 = BankListModel()
        bankListModel1.bankname = "Agrani Bank Ltd"
        bankListModel1.isLast = false
        bankListModel1.bankIconUrl = R.drawable.temp_bank1
        bankList.add(bankListModel1)

        bankListModel = BankListModel()
        bankListModel.bankname = "IBB Ltd"
        bankListModel.isLast = false
        bankListModel.bankIconUrl = R.drawable.temp_bank3
        bankList.add(bankListModel)

        bankListModel1 = BankListModel()
        bankListModel1.bankname = "Prime Bank Ltd"
        bankListModel1.isLast = false
        bankListModel1.bankIconUrl = R.drawable.temp_bank4
        bankList.add(bankListModel1)

        return bankList
    }

    private fun initHeader() {
        headerTitle.text = getString(R.string.link_account)
        imgBack.setOnClickListener {
            onBackPressed()
        }
        visaMaterCard.setOnClickListener {
            val callIntent = Intent(this, EnterCardDetailActivity::class.java)
            callIntent.putExtra("isfrombank", false)
            startActivityForResult(callIntent, 100)
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

    }
}