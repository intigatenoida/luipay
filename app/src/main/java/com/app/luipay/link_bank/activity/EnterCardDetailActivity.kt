package com.app.luipay.link_bank.activity

import android.graphics.Color
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import androidx.core.content.ContextCompat
import com.app.luipay.R
import com.app.luipay.util.BaseActivity
import kotlinx.android.synthetic.main.activity_enter_card_detail.*
import kotlinx.android.synthetic.main.header_center_text_left_arrow.*

class EnterCardDetailActivity : BaseActivity() {

    var isFromBank: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_enter_card_detail)
        initHeader()
    }

    private fun initHeader() {
        imgBack.setOnClickListener { finish() }
        headerTitle.text = intent.extras?.let {
            intent.extras!!.getString("title")
        } ?: getString(R.string.visa_master_card)

        nextButton.setOnClickListener {
            validateData()
        }
        isFromBank =
            intent.extras?.let { intent.extras!!.getBoolean("isfrombank", false) } ?: false
        if (isFromBank) {
            cvvEdt.visibility = View.GONE
            expireDateEdt.hint = getString(R.string.id_passport)
        }

        cardNumberEdt.addTextChangedListener(object:TextWatcher{
            override fun afterTextChanged(s: Editable?) {
                enableButton()
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }
        })

        nameEdt.addTextChangedListener(object:TextWatcher{
            override fun afterTextChanged(s: Editable?) {
                enableButton()
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }
        })

        expireDateEdt.addTextChangedListener(object:TextWatcher{
            override fun afterTextChanged(s: Editable?) {
                enableButton()
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }
        })

        cvvEdt.addTextChangedListener(object:TextWatcher{
            override fun afterTextChanged(s: Editable?) {
                enableButton()
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }
        })

    }

    private fun enableButton() {
        if (isFromBank){
            if ((!cardNumberEdt.text.toString().trim().isEmpty()) and
                (!nameEdt.text.toString().trim().isEmpty()) and
                (!expireDateEdt.text.toString().trim().isEmpty())){
                nextButton.setTextColor(Color.parseColor("#092058"))
                nextButton.background =
                    ContextCompat.getDrawable(this, R.drawable.rectangle_white_back_theme_border)
                nextButton.isEnabled = true
            } else {
                nextButton.setTextColor(Color.parseColor("#AABDD5"))
                nextButton.background =
                    ContextCompat.getDrawable(this, R.drawable.rectangle_white_background_no_border)
                nextButton.isEnabled = false
            }
        }else{
            if ((!cardNumberEdt.text.toString().trim().isEmpty()) and
                (!nameEdt.text.toString().trim().isEmpty()) and
                (!expireDateEdt.text.toString().trim().isEmpty()) and
                (!cvvEdt.text.toString().trim().isEmpty()) ){
                nextButton.setTextColor(Color.parseColor("#092058"))
                nextButton.background =
                    ContextCompat.getDrawable(this, R.drawable.rectangle_white_back_theme_border)
                nextButton.isEnabled = true
            } else {
                nextButton.setTextColor(Color.parseColor("#AABDD5"))
                nextButton.background =
                    ContextCompat.getDrawable(this, R.drawable.rectangle_white_background_no_border)
                nextButton.isEnabled = false
            }
        }

    }

    private fun validateData() {
        finish()
    }

}