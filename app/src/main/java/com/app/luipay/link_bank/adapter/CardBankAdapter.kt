package com.app.luipay.link_bank.adapter

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.app.luipay.R
import com.app.luipay.deposite.model.BankListModel
import com.app.luipay.link_bank.activity.LinkBankActivity
import com.app.luipay.link_bank.activity.UnlinkCardbankActivity
import kotlinx.android.synthetic.main.layout_item_add_cardbank.view.*
import kotlinx.android.synthetic.main.layout_item_cardbank.view.*

class CardBankAdapter(
    val context: Context,
    val bankData: ArrayList<BankListModel>
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    class BankViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {}
    class PlusViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {}

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (viewType == 0) {
            return BankViewHolder(
                LayoutInflater.from(context).inflate(
                    R.layout.layout_item_cardbank,
                    parent,
                    false
                )
            )
        } else {
            return PlusViewHolder(
                LayoutInflater.from(context).inflate(
                    R.layout.layout_item_add_cardbank,
                    parent,
                    false
                )
            )
        }
    }

    override fun getItemCount(): Int {
        return bankData.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is BankViewHolder) {
            var data = bankData[position]
            holder.itemView.cardImg.setImageResource(data.bankIconUrl)
            holder.itemView.bankName.text = data.bankname
            holder.itemView.bankContainer.setOnClickListener {
                var intent = Intent(context, UnlinkCardbankActivity::class.java)
                val bundle = Bundle()
                bundle.putParcelable("bank", data)
                intent.putExtras(bundle)
                context.startActivity(intent)
            }
        } else {
            holder.itemView.plusContainer.setOnClickListener {
                context.startActivity(Intent(context, LinkBankActivity::class.java))
            }
        }

    }

    override fun getItemViewType(position: Int): Int {
        var viewType = 0
        if (position == bankData.size - 1) {
            viewType = 1
        }
        return viewType
    }
}