package com.app.luipay.loginuser

import android.animation.ObjectAnimator
import android.app.Activity
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import androidx.core.content.ContextCompat
import com.app.luipay.R
import com.app.luipay.dashboard.ui.DashboardActivity
import com.app.luipay.network.ApiAuthHelper
import com.app.luipay.network.ApiClient
import com.app.luipay.network.CallBackManager
import com.app.luipay.network.RetroError
import com.app.luipay.request.EmailNumberValidationRequest
import com.app.luipay.response.BaseResponse
import com.app.luipay.signup.EnterMobileActivity
import com.app.luipay.util.Appconstant
import com.app.luipay.util.BaseActivity
import com.app.luipay.util.CommonUtils
import com.app.luipay.util.Prefutils
import kotlinx.android.synthetic.main.activity_login_selection.*


class LoginSelectionActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login_selection)
        tvLoginHere.setOnClickListener {
            val intent = Intent(this@LoginSelectionActivity, EnterMobileActivity::class.java)
            startActivityForResult(intent, Appconstant.common_request_code)
        }
        loginGap.visibility = View.GONE
        initListener()
    }



    private fun initListener() {
        rbLoginviaEmail.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                edtEmail.setText("")
                rbLoginviaPhone.isChecked = false
                edtEmail.visibility = View.VISIBLE
            } else {
                edtEmail.visibility = View.GONE
            }
            loginGap.visibility = View.VISIBLE
            scrollToBottom()
        }

        rbLoginviaPhone.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                edtPhone.setText("")
                rbLoginviaEmail.isChecked = false
                edtPhone.visibility = View.VISIBLE
            } else {
                edtPhone.visibility = View.GONE
            }
            loginGap.visibility = View.VISIBLE
            scrollToBottom()
        }

        edtEmail.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                notifyButton(s.toString(), true)
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }
        })

        edtPhone.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                notifyButton(s.toString(), false)
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }
        })

        llSlide.setOnClickListener {

            if (rbLoginviaEmail.isChecked) {
                validateEmail(edtEmail.text.toString())
            } else if (rbLoginviaPhone.isChecked) {
                validateNumber(edtPhone.text.toString())
            }
        }

    }

    private fun scrollToBottom() {
        scrollView.postDelayed(object : Runnable {
            override fun run() {
                val anim =
                    ObjectAnimator.ofInt(scrollView, "scrollY", scrollView.bottom)
                anim.duration = 1000
                anim.start()
            }
        }, 0)
    }

    private fun validateNumber(number: String) {
        if (number.isEmpty()) {
            CommonUtils.showToast(this@LoginSelectionActivity, getString(R.string.enter_mobile_no))
            return
        } else if (number.length > 10) {
            CommonUtils.showToast(this@LoginSelectionActivity, getString(R.string.valid_phone))
            return
        }
        varifyLoginDetail(number, true)
    }

    private fun varifyLoginDetail(number: String, fromNumber: Boolean) {
        if (!isOnline()) {
            CommonUtils.showToast(this, getString(R.string.no_internet_connection))
            return
        }
        showProcessDialog()
        val helper = ApiClient.getClient().create(ApiAuthHelper::class.java)
        val request = EmailNumberValidationRequest()
        request.CountryCodeId =
            Prefutils.getFromPrefsInt(this@LoginSelectionActivity, Prefutils.COUNTRY_CODE_API)

        request.LanguageTypeId =
            Prefutils.getFromPrefsInt(this@LoginSelectionActivity, Prefutils.API_LANGUAGE)
        if (fromNumber) {
            request.MobileNo = number
            request.Flag = "MOBN"
        } else {
            request.EmailId = number
            request.Flag = "EMAL"
        }
        request.ServiceAuthenticationToken = Appconstant.API_KEY
        val call = helper.validateEmailNumber(request, Appconstant.API_HEADER)
        call.enqueue(object : CallBackManager<BaseResponse>() {
            override fun onSuccess(any: BaseResponse?, message: String) {
                hideProcessDailog()
                if (fromNumber) {
                    Prefutils.saveToPrefs(
                        this@LoginSelectionActivity,
                        Prefutils.MOBILE,
                        request.MobileNo
                    )
                }
                var signupIntent = Intent(this@LoginSelectionActivity, SignupActivity::class.java)
                signupIntent.putExtra("type", request.Flag)
                signupIntent.putExtra("number", number)
                startActivityForResult(signupIntent, Appconstant.common_request_code)
            }

            override fun onFailure(message: String) {
                hideProcessDailog()
                CommonUtils.showToast(this@LoginSelectionActivity, message)
            }

            override fun onError(error: RetroError) {
                hideProcessDailog()
            }

        })

//        helper.loginWithMobileNumber(loginRequest, Appconstant.API_HEADER)


    }

    private fun validateEmail(email: String) {
        if (email.isEmpty()) {
            CommonUtils.showToast(this@LoginSelectionActivity, getString(R.string.enter_ur_email))
            return
        } else if (!CommonUtils.emailValidator(email)) {
            CommonUtils.showToast(this@LoginSelectionActivity, getString(R.string.valid_email))
            return
        }
        varifyLoginDetail(email, false)

    }


    private fun notifyButton(text: String, isEmail: Boolean) {
        if (isEmail and (CommonUtils.emailValidator(text))) {
            enableButton(true)
        } else if (!isEmail and (text.trim().length == 10)) {
            enableButton(true)
        } else {
            enableButton(false)
        }

    }

    private fun enableButton(enable: Boolean) {
        if (enable) {
            llSlide.background = ContextCompat.getDrawable(this, R.drawable.blue_button_gradient)
            txtLogin.setTextColor(ContextCompat.getColor(this, R.color.colorWhite))
            llSlide.isClickable = true
        } else {
            llSlide.background = ContextCompat.getDrawable(this, R.drawable.gray_button_gradient)
            txtLogin.setTextColor(Color.parseColor("#092058"))
            llSlide.isClickable = false
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == Appconstant.common_request_code && resultCode == Activity.RESULT_OK) {
            startActivity(Intent(this@LoginSelectionActivity, DashboardActivity::class.java))
            finish()
        }
    }

}