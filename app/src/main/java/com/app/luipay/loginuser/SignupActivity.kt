package com.app.luipay.loginuser

import android.app.Activity
import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import androidx.core.content.ContextCompat
import com.app.luipay.R
import com.app.luipay.dashboard.ui.DashboardActivity
import com.app.luipay.network.ApiAuthHelper
import com.app.luipay.network.ApiClient
import com.app.luipay.network.CallBackManager
import com.app.luipay.network.RetroError
import com.app.luipay.request.LoginWithEmail
import com.app.luipay.request.LoginWithMobileNumber
import com.app.luipay.response.BaseResponse
import com.app.luipay.response.login_response.LoginResponse
import com.app.luipay.signup.CompleteKYCActivity
import com.app.luipay.util.*
import kotlinx.android.synthetic.main.activity_signup.*


class SignupActivity : BaseActivity(), View.OnClickListener,
    BiomatricAuthencation.BiomatricAuthencationListener {

    private lateinit var mobileNumber: String
    private lateinit var type: String
    var showPassword = true
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signup)
        getIntentData()
        initListener()
        enableFingrePrint()
    }

    private fun getIntentData() {
        if ((intent.hasExtra("number")) and (intent.hasExtra("type"))) {
            mobileNumber = intent.getStringExtra("number")!!
            type = intent.getStringExtra("type")!!
        }else{
            mobileNumber = Prefutils.getFromPrefs(this@SignupActivity,Prefutils.MOBILE)!!
            type = "MOBN"
        }
        number.text="${Prefutils.getFromPrefs(this@SignupActivity,Prefutils.COUNTRY_CODE)} $mobileNumber"

    }

    private fun enableFingrePrint() {
        if (CommonUtils.isBioMetricEnable(this@SignupActivity)
            and (Prefutils.getFromPrefs(this@SignupActivity, Prefutils.PASSWORD)!!.isNotEmpty())
        ) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                BiomatricAuthencation.initlizeApp(this@SignupActivity)
                txtFingerLayout.visibility = View.VISIBLE
                txtFingerPrint.visibility = View.VISIBLE
                imgFingerprint.visibility = View.VISIBLE
            } else {
                txtFingerLayout.visibility = View.GONE
                txtFingerPrint.visibility = View.GONE
                imgFingerprint.visibility = View.GONE
            }
        } else {
            txtFingerLayout.visibility = View.GONE
            txtFingerPrint.visibility = View.GONE
            imgFingerprint.visibility = View.GONE
        }
    }


    private fun initListener() {
        txtForgetPassword.setOnClickListener(this)
        txtFingerLayout.setOnClickListener(this)
        txtSignOut.setOnClickListener(this)
        imgShowPassword.setOnClickListener(this)
        txtSignup.setOnClickListener(this)

        edtPassword.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                changeSignUpButtonState(s.toString())
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }
        })
    }

    private fun changeSignUpButtonState(otp: String) {
        if (otp.isEmpty()) {
            imgShowPassword.visibility = View.GONE
        } else {
            imgShowPassword.visibility = View.VISIBLE
            if (otp.length >= 6) {
                txtSignup.setTextColor(Color.parseColor("#092058"))
                txtSignup.background =
                    ContextCompat.getDrawable(this, R.drawable.rectangle_white_back_theme_border)
                txtSignup.isEnabled = true

            } else {
                txtSignup.setTextColor(Color.parseColor("#AABDD5"))
                txtSignup.background =
                    ContextCompat.getDrawable(this, R.drawable.rectangle_white_background_no_border)
                txtSignup.isEnabled = false
            }
        }
    }


    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.txtForgetPassword -> {

            }
            R.id.txtSignup -> {
                if (isValidate()) {
                    checkPasswordFromAPi()
                }
            }
            R.id.txtFingerLayout -> {

            }
            R.id.txtSignOut -> {
                startActivity(Intent(this@SignupActivity, LoginSelectionActivity::class.java))
                finish()
            }
            R.id.imgShowPassword -> {
                CommonUtils.changePasswordFormat(showPassword, edtPassword, imgShowPassword)
                showPassword = !showPassword
            }
        }
    }

    private fun isValidate(): Boolean {
        if (edtPassword.text!!.isEmpty()) {
            CommonUtils.showToast(
                this@SignupActivity,
                getString(R.string.enter_ur_password)
            )
            return false
        } else if (edtPassword.text!!.toString().trim().length < 6) {
            CommonUtils.showToast(this@SignupActivity, getString(R.string.password_match))
            return false
        } else if (!CommonUtils.checkPasswordPattern(edtPassword.text!!.toString())) {
            CommonUtils.showToast(this@SignupActivity, getString(R.string.password_pattern))
            return false
        }
        return true

    }

    private fun checkPasswordFromAPi() {
        if ((::mobileNumber.isInitialized) and (::type.isInitialized)) {
            if ((type.isNotEmpty()) and (type.equals("MOBN"))) {
                loginWithNumber(false)
            } else if ((type.isNotEmpty()) and (type.equals("EMAL"))) {
                loginWithEmail()
            }
        } else {
            setResult(Activity.RESULT_CANCELED)
            finish()
        }
    }

    private fun loginWithNumber(fromBio: Boolean) {
        if (!isOnline()) {
            CommonUtils.showToast(this, getString(R.string.no_internet_connection))
            return
        }
        showProcessDialog()
        val request = LoginWithMobileNumber()


        request.CountryCodeId = Prefutils.getFromPrefsInt(this, Prefutils.COUNTRY_CODE_API)
        request.LanguageTypeId = Prefutils.getFromPrefsInt(this, Prefutils.API_LANGUAGE)
        request.ServiceAuthenticationToken = Appconstant.API_KEY
        request.DeviceNotificationID = "adbc1234"

        if (fromBio) {
            request.Password = Prefutils.getFromPrefs(this@SignupActivity, Prefutils.PASSWORD)!!
            request.MobileNo = Prefutils.getFromPrefs(this@SignupActivity, Prefutils.MOBILE)!!
        } else {
            request.Password = CustomPassword.encrypt(edtPassword.text.toString().trim())
            request.MobileNo = mobileNumber
        }


        val helper = ApiClient.getClient().create(ApiAuthHelper::class.java)
        val call = helper.loginWithMobileNumber(request, Appconstant.API_HEADER)
        call.enqueue(object : CallBackManager<LoginResponse>() {
            override fun onSuccess(any: LoginResponse?, message: String) {
                var response = any as LoginResponse
                hideProcessDailog()
                Prefutils.saveToPrefs(
                    this@SignupActivity,
                    Prefutils.ACCESS_TOKEN,
                    response.Response.AccessToken.access_token
                )
                CommonUtils.savePassword(this@SignupActivity, request.Password)
                if (response.Response.IsKycPending) {
                    startActivityForResult(
                        Intent(
                            this@SignupActivity,
                            CompleteKYCActivity::class.java
                        ), 1000
                    )
                } else {
                    startActivity(Intent(this@SignupActivity,DashboardActivity::class.java))
                    finish()
                }
            }

            override fun onFailure(message: String) {
                CommonUtils.showToast(this@SignupActivity, message)
                hideProcessDailog()
            }

            override fun onError(error: RetroError) {
                hideProcessDailog()
            }
        })
    }

    private fun loginWithEmail() {
        if (!isOnline()) {
            CommonUtils.showToast(this, getString(R.string.no_internet_connection))
            return
        }
        showProcessDialog()
        val request = LoginWithEmail()
        request.EmailId = mobileNumber
        request.CountryCodeId = Prefutils.getFromPrefsInt(this, Prefutils.COUNTRY_CODE_API)
        request.LanguageTypeId = Prefutils.getFromPrefsInt(this, Prefutils.API_LANGUAGE)
        request.ServiceAuthenticationToken = Appconstant.API_KEY
        request.DeviceNotificationID = "adbc1234"
        request.Password = edtPassword.text.toString().trim()

        val helper = ApiClient.getClient().create(ApiAuthHelper::class.java)
        val call = helper.loginWithEmail(request, Appconstant.API_HEADER)
        call.enqueue(object : CallBackManager<BaseResponse>() {
            override fun onSuccess(any: BaseResponse?, message: String) {
                hideProcessDailog()
                setResult(Activity.RESULT_OK)
                finish()
            }

            override fun onFailure(message: String) {
                CommonUtils.showToast(this@SignupActivity, message)
                hideProcessDailog()
            }

            override fun onError(error: RetroError) {
                hideProcessDailog()
            }

        })


    }


    override fun onAuthenticationSucceeded() {
        loginWithNumber(true)
    }

    override fun onAuthenticationError(errorCode: Int) {

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if ((requestCode == 1000) and (resultCode == Activity.RESULT_OK)) {
            startActivity(Intent(this@SignupActivity,DashboardActivity::class.java))
            finish()

        }
    }


}