package com.app.luipay.giftcard.activity

import android.os.Bundle
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.luipay.R
import com.app.luipay.giftcard.adapter.GiftcardAdapter
import com.app.luipay.util.BaseActivity
import kotlinx.android.synthetic.main.activity_gift_card.*
import kotlinx.android.synthetic.main.header_center_text_left_arrow.*

class GiftCardActivity : BaseActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_gift_card)
        initHeader()
    }

    private fun initHeader() {
        imgBack.setOnClickListener { finish() }
        headerTitle.text=getString(R.string.gift_card)
        recyclerGift.layoutManager = LinearLayoutManager(this)
        val verticalDecoration = DividerItemDecoration(
            this,
            DividerItemDecoration.VERTICAL
        )
        val verticalDivider =
            ContextCompat.getDrawable(this, R.drawable.divider_16)
        verticalDecoration.setDrawable(verticalDivider!!)
        recyclerGift.addItemDecoration(verticalDecoration)
        recyclerGift.adapter = GiftcardAdapter(this)
    }
}