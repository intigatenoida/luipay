package com.app.luipay.util

import android.content.Context
import android.net.ConnectivityManager
import android.os.Bundle
import android.view.Window
import androidx.appcompat.app.AppCompatActivity

open class BaseActivity : AppCompatActivity() {


    private lateinit var dialogFragment: ProcessDailog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE);
       try{
           supportActionBar!!.hide(); //hide the title bar
       }catch (e:Exception){}
    }

    fun showProcessDialog() {
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        val prev = supportFragmentManager.findFragmentByTag("ProcessDialog")
        if (prev != null) {
            val df = prev as ProcessDailog
            df.dismissAllowingStateLoss()
//            fragmentTransaction.remove(prev)
        }
        fragmentTransaction.addToBackStack(null)
        dialogFragment = ProcessDailog.getInstance()
        fragmentTransaction.add(dialogFragment,"ProcessDialog").commitAllowingStateLoss()
//        dialogFragment.show(fragmentTransaction, "ProcessDialog")
    }

    fun hideProcessDailog() {
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        val prev = supportFragmentManager.findFragmentByTag("ProcessDialog")
        if (prev != null) {
            val df = prev as ProcessDailog
            df.dismissAllowingStateLoss()
//            fragmentTransaction.remove(prev)
        }
        if (::dialogFragment.isInitialized ) {
            dialogFragment.dismiss()

        }
    }

    open fun isOnline(): Boolean {
        val cm =
            getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val netInfo = cm.activeNetworkInfo
        return netInfo != null && netInfo.isConnectedOrConnecting
    }


}