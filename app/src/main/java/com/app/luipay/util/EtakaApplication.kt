package com.app.luipay.util

import android.app.Application
import androidx.multidex.MultiDex

class EtakaApplication:Application() {

    override fun onCreate() {
        super.onCreate()
        MultiDex.install(this);
    }
}