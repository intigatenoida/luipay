package com.app.luipay.util

import android.app.Activity
import android.content.ContentValues
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.hardware.fingerprint.FingerprintManager
import android.net.Uri
import android.os.Build
import android.os.Environment
import android.provider.MediaStore
import android.provider.Settings
import android.text.Editable
import android.text.Selection
import android.text.TextUtils
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.util.Log
import android.util.Patterns
import android.widget.EditText
import android.widget.ImageView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.content.FileProvider
import com.app.luipay.BuildConfig
import com.app.luipay.R
import com.app.luipay.network.ApiAuthHelper
import com.app.luipay.network.ApiClient
import com.app.luipay.network.CallBackManager
import com.app.luipay.network.RetroError
import com.app.luipay.request.AppStatusRequest
import com.app.luipay.response.AppStatusResponse.AppStatusReponse
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.gson.GsonBuilder
import java.io.*
import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Pattern


class CommonUtils {

    companion object {

        fun showToast(context: Context, msg: String) {
            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show()
        }

        fun emailValidator(email: String): Boolean {
            return ((!TextUtils.isEmpty(email)) and (Patterns.EMAIL_ADDRESS.matcher(email).matches()));
        }

        fun changePasswordFormat(isShow: Boolean, editText: EditText, imageView: ImageView) {
            if (isShow) {
                imageView.setImageResource(R.drawable.eye_blue)
                editText.setTransformationMethod(HideReturnsTransformationMethod.getInstance())
            } else {
                imageView.setImageResource(R.drawable.eye_gray)
                editText.setTransformationMethod(PasswordTransformationMethod.getInstance())
            }
            val position: Int = editText.text.toString().trim().length
            val etext: Editable = editText.text!!
            Selection.setSelection(etext, position)
        }

        fun isBioMetricEnable(context: Context): Boolean {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                try {
                    val fingerprintManager =
                        context.getSystemService(Context.FINGERPRINT_SERVICE) as? FingerprintManager
                    if (fingerprintManager != null) {
                        return fingerprintManager.isHardwareDetected && fingerprintManager.hasEnrolledFingerprints()
                    } else {
                        return false
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                    return false
                }
            } else {
                return false
            }

        }


        fun openSettingDialog(context: Context) {

            var builder = AlertDialog.Builder(context)
            //Uncomment the below code to Set the message and title from the strings.xml file
            //Uncomment the below code to Set the message and title from the strings.xml file
            builder.setMessage("Permission is required for the further process, Please provide the permisson")
                .setTitle("Permission Dialog")
                .setCancelable(false)
                .setPositiveButton("Yes",
                    DialogInterface.OnClickListener { dialog, id ->
                        dialog.cancel()
                        val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
                        val uri: Uri = Uri.fromParts("package", context.getPackageName(), null)
                        intent.data = uri
                        (context as Activity).startActivityForResult(
                            intent,
                            EtakaPermissiom.SETTING_CODE
                        )

                    })
                .setNegativeButton("No",
                    DialogInterface.OnClickListener { dialog, id ->
                        //  Action for 'NO' Button
                        dialog.cancel()

                    })
            //Creating dialog box
            //Creating dialog box
            val alert: AlertDialog = builder.create()
            //Setting the title manually
            //Setting the title manually
            alert.show()
        }


        public fun getTimeInUTC(): String {
            val simpleDateFormat = SimpleDateFormat("yyyy/MM/dd HH:mm:ss", Locale.ENGLISH)
            simpleDateFormat.timeZone = TimeZone.getTimeZone("UTC")
            val timeInUTC = simpleDateFormat.format(Date())
            return timeInUTC
        }

        fun isDebug(): Boolean {
            return BuildConfig.ENABLE_LOG
        }

        fun checkStatusMessage(messageKey: String) {
            val file =
                File("data/data/com.app.luipay/responsecode/statusMessage.text")
            if (file.exists()) {
                val data = StringBuilder()
                var br: BufferedReader? = null
                try {
                    br = BufferedReader(FileReader(file))
                    var line: String?
                    while (br.readLine().also { line = it } != null) {
                        data.append(line)
                        data.append('\n')
                    }
                } catch (e: IOException) {
                    e.printStackTrace()
                } finally {
                    br!!.close()
                }
                if (!data.toString().trim().equals(messageKey, false)) {
                    updateResponseMessage(messageKey)
                }
            } else {
                saveStatusMessage(messageKey)
            }
        }

        private fun updateResponseMessage(messageKey: String) {
            val helper = ApiClient.getClient().create(ApiAuthHelper::class.java)
            val request = AppStatusRequest()
            request.LanguageTypeId = 1
            request.ServiceAuthenticationToken = Appconstant.API_KEY
            val call = helper.getAppResponse(request, Appconstant.API_HEADER)
            call.enqueue(object : CallBackManager<AppStatusReponse>() {
                override fun onSuccess(any: AppStatusReponse?, message: String) {
                    val responseData = any as AppStatusReponse
                    responseData.let {
                        createFile(GsonBuilder().disableHtmlEscaping().create().toJson(responseData))
                        saveStatusMessage(messageKey)
                    }
                }

                override fun onFailure(message: String) {

                }

                override fun onError(error: RetroError) {

                }
            })
        }

        private fun createFile(responseData: String) {
            var fos: FileWriter? = null
            var isDirectoryCreated = false
            try {
                val directory =
                    File("data/data/com.app.luipay/responsecode")
                if (directory.exists()) {
                    isDirectoryCreated = true
                } else {
                    isDirectoryCreated = directory.mkdir()
                }
                if (isDirectoryCreated) {
                    val file =
                        File("data/data/com.app.luipay/responsecode/rescode.text")
                    if (file.createNewFile()) {
                        fos = FileWriter(file)
                        fos.write(responseData)
                    }
                }
            } catch (fileNotFound: FileNotFoundException) {
            } catch (ioException: IOException) {
                ioException.printStackTrace()
            } finally {
                fos?.close()
            }
        }


        fun saveStatusMessage(messageKey: String) {
            var fos: FileWriter? = null
            var isDirectoryCreated = false
            try {
                val directory =
                    File("data/data/com.app.luipay/responsecode")
                if (directory.exists()) {
                    isDirectoryCreated = true
                } else {
                    isDirectoryCreated = directory.mkdir()
                }
                if (isDirectoryCreated) {
                    val file =
                        File("data/data/com.app.luipay/responsecode/statusMessage.text")
                    if (file.exists()) {
                        file.delete()
                    }
                    if (file.createNewFile()) {
                        fos = FileWriter(file)
                        fos.write(messageKey)
                    }
                }
            } catch (fileNotFound: FileNotFoundException) {
                fileNotFound.printStackTrace()
            } catch (ioException: Exception) {
                ioException.printStackTrace()
            } finally {
                fos?.close()
            }
        }

        fun showLog(message: String?) {
            showLog("Etaka", message)
        }

        fun showLog(s: String, message: String?) {
            if (isDebug()) {
                Log.e(s, message)
            }
        }

        fun convertMiliIntoMinuteSeconds(milliseconds: Long): String {
            val minutes = (milliseconds / 1000) / 60
            val seconds = (milliseconds / 1000) % 60
            val secondsStr = seconds.toString()
            var secs = ""
            if (secondsStr.length >= 2) {
                secs = secondsStr.substring(0, 2)
            } else {
                secs = "0" + secondsStr
            }


            return "0$minutes : $secs"

        }

        fun checkPasswordPattern(password: String): Boolean {
            val patternStr = "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[_!@#$%^&?]).{6,20})"
            val pattern = Pattern.compile(patternStr)
            return pattern.matcher(password).matches()
        }

        fun savePassword(context: Context, password: String) {
            Prefutils.saveToPrefs(context, Prefutils.PASSWORD, password)
        }

        fun getPassword(context: Context) {
            Prefutils.getFromPrefs(context, Prefutils.PASSWORD)
        }

        fun getCameraUri(context: Context): Uri {
            val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(Date())
            val fileName = "IMG_${timeStamp}"

            var uri: Uri? = null
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                val resolver = context.contentResolver
                val contentValues = ContentValues().apply {
                    put(MediaStore.MediaColumns.DISPLAY_NAME, fileName + ".jpeg")
                    put(MediaStore.MediaColumns.MIME_TYPE, "image/jpeg")
                    put(MediaStore.MediaColumns.RELATIVE_PATH, "DCIM/luipay/")
                }

                uri = resolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, contentValues)
            }
            return uri ?: getUriForPreQ(fileName, context)
        }

        private fun getUriForPreQ(fileName: String, context: Context): Uri {
            val dir = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES)

            val file = File.createTempFile(fileName, ".jpeg", dir)

            return FileProvider.getUriForFile(
                context,
                BuildConfig.APPLICATION_ID + ".provider",
                file
            )
        }


        fun loadImage(context: Context, url: String, imageView: ImageView) {
            Glide.with(context)
                .load(url)
//                .apply(RequestOptions.circleCropTransform())
                .into(imageView)
        }

        fun loadImageCircular(context: Context, url: String, imageView: ImageView) {
            Glide.with(context)
                .load(url)
                .apply(RequestOptions.circleCropTransform())
                .into(imageView)
        }

        fun loadProfilePic(context: Context, imageView: ImageView) {
            val profileData = Prefutils.getProfileData(context)
            profileData?.let {
                loadImageCircular(context, it.ProfilePicUrl, imageView)
            }
        }


    }


}