package com.app.luipay.util

import android.content.Context
import androidx.fragment.app.Fragment

open class BaseFragment : Fragment() {

    var mContext: Context? = null

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mContext = context
    }

    fun showProcessDialog() {
        mContext?.let {
            (mContext as BaseActivity).showProcessDialog()
        }
    }

    fun hideProcessDailog() {
        mContext?.let {
            (mContext as BaseActivity).hideProcessDailog()
        }
    }

    fun isOnline(): Boolean {
        if (mContext != null) {
            return (mContext as BaseActivity).isOnline()
        }
        return false
    }
}