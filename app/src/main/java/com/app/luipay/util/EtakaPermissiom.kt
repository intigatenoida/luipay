package com.app.luipay.util

import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat


open class EtakaPermissiom {

    companion object {
        val Contact_permission = android.Manifest.permission.READ_CONTACTS
        val Camera_permission = android.Manifest.permission.CAMERA
        val BIO_permission = android.Manifest.permission.USE_BIOMETRIC

        val CONTACT_CODE = 100
        val CAMERA_CODE = 101
        val BIO_CODE = 102
        val SETTING_CODE = 1000


        fun checkAppPermission(context: Context, code: Int): Boolean {
            val permissionStr = getPermission(code)
            if (ContextCompat.checkSelfPermission(
                    context.applicationContext,
                    permissionStr
                ) == PackageManager.PERMISSION_GRANTED
            ) {
                return true
            } else {
                ActivityCompat.requestPermissions(
                    context as Activity, arrayOf(permissionStr),
                    code
                )
            }
            return false
        }


        private fun getPermission(code: Int): String {
            when (code) {
                BIO_CODE -> {
                    return BIO_permission
                }
                CONTACT_CODE -> {
                    return Contact_permission
                }
                CAMERA_CODE -> {
                    return Camera_permission
                }
            }
            return ""
        }

        fun CheckForNeverAsk(context: Context, code: Int) {
            val permissionStr = getPermission(code)
            if (ActivityCompat.shouldShowRequestPermissionRationale(
                    context as Activity,
                    permissionStr
                )
            ) {
                checkAppPermission(context, code)
            } else {
                CommonUtils.openSettingDialog(context)
            }
        }
    }


}