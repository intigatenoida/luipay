package com.app.luipay.util

import android.content.Context
import androidx.preference.PreferenceManager
import com.app.luipay.response.PrefileResponse.Response
import com.google.gson.Gson

class Prefutils {

    companion object {

        const val PASSWORD = "PASSWORD"
        const val COUNTRY_CODE_API = "COUNTRY_CODE_API"
        const val COUNTRY_CODE = "COUNTRY_CODE"
        const val MOBILE = "MOBILE"
        val ACCESS_TOKEN = "ACCESS_TOKEN"
        val API_LANGUAGE = "API_LANGUAGE"
        val PROFILE = "PROFILE"


        fun saveToPrefs(
            context: Context?,
            key: String?,
            value: String?
        ): String? {
            val prefs = PreferenceManager
                .getDefaultSharedPreferences(context)
            val editor = prefs.edit()
            editor.putString(key, value)
            editor.apply()
            return key
        }

        fun saveToPrefsInt(
            context: Context?,
            key: String?,
            value: Int
        ): String? {
            val prefs = PreferenceManager
                .getDefaultSharedPreferences(context)
            val editor = prefs.edit()
            editor.putInt(key, value)
            editor.apply()
            return key
        }

        fun logout(c: Context?) {
            val prefs = PreferenceManager
                .getDefaultSharedPreferences(c)
            val editor = prefs.edit()
            editor.clear()
            editor.apply()
        }

        fun getFromPrefs(
            context: Context?,
            key: String?
        ): String? {
            return if (context != null) {
                val sharedPrefs = PreferenceManager
                    .getDefaultSharedPreferences(context)
                try {
                    sharedPrefs.getString(key, "")
                } catch (e: Exception) {
                    ""
                }
            } else {
                ""
            }
        }

        fun getFromPrefsInt(
            context: Context?,
            key: String?
        ): Int {
            return if (context != null) {
                val sharedPrefs = PreferenceManager
                    .getDefaultSharedPreferences(context)
                try {
                    sharedPrefs.getInt(key, 0)
                } catch (e: Exception) {
                    0
                }
            } else {
                0
            }
        }


        fun getProfileData(context: Context): Response? {
            val profileResponse = getFromPrefs(context, PROFILE)
            if (profileResponse!!.isNotEmpty()) {
                return Gson().fromJson(profileResponse, Response::class.java)
            }
            return null
        }
    }

}