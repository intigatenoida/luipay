package com.app.luipay.util

class Appconstant {
    companion object {
        val key_message_body= "sms_body"
        val mobile: String = "KEY_MOBILE"
        val COUNTRY_CODE: String = "COUNTRY_CODE"
        val common_request_code: Int = 1
        val TYPE_ID = 2
        val TYPE_DOB = 3
        val TYPE_ADDRESS = 4
        val API_KEY = "A40BE959-D225-4C10-86E6-04419D83FD26"
        val API_HEADER = "application/json"
        val API_LANGUAGE = 1
    }

}