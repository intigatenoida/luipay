///*
// * Copyright (c) oneAD
// */
//
//package com.app.luipay.util;
//
//import android.app.Activity;
//import android.content.Intent;
//import android.os.Bundle;
//import android.support.annotation.Nullable;
//import android.text.TextUtils;
//
//import com.razorpay.Checkout;
//import com.razorpay.PaymentResultListener;
//
//import org.json.JSONObject;
//
//import add_more.Login.R;
//import baseactivity.BaseActivity;
//import utils.CommonUtils;
//import utils.PrefUtilsOneAd;
//
//public class PaymentActivity extends BaseActivity implements PaymentResultListener {
//
//
//    private String TAG = "PaymentActivity";
//
//    @Override
//    protected void onCreate(@Nullable Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_recharge_payment);
//        Double amount = null;
//        String number = "";
//        if (getIntent() != null && getIntent().getExtras() != null) {
//            amount = getIntent().getExtras().getDouble("amount", 0);
//            number = getIntent().getExtras().getString("number");
//
//        }
//
//
//
//        startPayment(CommonUtils.getCurrenyFortmatter(""+amount), number);
//    }
//
////    private Double getAmount(Double amount) {
//////        String amountStr = String.format("%.2f", amount);
//////        if (amountStr.contains(".")){
//////            return Double.parseDouble(amountStr);
//////        }
////        CommonUtils.getCurrenyFortmatter(""+amount);
////        return Double.parseDouble();
////    }
//
//
//    public void startPayment(String amount, String number) {
//        /**
//         * Instantiate Checkout
//         */
//        Checkout checkout = new Checkout();
//        checkout.setFullScreenDisable(true);
//        /**
//         * Set your logo here
//         */
//        checkout.setImage(R.mipmap.logo_one_add);
//
//        /**
//         * Reference to current activity
//         */
//        final Activity activity = this;
//
//        /**
//         * Pass your payment options to the Razorpay Checkout as a JSONObject
//         */
//        try {
//            JSONObject options = new JSONObject();
//
//            /**
//             * Set Merchant Name
//             * i.e. OneAD.
//             */
//            options.put("name", "OneAD");
//
//            /**
//             * Description
//             * Pay x.x
//             */
//            options.put("description", "Pay " + amount);
//
//            options.put("currency", "INR");
//            options.put("prefill.contact", PrefUtilsOneAd.getUserModel(PaymentActivity.this).getResponse().getPhoneNumber());
//            String email = PrefUtilsOneAd.getUserModel(PaymentActivity.this).getResponse().getEmail();
//            options.put("prefill.email", TextUtils.isEmpty(email) ? "" : email);
//
//            /**
//             * Amount is always passed in PAISE
//             * Eg: "500" = Rs 5.00
//             */
//            options.put("amount", "" +  ((int)(Math.round(Double.parseDouble(amount)*100))));
//            checkout.open(activity, options);
//        } catch (Exception e) {
//            CommonUtils.showLog(TAG, "Error in starting Razorpay Checkout" + e.getMessage());
//        }
//    }
//
//    @Override
//    public void onPaymentSuccess(String s) {
//        CommonUtils.showLog("****onPaymentSuccess", s);
//        Intent bundle = new Intent();
//        bundle.putExtra("orderId", s);
//        setResult(RESULT_OK, bundle);
//        finish();
//    }
//
//    @Override
//    public void onPaymentError(int i, String s) {
//        Intent bundle = new Intent();
//        bundle.putExtra("orderId", "");
//        setResult(RESULT_CANCELED, bundle);
//        finish();
//
//    }
//
//}


//implementation 'com.razorpay:checkout:1.5.12'
