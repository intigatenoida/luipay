package com.app.luipay.util

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.app.luipay.R


class ProcessDailog : BaseDialogFragment() {

    lateinit var mContext: Context

    companion object {
        fun getInstance(): ProcessDailog {
            return ProcessDailog()
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mContext = context
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val rootView = inflater.inflate(R.layout.process_loader, container, false)
//        var logoImage = rootView.findViewById<ImageView>(R.id.logo)
//        val aniRotateClk: Animation = AnimationUtils.loadAnimation(
//            mContext,
//            R.anim.rotate_clockwise
//        )
//        logoImage.startAnimation(aniRotateClk)
        return rootView
    }
}