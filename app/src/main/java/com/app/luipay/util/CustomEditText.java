package com.app.luipay.util;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import androidx.appcompat.widget.AppCompatEditText;
import com.app.luipay.R;


public class CustomEditText extends AppCompatEditText {

    /**
     * @param context refer to the context
     */
    public CustomEditText(Context context) {
        super(context);
    }

    /**
     * @param context refer to the context
     * @param attrs   the attributes to be applied
     */
    public CustomEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        setCustomFont(context, attrs);
    }

    /**
     * @param context  refer to the context
     * @param attrs    the attributes to be applied
     * @param defStyle the style to be applied
     */
    public CustomEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setCustomFont(context, attrs);
    }

    /**
     * @param ctx   refer to the context
     * @param attrs the attributes to be applied
     */
    private void setCustomFont(Context ctx, AttributeSet attrs) {
        TypedArray a = ctx.obtainStyledAttributes(attrs, R.styleable.custom_editview_font);
        String customFont = a.getString(R.styleable.custom_editview_font_custom_font_edit);
      //  String fontName = "fonts/Lato-Regular.ttf";
        setCustomFont(ctx, customFont);
        a.recycle();
    }

    /**
     * @param ctx   refer to the context
     * @param asset the asset to be applied
     * @return returns a boolean value
     */
    public boolean setCustomFont(Context ctx, String asset) {
        Typeface tf;
        try {
            tf = Typeface.createFromAsset(ctx.getAssets(), asset);
        } catch (Exception e) {
            return false;
        }
        setTypeface(tf);
        return true;
    }
}
