package com.app.luipay.util

import android.content.Context
import android.hardware.fingerprint.FingerprintManager
import android.os.Build
import android.os.CancellationSignal
import androidx.annotation.RequiresApi
import javax.crypto.Cipher

class BiomatricAuthencation {

    interface BiomatricAuthencationListener {
        fun onAuthenticationSucceeded()
        fun onAuthenticationError(errorCode: Int)
    }

    companion object {
        private lateinit var fingerprintManager: FingerprintManager
        /*        private lateinit var promptInfo: BiometricPrompt.PromptInfo
                        private lateinit var keyStore: KeyStore
                        private lateinit var keyGenerator: KeyGenerator
                        private lateinit var biometricPrompt: BiometricPrompt
                        private lateinit var defaultCipher: javax.crypto.Cipher
                         private val ANDROID_KEY_STORE = "AndroidKeyStore"
                */
        private var cryptoObject: FingerprintManager.CryptoObject? = null
        private var cipherHelper: CipherHelper? = null
        private var cancellationSignal: CancellationSignal? = null
        private val DEFAULT_KEY = "default_key"

        lateinit var biomatricAuthencationListener: BiomatricAuthencationListener

        @RequiresApi(Build.VERSION_CODES.M)
        fun initlizeApp(context: Context) {
            /*        setupKeyStoreAndKeyGenerator()
                    defaultCipher = setupCiphers()
                    biometricPrompt = createBiometricPrompt(context)
                    promptInfo = createPromptInfo(context)*/
            biomatricAuthencationListener = context as BiomatricAuthencationListener
            initFingerPrint(context)
        }

        @RequiresApi(Build.VERSION_CODES.M)
        private fun initFingerPrint(context: Context) {
            cipherHelper=CipherHelper(DEFAULT_KEY)
            cryptoObject = cipherHelper!!.encryptionCryptoObject
            cancellationSignal = CancellationSignal()
            fingerprintManager =
                context.getSystemService(Context.FINGERPRINT_SERVICE) as FingerprintManager


            if (fingerprintManager.isHardwareDetected() && fingerprintManager.hasEnrolledFingerprints()) {
                fingerprintManager.authenticate(
                    cryptoObject,
                    cancellationSignal,
                    0,
                    object : FingerprintManager.AuthenticationCallback() {
                        override fun onAuthenticationError(
                            errorCode: Int,
                            errString: CharSequence
                        ) {
                            super.onAuthenticationError(errorCode, errString)
                            biomatricAuthencationListener.onAuthenticationError(errorCode)

                        }

                        override fun onAuthenticationHelp(
                            helpCode: Int,
                            helpString: CharSequence
                        ) {
                            super.onAuthenticationHelp(helpCode, helpString)
                            biomatricAuthencationListener.onAuthenticationError(-1)
                        }

                        override fun onAuthenticationSucceeded(result: FingerprintManager.AuthenticationResult) {
                            super.onAuthenticationSucceeded(result)
                            biomatricAuthencationListener.onAuthenticationSucceeded()
                        }

                        override fun onAuthenticationFailed() {
                            super.onAuthenticationFailed()
                            biomatricAuthencationListener.onAuthenticationError(-2)
                        }
                    },
                    null
                )
            }
        }


        /*   fun showDialog(): Boolean {
               if ((::defaultCipher.isInitialized) and (::promptInfo.isInitialized)) {
                   if (initCipher(defaultCipher, DEFAULT_KEY)) {
                       biometricPrompt.authenticate(
                           promptInfo,
                           BiometricPrompt.CryptoObject(defaultCipher)
                       )
                       return true
                   } else {
                       return false
                   }
               } else {
                   return false
               }

           }

           */
        /**
         * Initialize the [Cipher] instance with the created key in the [createKey] method.
         *
         * @param keyName the key name to init the cipher
         * @return `true` if initialization succeeded, `false` if the lock screen has been disabled or
         * reset after key generation, or if a fingerprint was enrolled after key generation.
         *//*
        private fun initCipher(cipher: Cipher, keyName: String): Boolean {
            try {
                keyStore.load(null, null)
                cipher.init(
                    Cipher.ENCRYPT_MODE,
                    keyStore.getKey(keyName, "luipay".toCharArray()) as SecretKey
                )
                return true
            } catch (e: Exception) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    when (e) {
                        is KeyPermanentlyInvalidatedException -> return false
                        is KeyStoreException,
                        is CertificateException,
                        is UnrecoverableKeyException,
                        is IOException,
                        is NoSuchAlgorithmException,
                        is InvalidKeyException -> throw RuntimeException("Failed to init Cipher", e)
                        else -> throw e
                    }
                } else {
                    return false
                }
            }
        }


        private fun createBiometricPrompt(context: Context): BiometricPrompt {
            val executor = ContextCompat.getMainExecutor(context)

            val callback = @RequiresApi(Build.VERSION_CODES.P)
            object : BiometricPrompt.AuthenticationCallback() {
                override fun onAuthenticationError(errorCode: Int, errString: CharSequence) {
                    super.onAuthenticationError(errorCode, errString)
                    if (::biomatricAuthencationListener.isInitialized) {
                        biomatricAuthencationListener.onAuthenticationError(errorCode)
                    }
//                if (errorCode == BiometricPrompt.ERROR_NEGATIVE_BUTTON) {
////                    loginWithPassword() // Because negative button says use application password
//                }
                }

                override fun onAuthenticationFailed() {
                    super.onAuthenticationFailed()
                    if (::biomatricAuthencationListener.isInitialized) {
                        biomatricAuthencationListener.onAuthenticationError(-1)
                    }
                }

                override fun onAuthenticationSucceeded(result: BiometricPrompt.AuthenticationResult) {
                    super.onAuthenticationSucceeded(result)
                    if (::biomatricAuthencationListener.isInitialized) {
                        biomatricAuthencationListener.onAuthenticationSucceeded()
                    }
//                Log.d(TAG, "Authentication was successful")
//                onPurchased(true, result.cryptoObject)
                }
            }

            return BiometricPrompt(context as FragmentActivity, executor, callback)

        }

        private fun createPromptInfo(context: Context): BiometricPrompt.PromptInfo {
            val promptInfo = BiometricPrompt.PromptInfo.Builder()
                .setTitle(context.getString(R.string.app_name))
                *//* .setSubtitle(getString(R.string.prompt_info_subtitle))
                 .setDescription(getString(R.string.prompt_info_description))*//*
                .setConfirmationRequired(false)
                .setNegativeButtonText(context.getString(R.string.prompt_info_use_app_password))
                // .setDeviceCredentialAllowed(true) // Allow PIN/pattern/password authentication.
                // Also note that setDeviceCredentialAllowed and setNegativeButtonText are
                // incompatible so that if you uncomment one you must comment out the other
                .build()
            return promptInfo
        }

        */
        /**
         * Sets up KeyStore and KeyGenerator
         *//*
        @RequiresApi(Build.VERSION_CODES.M)
        private fun setupKeyStoreAndKeyGenerator() {
            try {
                keyStore = KeyStore.getInstance(ANDROID_KEY_STORE)
            } catch (e: KeyStoreException) {
                throw RuntimeException("Failed to get an instance of KeyStore", e)
            }

            try {
                keyGenerator =
                    KeyGenerator.getInstance(KeyProperties.KEY_ALGORITHM_AES, ANDROID_KEY_STORE)
            } catch (e: Exception) {
                when (e) {
                    is NoSuchAlgorithmException,
                    is NoSuchProviderException ->
                        throw RuntimeException("Failed to get an instance of KeyGenerator", e)
                    else -> throw e
                }
            }
            createKey(DEFAULT_KEY, true)
        }


        @TargetApi(Build.VERSION_CODES.N)
        @RequiresApi(Build.VERSION_CODES.M)
        fun createKey(keyName: String, invalidatedByBiometricEnrollment: Boolean) {
            // The enrolling flow for fingerprint. This is where you ask the user to set up fingerprint
            // for your flow. Use of keys is necessary if you need to know if the set of enrolled
            // fingerprints has changed.
            try {
                keyStore.load(null)

                val keyProperties = KeyProperties.PURPOSE_ENCRYPT or KeyProperties.PURPOSE_DECRYPT
                val builder = KeyGenParameterSpec.Builder(keyName, keyProperties)
                    .setBlockModes(KeyProperties.BLOCK_MODE_CBC)
                    .setUserAuthenticationRequired(true)
                    .setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_PKCS7)
                    .setInvalidatedByBiometricEnrollment(invalidatedByBiometricEnrollment)

                keyGenerator.run {
                    init(builder.build())
                    generateKey()
                }
            } catch (e: Exception) {
                when (e) {
                    is NoSuchAlgorithmException,
                    is InvalidAlgorithmParameterException,
                    is CertificateException,
                    is IOException -> throw RuntimeException(e)
                    else -> throw e
                }
            }
        }

        */
        /**
         * Sets up default cipher and a non-invalidated cipher
         *//*
        private fun setupCiphers(): Cipher {
            val defaultCipher: Cipher

            try {
                val cipherString =
                    "${KeyProperties.KEY_ALGORITHM_AES}/${KeyProperties.BLOCK_MODE_CBC}/${KeyProperties.ENCRYPTION_PADDING_PKCS7}"
                defaultCipher = Cipher.getInstance(cipherString)
            } catch (e: Exception) {
                when (e) {
                    is NoSuchAlgorithmException,
                    is NoSuchPaddingException ->
                        throw RuntimeException("Failed to get an instance of Cipher", e)
                    else -> throw e
                }
            }
            return defaultCipher
        }*/
    }


}