package com.app.luipay

import android.content.Intent
import android.os.Bundle
import android.view.View
import com.app.luipay.loginuser.SignupActivity
import com.app.luipay.network.ApiAuthHelper
import com.app.luipay.network.ApiClient
import com.app.luipay.network.CallBackManager
import com.app.luipay.network.RetroError
import com.app.luipay.request.AppStatusRequest
import com.app.luipay.response.AppStatusResponse.AppStatusReponse
import com.app.luipay.signup.NewUserActivity
import com.app.luipay.util.*
import com.google.gson.GsonBuilder
import kotlinx.android.synthetic.main.activity_splash.*
import java.io.File
import java.io.FileNotFoundException
import java.io.FileWriter
import java.io.IOException

class SelectUserActivity : BaseActivity(), View.OnClickListener {



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        Prefutils.saveToPrefsInt(this, Prefutils.API_LANGUAGE, 1)
        Prefutils.saveToPrefsInt(this, Prefutils.COUNTRY_CODE_API, 1)
        Prefutils.saveToPrefs(this, Prefutils.COUNTRY_CODE, "+91")
        initListener()
    }

    private fun initListener() {
        txtNewUser.setOnClickListener(this)
        txtExistingUser.setOnClickListener(this)
        checkResponse()
    }

    private fun checkResponse() {

        val file = File("data/data/com.app.luipay/responsecode/rescode.text")
        if (!file.exists()) {
            getResponse()
        }
    }

    private fun getResponse() {
        showProcessDialog()
        val helper = ApiClient.getClient().create(ApiAuthHelper::class.java)
        val request = AppStatusRequest()
        request.LanguageTypeId = Appconstant.API_LANGUAGE
        request.ServiceAuthenticationToken = Appconstant.API_KEY
        var call = helper.getAppResponse(request, Appconstant.API_HEADER)
        call.enqueue(object : CallBackManager<AppStatusReponse>() {
            override fun onSuccess(any: AppStatusReponse?, message: String) {
                hideProcessDailog()
                val responseData = any as AppStatusReponse
                responseData.let {
                    createFile(GsonBuilder().disableHtmlEscaping().create().toJson(responseData))
                }
            }

            override fun onFailure(message: String) {
                hideProcessDailog()
            }

            override fun onError(error: RetroError) {
                hideProcessDailog()
            }

        })
    }

    private fun createFile(responseData: String) {
        var fos: FileWriter? = null
        var isDirectoryCreated = false
        try {
            val directory =
                File("data/data/com.app.luipay/responsecode")
            if (directory.exists()) {
                isDirectoryCreated = true
            } else {
                isDirectoryCreated = directory.mkdir()
            }
            if (isDirectoryCreated) {
                val file =
                    File("data/data/com.app.luipay/responsecode/rescode.text")
                if (file.createNewFile()) {
                    fos = FileWriter(file)
                    fos.write(responseData)
                }
            }
        } catch (fileNotFound: FileNotFoundException) {
        } catch (ioException: IOException) {
            ioException.printStackTrace()
        } finally {
            fos?.close()
        }
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.txtNewUser -> {
//                password = AESHelper.encrypt("ravi sharma","206DFECF-3331-4CA7-9386-DB5052CCC257")
//                CommonUtils.showLog("*****log", password)
                val intent = Intent(this@SelectUserActivity, NewUserActivity::class.java)
                startActivity(intent)
                finish()
            }
            R.id.txtExistingUser -> {
//                password = AESHelper.decrypt(password,"206DFECF-3331-4CA7-9386-DB5052CCC257")
//                CommonUtils.showLog("*****log", password)
                startActivity(Intent(this@SelectUserActivity, SignupActivity::class.java))
                finish()
            }
        }
    }
}
