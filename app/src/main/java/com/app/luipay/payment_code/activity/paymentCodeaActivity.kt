package com.app.luipay.payment_code.activity

import android.os.Bundle
import com.app.luipay.R
import com.app.luipay.util.BaseActivity
import kotlinx.android.synthetic.main.header_center_text_left_arrow.*

class PaymentCodeActivity : BaseActivity() {



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_payment_code)
        initheader()
    }

    private fun initheader() {
        imgBack.setOnClickListener { finish() }
        headerTitle.text=getString(R.string.payment)
    }

}