package com.app.luipay.request

import com.google.gson.annotations.SerializedName

class UpdatePassword(
    @SerializedName("LanguageTypeId")
    var LanguageTypeId: Int? = 0,
    @SerializedName("ServiceAuthenticationToken")
    var ServiceAuthenticationToken: String = "",
    @SerializedName("CountryCodeId")
    var CountryCodeId: Int = 0,
    @SerializedName("MobileNo")
    var MobileNo: String = "",
    @SerializedName("Password")
    var Password: String = "",
    @SerializedName("ConfirmPassword")
    var ConfirmPassword: String = "",
    @SerializedName("DeviceNotificationID")
    var DeviceNotificationID: String = ""
)