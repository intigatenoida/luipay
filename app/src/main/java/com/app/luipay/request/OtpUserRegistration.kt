package com.app.luipay.request

import com.google.gson.annotations.SerializedName

class OtpUserRegistration(
    @SerializedName("LanguageTypeId")
    var LanguageTypeId: Int? = 0,
    @SerializedName("ServiceAuthenticationToken")
    var ServiceAuthenticationToken: String = "",
    @SerializedName("CountryCodeId")
    var CountryCodeId: Int = 0,
    @SerializedName("MobileNo")
    var MobileNo: String = "",
    @SerializedName("Otp")
    var Otp: String = ""

)