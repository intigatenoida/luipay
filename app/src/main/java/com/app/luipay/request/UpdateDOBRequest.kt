package com.app.luipay.request

import com.google.gson.annotations.SerializedName

class UpdateDOBRequest (
    @SerializedName("LanguageTypeId")
    var LanguageTypeId: Int? = 0,
    @SerializedName("ServiceAuthenticationToken")
    var ServiceAuthenticationToken: String = "",
    @SerializedName("DateOfBirth")
    var DateOfBirth: String = ""
)