package com.app.luipay.request

import com.google.gson.annotations.SerializedName

class CountryCodeRequest(
    @SerializedName("LanguageTypeId")
    var LanguageTypeId: Int? = 0,
    @SerializedName("ServiceAuthenticationToken")
    var ServiceAuthenticationToken: String = "",
    @SerializedName("Flag")
    var Flag: String = "CountryCode"
)
