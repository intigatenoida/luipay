package com.app.luipay.request

import com.google.gson.annotations.SerializedName

class EmailRequest(
    @SerializedName("LanguageTypeId")
    var LanguageTypeId: Int? = 0,
    @SerializedName("ServiceAuthenticationToken")
    var ServiceAuthenticationToken: String = "",
    @SerializedName("EmailAddress")
    var EmailAddress: String = ""
)