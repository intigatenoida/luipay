package com.app.luipay.request

import com.google.gson.annotations.SerializedName

data class EmailNumberValidationRequest(
    @SerializedName("CountryCodeId")
    var CountryCodeId: Int? = 0,
    @SerializedName("LanguageTypeId")
    var LanguageTypeId: Int? = 0,
    @SerializedName("ServiceAuthenticationToken")
    var ServiceAuthenticationToken: String = "",
    @SerializedName("EmailId")
    var EmailId: String = "",
    @SerializedName("Flag")
    var Flag: String = "",
    @SerializedName("MobileNo")
    var MobileNo: String = ""
)