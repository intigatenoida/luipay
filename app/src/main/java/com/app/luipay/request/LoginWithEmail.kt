package com.app.luipay.request

import com.google.gson.annotations.SerializedName

class LoginWithEmail (
    @SerializedName("LanguageTypeId")
    var LanguageTypeId: Int? = 0,
    @SerializedName("ServiceAuthenticationToken")
    var ServiceAuthenticationToken: String = "",
    @SerializedName("CountryCodeId")
    var CountryCodeId: Int = 0,
    @SerializedName("EmailId")
    var EmailId: String = "",
    @SerializedName("Password")
    var Password: String = "",
    @SerializedName("DeviceNotificationID")
    var DeviceNotificationID: String = ""
)