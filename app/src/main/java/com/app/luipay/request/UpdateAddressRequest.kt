package com.app.luipay.request

import com.google.gson.annotations.SerializedName

class UpdateAddressRequest(
    @SerializedName("LanguageTypeId")
    var LanguageTypeId: Int? = 0,
    @SerializedName("ServiceAuthenticationToken")
    var ServiceAuthenticationToken: String = "",
    @SerializedName("Address")
    var Address: String = ""
)