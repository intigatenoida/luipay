package com.app.luipay.request

import com.google.gson.annotations.SerializedName

class UpdateName(
    @SerializedName("LanguageTypeId")
    var LanguageTypeId: Int? = 0,
    @SerializedName("ServiceAuthenticationToken")
    var ServiceAuthenticationToken: String = "",
    @SerializedName("CountryCodeId")
    var CountryCodeId: Int = 0,
    @SerializedName("FirstName")
    var FirstName: String = "",
    @SerializedName("MiddleName")
    var MiddleName: String = "",
    @SerializedName("LastName")
    var LastName: String = "",
    @SerializedName("MobileNo")
    var MobileNo: String = ""
)

