package com.app.luipay.request

import com.google.gson.annotations.SerializedName

class GetNotoficationListRequest
    (
    @SerializedName("LanguageTypeId")
    var LanguageTypeId: Int? = 0,
    @SerializedName("ServiceAuthenticationToken")
    var ServiceAuthenticationToken: String = "",
    @SerializedName("CurrentPage")
    var CurrentPage: Int = 0,
    @SerializedName("RecordsPerPage")
    var RecordsPerPage: Int = 0

)