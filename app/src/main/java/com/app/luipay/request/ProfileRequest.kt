package com.app.luipay.request

import com.google.gson.annotations.SerializedName

class ProfileRequest(
    @SerializedName("LanguageTypeId")
    var LanguageTypeId: Int? = 0,
    @SerializedName("ServiceAuthenticationToken")
    var ServiceAuthenticationToken: String = ""
)