package com.app.luipay.request

import com.google.gson.annotations.SerializedName

class UpdateUserIdentificationRequest(
    @SerializedName("LanguageTypeId")
    var LanguageTypeId: Int? = 0,
    @SerializedName("ServiceAuthenticationToken")
    var ServiceAuthenticationToken: String = "",
    @SerializedName("GenderId")
    var GenderId: Int = 0
)