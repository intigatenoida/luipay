package com.app.luipay.signup

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.core.view.ViewCompat
import com.app.luipay.R
import com.app.luipay.network.ApiAuthHelper
import com.app.luipay.network.ApiClient
import com.app.luipay.network.CallBackManager
import com.app.luipay.network.RetroError
import com.app.luipay.request.EmailRequest
import com.app.luipay.response.BaseResponse
import com.app.luipay.util.Appconstant
import com.app.luipay.util.BaseActivity
import com.app.luipay.util.CommonUtils
import kotlinx.android.synthetic.main.activity_email.*
import kotlinx.android.synthetic.main.activity_password.fbNext
import kotlinx.android.synthetic.main.header_login.*

class EnterEmailAddress : BaseActivity(), View.OnFocusChangeListener {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_email)
        tvQuestion.text = getString(R.string.whats_ur_email)
        tvHint.visibility = View.GONE

        edEmail.setOnFocusChangeListener(this)
        fbNext.setOnClickListener {
            if (isValidate()) {
                updateEmail()
            }
        }
        ivBackArrow.setOnClickListener {
            onBackPressed()
        }
    }

    private fun updateEmail() {
        if (!isOnline()){
            CommonUtils.showToast(this,getString(R.string.no_internet_connection))
            return
        }
        showProcessDialog()
        val helper = ApiClient.getClient(this@EnterEmailAddress).create(ApiAuthHelper::class.java)
        val otpUserRegistration = EmailRequest()
//        otpUserRegistration.CountryCodeId =
//            Prefutils.getFromPrefsInt(this@EnterEmailAddress, Prefutils.COUNTRY_CODE)
        otpUserRegistration.LanguageTypeId = Appconstant.API_LANGUAGE
        otpUserRegistration.ServiceAuthenticationToken = Appconstant.API_KEY
        otpUserRegistration.EmailAddress = edEmail.text.toString().trim()
        val call = helper.updateEmailAddress(otpUserRegistration, Appconstant.API_HEADER)
        call.enqueue(object : CallBackManager<BaseResponse>() {
            override fun onSuccess(any: BaseResponse?, message: String) {
                hideProcessDailog()
                val intent = Intent(this@EnterEmailAddress, SelectGenderActivity::class.java)
                startActivityForResult(intent, Appconstant.common_request_code)
            }

            override fun onFailure(message: String) {
                hideProcessDailog()
                CommonUtils.showToast(this@EnterEmailAddress, message)
            }

            override fun onError(error: RetroError) {
                hideProcessDailog()
            }
        })
    }

    private fun isValidate(): Boolean {
        if (!CommonUtils.emailValidator(edEmail.text.toString().trim())) {
            CommonUtils.showToast(this@EnterEmailAddress, getString(R.string.enter_ur_email))
            return false
        }
        return true
    }

    override fun onFocusChange(v: View?, hasFocus: Boolean) {

        if (v == edEmail) {
            if (hasFocus) {
                ViewCompat.setElevation(edEmail, 24f)
            } else {
                ViewCompat.setElevation(edEmail, 0f)
            }
        }


    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == Appconstant.common_request_code && resultCode == Activity.RESULT_OK) {
            setResult(Activity.RESULT_OK)
            finish()
        }
    }
}