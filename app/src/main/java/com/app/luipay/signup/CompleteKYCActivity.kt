package com.app.luipay.signup

import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.text.Spannable
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.view.LayoutInflater
import android.widget.LinearLayout
import android.widget.RadioButton
import androidx.core.content.ContextCompat
import com.app.luipay.R
import com.app.luipay.network.ApiAuthHelper
import com.app.luipay.network.ApiClient
import com.app.luipay.network.CallBackManager
import com.app.luipay.network.RetroError
import com.app.luipay.profile.dialog_fragment.ChangeImageDialog
import com.app.luipay.request.CountryCodeRequest
import com.app.luipay.response.BaseResponse
import com.app.luipay.response.kyc_list_response.KYCListResponse
import com.app.luipay.response.kyc_list_response.ResponseCollection
import com.app.luipay.util.*
import kotlinx.android.synthetic.main.activity_complete_kyc.*
import kotlinx.android.synthetic.main.header_login.*
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import java.io.File
import java.lang.Exception


class CompleteKYCActivity : BaseActivity(), ChangeImageDialog.OnImageSelectionListener {

    private lateinit var kycList: List<ResponseCollection>
    val SELECT_IMAGE = 100
    val PICK_PDF_CODE = 101

    var selectedPdf: Uri? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_complete_kyc)
        tvQuestion.text = getString(R.string.complete_kyc)
        tvHint.text = getString(R.string.kyc_hint)
        val spannable = SpannableString(getString(R.string.term_condition))
        spannable.setSpan(
            ForegroundColorSpan(Color.parseColor("#3DBCFE")),
            spannable.length - 21, spannable.length,
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )

        tvTermsCondition.text = spannable

        llSlide.setOnClickListener {
            uploadDocument()
        }

        ivBackArrow.setOnClickListener {
            onBackPressed()
        }

        getKycList()
        findAttachment.setOnClickListener {
            openImageSelectionDialog()
        }

        llSlide.setOnClickListener {
            if (isValidate()) {
                uploadDocument()

            }
        }
    }

    private fun isValidate(): Boolean {
        val checkedId = rg.checkedRadioButtonId
        if (checkedId == -1) {
            CommonUtils.showToast(
                this@CompleteKYCActivity,
                getString(R.string.select_document_type)
            )
            return false
        } else if (selectedPdf == null) {
            CommonUtils.showToast(this@CompleteKYCActivity, getString(R.string.select_document))
            return false
        }
        return true
    }

    private fun openImageSelectionDialog() {
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        val prev = supportFragmentManager.findFragmentByTag("dialog")
        if (prev != null) {
            fragmentTransaction.remove(prev)
        }
        fragmentTransaction.addToBackStack(null)
        val dialogFragment = ChangeImageDialog.getInstance(true)
        dialogFragment.show(fragmentTransaction, "dialog")
    }

    private fun getKycList() {
        if (!isOnline()){
            CommonUtils.showToast(this,getString(R.string.no_internet_connection))
            return
        }

        showProcessDialog()
        val helper = ApiClient.getClient().create(ApiAuthHelper::class.java)
        val countrycCode = CountryCodeRequest()
        countrycCode.LanguageTypeId = 1
        countrycCode.ServiceAuthenticationToken = Appconstant.API_KEY
        countrycCode.Flag = "IdentityProof"
        val call = helper.getKycList(countrycCode, Appconstant.API_HEADER)
        call.enqueue(object : CallBackManager<KYCListResponse>() {
            override fun onSuccess(any: KYCListResponse?, message: String) {
                hideProcessDailog()
                val response = any as KYCListResponse
                kycList = response.ResponseCollection
                updateRadioGroup(response.ResponseCollection)
            }

            override fun onFailure(message: String) {
                hideProcessDailog()
            }

            override fun onError(error: RetroError) {
                hideProcessDailog()
            }
        })
    }

    private fun updateRadioGroup(responseCollection: List<ResponseCollection>) {
        rg.removeAllViews()
        val inflator = LayoutInflater.from(this@CompleteKYCActivity)
        for (i in 0..responseCollection.size - 1) {
            val rb = inflator.inflate(
                R.layout.layout_item_radio_button,
                null,
                false
            ) as RadioButton
//            rb.layoutParams.height=30
            rb.id = responseCollection[i].Value
            rb.text = responseCollection[i].Text
            rb.buttonDrawable = ContextCompat.getDrawable(
                this@CompleteKYCActivity,
                R.drawable.selector_radio
            )

            val param = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 80)
            rg.addView(rb, param)
        }
    }

    private fun uploadDocument() {
        if (!isOnline()){
            CommonUtils.showToast(this,getString(R.string.no_internet_connection))
            return
        }
       try{
           showProcessDialog()
           val id = rg.checkedRadioButtonId
           val LanguageTypeId: RequestBody =
               "${Prefutils.getFromPrefsInt(this@CompleteKYCActivity, Prefutils.API_LANGUAGE)}"
                   .toRequestBody("multipart/form-data".toMediaTypeOrNull())
//
           val documentType: RequestBody =
               "$id"
                   .toRequestBody("multipart/form-data".toMediaTypeOrNull())

           val ServiceAuthenticationToken: RequestBody = Appconstant.API_KEY
               .toRequestBody("multipart/form-data".toMediaTypeOrNull())

           val file: File =FileUtils.from(this@CompleteKYCActivity,selectedPdf)
//           val file: File = File(selectedPdf!!.path)
           val requestFile: RequestBody = RequestBody.create(
               contentResolver.getType(selectedPdf!!)!!.toMediaTypeOrNull(),
               file
           )

           val body: MultipartBody.Part =
               MultipartBody.Part.createFormData("document", file.getName(), requestFile)

           val helper = ApiClient.getClient(this@CompleteKYCActivity).create(ApiAuthHelper::class.java)
           val call = helper.   uploadKyC(body, LanguageTypeId, documentType, ServiceAuthenticationToken)
           call.enqueue(object : CallBackManager<BaseResponse>() {
               override fun onSuccess(any: BaseResponse?, message: String) {
                   hideProcessDailog()
                   setResult(Activity.RESULT_OK)
                   finish()
               }

               override fun onFailure(message: String) {
                   hideProcessDailog()
               }

               override fun onError(error: RetroError) {
                   hideProcessDailog()
               }

           })
       }catch (e:Exception){
           CommonUtils.showLog("0000",e.localizedMessage)
           hideProcessDailog()
       }

    }


    override fun onBackPressed() {
        setResult(Activity.RESULT_CANCELED)
        finish()
    }

    fun openGallery() {
        val intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), SELECT_IMAGE)
    }

    override fun onGallerySelectionListener() {
        openGallery()
    }

    override fun onCameraSelectionListener() {
        if (EtakaPermissiom.checkAppPermission(
                this@CompleteKYCActivity,
                EtakaPermissiom.CAMERA_CODE
            )
        ) {
            openCamera()
        }
    }

    override fun onPDFSelectionListener() {
        val browseStorage = Intent(Intent.ACTION_GET_CONTENT)
        browseStorage.type = "application/pdf"
        browseStorage.addCategory(Intent.CATEGORY_OPENABLE)
        startActivityForResult(Intent.createChooser(browseStorage, "Select PDF"), PICK_PDF_CODE)
    }

    private fun openCamera() {
        /*open camera and capture pictuer code here*/
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if ((requestCode == EtakaPermissiom.CAMERA_CODE)) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                openCamera()
            } else {
                EtakaPermissiom.CheckForNeverAsk(this, EtakaPermissiom.CAMERA_CODE)
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == PICK_PDF_CODE && resultCode == RESULT_OK && data != null) {
            selectedPdf = data.data
            CommonUtils.showToast(this@CompleteKYCActivity, getString(R.string.pdf_file_selected))
        } else if (requestCode == SELECT_IMAGE && resultCode == RESULT_OK && data != null) {
            selectedPdf = data.data!!
            CommonUtils.showToast(this@CompleteKYCActivity, getString(R.string.image_selected))
        }
    }
}