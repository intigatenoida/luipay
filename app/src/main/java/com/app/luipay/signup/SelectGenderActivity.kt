package com.app.luipay.signup

import android.app.Activity
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.View
import androidx.core.view.ViewCompat
import com.app.luipay.R
import com.app.luipay.network.ApiAuthHelper
import com.app.luipay.network.ApiClient
import com.app.luipay.network.CallBackManager
import com.app.luipay.network.RetroError
import com.app.luipay.request.UpdateUserIdentificationRequest
import com.app.luipay.response.BaseResponse
import com.app.luipay.util.Appconstant
import com.app.luipay.util.BaseActivity
import com.app.luipay.util.CommonUtils
import kotlinx.android.synthetic.main.activity_idenitfy.*
import kotlinx.android.synthetic.main.activity_password.fbNext
import kotlinx.android.synthetic.main.header_login.*

class SelectGenderActivity : BaseActivity() {
    var selectGender = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_idenitfy)
        tvQuestion.text = getString(R.string.how_to_idenitfy)
        tvHint.visibility = View.GONE


        fbNext.setOnClickListener {
            if (!isOnline()) {
                CommonUtils.showToast(
                    this@SelectGenderActivity,
                    getString(R.string.no_internet_connection)
                )
            } else {
                showProcessDialog()
                val helper =
                    ApiClient.getClient(this@SelectGenderActivity).create(ApiAuthHelper::class.java)
                val otpUserRegistration = UpdateUserIdentificationRequest()
//        otpUserRegistration.CountryCodeId =
//            Prefutils.getFromPrefsInt(this@EnterEmailAddress, Prefutils.COUNTRY_CODE)
                otpUserRegistration.LanguageTypeId = Appconstant.API_LANGUAGE
                otpUserRegistration.ServiceAuthenticationToken = Appconstant.API_KEY
                otpUserRegistration.GenderId = selectGender
                val call =
                    helper.updateUserIdentification(otpUserRegistration, Appconstant.API_HEADER)
                call.enqueue(object : CallBackManager<BaseResponse>() {
                    override fun onSuccess(any: BaseResponse?, message: String) {
                        hideProcessDailog()
                        val intent =
                            Intent(this@SelectGenderActivity, CompleteKYCActivity::class.java)
                        startActivityForResult(intent, Appconstant.common_request_code)
                    }

                    override fun onFailure(message: String) {
                        CommonUtils.showToast(this@SelectGenderActivity, message)
                        hideProcessDailog()
                    }

                    override fun onError(error: RetroError) {
                        hideProcessDailog()
                    }
                })
            }
        }

        btnMale.setOnClickListener {
            selectGender = 1
            btnMale.setTextColor(Color.parseColor("#ffffff"))
            btnFemale.setTextColor(Color.parseColor("#333333"))
            btnMale.setBackgroundResource(R.drawable.rectangle_sky_blue)
            btnFemale.setBackgroundResource(R.drawable.rectangle_gray_border_30_dp)
            ViewCompat.setElevation(btnMale, 24f)
            ViewCompat.setElevation(btnFemale, 0f)
        }

        btnFemale.setOnClickListener {
            selectGender = 2
            btnMale.setTextColor(Color.parseColor("#333333"))
            btnFemale.setTextColor(Color.parseColor("#ffffff"))
            btnMale.setBackgroundResource(R.drawable.rectangle_gray_border_30_dp)
            btnFemale.setBackgroundResource(R.drawable.rectangle_sky_blue)
            ViewCompat.setElevation(btnFemale, 24f)
            ViewCompat.setElevation(btnMale, 0f)
        }
        ivBackArrow.setOnClickListener {
            onBackPressed()
        }
        btnMale.performClick()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == Appconstant.common_request_code && resultCode == Activity.RESULT_OK) {
            setResult(Activity.RESULT_OK)
            finish()
        }
    }


}