package com.app.luipay.signup

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import androidx.core.view.ViewCompat
import com.app.luipay.R
import com.app.luipay.network.ApiAuthHelper
import com.app.luipay.network.ApiClient
import com.app.luipay.network.CallBackManager
import com.app.luipay.network.RetroError
import com.app.luipay.request.UpdatePassword
import com.app.luipay.response.passwordResponse.CreatePasswordResponse
import com.app.luipay.util.*
import kotlinx.android.synthetic.main.activity_password.*
import kotlinx.android.synthetic.main.header_login.*

class CreatePasswordActivity : BaseActivity(), View.OnFocusChangeListener {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_password)
        tvQuestion.text = getString(R.string.create_password)
        tvHint.visibility = View.GONE

        edConfirmPassword.setOnFocusChangeListener(this)
        edPassword.setOnFocusChangeListener(this)
        fbNext.setOnClickListener {
            if (isValidate()) {
                updatePassword()
            }
        }
        ivBackArrow.setOnClickListener {
            onBackPressed()
        }
    }

    private fun updatePassword() {
        if (!isOnline()){
            CommonUtils.showToast(this,getString(R.string.no_internet_connection))
            return
        }

        showProcessDialog()
        val helper = ApiClient.getClient().create(ApiAuthHelper::class.java)
        val request = UpdatePassword()
        request.CountryCodeId =
            Prefutils.getFromPrefsInt(this@CreatePasswordActivity, Prefutils.COUNTRY_CODE_API)
        request.LanguageTypeId = Appconstant.API_LANGUAGE
        request.ServiceAuthenticationToken = Appconstant.API_KEY
        request.Password = CustomPassword.encrypt(edPassword.text.toString().trim())
        request.ConfirmPassword = CustomPassword.encrypt(edConfirmPassword.text.toString().trim())
        request.MobileNo =
            Prefutils.getFromPrefs(this@CreatePasswordActivity, Prefutils.MOBILE)!!
        request.DeviceNotificationID = "abcd1234"
        val call = helper.updatePassword(request, Appconstant.API_HEADER)
        call.enqueue(object : CallBackManager<CreatePasswordResponse>() {
            override fun onSuccess(any: CreatePasswordResponse?, message: String) {
                hideProcessDailog()
                val response = any as CreatePasswordResponse
                if ((response != null) and (response.Response != null) and (!TextUtils.isEmpty(
                        response.Response.AccessToken.access_token
                    ))
                ) {
                    Prefutils.saveToPrefs(
                        this@CreatePasswordActivity,
                        Prefutils.ACCESS_TOKEN,
                        response.Response.AccessToken.access_token
                    )
                }
                CommonUtils.savePassword(this@CreatePasswordActivity, request.Password)

                val intent = Intent(
                    this@CreatePasswordActivity,
                    EnterEmailAddress::class.java
                )
                startActivityForResult(intent, Appconstant.common_request_code)
            }

            override fun onFailure(message: String) {
                hideProcessDailog()
                CommonUtils.showToast(this@CreatePasswordActivity, message)
            }

            override fun onError(error: RetroError) {
                hideProcessDailog()
            }

        })
    }

    private fun isValidate(): Boolean {
        if (edPassword.text!!.isEmpty()) {
            CommonUtils.showToast(
                this@CreatePasswordActivity,
                getString(R.string.enter_ur_password)
            )
            return false
        } else if (edConfirmPassword.text!!.isEmpty()) {
            CommonUtils.showToast(
                this@CreatePasswordActivity,
                getString(R.string.enter_ur_confirm_password)
            )
            return false
        } else if (edPassword.text!!.toString().trim().length < 6) {
            CommonUtils.showToast(this@CreatePasswordActivity, getString(R.string.password_match))
            return false
        } else if (edConfirmPassword.text!!.toString().trim().length < 6) {
            CommonUtils.showToast(
                this@CreatePasswordActivity,
                getString(R.string.confirm_password_length)
            )
            return false
        } else if (!CommonUtils.checkPasswordPattern(edPassword.text!!.toString())) {
            CommonUtils.showToast(this@CreatePasswordActivity, getString(R.string.password_pattern))
            return false
        } else if (!CommonUtils.checkPasswordPattern(edConfirmPassword.text!!.toString())) {
            CommonUtils.showToast(
                this@CreatePasswordActivity,
                getString(R.string.confirm_password_pattern)
            )
            return false
        } else if (!edPassword.text!!.toString().equals(edConfirmPassword.text!!.toString())) {
            CommonUtils.showToast(this@CreatePasswordActivity, getString(R.string.password_match))
            return false
        }
        return true

    }

    override fun onFocusChange(v: View?, hasFocus: Boolean) {

        if (v == edPassword) {
            if (hasFocus) {
                ViewCompat.setElevation(edPassword, 24f)
            } else {
                ViewCompat.setElevation(edPassword, 0f)
            }
        }

        if (v == edConfirmPassword) {
            if (hasFocus) {
                ViewCompat.setElevation(edConfirmPassword, 24f)
            } else {
                ViewCompat.setElevation(edConfirmPassword, 0f)
            }
        }


    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == Appconstant.common_request_code && resultCode == Activity.RESULT_OK) {
            setResult(Activity.RESULT_OK)
            finish()
        }
    }

}