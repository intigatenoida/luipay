package com.app.luipay.signup;

public class UserInfoData {
    private String mobileNo,firstName,LastName,password,email,otp,gender;

    public static UserInfoData getInfoData() {
        return infoData;
    }

    public static void setInfoData(UserInfoData infoData) {
        UserInfoData.infoData = infoData;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    private static UserInfoData infoData;

    private UserInfoData(){

    }
    public static UserInfoData getObject(){
        if(infoData==null){
            infoData=new UserInfoData();
        }
        return infoData;
    }

}
