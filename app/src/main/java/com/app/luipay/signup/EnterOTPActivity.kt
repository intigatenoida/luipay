package com.app.luipay.signup

import android.app.Activity
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.graphics.Color
import android.os.Bundle
import android.os.CountDownTimer
import android.text.Spannable
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import androidx.core.content.ContextCompat
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.app.luipay.R
import com.app.luipay.network.ApiAuthHelper
import com.app.luipay.network.ApiClient
import com.app.luipay.network.CallBackManager
import com.app.luipay.network.RetroError
import com.app.luipay.request.OtpUserRegistration
import com.app.luipay.request.ResendOtpSignUp
import com.app.luipay.response.BaseResponse
import com.app.luipay.sms_receiver.MySMSBroadcastReceiver
import com.app.luipay.util.Appconstant
import com.app.luipay.util.BaseActivity
import com.app.luipay.util.CommonUtils
import com.app.luipay.util.Prefutils
import com.app.luipay.util.otpview.OnOtpCompletionListener
import com.google.android.gms.auth.api.phone.SmsRetriever
import kotlinx.android.synthetic.main.activity_enter_pass_code.*
import kotlinx.android.synthetic.main.header_login.*

class EnterOTPActivity : BaseActivity(), OnOtpCompletionListener {

    var countTimer: CountDownTimer? = null
    private var mobileNo: String = ""
    private var otpValue: String = ""
    private val time = 600000
    var lastTime:Long=0


    override fun onOtpCompleted(otp: String?) {
        otpValue = otp!!
        UserInfoData.getObject().otp = otp
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_enter_pass_code)

        getIntentData()
        startSmsListener()
        intializeBroadCastReceiver()
        otpView.setOtpCompletionListener(this);

        tvQuestion.text = getString(R.string.whats_otp)

        val spannable = SpannableString(
            getString(R.string.hint_code) + " ${Prefutils.getFromPrefsInt(
                this@EnterOTPActivity,
                Prefutils.COUNTRY_CODE_API
            )} $mobileNo"
        )
        spannable.setSpan(
            ForegroundColorSpan(Color.BLACK),
            22, spannable.length,
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )
        tvHint.text = spannable

        fbNext.setOnClickListener {
            if (!otpValue.isEmpty()) {
                updatePassword()
            } else {
                CommonUtils.showToast(this@EnterOTPActivity, getString(R.string.enter_otp))
            }
        }
        ivBackArrow.setOnClickListener {
            onBackPressed()
        }


        resendButton.setOnClickListener {
            resentOtp()
        }
    }

    override fun onStart() {
        super.onStart()
        startTimer()
    }

    private fun resentOtp() {
        if (!isOnline()){
            CommonUtils.showToast(this,getString(R.string.no_internet_connection))
            return
        }
        showProcessDialog()
        val helper = ApiClient.getClient().create(ApiAuthHelper::class.java)
        val otpUserRegistration = ResendOtpSignUp()
        otpUserRegistration.CountryCodeId =
            Prefutils.getFromPrefsInt(this@EnterOTPActivity, Prefutils.COUNTRY_CODE_API)
        otpUserRegistration.LanguageTypeId = Appconstant.API_LANGUAGE
        otpUserRegistration.ServiceAuthenticationToken = Appconstant.API_KEY
        otpUserRegistration.MobileNo = intent!!.getStringExtra(Appconstant.mobile)!!
        val call = helper.resendOtpSignUp(otpUserRegistration, Appconstant.API_HEADER)
        call.enqueue(object : CallBackManager<BaseResponse>() {
            override fun onSuccess(any: BaseResponse?, message: String) {
                hideProcessDailog()
                startTimer()
            }

            override fun onFailure(message: String) {
                hideProcessDailog()
                CommonUtils.showToast(this@EnterOTPActivity, message)
            }

            override fun onError(error: RetroError) {
                hideProcessDailog()
            }

        })
    }

    private fun startTimer() {
        resendButton.setTextColor(Color.parseColor("#333333"))
        resendButton.background =
            ContextCompat.getDrawable(this, R.drawable.rectangle_gray)
        resendButton.isEnabled = false
        countTimer = object : CountDownTimer((time).toLong(), 1000) {
            override fun onFinish() {

                resendButton.setTextColor(Color.parseColor("#092058"))
                resendButton.background =
                    ContextCompat.getDrawable(
                        this@EnterOTPActivity,
                        R.drawable.rectangle_white_back_theme_border
                    )
                resendButton.isEnabled = true
            }

            override fun onTick(millisUntilFinished: Long) {
                lastTime=millisUntilFinished
                timer.text = CommonUtils.convertMiliIntoMinuteSeconds(millisUntilFinished)
                CommonUtils.showLog(timer.text.toString())
            }

        }.start()

    }

    private fun updatePassword() {
        if (!isOnline()){
            CommonUtils.showToast(this,getString(R.string.no_internet_connection))
            return
        }
        showProcessDialog()
        val helper = ApiClient.getClient().create(ApiAuthHelper::class.java)
        val otpUserRegistration = OtpUserRegistration()
        otpUserRegistration.CountryCodeId =
            Prefutils.getFromPrefsInt(this@EnterOTPActivity, Prefutils.COUNTRY_CODE_API)
        otpUserRegistration.LanguageTypeId = Appconstant.API_LANGUAGE
        otpUserRegistration.ServiceAuthenticationToken = Appconstant.API_KEY
        otpUserRegistration.MobileNo = intent!!.getStringExtra(Appconstant.mobile)!!
        otpUserRegistration.Otp = otpValue
        val call = helper.checkOTPUserRegistration(otpUserRegistration, Appconstant.API_HEADER)
        call.enqueue(object : CallBackManager<BaseResponse>() {
            override fun onSuccess(any: BaseResponse?, message: String) {
                hideProcessDailog()
                Prefutils.saveToPrefs(
                    this@EnterOTPActivity,
                    Prefutils.MOBILE,
                    intent!!.getStringExtra(Appconstant.mobile)!!
                )
                val intent = Intent(this@EnterOTPActivity, EnterYourName::class.java)
                startActivityForResult(intent, Appconstant.common_request_code)
            }

            override fun onFailure(message: String) {
                hideProcessDailog()
                CommonUtils.showToast(this@EnterOTPActivity, message)
            }

            override fun onError(error: RetroError) {
                hideProcessDailog()
            }

        })
    }

    private fun getIntentData() {
        mobileNo = intent.getStringExtra(Appconstant.mobile)!!
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == Appconstant.common_request_code && resultCode == Activity.RESULT_OK) {
            setResult(Activity.RESULT_OK)
            finish()
        }
    }

    private fun intializeBroadCastReceiver() {
        val lbm = LocalBroadcastManager.getInstance(this)
        val broadcastReceiverReadOTP = object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent) {
                val action = intent.action
                if (action != null && action == "SEND_MESSAGE") {
                    val messageText =
                        intent.getStringExtra(Appconstant.key_message_body)
                    messageText?.let {
                        if (messageText.contains("OneAD")) {
//                        otp = messageText.split(" ").toTypedArray()[0]
//                        edOTP.setText(CommonUtils.parseData(otp))
                        }
                    }
                }
            }
        }
        val intent = IntentFilter()
        intent.addAction("SEND_MESSAGE")
        val smsReceiverNew = MySMSBroadcastReceiver()
        val intentFilter = IntentFilter()
        intentFilter.addAction(SmsRetriever.SMS_RETRIEVED_ACTION)
        lbm.registerReceiver(smsReceiverNew, intentFilter)
        lbm.registerReceiver(broadcastReceiverReadOTP, intent)
    }


    private fun startSmsListener() {
        val client = SmsRetriever.getClient(this /* context */)
        val task = client.startSmsRetriever()
        // Listen for success/failure of the start Task. If in a background thread, this
        // can be made blocking using Tasks.await(task, [timeout]);
        task.addOnSuccessListener {
            // Successfully started retriever, expect broadcast intent
            // ...

        }

        task.addOnFailureListener {
            // Failed to start retriever, inspect Exception for more details
            // ...

        }
    }

    override fun onStop() {
        super.onStop()
        if (countTimer != null) {
            countTimer!!.onFinish()
            countTimer!!.cancel()
            countTimer = null
        }
    }

}