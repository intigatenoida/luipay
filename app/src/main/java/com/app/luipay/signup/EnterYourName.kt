package com.app.luipay.signup

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.core.view.ViewCompat
import com.app.luipay.R
import com.app.luipay.network.ApiAuthHelper
import com.app.luipay.network.ApiClient
import com.app.luipay.network.CallBackManager
import com.app.luipay.network.RetroError
import com.app.luipay.request.UpdateName
import com.app.luipay.response.BaseResponse
import com.app.luipay.util.Appconstant
import com.app.luipay.util.BaseActivity
import com.app.luipay.util.CommonUtils
import com.app.luipay.util.Prefutils
import kotlinx.android.synthetic.main.activity_ur_name.*
import kotlinx.android.synthetic.main.header_login.*

class EnterYourName : BaseActivity(), View.OnFocusChangeListener {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ur_name)
        tvQuestion.text = getString(R.string.ur_name)
        tvHint.visibility = View.GONE

        edFirstName.setOnFocusChangeListener(this)
        edLastName.setOnFocusChangeListener(this)

        fbNext.setOnClickListener {
            if (isValidate()) {
                updateName()
            }
        }
        ivBackArrow.setOnClickListener {
            onBackPressed()
        }
    }

    private fun updateName() {
        if (!isOnline()){
            CommonUtils.showToast(this,getString(R.string.no_internet_connection))
            return
        }
        showProcessDialog()
        val helper = ApiClient.getClient().create(ApiAuthHelper::class.java)
        val otpUserRegistration = UpdateName()
        otpUserRegistration.CountryCodeId =
            Prefutils.getFromPrefsInt(this@EnterYourName, Prefutils.COUNTRY_CODE_API)
        otpUserRegistration.LanguageTypeId = Appconstant.API_LANGUAGE
        otpUserRegistration.ServiceAuthenticationToken = Appconstant.API_KEY
        otpUserRegistration.FirstName = edFirstName.text.toString().trim()
        otpUserRegistration.LastName = edLastName.text.toString().trim()
        otpUserRegistration.MiddleName = ""
        otpUserRegistration.MobileNo =
            Prefutils.getFromPrefs(this@EnterYourName, Prefutils.MOBILE)!!
        val call = helper.updateName(otpUserRegistration, Appconstant.API_HEADER)
        call.enqueue(object : CallBackManager<BaseResponse>() {
            override fun onSuccess(any: BaseResponse?, message: String) {
                hideProcessDailog()
                val intent = Intent(this@EnterYourName, CreatePasswordActivity::class.java)
                startActivityForResult(intent, Appconstant.common_request_code)
            }

            override fun onFailure(message: String) {
                hideProcessDailog()
                CommonUtils.showToast(this@EnterYourName, message)
            }

            override fun onError(error: RetroError) {
                hideProcessDailog()
            }

        })
    }

    private fun isValidate(): Boolean {
        if (edFirstName.text!!.isEmpty()) {
            CommonUtils.showToast(this@EnterYourName, getString(R.string.enter_ur_name))
            return false
        } else if (edLastName.text!!.isEmpty()) {
            CommonUtils.showToast(this@EnterYourName, getString(R.string.enter_ur_last_name))
            return false
        }
        return true

    }

    override fun onFocusChange(v: View?, hasFocus: Boolean) {

        if (v == edFirstName) {
            if (hasFocus) {
                ViewCompat.setElevation(edFirstName, 24f)
            } else {
                ViewCompat.setElevation(edFirstName, 0f)
            }
        }

        if (v == edLastName) {
            if (hasFocus) {
                ViewCompat.setElevation(edLastName, 24f)
            } else {
                ViewCompat.setElevation(edLastName, 0f)
            }
        }


    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == Appconstant.common_request_code && resultCode == Activity.RESULT_OK) {
            setResult(Activity.RESULT_OK)
            finish()
        }
    }

}