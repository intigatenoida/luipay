package com.app.luipay.signup

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import com.app.luipay.R
import com.app.luipay.dashboard.ui.DashboardActivity
import com.app.luipay.loginuser.LoginSelectionActivity
import com.app.luipay.util.Appconstant
import com.app.luipay.util.BaseActivity
import com.app.luipay.util.CommonUtils
import kotlinx.android.synthetic.main.activity_new_user_start.*

class NewUserActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_user_start)
//        llSlide.setOnClickListener {
//            val  intent=Intent(this@NewUserActivity,EnterMobileActivity::class.java)
//            startActivityForResult(intent, Appconstant.common_request_code)
//        }

        tvLoginHere.setOnClickListener {
            val  intent=Intent(this@NewUserActivity,LoginSelectionActivity::class.java)
            startActivity(intent)
            finish()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode==Appconstant.common_request_code && resultCode== Activity.RESULT_OK){
            val userInfo=UserInfoData.getObject().mobileNo
            CommonUtils.showToast(this@NewUserActivity,userInfo)
            UserInfoData.setInfoData(null)
            val  intent=Intent(this@NewUserActivity,DashboardActivity::class.java)
            startActivity(intent)
            finish()
        }
    }
}