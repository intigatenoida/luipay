package com.app.luipay.signup

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import com.app.luipay.R
import com.app.luipay.network.ApiAuthHelper
import com.app.luipay.network.ApiClient
import com.app.luipay.network.CallBackManager
import com.app.luipay.network.RetroError
import com.app.luipay.request.CountryCodeRequest
import com.app.luipay.request.UserRegistrationRequest
import com.app.luipay.response.AppStatusResponse.country_code.CountryCodeResponse
import com.app.luipay.response.BaseResponse
import com.app.luipay.util.Appconstant
import com.app.luipay.util.BaseActivity
import com.app.luipay.util.CommonUtils
import com.app.luipay.util.Prefutils
import kotlinx.android.synthetic.main.activity_enter_mobile.*
import kotlinx.android.synthetic.main.header_login.*

class EnterMobileActivity : BaseActivity(), ConfirmMobileNumberDialog.dialogClickListener {


    override fun onClickPositive(mobile: String) {
        userRegistration(mobile)

    }

    private fun userRegistration(mobile: String) {
        if (!isOnline()) {
            CommonUtils.showToast(
                this@EnterMobileActivity,
                getString(R.string.no_internet_connection)
            )
            return
        }
        showProcessDialog()
        val helper = ApiClient.getClient().create(ApiAuthHelper::class.java)
        val userRegistration = UserRegistrationRequest()
        userRegistration.CountryCodeId =
            Prefutils.getFromPrefsInt(this@EnterMobileActivity, Prefutils.COUNTRY_CODE_API)
        userRegistration.ServiceAuthenticationToken = Appconstant.API_KEY
        userRegistration.LanguageTypeId = Appconstant.API_LANGUAGE
        userRegistration.MobileNo = mobile
        val call = helper.getuserRegistration(userRegistration, Appconstant.API_HEADER)
        call.enqueue(object : CallBackManager<BaseResponse>() {
            override fun onSuccess(any: BaseResponse?, message: String) {

                val intent = Intent(this@EnterMobileActivity, EnterOTPActivity::class.java)
                intent.putExtra(Appconstant.mobile, mobile)
                startActivityForResult(intent, Appconstant.common_request_code)
                hideProcessDailog()
            }

            override fun onFailure(message: String) {
                hideProcessDailog()
                CommonUtils.showToast(this@EnterMobileActivity, message)
            }

            override fun onError(error: RetroError) {
                hideProcessDailog()
            }

        })

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_enter_mobile)
        fbNext.setOnClickListener {
            if (edMobileNo.text.toString().isEmpty()) {
                CommonUtils.showToast(this@EnterMobileActivity, getString(R.string.enter_mobile_no))
                return@setOnClickListener
            }

            confirmYourMobile()
        }
        ivBackArrow.setOnClickListener {
            onBackPressed()
        }

        getCountryCode()
    }

    private fun getCountryCode() {
        showProcessDialog()
        val helper = ApiClient.getClient().create(ApiAuthHelper::class.java)
        val countrycCode = CountryCodeRequest()
        countrycCode.LanguageTypeId = 1
        countrycCode.ServiceAuthenticationToken = Appconstant.API_KEY
        val call = helper.getCountryCode(countrycCode, Appconstant.API_HEADER)
        call.enqueue(object : CallBackManager<CountryCodeResponse>() {
            override fun onSuccess(any: CountryCodeResponse?, message: String) {
                hideProcessDailog()
                val response = any as CountryCodeResponse

                response.ResponseCollection.let {
                    val data = it[0]
                    countryCode.text = data.Text
                    Prefutils.saveToPrefsInt(
                        this@EnterMobileActivity,
                        Prefutils.COUNTRY_CODE_API,
                        data.Value

                    )

                    Prefutils.saveToPrefs(
                        this@EnterMobileActivity,
                        Prefutils.COUNTRY_CODE,
                        data.Text

                    )


                }
            }

            override fun onFailure(message: String) {
                hideProcessDailog()
                CommonUtils.showToast(this@EnterMobileActivity, message)
            }

            override fun onError(error: RetroError) {
                hideProcessDailog()
            }

        })
    }

    private fun confirmYourMobile() {
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        val prev = supportFragmentManager.findFragmentByTag("dialog")
        if (prev != null) {
            fragmentTransaction.remove(prev)
        }
        fragmentTransaction.addToBackStack(null)
        val dialogFragment = ConfirmMobileNumberDialog.getInstance(
            edMobileNo.text.toString(),
            countryCode.text.toString().trim()
        )
        dialogFragment.show(fragmentTransaction, "dialog")
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == Appconstant.common_request_code && resultCode == Activity.RESULT_OK) {
            setResult(Activity.RESULT_OK)
            finish()
        }
    }


}

