package com.app.luipay.notification.activity

import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.luipay.R
import com.app.luipay.network.ApiAuthHelper
import com.app.luipay.network.ApiClient
import com.app.luipay.network.CallBackManager
import com.app.luipay.network.RetroError
import com.app.luipay.request.GetNotoficationListRequest
import com.app.luipay.response.BaseResponse
import com.app.luipay.util.*
import kotlinx.android.synthetic.main.activity_notification.*
import kotlinx.android.synthetic.main.header_white_center_text_image.*

class NotificationActivity : BaseActivity(), View.OnClickListener {

    private var pageCount = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_notification)

        initHeader()
        initRecyclerView()
    }

    private fun initRecyclerView() {
        val layoutManager = LinearLayoutManager(this)
        val verticalDecoration = DividerItemDecoration(
            this,
            DividerItemDecoration.VERTICAL
        )
        val verticalDivider =
            ContextCompat.getDrawable(this, R.drawable.divider_10)
        verticalDecoration.setDrawable(verticalDivider!!)
        notificationList.addItemDecoration(verticalDecoration)
        val scrollListener = EtakaCustomLoadMoreListener(layoutManager as LinearLayoutManager, 20)
        scrollListener.setOnLoadMoreListener(object : EtakaCustomLoadMoreListener.OnLoadMoreListener {
            override fun onLoadMore() {
                getNotificationList(++pageCount)
            }
        })
        notificationList.addOnScrollListener(scrollListener)
//        notificationList.adapter = NotificationAdapter(this)
        getNotificationList(pageCount)
    }

    private fun getNotificationList(pageNumber: Int) {

        if (!isOnline()) {
            CommonUtils.showToast(
                this@NotificationActivity,
                getString(R.string.no_internet_connection)
            )
            return
        }

        showProcessDialog()
        val helper =
            ApiClient.getClient(this@NotificationActivity).create(ApiAuthHelper::class.java)
        val request = GetNotoficationListRequest();
        request.CurrentPage = pageNumber
        request.LanguageTypeId =
            Prefutils.getFromPrefsInt(this@NotificationActivity, Prefutils.API_LANGUAGE)
        request.ServiceAuthenticationToken = Appconstant.API_KEY
        request.RecordsPerPage = 20
        val call = helper.getNotificationList(request, Appconstant.API_HEADER)
        call.enqueue(object : CallBackManager<BaseResponse>() {
            override fun onSuccess(any: BaseResponse?, message: String) {
                hideProcessDailog()
            }

            override fun onFailure(message: String) {
                hideProcessDailog()
                txtNotificartionError.text = message
                txtNotificartionError.visibility = View.VISIBLE
                notificationList.visibility = View.GONE
            }

            override fun onError(error: RetroError) {
                hideProcessDailog()
            }

        })

    }

    private fun initHeader() {
        txtTitle.text = getString(R.string.notification)
        imgBack.setOnClickListener(this)
        imgFilter.setOnClickListener(this)
        imgFilter.visibility = View.VISIBLE
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.imgBack -> {
                onBackPressed()
            }
            R.id.imgFilter -> {

            }
        }
    }
}