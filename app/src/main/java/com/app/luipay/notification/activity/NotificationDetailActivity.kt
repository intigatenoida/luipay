package com.app.luipay.notification.activity

import android.os.Bundle
import android.view.View
import com.app.luipay.R
import com.app.luipay.util.BaseActivity
import kotlinx.android.synthetic.main.header_white_center_text_image.*

class NotificationDetailActivity : BaseActivity(), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_notification_detail)
        initHeader()
    }

    private fun initHeader() {
        txtTitle.text = getString(R.string.notification)
        imgBack.setOnClickListener(this)
        imgFilter.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.imgBack -> {
                onBackPressed()
            }
            R.id.imgFilter -> {

            }
        }
    }

}