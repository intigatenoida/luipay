package com.app.luipay.request_transfer.activity

import android.app.Activity
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.luipay.R
import com.app.luipay.request_transfer.adapter.EnterAmountAdapter
import com.app.luipay.transfer.model.ContactNumberModel
import com.app.luipay.util.BaseActivity
import kotlinx.android.synthetic.main.activity_request_money_amount.*
import kotlinx.android.synthetic.main.header_center_text_left_arrow.*


class EnterAmountRequestActivity : BaseActivity() {

    lateinit var selectedContactList: ArrayList<ContactNumberModel>
    lateinit var adapter: EnterAmountAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_request_money_amount)
        initListener()
        getIntentData()
    }

    private fun getIntentData() {
        selectedContactList =
            intent.extras!!.getParcelableArrayList<ContactNumberModel>("contact") as ArrayList<ContactNumberModel>

        adapter = EnterAmountAdapter(
            this@EnterAmountRequestActivity,
            selectedContactList, true
        )
        recyclerView.layoutManager = LinearLayoutManager(this)
        msgTitle.text = getString(R.string.people_are_required).replace(
            "#",
            "${selectedContactList.size}",
            false
        )

        val verticalDecoration = DividerItemDecoration(
            this,
            DividerItemDecoration.VERTICAL
        )
        val verticalDivider =
            ContextCompat.getDrawable(this, R.drawable.divider_16)
        verticalDecoration.setDrawable(verticalDivider!!)
        recyclerView.addItemDecoration(verticalDecoration)
        recyclerView.adapter = adapter
    }

    private fun initListener() {
        imgBack.setOnClickListener {
            onBackPressed()
        }
        headerTitle.text = getString(R.string.request_money_transfer)

        next.isEnabled = false
        edtAmount.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                changeNextButtonState(s.toString())
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

        })

        next.setOnClickListener() {
            var bundle = Bundle()
            bundle.putParcelableArrayList("contact", selectedContactList)
            bundle.putString("amount",edtAmount.text.toString())
            bundle.putString("msg",edtMessage.text.toString())
            val intent = Intent(
                this@EnterAmountRequestActivity,
                RequestMoneyTransferConfirmationActivity::class.java
            )
            intent.putExtras(bundle)
            startActivityForResult(intent,100)
        }
    }


    private fun changeNextButtonState(otp: String) {
        if ((otp.isEmpty())) {
            next.setTextColor(Color.parseColor("#AABDD5"))
            next.background =
                ContextCompat.getDrawable(this, R.drawable.rectangle_white_background_no_border)
            next.isEnabled = false
        } else {
            if ((otp.toInt() < 50) or (otp.toInt() > 10000)) {
                next.setTextColor(Color.parseColor("#AABDD5"))
                next.background =
                    ContextCompat.getDrawable(this, R.drawable.rectangle_white_background_no_border)
                next.isEnabled = false
                notifyContact("")
            } else {
                next.setTextColor(Color.parseColor("#092058"))
                next.background =
                    ContextCompat.getDrawable(this, R.drawable.rectangle_white_back_theme_border)
                next.isEnabled = true
                notifyContact(otp)
            }
        }
    }

    private fun notifyContact(amount: String) {
        if (::selectedContactList.isInitialized) {
            var amountStr = ""
            if (amount.isEmpty()) {
                amountStr = "tk"
            } else {
                val amountTemp = amount.toDouble() / selectedContactList.size
                amountStr = "tk ${String.format("%.2f", amountTemp)}"
            }
            for (i in 0..selectedContactList.size - 1) {
                selectedContactList[i].amount = amountStr
            }

            if (::adapter.isInitialized) {
                adapter.notifyDataSetChanged()
            }

        }
    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode==100 && resultCode== Activity.RESULT_OK){
            val resultIntent = Intent()
            setResult(Activity.RESULT_OK, resultIntent)
            finish()
        }
    }
}