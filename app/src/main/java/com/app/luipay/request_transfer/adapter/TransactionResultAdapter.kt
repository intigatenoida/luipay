package com.app.luipay.request_transfer.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.app.luipay.R
import com.app.luipay.transfer.model.ContactNumberModel
import kotlinx.android.synthetic.main.layout_item_success_result.view.*

class TransactionResultAdapter(val context: Context, val listData: ArrayList<ContactNumberModel>) :
    RecyclerView.Adapter<TransactionResultAdapter.ViewHolder>() {


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return TransactionResultAdapter.ViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.layout_item_success_result,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return listData.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        var contactData = listData[position]
        holder.itemView.name.text=contactData.name
        holder.itemView.amount.text=contactData.amount

    }

}