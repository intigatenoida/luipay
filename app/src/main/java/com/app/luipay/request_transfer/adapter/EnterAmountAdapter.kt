package com.app.luipay.request_transfer.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.app.luipay.R
import com.app.luipay.transfer.model.ContactNumberModel
import kotlinx.android.synthetic.main.layout_money_transfer.view.*

class EnterAmountAdapter(
    val context: Context,
    val dataList: ArrayList<ContactNumberModel>,
    val isEditShow: Boolean
) :
    RecyclerView.Adapter<EnterAmountAdapter.ViewHolder>() {


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return EnterAmountAdapter.ViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.layout_money_transfer,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return dataList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        var contact = dataList[position]
        holder.itemView.txtName.text = contact.name
        holder.itemView.txtNumber.text = contact.number
        holder.itemView.amount.text = contact.amount
        holder.itemView.txtAlphabet1.text = contact.name.toCharArray()[0].toString().toUpperCase()
        if (isEditShow) {
            holder.itemView.imgChecked.visibility = View.VISIBLE
        } else {
            holder.itemView.imgChecked.visibility = View.GONE
        }

    }
}