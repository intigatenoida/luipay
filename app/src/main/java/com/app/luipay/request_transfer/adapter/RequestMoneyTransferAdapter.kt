package com.app.luipay.request_transfer.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.app.luipay.R
import com.app.luipay.transfer.model.ContactNumberModel
import kotlinx.android.synthetic.main.layout_item_transfer_number.view.*
import java.util.*
import kotlin.collections.ArrayList

class RequestMoneyTransferAdapter(
    val context: Context,
    val listData: ArrayList<ContactNumberModel>,
    val listener: setOnNumberSelectListener
) :
    RecyclerView.Adapter<RequestMoneyTransferAdapter.ViewHolder>() {

    public interface setOnNumberSelectListener {
        public fun onNumberSelect(data: ContactNumberModel, position: Int)
    }


    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): RequestMoneyTransferAdapter.ViewHolder {
        return ViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.layout_item_transfer_number,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return listData.size
    }

    override fun onBindViewHolder(holder: RequestMoneyTransferAdapter.ViewHolder, position: Int) {
        val model = listData[position]
        holder.itemView.txtName.text = model.name
        holder.itemView.txtNumber.text = model.number
        if (model.isSelected) {
            holder.itemView.imgChecked.visibility = View.VISIBLE
        } else {
            holder.itemView.imgChecked.visibility = View.INVISIBLE
        }
        holder.itemView.txtAlphabet1.text =
            model.name.toCharArray()[0].toString().toUpperCase(Locale.getDefault())

        holder.itemView.container.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                listener.let {
                    listener.onNumberSelect(model, position)
                    model.isSelected = !model.isSelected
                }
            }
        })
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}