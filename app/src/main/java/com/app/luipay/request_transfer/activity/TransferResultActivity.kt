package com.app.luipay.request_transfer.activity

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.luipay.R
import com.app.luipay.request_transfer.adapter.TransactionResultAdapter
import com.app.luipay.transfer.model.ContactNumberModel
import com.app.luipay.util.BaseActivity
import kotlinx.android.synthetic.main.activity_transaction_result.*
import kotlinx.android.synthetic.main.header_center_text_left_arrow.*


class TransferResultActivity : BaseActivity() {

    lateinit var selectedContactList: ArrayList<ContactNumberModel>
    lateinit var adapter: TransactionResultAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_transaction_result)
        initHeader()
        initIntentData()
    }

    private fun initIntentData() {
        selectedContactList =
            intent.extras!!.getParcelableArrayList<ContactNumberModel>("contact") as ArrayList<ContactNumberModel>

        adapter = TransactionResultAdapter(
            this@TransferResultActivity,
            selectedContactList
        )
        recyclerSuccess.layoutManager = LinearLayoutManager(this)
        recyclerSuccess.adapter = adapter
    }

    private fun initHeader() {
        imgBack.visibility = View.GONE
        headerTitle.text = getString(R.string.request_money_transfer)
        next.setOnClickListener {
            val resultIntent = Intent()
            setResult(Activity.RESULT_OK, resultIntent)
            finish()
        }

    }

    override fun onBackPressed() {

    }
}