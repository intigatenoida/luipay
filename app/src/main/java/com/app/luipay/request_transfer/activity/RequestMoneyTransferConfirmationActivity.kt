package com.app.luipay.request_transfer.activity

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.luipay.R
import com.app.luipay.request_transfer.adapter.EnterAmountAdapter
import com.app.luipay.transfer.dailog.FingerprintDailog
import com.app.luipay.transfer.model.ContactNumberModel
import com.app.luipay.util.BaseActivity
import kotlinx.android.synthetic.main.activity_request_transfer_confirmation.*
import kotlinx.android.synthetic.main.header_center_text_left_arrow.*

class RequestMoneyTransferConfirmationActivity : BaseActivity(),FingerprintDailog.onCancelListener {

    lateinit var selectedContactList: ArrayList<ContactNumberModel>
    lateinit var adapter: EnterAmountAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_request_transfer_confirmation)
        initListener()
        getIntentData()
    }

    private fun getIntentData() {
        txtTotalamount.text = intent.extras!!.getString("amount")
        txtMessage.text = intent.extras!!.getString("msg")
        selectedContactList =
            intent.extras!!.getParcelableArrayList<ContactNumberModel>("contact") as ArrayList<ContactNumberModel>

        adapter = EnterAmountAdapter(
            this@RequestMoneyTransferConfirmationActivity,
            selectedContactList, false
        )
        recyclerView.layoutManager = LinearLayoutManager(this)
        txtPeopleRequired.text = getString(R.string.people_are_required).replace(
            "#",
            "${selectedContactList.size}",
            false
        )

        val verticalDecoration = DividerItemDecoration(
            this,
            DividerItemDecoration.VERTICAL
        )
        val verticalDivider =
            ContextCompat.getDrawable(this, R.drawable.divider_16)
        verticalDecoration.setDrawable(verticalDivider!!)
        recyclerView.addItemDecoration(verticalDecoration)
        recyclerView.adapter = adapter
    }

    private fun initListener() {
        headerTitle.text = getString(R.string.request_money_transfer)
        imgBack.setOnClickListener {
            onBackPressed()
        }
        next.setOnClickListener {
            openFingerprintDialog()

        }
    }

    private fun openResultActivity() {
        var bundle = Bundle()
        bundle.putParcelableArrayList("contact", selectedContactList)
        val intent = Intent(
            this@RequestMoneyTransferConfirmationActivity,
            TransferResultActivity::class.java
        )
        intent.putExtras(bundle)
        startActivityForResult(intent,100)
    }


    private fun openFingerprintDialog() {
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        val prev = supportFragmentManager.findFragmentByTag("dialog")
        if (prev != null) {
            fragmentTransaction.remove(prev)
        }
        fragmentTransaction.addToBackStack(null)
        val dialogFragment = FingerprintDailog.getInstance()
        dialogFragment.show(fragmentTransaction, "dialog")
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode==100 && resultCode== Activity.RESULT_OK){
            val resultIntent = Intent()
            setResult(Activity.RESULT_OK, resultIntent)
            finish()
        }
    }

    override fun onCancel() {
        openResultActivity()
    }

}