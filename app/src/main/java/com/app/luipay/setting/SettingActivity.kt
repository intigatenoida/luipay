package com.app.luipay.setting

import android.content.Intent
import android.os.Bundle
import android.view.View
import com.app.luipay.R
import com.app.luipay.loginuser.LoginSelectionActivity
import com.app.luipay.profile.activity.ChangePasswordActivity
import com.app.luipay.util.BaseActivity
import kotlinx.android.synthetic.main.activity_setting.*
import kotlinx.android.synthetic.main.header_center_text_left_arrow.*

class SettingActivity : BaseActivity(), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_setting)
        initHeader()

    }

    private fun initHeader() {
        imgBack.setOnClickListener { finish() }
        headerTitle.text = getString(R.string.settings)
        signout.setOnClickListener(this)
        changePasswordlayout.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.changePasswordlayout -> {
                startActivity(Intent(this@SettingActivity, ChangePasswordActivity::class.java))
            }
            R.id.signout -> {
                val intent = Intent(this@SettingActivity, LoginSelectionActivity::class.java)
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                startActivity(intent)
                finish()
            }
        }
    }
}