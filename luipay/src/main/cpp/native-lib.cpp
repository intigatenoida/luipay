//
// Created by ratnad on 8/10/2018.
//

#include <jni.h>
#include <string>
#include "sha512.h"
#include <unistd.h>
/*#include "function1990.h"*/
extern "C"
JNIEXPORT jstring
JNICALL
Java_com_app_luipay_network_SyncData_invokeNativeFunction(JNIEnv *env, jobject foo_obj, jstring jStr) {

    const jclass stringClass = env->GetObjectClass(jStr);
    const jmethodID getBytes = env->GetMethodID(stringClass, "getBytes", "(Ljava/lang/String;)[B");
    const jbyteArray stringJbytes = (jbyteArray) env->CallObjectMethod(jStr, getBytes, env->NewStringUTF("UTF-8"));

    size_t length = (size_t) env->GetArrayLength(stringJbytes);
    jbyte* pBytes = env->GetByteArrayElements(stringJbytes, NULL);

    std::string ret = std::string((char *)pBytes, length);

    env->ReleaseByteArrayElements(stringJbytes, pBytes, JNI_ABORT);

    env->DeleteLocalRef(stringJbytes);
    env->DeleteLocalRef(stringClass);

    return env->NewStringUTF(sha512(ret).c_str());

}




//extern "C"
//JNIEXPORT jstring
//JNICALL
//Java_com_app_etaka_util_CustomPassword_passwordProtectore(JNIEnv *env, jclass object){
//std::string hello = "206DFECF-3331-4CA7-9386-DB5052CCC257";
//return env->NewStringUTF(hello.c_str());
//}