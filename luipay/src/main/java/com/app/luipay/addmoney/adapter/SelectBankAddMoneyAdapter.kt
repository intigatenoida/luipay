package com.app.luipay.addmoney.adapter

import android.content.Context
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.app.luipay.R
import com.app.luipay.addmoney.SelectBankAddMoneyActivity
import com.app.luipay.response.BankListResponse.ResponseCollection
import com.app.luipay.utils.CommonUtils
import kotlinx.android.synthetic.main.bank_item_layout.view.*

class SelectBankAddMoneyAdapter(
    private val context: Context,
    private val bankList: ArrayList<ResponseCollection>,
    private val listener: SelectBankAddMoneyActivity.BankTypeListener
) : RecyclerView.Adapter<SelectBankAddMoneyAdapter.ItemViewHolder>() {


    class ItemViewHolder(view: View) : RecyclerView.ViewHolder(view)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ItemViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.bank_item_layout,
                parent,
                false
            )
        )

    override fun getItemCount(): Int {
        return bankList.size
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        val data = bankList[position]
        holder.itemView.bankNameTv.text = data.bankName
        holder.itemView.container.setOnClickListener {
            listener?.let {
                listener.onBankSelect(data.id, data.bankName, data.bankCode, data.bankImageUrl)
            }
        }
        if (position == itemCount - 1) {
            holder.itemView.divider.visibility = View.GONE
        } else {
            holder.itemView.divider.visibility = View.VISIBLE
        }

        if (!data.bankImageUrl.isNullOrEmpty()) {
            CommonUtils.loadImage(
                context,
                data.bankImageUrl,
                holder.itemView.bankLogoImg,
                R.drawable.ic_bank
            )
        } else {
            holder.itemView.bankLogoImg.setImageResource(R.drawable.ic_bank)
            holder.itemView.bankLogoImg.setBackgroundResource(R.drawable.white_circle)
            holder.itemView.bankLogoImg.setPaddingRelative(
                dpToPx(7),
                dpToPx(0),
                dpToPx(7),
                dpToPx(4)
            )
        }
    }

    private fun dpToPx(dp: Int): Int {
        val displayMetrics = context!!.resources.displayMetrics
        return Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT))
    }
}