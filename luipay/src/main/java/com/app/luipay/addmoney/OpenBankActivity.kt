package com.app.luipay.addmoney

import android.os.Bundle
import com.app.luipay.R
import com.app.luipay.base.BaseActivity
import kotlinx.android.synthetic.main.activity_add_money.*

class OpenBankActivity :BaseActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_open_bank)

        ivBack.setOnClickListener {
            finish()
        }

    }
}