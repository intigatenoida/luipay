package com.app.luipay.addmoney

import android.app.Activity
import android.os.Bundle
import com.app.luipay.R
import com.app.luipay.base.BaseActivity
import com.app.luipay.utils.Appconstant
import kotlinx.android.synthetic.main.activity_trnx_status.*
import kotlinx.android.synthetic.main.button_view.*
import kotlinx.android.synthetic.main.center_title_layout.*

class TranxStatusActivity : BaseActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_trnx_status)

        tvLogin.text = getString(R.string.main_screen)

        val amount = intent.getStringExtra("amount")

        tvAmount.text = amount

        tvLogin.setOnClickListener {
            onBackPressed()
        }

        var temp = intent?.getBooleanExtra(Appconstant.API_KEY, false) ?: false
        if (temp) {
            titleTv.text = "Money Transfer"
        } else {
            titleTv.text = getString(R.string.add_money)
        }


        backBtn.setOnClickListener {
            onBackPressed()
        }

        if (!intent?.getStringExtra(Appconstant.NAME).isNullOrEmpty()) {
            note.text =
                "You have successfully transferred money to ${intent?.getStringExtra(Appconstant.NAME)} LuiPay account"
        }

    }

    override fun onBackPressed() {
        setResult(Activity.RESULT_OK)
        finish()
    }

}