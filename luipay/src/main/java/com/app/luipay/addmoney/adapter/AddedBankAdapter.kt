package com.app.luipay.addmoney.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.app.luipay.DBHandler.BankAccountData
import com.app.luipay.R
import com.app.luipay.addmoney.AddedBankActivity
import com.app.luipay.utils.Appconstant
import com.app.luipay.utils.CommonUtils

import kotlinx.android.synthetic.main.bank_item_layout.view.*

class AddedBankAdapter(
    private val bankList: ArrayList<BankAccountData>,
    private val listener: AddedBankActivity.BankTypeListener
) : RecyclerView.Adapter<AddedBankAdapter.ItemViewHolder>() {


    class ItemViewHolder(view: View) : RecyclerView.ViewHolder(view)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ItemViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.bank_item_layout,
                parent,
                false
            )
        )

    override fun getItemCount(): Int {
        return bankList.size
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        val data = bankList[position]
        if (data.type == Appconstant.CARD_TYPE) {
            holder.itemView.bankNameTv.text = data.name
        } else {
            holder.itemView.bankNameTv.text = data.bankName
        }

        holder.itemView.container.setOnClickListener {
            listener?.let {
                listener.onBankSelect(data.bankid, data.name)
            }
        }
        CommonUtils.loadImage(
            holder.itemView.bankLogoImg.context,
            data.bankImageUrl,
            holder.itemView.bankLogoImg,
            R.mipmap.ic_launcher_round
        )
        if (position == itemCount - 1) {
            holder.itemView.divider.visibility = View.GONE
        } else {
            holder.itemView.divider.visibility = View.VISIBLE
        }
    }
}