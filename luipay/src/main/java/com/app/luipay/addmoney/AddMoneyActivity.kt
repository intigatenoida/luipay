package com.app.luipay.addmoney

import android.app.Activity
import android.app.KeyguardManager
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat
import com.app.luipay.R
import com.app.luipay.base.BaseActivity
import com.app.luipay.money_transfer.phonebook.PhoneBookStatusActivity
import com.app.luipay.request.MoneyRequest
import com.app.luipay.request.MoneyTransferRequest
import com.app.luipay.response.BaseResponse
import com.app.luipay.response.SearchContactResponse.ResponseCollection
import com.app.luipay.retrofit.ApiClient
import com.app.luipay.retrofit.ApiInterface
import com.app.luipay.retrofit.CallbackManager
import com.app.luipay.utils.Appconstant
import com.app.luipay.utils.FingerprintDailog
import com.app.luipay.utils.LuipayToast
import com.app.luipay.utils.Prefutils
import kotlinx.android.synthetic.main.activity_add_money.*

class AddMoneyActivity : BaseActivity(), FingerprintDailog.onCancelListener {
    private var fromRequest: Boolean = false
    lateinit var data: ResponseCollection
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_money)

        intent.extras?.let {
            if (it.containsKey(Appconstant.LUI_REQUEST)) {
                fromRequest = it.getBoolean(Appconstant.LUI_REQUEST, false)
            }
            data = it.getSerializable(Appconstant.NAME) as ResponseCollection
            data?.let {
                tvBankName.text = it.name
            }
//            Glide

        }


        edAmount.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

                if (p0.toString().length > 0) {
                    tvbtnAddProcess.setTextColor(Color.parseColor("#272561"))
                    view1.setBackgroundColor(Color.parseColor("#ffffff"))
                    tvbtnAddProcess.background =
                        ContextCompat.getDrawable(this@AddMoneyActivity, R.drawable.enable_25)

                } else {
                    view1.setBackgroundColor(Color.parseColor("#50ffffff"))
                    tvbtnAddProcess.setTextColor(Color.parseColor("#50272561"))
                    tvbtnAddProcess.background =
                        ContextCompat.getDrawable(this@AddMoneyActivity, R.drawable.disable_25)

                }

            }

        })

        ivBack.setOnClickListener {
            setResult(Activity.RESULT_CANCELED)
            finish()
        }

        tvbtnAddProcess.setOnClickListener {
            if (edAmount.text.toString().isNotEmpty()) {
                if (fromRequest) {
                    RequestMoneyTransfer()
                } else {
                    MoneyTransfer()
                }
            } else {
                Toast.makeText(this, "Please enter amount", Toast.LENGTH_LONG).show()
            }
        }

        if (fromRequest) {
            tvnote.visibility = View.INVISIBLE
            tvWallet.visibility = View.GONE
            tvbtnAddProcess.text = "Proceed to Request"
            tvAddMoney.text = "Request Money from"
            tvWallet.text = ""
        } else {
            tvWallet.text = ""
            tvWallet.visibility = View.VISIBLE
            tvnote.visibility = View.VISIBLE
            tvbtnAddProcess.text = "Process to send"
            tvAddMoney.text = "Transfer Money to"
        }
    }

    private fun MoneyTransfer() {
        if (::data.isInitialized.not()) return
        showProcessDialog()
        val request = MoneyTransferRequest()
        request.amount = edAmount.text?.toString() ?: "0.0"
        request.pin = Prefutils.getFromPrefs(this@AddMoneyActivity, Prefutils.PASSWORD)!!
        request.serviceAuthenticationToken = ApiClient.android_token
        request.receiverUserId = data.id
        request.memo = edtReson.text.toString()
        val helper = ApiClient.getClient(this).create(ApiInterface::class.java)
        val call = helper.sendMoney(request, Appconstant.API_HEADER)
        call.enqueue(object : CallbackManager<BaseResponse>() {
            override fun onSuccess(any: BaseResponse?, message: String) {
                hideProcessDailog()
                val intent = Intent(this@AddMoneyActivity, TranxStatusActivity::class.java)
                intent.putExtra("amount", edAmount.text.toString())
                intent.putExtra(Appconstant.API_KEY, true)
                intent.putExtra(Appconstant.NAME, data?.name)
                startActivityForResult(intent, 1)
            }

            override fun onFailure(message: String) {
                hideProcessDailog()
                LuipayToast.showToast(this@AddMoneyActivity, message)
            }

            override fun onError(error: String) {
                hideProcessDailog()
            }

        })
    }

    private fun RequestMoneyTransfer() {
        if (::data.isInitialized.not()) return
        showProcessDialog()
        var request = MoneyRequest()
        request.amount = edAmount.text?.toString()?:"0.0"
        request.friendId = data.id
        request.serviceAuthenticationToken = ApiClient.android_token
        request.memo = edtReson.text.toString()

        val helper = ApiClient.getClient(this).create(ApiInterface::class.java)
        val call = helper.moneyRequest(request, Appconstant.API_HEADER)
        call.enqueue(object : CallbackManager<BaseResponse>() {
            override fun onSuccess(any: BaseResponse?, message: String) {
                hideProcessDailog()
                val intent = Intent(this@AddMoneyActivity, PhoneBookStatusActivity::class.java)
                intent.putExtra("amount", edAmount.text.toString())
                data?.let {
                    intent.putExtra(Appconstant.NAME, it.name)
                }
                startActivityForResult(intent, 1)
            }

            override fun onFailure(message: String) {
                hideProcessDailog()
                LuipayToast.showToast(this@AddMoneyActivity, message)
            }

            override fun onError(error: String) {
                hideProcessDailog()
            }
        })
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    fun showAuthScreen() {
        val keyguardManager = getSystemService(
            Context.KEYGUARD_SERVICE
        ) as KeyguardManager
        if (keyguardManager == null || !keyguardManager.isKeyguardSecure) {
            usePin()

        } else {
            val intent = keyguardManager
                .createConfirmDeviceCredentialIntent(
                    ("Confirm your phone screen lock pattern, PIN or password"),
                    ("Unlock luiPay")
                )
            if (intent != null) {
                startActivityForResult(intent, 500)
            }
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 1 && resultCode == Activity.RESULT_OK) {
            onBackPressed()
        } else if (requestCode == 500 && resultCode == Activity.RESULT_OK) {
            val intent = Intent(this@AddMoneyActivity, TranxStatusActivity::class.java)
            intent.putExtra("amount", edAmount.text.toString())
            startActivityForResult(intent, 1)
        }
    }

    private fun open() {
//        val fragmentTransaction = supportFragmentManager.beginTransaction()
//        val prev = supportFragmentManager.findFragmentByTag("dialog")
//        if (prev != null) {
//            fragmentTransaction.remove(prev)
//        }
//        fragmentTransaction.addToBackStack(null)
//        val dialogFragment = FingerprintDailog.getInstance()
//        dialogFragment.show(fragmentTransaction, "dialog")

//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            showAuthScreen()
//        } else {

//        }
    }

    override fun onCancel() {

    }


    override fun usePin() {
        intent?.let {
            if (it.hasExtra("fromPhoneBook") && it.getBooleanExtra("fromPhoneBook", false)) {
                val intent = Intent(this@AddMoneyActivity, PhoneBookStatusActivity::class.java)
                intent.putExtra("amount", edAmount.text.toString())
                startActivityForResult(intent, 1)
            } else {
                val intent = Intent(this@AddMoneyActivity, TranxStatusActivity::class.java)
                intent.putExtra("amount", edAmount.text.toString())
                startActivityForResult(intent, 1)
            }
        }

    }

    override fun onBackPressed() {

        setResult(Activity.RESULT_OK)
        finish()
    }
}