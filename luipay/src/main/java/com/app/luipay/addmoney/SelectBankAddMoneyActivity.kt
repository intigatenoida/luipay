package com.app.luipay.addmoney

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.luipay.R
import com.app.luipay.addmoney.adapter.SelectBankAddMoneyAdapter
import com.app.luipay.base.BaseActivity
import com.app.luipay.link_account.bank_details.BankDetailsActivity
import com.app.luipay.request.ServiceAuthenticationToken
import com.app.luipay.response.BankListResponse.BankListResponse
import com.app.luipay.response.BankListResponse.ResponseCollection
import com.app.luipay.retrofit.ApiClient
import com.app.luipay.retrofit.ApiInterface
import com.app.luipay.retrofit.CallbackManager
import com.app.luipay.utils.Appconstant
import kotlinx.android.synthetic.main.activity_choose_bank_add_money.*
import kotlinx.android.synthetic.main.center_title_layout.*

class SelectBankAddMoneyActivity : BaseActivity() {
    private var list = ArrayList<ResponseCollection>()
    private var fromLinkAccount = false
    lateinit var bankAccountListAdapter: SelectBankAddMoneyAdapter
    interface BankTypeListener {
        fun onBankSelect(type: Int, name: String?, bankCode: String?, bankImageUrl: String?)
    }

    val listener = object : BankTypeListener {
        override fun onBankSelect(type: Int, name: String?, bankCode: String?, bankImageUrl: String?) {
            val intent1 = Intent(
                this@SelectBankAddMoneyActivity,
                BankDetailsActivity::class.java
            )
            intent1.putExtra(Appconstant.NAME, name)
            intent1.putExtra(Appconstant.CONTACT_ID, type)
            intent1.putExtra(Appconstant.BANK_CODE, bankCode)
            intent1.putExtra(Appconstant.BANK_IMAGE_URL, bankImageUrl)
            startActivityForResult(
                intent1, 1000
            )
           /* if (fromLinkAccount) {
            } else {
                when (type) {
                    1 -> {
                        startActivityForResult(
                            Intent(
                                this@SelectBankAddMoneyActivity,
                                MoneyTransferActivity::class.java
                            ).putExtra(Appconstant.CONTACT_ID,1), 1000
                        )
                    }
                    2 -> {
                        startActivityForResult(
                            Intent(
                                this@SelectBankAddMoneyActivity,
                                AddMoneyActivity::class.java
                            ).putExtra(Appconstant.CONTACT_ID,2), 1000
                        )
                    }
                    3 -> {
                        startActivityForResult(
                            Intent(
                                this@SelectBankAddMoneyActivity,
                                OpenBankActivity::class.java
                            ), 1000
                        )
                    }
                }
            }*/
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_choose_bank_add_money)
        manageUI()
        intent?.let {
            fromLinkAccount = it.getBooleanExtra(Appconstant.NAME, false)
        }

    }

    private fun manageUI() {
        backBtn.setOnClickListener { finish() }
        titleTv.text = getString(R.string.link_account)

        recyclerView.layoutManager = LinearLayoutManager(this)
        bankAccountListAdapter = SelectBankAddMoneyAdapter(this,list, listener)
        recyclerView.adapter = bankAccountListAdapter
        getBankData()
    }

    private fun getBankData() {
        showProcessDialog()
        val token = ServiceAuthenticationToken()
        token.serviceAuthenticationToken = ApiClient.android_token
        val helper =
            ApiClient.getClient(this@SelectBankAddMoneyActivity).create(ApiInterface::class.java)
        val call = helper.getBankList(token, "application/json")
        call.enqueue(object : CallbackManager<BankListResponse>() {
            override fun onSuccess(any: BankListResponse?, message: String) {
                hideProcessDailog()
                val response = any as BankListResponse
                response.responseCollection?.let {
                    list.addAll(response.responseCollection)
                    if (::bankAccountListAdapter.isInitialized) {
                        bankAccountListAdapter.notifyDataSetChanged()
                    }
                }
            }

            override fun onFailure(message: String) {
                hideProcessDailog()
            }

            override fun onError(error: String) {
                hideProcessDailog()
            }

        })
    }



    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode== Activity.RESULT_OK && requestCode==1000){
            setResult(Activity.RESULT_OK)
            finish()
        }
    }
}