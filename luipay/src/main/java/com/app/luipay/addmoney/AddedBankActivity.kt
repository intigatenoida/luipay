package com.app.luipay.addmoney

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.luipay.R
import com.app.luipay.addmoney.adapter.SelectBankAddMoneyAdapter
import com.app.luipay.base.BaseActivity
import com.app.luipay.money_transfer.MoneyTransferActivity
import com.app.luipay.request.ServiceAuthenticationToken
import com.app.luipay.response.BankListResponse.BankListResponse
import com.app.luipay.response.BankListResponse.ResponseCollection
import com.app.luipay.retrofit.ApiClient
import com.app.luipay.retrofit.ApiInterface
import com.app.luipay.retrofit.CallbackManager
import com.app.luipay.utils.Appconstant
import kotlinx.android.synthetic.main.activity_choose_bank_add_money.*
import kotlinx.android.synthetic.main.center_title_layout.*

class AddedBankActivity : BaseActivity() {
    private var list = ArrayList<ResponseCollection>()

    //    var bankData = ArrayList<BankAccountData>()
    lateinit var bankAccountListAdapter: SelectBankAddMoneyAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_choose_bank_add_money)
        manageUI()
    }

    private fun manageUI() {
        backBtn.setOnClickListener { finish() }
        titleTv.text = getString(R.string.add_money)

        recyclerView.layoutManager = LinearLayoutManager(this)
        bankAccountListAdapter = SelectBankAddMoneyAdapter(this, list, listener)
        recyclerView.adapter = bankAccountListAdapter
        getBankData()
    }

    private fun getBankData() {
        showProcessDialog()
        val token = ServiceAuthenticationToken()
        token.serviceAuthenticationToken = ApiClient.android_token
        val helper =
            ApiClient.getClient(this@AddedBankActivity).create(ApiInterface::class.java)
        val call = helper.getBankList(token, "application/json")
        call.enqueue(object : CallbackManager<BankListResponse>() {
            override fun onSuccess(any: BankListResponse?, message: String) {
                hideProcessDailog()
                val response = any as BankListResponse
                response.responseCollection?.let {
                    list.addAll(response.responseCollection)
                    if (::bankAccountListAdapter.isInitialized) {
                        bankAccountListAdapter.notifyDataSetChanged()
                    }
                }
            }

            override fun onFailure(message: String) {
                hideProcessDailog()
            }

            override fun onError(error: String) {
                hideProcessDailog()
            }

        })
    }
/*    private fun manageUI() {
        backBtn.setOnClickListener { finish() }
        titleTv.text = getString(R.string.link_account)

        recyclerView.layoutManager = LinearLayoutManager(this)
         bankAccountListAdapter = AddedBankAdapter(bankData, listener)
        recyclerView.adapter = bankAccountListAdapter
    }

    private fun getData() {
        bankData.clear()
        bankData.addAll(getAllData())
        if (bankData.isNotEmpty() && ::bankAccountListAdapter.isInitialized) {
            bankAccountListAdapter.notifyDataSetChanged()
            emptyGroup.visibility = View.GONE
            groupBank.visibility = View.VISIBLE
        } else {
            tvLogin.text = getString(R.string.link_account)
            emptyGroup.visibility = View.VISIBLE
            groupBank.visibility = View.GONE
            tvLogin.setOnClickListener {
                val intent = Intent(this@AddedBankActivity, LinkAccountActivity::class.java)
                startActivity(intent)
            }
        }
    }

    private fun getAllData(): Collection<BankAccountData> {
        val tempData = DBHelper(this@AddedBankActivity).allData
        if (tempData.size > 1) {
            val tempList = ArrayList<String>()
            val tempBankList = ArrayList<BankAccountData>()
            for (value in tempData) {
                if (!tempList.contains(value.bankName) && value.type != Appconstant.CARD_TYPE) {
                    tempList.add(value.bankName)
                    tempBankList.add(value)
                }
            }
            return tempBankList
        }
        return tempData
    }

    override fun onResume() {
        super.onResume()
        getData()
    }*/


    interface BankTypeListener {
        fun onBankSelect(type: Int, name: String)
    }

    val listener = object : SelectBankAddMoneyActivity.BankTypeListener {
        override fun onBankSelect(
            id: Int,
            name: String?,
            bankCode: String?,
            bankImageUrl: String?
        ) {
            startActivityForResult(
                Intent(
                    this@AddedBankActivity,
                    MoneyTransferActivity::class.java
                ).putExtra(Appconstant.CONTACT_ID, id)
                    .putExtra(Appconstant.BANK_NAME, name)
                    .putExtra(Appconstant.BANK_CODE, bankCode)
                    .putExtra(Appconstant.BANK_IMAGE_URL, bankImageUrl)
                , 1000
            )
        }
        /*override fun onBankSelect(type: Int, name: String) {
            startActivityForResult(
                Intent(
                    this@AddedBankActivity,
                    MoneyTransferActivity::class.java
                ).putExtra(Appconstant.CONTACT_ID,type), 1000
            )
        }*/
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && requestCode == 1000) {
            finish()
        }
    }

}