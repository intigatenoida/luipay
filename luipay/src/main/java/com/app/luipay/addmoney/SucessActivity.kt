package com.app.luipay.addmoney

import android.app.Activity
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import com.app.luipay.R
import com.app.luipay.utils.Appconstant
import kotlinx.android.synthetic.main.activity_sucess.*
import kotlinx.android.synthetic.main.header_layout_profile.*

class SucessActivity : AppCompatActivity() {

    var trasAmt : String =""
    var trasCode : String =""
    var trasStatus : Boolean =false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sucess)

        //--> Update toolbar
        tvMenuName.visibility = View.VISIBLE
        tvMenuName.text = "Add Money"


        //--> Get Intent Data
        trasAmt = if(intent.hasExtra(Appconstant.TRANSAMOUNT)){
            intent.getStringExtra(Appconstant.TRANSAMOUNT)
        }else{
            ""
        }

        trasCode = if(intent.hasExtra(Appconstant.TRANSCODE)){
            intent.getStringExtra(Appconstant.TRANSCODE)
        }else{
            ""
        }

        trasStatus = if(intent.hasExtra(Appconstant.TRANSSTATUS)){
            intent.getBooleanExtra(Appconstant.TRANSSTATUS,false)
        }else{
           false
        }
        updateUI()
        ivBack.setOnClickListener {
            onBackPressed()
        }

        //--> Main Screen Navigation
        btn_mainScreen.setOnClickListener {
            onBackPressed()
        }

    }

    override fun onBackPressed() {
        setResult(Activity.RESULT_OK)
        finish()
    }

    private fun updateUI(){
        //--> Transaction Status
        if(trasStatus){
            container.setBackgroundColor(ContextCompat.getColor(this@SucessActivity,R.color.pending_trans))
            text_trans_status.setCompoundDrawablesRelativeWithIntrinsicBounds(0,0,R.drawable.ic_timer,0)
            text_trans_status.text = getString(R.string.pending_transaction)
        }else{
            btn_help.visibility = View.VISIBLE
            container.setBackgroundColor(ContextCompat.getColor(this@SucessActivity,R.color.red_1))
            text_trans_status.setCompoundDrawablesRelativeWithIntrinsicBounds(0,0,R.drawable.ic_canceled,0)
            text_trans_status.text = getString(R.string.cancel_transaction)
        }
        //--> Amount
        txt_ammount.text = trasAmt

        //--> Transaction Code
        txt_transCode.text = trasCode

    }

}
