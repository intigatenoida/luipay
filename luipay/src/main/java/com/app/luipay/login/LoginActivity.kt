package com.app.luipay.login

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.InputFilter
import android.text.InputFilter.LengthFilter
import android.text.InputType
import android.text.Spannable
import android.text.SpannableString
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.text.style.UnderlineSpan
import android.view.View.VISIBLE
import android.view.inputmethod.EditorInfo
import com.app.luipay.R
import com.app.luipay.base.BaseActivity
import com.app.luipay.dashboard.DashboardActivity
import com.app.luipay.request.LoginRequest
import com.app.luipay.response.otpresponse.otp
import com.app.luipay.retrofit.ApiClient
import com.app.luipay.retrofit.ApiInterface
import com.app.luipay.retrofit.CallbackManager
import com.app.luipay.signup.SignupActivity
import com.app.luipay.utils.CommonUtils
import com.app.luipay.utils.CustomPassword
import com.app.luipay.utils.Prefutils
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.edittext_view.view.*


class LoginActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        edMobileNumber.tvLabel.text = getString(R.string.mobile)
        edPassword.tvLabel.text = getString(R.string.password)

        edMobileNumber.edInput.inputType = InputType.TYPE_CLASS_NUMBER
        edMobileNumber.edInput.filters = arrayOf<InputFilter>(LengthFilter(8))

        edPassword.edInput.inputType =
            InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD
        edPassword.edInput.filters = arrayOf<InputFilter>(LengthFilter(8))
        edPassword.edInput.imeOptions = EditorInfo.IME_ACTION_DONE
        edPassword.img_eye.visibility = VISIBLE
        edPassword.img_eye.setImageResource(R.drawable.ic_show)
        edPassword.edInput.transformationMethod = PasswordTransformationMethod.getInstance()
        edPassword.img_eye.setOnClickListener {
            if (edPassword.edInput.transformationMethod.equals(PasswordTransformationMethod.getInstance())) {
                edPassword.img_eye.setImageResource(R.drawable.ic_hide)
                edPassword.edInput.transformationMethod =
                    HideReturnsTransformationMethod.getInstance()
            } else {
                edPassword.img_eye.setImageResource(R.drawable.ic_show)
                edPassword.edInput.transformationMethod = PasswordTransformationMethod.getInstance()
            }
            edPassword.edInput.setSelection(edPassword.edInput.text?.toString()?.length ?: 0)
        }

        tvDontAccount.setOnClickListener {
            val intent = Intent(this@LoginActivity, SignupActivity::class.java)
            startActivityForResult(intent, 1)
        }

        btnLogin.setOnClickListener {
            if (isValidate()) {
                loginApi()
            }
        }

        val spannable = SpannableString(getString(R.string.dont_have_account_yet_signup))

        spannable.setSpan(
            UnderlineSpan(),
            spannable.indexOf("?") + 2, spannable.length,
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )

//        val semibold=Typeface.createFromAsset(assets,"poppins_semibold.ttf")
//        val soan=CustomT

        tvDontAccount.text = spannable

    }

    public fun isValidate(): Boolean {

        if (edMobileNumber.edInput.text.toString()
                .isEmpty() && edMobileNumber.edInput.text?.length!! < 10
        ) {
            CommonUtils.showToast(this, "Please enter mobile number")
            return false
        } else if (edPassword.edInput.text.toString().isEmpty()) {
            CommonUtils.showToast(this, "Please enter password")
            return false
        }
        return true
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 1 && resultCode == Activity.RESULT_OK) {
            finish()
        }
    }

    private fun loginApi() {
        if (!isOnline()) {
            CommonUtils.showToast(this, getString(R.string.turn_on_internet))
            return
        }
        showProcessDialog()
        val helper = ApiClient.getClient(this).create(ApiInterface::class.java)
        val request = LoginRequest()
        request.MobileNo = edMobileNumber.edInput.text.toString()
        request.Password = CustomPassword.encrypt(edPassword.edInput.text.toString())
        request.CountryCodeId = 1
        request.DeviceNotificationID = ""
        request.LanguageTypeId = 1
        request.ServiceAuthenticationToken = ApiClient.android_token


        val call = helper.loginWithMobileNo(request, "application/json")

        call.enqueue(object : CallbackManager<otp>() {
            override fun onSuccess(res: otp?, message: String) {
                hideProcessDailog()
                if (res?.responseStatus.equals("1")) {
                    Prefutils.saveToPrefs(this@LoginActivity, Prefutils.MOBILE, request.MobileNo)
                    Prefutils.saveToPrefs(this@LoginActivity, Prefutils.PASSWORD, request.Password)
                    Prefutils.saveToPrefs(
                        this@LoginActivity,
                        Prefutils.USERID,
                        res?.response?.id ?: ""
                    )
                    Prefutils.saveToPrefs(
                        this@LoginActivity,
                        Prefutils.ACCESS_TOKEN,
                        res?.response?.accessToken?.accessToken
                    )
                    val intent = Intent(this@LoginActivity, DashboardActivity::class.java)
                    startActivity(intent)
                    finish()
                }

            }

            override fun onFailure(message: String) {
                hideProcessDailog()
                CommonUtils.showToast(this@LoginActivity, message)
            }

            override fun onError(error: String) {
                hideProcessDailog()
                CommonUtils.showToast(this@LoginActivity, error)

            }

        })
    }

}