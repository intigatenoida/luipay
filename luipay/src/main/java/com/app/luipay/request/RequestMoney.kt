package com.app.luipay.request


import com.google.gson.annotations.SerializedName

data class RequestMoney(
    @SerializedName("Action")
    var action: String = "",
    @SerializedName("FriendId")
    var friendId: String = "",
    @SerializedName("LanguageTypeId")
    var languageTypeId: Int = 0,
    @SerializedName("ServiceAuthenticationToken")
    var serviceAuthenticationToken: String = ""
)