package com.app.luipay.request

import com.google.gson.annotations.SerializedName

class ChangePasswordRequest(
    @SerializedName("LanguageTypeId")
    var LanguageTypeId: Int? = 0,
    @SerializedName("ServiceAuthenticationToken")
    var ServiceAuthenticationToken: String = "",
    @SerializedName("OldPassword")
    var OldPassword: String = "",
    @SerializedName("NewPassword")
    var NewPassword: String = ""
)