package com.app.luipay.request


import com.google.gson.annotations.SerializedName

data class SignupRequest(
    @SerializedName("FullName")
var FullName: String="",
@SerializedName("MobileNo")
var MobileNo: String="",
@SerializedName("Password")
var Password: String=""

) : BaseRequest()