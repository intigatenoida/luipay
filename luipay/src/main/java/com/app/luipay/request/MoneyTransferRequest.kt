package com.app.luipay.request


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep

@Keep
data class MoneyTransferRequest(
    @SerializedName("Amount")
    var amount: String?="",
    @SerializedName("Memo")
    var memo: String="",
    @SerializedName("Pin")
    var pin: String="",
    @SerializedName("ReceiverUserId")
    var receiverUserId: String="",
    @SerializedName("ServiceAuthenticationToken")
    var serviceAuthenticationToken: String=""
)