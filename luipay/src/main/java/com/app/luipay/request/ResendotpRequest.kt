package com.app.luipay.request


import com.google.gson.annotations.SerializedName

data class ResendotpRequest(
    @SerializedName("MobileNo")
    var mobileNo: String = ""
): BaseRequest()