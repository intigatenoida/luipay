package com.app.luipay.request


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep

@Keep
data class SearchContactRequest(
    @SerializedName("LanguageTypeId")
    var languageTypeId: Int=0,
    @SerializedName("Search")
    var search: String="",
    @SerializedName("ServiceAuthenticationToken")
    var serviceAuthenticationToken: String=""
)