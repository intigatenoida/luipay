package com.app.luipay.request


import com.google.gson.annotations.SerializedName

data class LoginRequest(
    @SerializedName("MobileNo")
    var MobileNo: String="",
    @SerializedName("Password")
    var Password: String=""
): BaseRequest()