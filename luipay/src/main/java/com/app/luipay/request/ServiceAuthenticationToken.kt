package com.app.luipay.request

import com.google.gson.annotations.SerializedName

  data class ServiceAuthenticationToken(

    @SerializedName("ServiceAuthenticationToken")
    var serviceAuthenticationToken: String = ""
)