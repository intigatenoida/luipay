package com.app.luipay.request


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep

@Keep
data class MoneyRequest(
    @SerializedName("Amount")
    var amount: String? = "",
    @SerializedName("FriendId")
    var friendId: String = "",
    @SerializedName("Memo")
    var memo: String = "",
    @SerializedName("ServiceAuthenticationToken")
    var serviceAuthenticationToken: String = ""
)