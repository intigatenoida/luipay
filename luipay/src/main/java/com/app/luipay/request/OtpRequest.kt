package com.app.luipay.request


import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName

@Keep
data class OtpRequest(
    @SerializedName("MobileNo")
    var MobileNo: String="",
    @SerializedName("Otp")
    var Otp: String=""
):BaseRequest()