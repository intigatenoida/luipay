package com.app.luipay.request

import com.google.gson.annotations.SerializedName


open class BaseRequest(
    @SerializedName("CountryCodeId")
    var CountryCodeId: Int=0,
    @SerializedName("DeviceNotificationID")
    var DeviceNotificationID: String="",
    @SerializedName("LanguageTypeId")
    var LanguageTypeId: Int=0,
    @SerializedName("ServiceAuthenticationToken")
    var ServiceAuthenticationToken: String=""
)