package com.app.luipay.reacharge_pay_bill

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.app.luipay.R
import com.google.android.material.tabs.TabLayoutMediator
import com.google.android.material.tabs.TabLayoutMediator.TabConfigurationStrategy
import kotlinx.android.synthetic.main.activity_reacharge_pay_bill.*
import kotlinx.android.synthetic.main.center_title_layout.*


class ReachargePayBillActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reacharge_pay_bill)
        backBtn.setOnClickListener { finish() }
        titleTv.text = getString(R.string.recharge_pay_bills)
        viewPager.adapter = PagerAdapter(this)

        TabLayoutMediator(tabLayout, viewPager,
            TabConfigurationStrategy { tab, position ->
                val tabText: String
                when (position) {
                    0 -> tabText = getString(R.string.mobile_tab)
                    1 -> tabText = getString(R.string.broadband)
                    2 -> tabText = getString(R.string.electricity)
                    3 -> tabText = getString(R.string.dth)
                    4 -> tabText = getString(R.string.card_bill)
                    else -> tabText = getString(R.string.mobile)
                }
                tab.text = tabText
            }).attach()
    }
}
