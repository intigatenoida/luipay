package com.app.luipay.reacharge_pay_bill.proceed_reacharge

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.appcompat.app.AppCompatActivity
import com.app.luipay.R
import kotlinx.android.synthetic.main.activity_proceed_reacharge.*

class ProceedReachargeActivity : AppCompatActivity(), TextWatcher {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_proceed_reacharge)
        backBtn.setOnClickListener { finish() }
        amountEdt.addTextChangedListener(this)
    }

    override fun afterTextChanged(s: Editable?) {
        proceedRechargeBtn.isEnabled = !s!!.toString().isEmpty()

    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
    }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
    }
}
