package com.app.luipay.reacharge_pay_bill.mobile_reacharge

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.app.luipay.R
import com.app.luipay.base.BaseFragment
import com.app.luipay.reacharge_pay_bill.mobile_reacharge.search_mobile_number.SearchMobileNumberActivity
import com.app.luipay.reacharge_pay_bill.proceed_reacharge.ProceedReachargeActivity
import kotlinx.android.synthetic.main.fragment_mobile_reacharge.view.*


class MobileReachargeFragment : BaseFragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    lateinit var rootView: View

    var isChecked = false

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_mobile_reacharge, container, false)
        rootView.radio_group.setOnCheckedChangeListener { radioGroup, i ->
            isChecked = true
        }
        rootView.proceedRechargeBtn.setOnClickListener {
            if (!isChecked) {
                showMessage("Please select any one prepaid or postpaid")
                return@setOnClickListener
            }
            if (rootView.edMobile.text?.toString().isNullOrEmpty()) {
                showMessage("Please enter the mobile number")
                return@setOnClickListener
            }
            context!!.startActivity(Intent(context, ProceedReachargeActivity::class.java))
        }

        rootView.img_contacts.setOnClickListener {
            this@MobileReachargeFragment.startActivityForResult(
                Intent(
                    context!!,
                    SearchMobileNumberActivity::class.java
                ), 10011
            )
        }

        return rootView
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && requestCode == 10011) {
            rootView.edMobile.setText(data?.getStringExtra("mobile") ?: "")
        }
    }

    companion object {

        @JvmStatic
        fun newInstance(): MobileReachargeFragment {
            return MobileReachargeFragment()
        }

    }
}
