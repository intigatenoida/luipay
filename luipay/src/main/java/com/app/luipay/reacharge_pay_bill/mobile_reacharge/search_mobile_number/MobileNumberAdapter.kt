package com.app.luipay.reacharge_pay_bill.mobile_reacharge.search_mobile_number

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.app.luipay.R
import com.app.luipay.reacharge_pay_bill.proceed_reacharge.ProceedReachargeActivity
import kotlinx.android.synthetic.main.search_contact_layout.view.*
import java.util.*

class MobileNumberAdapter(var context: Context, var list: ArrayList<Objects>,val listener:Communicator) :
    RecyclerView.Adapter<MobileNumberAdapter.ViewHolder>() {
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view =
            LayoutInflater.from(context).inflate(R.layout.search_contact_layout, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        if (list.isEmpty()) {
            return 15
        }
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemView.parentLayout.setOnClickListener {
            listener.sendData("54182978")
        }
    }

    interface Communicator{
        fun sendData(obj:String)
    }
}