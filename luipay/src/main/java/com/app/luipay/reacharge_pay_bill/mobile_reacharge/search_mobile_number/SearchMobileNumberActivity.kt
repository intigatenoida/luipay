package com.app.luipay.reacharge_pay_bill.mobile_reacharge.search_mobile_number

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.luipay.R
import kotlinx.android.synthetic.main.activity_search_mobile_number.*
import kotlinx.android.synthetic.main.center_title_layout.*
import java.util.*
import kotlin.collections.ArrayList

class SearchMobileNumberActivity : AppCompatActivity(),MobileNumberAdapter.Communicator {
private lateinit var contactAdapter: MobileNumberAdapter
    private var list=ArrayList<Objects>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search_mobile_number)
        backBtn.setOnClickListener { finish() }
        titleTv.visibility=View.GONE
        notification.visibility=View.GONE
        contactAdapter= MobileNumberAdapter(this,list,this)
        contactRV.layoutManager=LinearLayoutManager(this)
        contactRV.adapter=contactAdapter
    }

    override fun sendData(obj: String) {
        val sendIt = Intent()
        sendIt.putExtra("mobile",obj)
        setResult(Activity.RESULT_OK,sendIt)
        finish()
    }

}
