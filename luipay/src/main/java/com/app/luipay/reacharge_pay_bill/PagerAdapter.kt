package com.app.luipay.reacharge_pay_bill

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.app.luipay.help.HelpFragment
import com.app.luipay.reacharge_pay_bill.mobile_reacharge.MobileReachargeFragment

class PagerAdapter(activity: AppCompatActivity) : FragmentStateAdapter(activity) {
    override fun getItemCount(): Int {
        return 5
    }

    override fun createFragment(position: Int): Fragment {
        when (position) {
            0 -> return MobileReachargeFragment()
            1 -> return MobileReachargeFragment()
            2 -> return MobileReachargeFragment()
            3 -> return MobileReachargeFragment()
            4 -> return MobileReachargeFragment()
            5 -> return MobileReachargeFragment()
            6 -> return MobileReachargeFragment()
            else -> return HelpFragment()
        }
    }




}