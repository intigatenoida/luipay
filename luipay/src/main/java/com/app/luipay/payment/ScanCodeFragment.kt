package com.app.luipay.payment

import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import com.app.luipay.R
import com.app.luipay.addmoney.AddMoneyActivity
import com.app.luipay.base.BaseFragment
import com.app.luipay.money_transfer.phonebook.PhonebookActivity
import com.app.luipay.permissions.PermissionHelper
import com.app.luipay.permissions.PermissionHelper.Companion.cameraPermission
import com.app.luipay.response.SearchContactResponse.ResponseCollection
import com.app.luipay.utils.Appconstant
import com.app.luipay.utils.Appconstant.Companion.PERMISSION_CAMERA
import com.app.luipay.utils.CommonUtils
import com.app.luipay.utils.Prefutils
import com.google.zxing.ResultPoint
import com.journeyapps.barcodescanner.BarcodeCallback
import com.journeyapps.barcodescanner.BarcodeResult
import kotlinx.android.synthetic.main.activity_scan_code.view.*
import org.json.JSONException
import org.json.JSONObject


class ScanCodeFragment : BaseFragment() {

    lateinit var rootview: View

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        rootview = inflater.inflate(R.layout.activity_scan_code, null)

        rootview.barcodeView.decodeContinuous(object : BarcodeCallback {
            override fun barcodeResult(result: BarcodeResult?) {
                Log.e("barcoderesult", result?.text)
                showProcessDialog()
                rootview.barcodeView.pause()
                parseResultText(result?.text)
            }

            override fun possibleResultPoints(resultPoints: MutableList<ResultPoint>?) {

            }

        })

        rootview.viewEditText.setOnClickListener {
            val intent = Intent(activity, PhonebookActivity::class.java)
            startActivity(intent)
        }

        if (PermissionHelper.isPermissionsAllowed2(
                this,
                cameraPermission,
                true,
                PERMISSION_CAMERA
            )
        ) {
            rootview.v_barcode.visibility = GONE
        } else {
            rootview.v_barcode.visibility = VISIBLE
            rootview.v_barcode.setOnClickListener {
                PermissionHelper.isPermissionsAllowed2(
                    this,
                    cameraPermission,
                    true,
                    PERMISSION_CAMERA
                )
            }
        }
        return rootview
    }

    private fun parseResultText(result: String?) {
        if (result.isNullOrEmpty()) {
            rootview.barcodeView.resume()
            hideProcessDialog()
            return
        }

        val json = JSONObject(result)
        val temp = ResponseCollection()
        try {
            temp.name = json.getString("un")
            temp.profilePictureUrl = json.getString("up")
            temp.id = json.getString("ui")
        } catch (e: JSONException) {

        } finally {
            hideProcessDialog()
            if (temp.id.isNotEmpty()) {
                if (temp.id != Prefutils.getFromPrefs(context!!, Prefutils.USERID) ?: "") {
                    openNextScreen(temp)
                } else {
                    rootview.barcodeView.resume()
                }
            } else {
                rootview.barcodeView.resume()
                showMessage(getString(R.string.please_try_again))
            }
        }
    }

    private fun openNextScreen(data: ResponseCollection) {
        val sendIt = Intent(
            context!!,
            AddMoneyActivity::class.java
        )

        val bundle = Bundle()
        bundle.putBoolean(
            Appconstant.LUI_REQUEST, false
        )
        bundle.putSerializable(Appconstant.NAME, data)
        sendIt.putExtras(bundle)
        this@ScanCodeFragment.startActivity(sendIt)
    }

    override fun onResume() {
        rootview.barcodeView.resume()
        super.onResume()
    }

    override fun onPause() {
        rootview.barcodeView.pause()
        super.onPause()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == PERMISSION_CAMERA) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                rootview.v_barcode.visibility = GONE
            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M &&
                    !shouldShowRequestPermissionRationale(permissions[0])
                ) {
                    CommonUtils.openSettingDialog(context!!)
                }
            }
        }
    }

}

//        val integrator = IntentIntegrator.forSupportFragment(this@ScanCodeFragment)
//        integrator.setDesiredBarcodeFormats(IntentIntegrator.ONE_D_CODE_TYPES)
//        integrator.setPrompt("Scan a barcode")
//        integrator.setCameraId(0) // Use a specific camera of the device
//        integrator.setOrientationLocked(false)
//        integrator.setBeepEnabled(false)
//        integrator.setBarcodeImageEnabled(true)
//        integrator.initiateScan()