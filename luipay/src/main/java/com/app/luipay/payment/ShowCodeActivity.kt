package com.app.luipay.payment

import android.os.Bundle
import com.app.luipay.R
import com.app.luipay.base.BaseActivity

class ShowCodeActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_payment_code)
    }
}