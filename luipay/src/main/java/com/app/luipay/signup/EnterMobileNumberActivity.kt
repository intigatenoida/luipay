package com.app.luipay.signup

import android.content.Intent
import android.os.Bundle
import android.view.View
import com.app.luipay.R
import com.app.luipay.base.BaseActivity
import com.app.luipay.login.LoginActivity
import com.app.luipay.utils.CommonUtils
import kotlinx.android.synthetic.main.activity_mobile.*
import kotlinx.android.synthetic.main.button_view.view.*
import kotlinx.android.synthetic.main.edittext_view.view.*
import kotlinx.android.synthetic.main.login_header.view.*

class EnterMobileNumberActivity : BaseActivity() ,ConfirmMobileNumberDialog.dialogClickListener{

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_mobile)


        edMobileNumber.tvLabel.text=getString(R.string.mobile)



        btnLogin.tvLogin.text=getString(R.string.next)


        header.tvHederHeading.text=getString(R.string.whats_ur_no)

        header.tvHederTitle.text=getString(R.string.send_msg)

        header.tvHederMsg.visibility= View.GONE

        btnLogin.setOnClickListener {
            if(edMobileNumber.edInput.text.toString().isNotEmpty()) {
                confirmYourMobile()
            }else{
                CommonUtils.showToast(this,"Please entere mobile number")
            }
        }

    }

    private fun confirmYourMobile() {
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        val prev = supportFragmentManager.findFragmentByTag("dialog")
        if (prev != null) {
            fragmentTransaction.remove(prev)
        }
        fragmentTransaction.addToBackStack(null)
        val dialogFragment = ConfirmMobileNumberDialog.getInstance(
            edMobileNumber.edInput.text.toString()
        )
        dialogFragment.show(fragmentTransaction, "dialog")
    }

    override fun onClickPositive(mobile: String) {
        val intent=Intent(this@EnterMobileNumberActivity,EnterOtpScreenActivity::class.java)
        startActivity(intent)
    }

}