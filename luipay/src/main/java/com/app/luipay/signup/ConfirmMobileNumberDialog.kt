package com.app.luipay.signup

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.app.luipay.R
import com.app.luipay.base.BaseDialogFragment
import kotlinx.android.synthetic.main.fragment_dialog_confirm_mobile.view.*


class ConfirmMobileNumberDialog : BaseDialogFragment() {


    private var dialogInterface: dialogClickListener? = null

    public interface dialogClickListener {
        fun onClickPositive(mobile: String)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        dialogInterface = (context as dialogClickListener)
    }


    companion object {
        fun getInstance(mobile: String): ConfirmMobileNumberDialog {
            val bundle = Bundle()
            bundle.putString("mobile", mobile)
            val fragment = ConfirmMobileNumberDialog()
            fragment.arguments = bundle
            return fragment
        }
    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val rootView = inflater.inflate(R.layout.fragment_dialog_confirm_mobile, null)

        val mobile = arguments!!.getString("mobile")

        rootView.tvMobileNo.text = "$mobile"

        rootView.tvNext.setOnClickListener {
            dialog!!.dismiss()
            dialogInterface!!.onClickPositive(mobile!!)
        }

        rootView.tvCancel.setOnClickListener {
            dialog!!.dismiss()
        }

        return rootView
    }


}