package com.app.luipay.signup

import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.text.SpannableString
import android.text.SpannableStringBuilder
import android.text.TextPaint
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.view.View
import androidx.core.content.ContextCompat
import com.app.luipay.R
import com.app.luipay.base.BaseActivity
import com.app.luipay.dashboard.DashboardActivity
import com.app.luipay.help.WebPageActivity
import com.app.luipay.profile.dialog_fragment.ChangeImageDialog
import com.app.luipay.response.BaseResponse
import com.app.luipay.retrofit.ApiClient
import com.app.luipay.retrofit.ApiInterface
import com.app.luipay.retrofit.CallbackManager
import com.app.luipay.utils.*
import kotlinx.android.synthetic.main.activity_complete_kyc.*
import kotlinx.android.synthetic.main.button_view.*
import kotlinx.android.synthetic.main.login_header.view.*
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody.Companion.toRequestBody

import java.io.File


class CompleteKYCActivity : BaseActivity(), ChangeImageDialog.OnImageSelectionListener {

    val SELECT_IMAGE = 100
    val PICK_PDF_CODE = 101

    var selectedPdf: Uri? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_complete_kyc)

        tvLogin.text = getString(R.string.Continue)

        btnComplete.setOnClickListener {
            if (isValidate()) {
                setResult(Activity.RESULT_OK)
                val intent = Intent(this@CompleteKYCActivity, DashboardActivity::class.java)
                startActivity(intent)
                finish()
            }
        }

        skip.setOnClickListener {
            val intent = Intent(this@CompleteKYCActivity, DashboardActivity::class.java)
            startActivity(intent)
            finish()
        }

        findAttachment.setOnClickListener {

        }

        ivBack.setOnClickListener {
            val intent = Intent(this@CompleteKYCActivity, DashboardActivity::class.java)
            startActivity(intent)
            finish()
        }

//        val spannable = SpannableString(getString(R.string.term_condition))
//        spannable.setSpan(
//            ForegroundColorSpan(Color.parseColor("#3DBCFE")),
//            spannable.length - 21, spannable.length,
//            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
//        )
//
//        tvTermsCondition.text = spannable

//        llSlide.setOnClickListener {
//            uploadDocument()
//        }

//        ivBackArrow.setOnClickListener {
//            onBackPressed()
//        }

//        findAttachment.setOnClickListener {
//            openImageSelectionDialog()
//        }

//        llSlide.setOnClickListener {
//            if (isValidate()) {
//                uploadDocument()
//
//            }
//        }

        setTextTerms()

    }

    private fun setTextTerms() {
        val spannableString =
            SpannableStringBuilder(getString(R.string.do_you_agree_to_luipay_terms_and_conditions))
        val clickableSpan: ClickableSpan = object : ClickableSpan() {
            override fun onClick(textView: View) {
                startActivity(
                    Intent(this@CompleteKYCActivity, WebPageActivity::class.java).putExtra(
                        Appconstant.NAME,
                        getString(R.string.terms_conditions)
                    )
                )
            }

            override fun updateDrawState(ds: TextPaint) {
                super.updateDrawState(ds)
                ds.isUnderlineText = false
                ds.color = ContextCompat.getColor(this@CompleteKYCActivity, R.color.colorAccent)
            }
        }

        spannableString.setSpan(
            clickableSpan,
            spannableString.length - 21,
            spannableString.length,
            SpannableString.SPAN_INCLUSIVE_INCLUSIVE
        )

        tvTermsCondition.movementMethod = LinkMovementMethod.getInstance()
        tvTermsCondition.highlightColor = Color.TRANSPARENT
        tvTermsCondition.text = spannableString
    }

    private fun isValidate(): Boolean {
        val checkedId = rg.checkedRadioButtonId
        if (checkedId == -1) {
            CommonUtils.showToast(
                this@CompleteKYCActivity,
                getString(R.string.select_document_type)
            )
            return false
        } else if (!agreeCheckBox.isChecked) {
            CommonUtils.showToast(
                this@CompleteKYCActivity,
                getString(R.string.term_condition_error_msg)
            )
            return false
        } else if (selectedPdf == null) {
            openImageSelectionDialog()
            return false
        }
        return true
    }

    private fun openImageSelectionDialog() {
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        val prev = supportFragmentManager.findFragmentByTag("dialog")
        if (prev != null) {
            fragmentTransaction.remove(prev)
        }
        fragmentTransaction.addToBackStack(null)
        val dialogFragment = ChangeImageDialog.getInstance()
        dialogFragment.show(fragmentTransaction, "dialog")
    }

    private fun uploadDocument() {
        if (!isOnline()) {
            CommonUtils.showToast(this, getString(R.string.turn_on_internet))
            return
        }
        try {
            showProcessDialog()
            val id = rg.checkedRadioButtonId
            val LanguageTypeId: RequestBody =
                "${"1"}"
                    .toRequestBody("multipart/form-data".toMediaTypeOrNull())
//
            val documentType: RequestBody =
                "$id".toRequestBody("multipart/form-data".toMediaTypeOrNull())

            val ServiceAuthenticationToken: RequestBody =
                ApiClient.android_token.toRequestBody("multipart/form-data".toMediaTypeOrNull())

            val file: File = FileUtils.from(this@CompleteKYCActivity, selectedPdf)
//           val file: File = File(selectedPdf!!.path)
            val requestFile: RequestBody = RequestBody.create(
                contentResolver.getType(selectedPdf!!)!!.toMediaTypeOrNull(),
                file
            )

            val body: MultipartBody.Part =
                MultipartBody.Part.createFormData("File", file.getName(), requestFile)

            val helper = ApiClient.getClient(this).create(ApiInterface::class.java)
            val call =
                helper.uploadKyC(body, LanguageTypeId, documentType, ServiceAuthenticationToken)
            call.enqueue(object : CallbackManager<BaseResponse>() {
                override fun onSuccess(any: BaseResponse?, message: String) {
                    hideProcessDailog()
                    setResult(Activity.RESULT_OK)
                    val intent = Intent(this@CompleteKYCActivity, DashboardActivity::class.java)
                    startActivity(intent)
                    finish()
                }

                override fun onFailure(message: String) {
                    hideProcessDailog()
                }

                override fun onError(error: String) {
                    hideProcessDailog()
                }

            })
        } catch (e: Exception) {
            hideProcessDailog()
        }

    }

    override fun onBackPressed() {
        setResult(Activity.RESULT_CANCELED)
        finish()
    }

    fun openGallery() {
        val intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), SELECT_IMAGE)
    }

    override fun onGallerySelectionListener() {
        openGallery()
    }

    override fun onCameraSelectionListener() {
        if (AppPermission.checkAppPermission(
                this@CompleteKYCActivity,
                AppPermission.CAMERA_CODE
            )
        ) {
            openCamera()
        }
    }

    override fun onPDFSelectionListener() {
        val browseStorage = Intent(Intent.ACTION_GET_CONTENT)
        browseStorage.type = "application/pdf"
        browseStorage.addCategory(Intent.CATEGORY_OPENABLE)
        startActivityForResult(Intent.createChooser(browseStorage, "Select PDF"), PICK_PDF_CODE)
    }

    private fun openCamera() {
        openGallery()
        /*open camera and capture pictuer code here*/
    }


    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if ((requestCode == AppPermission.CAMERA_CODE)) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                openCamera()
            } else {
                AppPermission.CheckForNeverAsk(this, AppPermission.CAMERA_CODE)
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == PICK_PDF_CODE && resultCode == RESULT_OK && data != null) {
            selectedPdf = data.data
            uploadDocument()
            CommonUtils.showToast(this@CompleteKYCActivity, getString(R.string.pdf_file_selected))
        } else if (requestCode == SELECT_IMAGE && resultCode == RESULT_OK && data != null) {
            selectedPdf = data.data!!
            uploadDocument()
            CommonUtils.showToast(this@CompleteKYCActivity, getString(R.string.image_selected))
        }
    }
}