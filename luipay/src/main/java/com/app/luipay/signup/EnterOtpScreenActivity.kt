package com.app.luipay.signup

import android.app.Activity
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.os.CountDownTimer
import android.view.View
import androidx.core.content.ContextCompat
import com.app.luipay.R
import com.app.luipay.base.BaseActivity
import com.app.luipay.dashboard.DashboardActivity
import com.app.luipay.request.LoginRequest
import com.app.luipay.request.OtpRequest
import com.app.luipay.request.ResendotpRequest
import com.app.luipay.response.BaseResponse
import com.app.luipay.response.otpresponse.otp
import com.app.luipay.retrofit.ApiClient
import com.app.luipay.retrofit.ApiInterface
import com.app.luipay.retrofit.CallbackManager
import com.app.luipay.utils.Appconstant
import com.app.luipay.utils.CommonUtils
import com.app.luipay.utils.CustomPassword
import com.app.luipay.utils.Prefutils
import kotlinx.android.synthetic.main.activity_enter_otp.*
import kotlinx.android.synthetic.main.button_view.view.*
import kotlinx.android.synthetic.main.button_view_gray.view.*
import kotlinx.android.synthetic.main.login_header.view.*
import java.util.concurrent.TimeUnit

class EnterOtpScreenActivity : BaseActivity()  {

    lateinit var mobile : String
    lateinit var password : String
     var isResendClickable=false
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_enter_otp)

         mobile=intent.getStringExtra(Appconstant.mobile)
        password=intent.getStringExtra(Appconstant.password)

        header.tvHederHeading.text=getString(R.string.code)

        header.tvHederTitle.text=getString(R.string.enter_code).plus(" ").plus(mobile)

        header.tvHederMsg.visibility=View.GONE


        btnLogin.tvLogin.text=getString(R.string.next)

        btnLogin.setOnClickListener {
            if(otpView.text.toString().isNotEmpty()) {
                checkOtp()
            }else{
                CommonUtils.showToast(this,"Please enter OTP")
            }
        }

        setTimer()

        btnResendCode.tvResend.setOnClickListener {
            if(isResendClickable) {
                resendotp()
            }
        }


    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode==1 && resultCode== Activity.RESULT_OK){
            setResult(Activity.RESULT_OK)
            finish()
        }
    }

    private fun checkOtp(){
        showProcessDialog()
        val helper = ApiClient.getClient(this).create(ApiInterface::class.java)
        val request= OtpRequest()
        request.MobileNo=mobile
        request.Otp=otpView.text.toString()
//        request.Otp="123456"
        request.CountryCodeId=1
        request.DeviceNotificationID=""
        request.LanguageTypeId=1
        request.ServiceAuthenticationToken=ApiClient.android_token

        val call = helper.CheckSignUpOtp(request,"application/json")

        call.enqueue(object : CallbackManager<otp>(){
            override fun onSuccess(res: otp?, message: String) {

                if(res?.responseStatus.equals("1")) {

                    Prefutils.saveToPrefs(this@EnterOtpScreenActivity,Prefutils.ACCESS_TOKEN,"Bearer "+res?.response?.accessToken?.accessToken)
                    loginApi()

                }else{
                    hideProcessDailog()
                    CommonUtils.showToast(this@EnterOtpScreenActivity,message)
                }
            }

            override fun onFailure(message: String) {
                hideProcessDailog()
            }

            override fun onError(error: String) {
                hideProcessDailog()
            }

        })
    }

    private fun resendotp(){
        showProcessDialog()
        val helper = ApiClient.getClient(this).create(ApiInterface::class.java)
        val request= ResendotpRequest()
        request.mobileNo=mobile
//        request.Otp=otpView.text.toString()
        request.CountryCodeId=1
        request.DeviceNotificationID=""
        request.LanguageTypeId=1
        request.ServiceAuthenticationToken=ApiClient.android_token

        val call = helper.ResendOtpForSignUp(request,"application/json")

        call.enqueue(object : CallbackManager<BaseResponse>(){
            override fun onSuccess(res: BaseResponse?, message: String) {
                hideProcessDailog()
                setTimer()

            }

            override fun onFailure(message: String) {
                hideProcessDailog()
            }

            override fun onError(error: String) {
                hideProcessDailog()
            }

        })
    }


    private fun setTimer(){
        tvTimer.visibility=View.VISIBLE
        btnResendCode.tvResend.background=ContextCompat.getDrawable(this@EnterOtpScreenActivity,R.drawable.button_border_gray)
        btnResendCode.tvResend.setTextColor(Color.parseColor("#333333"))
        isResendClickable=false
        val timer = object : CountDownTimer(120000, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                val min =TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished)
                val sec=TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))

                tvTimer.text=min.toString().plus(" : ").plus(sec)

            }

            override fun onFinish() {
                tvTimer.visibility=View.GONE
                btnResendCode.tvResend.background=ContextCompat.getDrawable(this@EnterOtpScreenActivity,R.drawable.button_border)
                btnResendCode.tvResend.setTextColor(Color.parseColor("#ffffff"))
                isResendClickable=true
            }
        }
        timer.start()
    }


    private fun loginApi() {
        if (!isOnline()) {
            CommonUtils.showToast(this, getString(R.string.turn_on_internet))
            return
        }

        val helper = ApiClient.getClient(this).create(ApiInterface::class.java)
        val request = LoginRequest()
        request.MobileNo = mobile
        request.Password = CustomPassword.encrypt(password)
        request.CountryCodeId = 1
        request.DeviceNotificationID = ""
        request.LanguageTypeId = 1
        request.ServiceAuthenticationToken = ApiClient.android_token


        val call = helper.loginWithMobileNo(request, "application/json")

        call.enqueue(object : CallbackManager<otp>() {
            override fun onSuccess(res: otp?, message: String) {
                hideProcessDailog()
                if (res?.responseStatus.equals("1")) {
                    Prefutils.saveToPrefs(this@EnterOtpScreenActivity, Prefutils.MOBILE, request.MobileNo)
                    Prefutils.saveToPrefs(this@EnterOtpScreenActivity, Prefutils.PASSWORD, request.Password)
                    Prefutils.saveToPrefs(
                        this@EnterOtpScreenActivity,
                        Prefutils.USERID,
                        res?.response?.id ?: ""
                    )
                    Prefutils.saveToPrefs(
                        this@EnterOtpScreenActivity,
                        Prefutils.ACCESS_TOKEN,
                        res?.response?.accessToken?.accessToken
                    )
                }

                setResult(Activity.RESULT_OK)
                val intent = Intent( this@EnterOtpScreenActivity, CompleteKYCActivity::class.java)
                startActivity(intent)
                finish()
            }

            override fun onFailure(message: String) {
                hideProcessDailog()
                setResult(Activity.RESULT_OK)
                val intent = Intent( this@EnterOtpScreenActivity, CompleteKYCActivity::class.java)
                startActivity(intent)
                finish()
            }

            override fun onError(error: String) {
                hideProcessDailog()
                setResult(Activity.RESULT_OK)
                val intent = Intent( this@EnterOtpScreenActivity, CompleteKYCActivity::class.java)
                startActivity(intent)
                finish()
            }

        })
    }
}