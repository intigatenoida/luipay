package com.app.luipay.signup

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.InputFilter
import android.text.InputType
import android.text.Spannable
import android.text.SpannableString
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.text.style.UnderlineSpan
import android.view.View
import android.view.inputmethod.EditorInfo
import com.app.luipay.R
import com.app.luipay.base.BaseActivity
import com.app.luipay.login.LoginActivity
import com.app.luipay.request.SignupRequest
import com.app.luipay.response.BaseResponse
import com.app.luipay.retrofit.ApiClient
import com.app.luipay.retrofit.ApiInterface
import com.app.luipay.retrofit.CallbackManager
import com.app.luipay.utils.Appconstant
import com.app.luipay.utils.CommonUtils
import com.app.luipay.utils.CustomPassword
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.activity_signup.*
import kotlinx.android.synthetic.main.activity_signup.btnLogin
import kotlinx.android.synthetic.main.activity_signup.edMobileNumber
import kotlinx.android.synthetic.main.activity_signup.edPassword
import kotlinx.android.synthetic.main.activity_signup.header
import kotlinx.android.synthetic.main.button_view.view.*
import kotlinx.android.synthetic.main.edittext_view.view.*
import kotlinx.android.synthetic.main.login_header.view.*
import kotlinx.android.synthetic.main.process_loader.*


class SignupActivity : BaseActivity(), ConfirmMobileNumberDialog.dialogClickListener {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signup)

        edFullName.tvLabel.text = getString(R.string.full_name)

        edMobileNumber.tvLabel.text = getString(R.string.mobile)


        edPassword.tvLabel.text = getString(R.string.password)

        btnLogin.tvLogin.text = getString(R.string.next)

        edFullName.edInput.inputType = InputType.TYPE_CLASS_TEXT
        edMobileNumber.edInput.inputType = InputType.TYPE_CLASS_NUMBER
        edMobileNumber.edInput.filters = arrayOf<InputFilter>(InputFilter.LengthFilter(8))
        edPassword.edInput.inputType =
            InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD
        edPassword.edInput.imeOptions = EditorInfo.IME_ACTION_DONE
        edPassword.img_eye.visibility = View.VISIBLE
        edPassword.img_eye.setImageResource(R.drawable.ic_show)
        edPassword.edInput.transformationMethod = PasswordTransformationMethod.getInstance()
        edPassword.img_eye.setOnClickListener {
            if (edPassword.edInput.transformationMethod.equals(PasswordTransformationMethod.getInstance())) {
                edPassword.img_eye.setImageResource(R.drawable.ic_hide)
                edPassword.edInput.transformationMethod =
                    HideReturnsTransformationMethod.getInstance()
            } else {
                edPassword.img_eye.setImageResource(R.drawable.ic_show)
                edPassword.edInput.transformationMethod = PasswordTransformationMethod.getInstance()
            }
            edPassword.edInput.setSelection(edPassword.edInput.text?.toString()?.length ?: 0)
        }


        header.tvHederHeading.text = getString(R.string.signup)

        header.tvHederTitle.text = getString(R.string.exp_best)

        header.tvHederMsg.visibility = View.GONE

        tvAlreadyAccount.setOnClickListener {
            val intent = Intent(this@SignupActivity, LoginActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            startActivity(intent)
            finish()
        }


        btnLogin.setOnClickListener {
            if (isValidate()) {

                confirmYourMobile()
//                val intent = Intent(this@SignupActivity, EnterMobileNumberActivity::class.java)
//                startActivity(intent)
            }
        }

        val spannable = SpannableString(getString(R.string.already_have_an_account_login))

        spannable.setSpan(
            UnderlineSpan(),
            spannable.indexOf("?") + 2, spannable.length,
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )

//        val semibold=Typeface.createFromAsset(assets,"poppins_semibold.ttf")
//        val soan=CustomT

        tvAlreadyAccount.text = spannable

    }


    public fun isValidate(): Boolean {

        if (edFullName.edInput.text.toString().isEmpty()) {
            CommonUtils.showToast(this, "Please enter full name")
            return false
        } else if (edMobileNumber.edInput.text.toString().isEmpty()) {
            CommonUtils.showToast(this, "Please enter mobile number")
            return false
        } else if (edPassword.edInput.text.toString().isEmpty()) {
            CommonUtils.showToast(this, "Please enter password")
            return false
        }
        return true
    }

    private fun confirmYourMobile() {
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        val prev = supportFragmentManager.findFragmentByTag("dialog")
        if (prev != null) {
            fragmentTransaction.remove(prev)
        }
        fragmentTransaction.addToBackStack(null)
        val dialogFragment = ConfirmMobileNumberDialog.getInstance(
            edMobileNumber.edInput.text.toString()
        )
        dialogFragment.show(fragmentTransaction, "dialog")
    }

    override fun onClickPositive(mobile: String) {
        signapi()

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 1 && resultCode == Activity.RESULT_OK) {
            setResult(Activity.RESULT_OK)
            finish()
        }
    }

    private fun signapi() {
        profileDialogLayout.visibility = View.VISIBLE

        val helper = ApiClient.getClient(this).create(ApiInterface::class.java)
        val request = SignupRequest()
        request.CountryCodeId = 1
        request.DeviceNotificationID = ""
        request.LanguageTypeId = 1
        request.FullName = edFullName.edInput.text.toString()
        request.MobileNo = edMobileNumber.edInput.text.toString()
        request.Password = CustomPassword.encrypt(edPassword.edInput.text.toString())
        request.ServiceAuthenticationToken = ApiClient.android_token

        val call = helper.UserRegistration(request, "application/json")

        call.enqueue(object : CallbackManager<BaseResponse>() {
            override fun onSuccess(any: BaseResponse?, message: String) {
                profileDialogLayout.visibility = View.GONE


                if (any?.responseStatus.equals("1")) {
                    val intent = Intent(this@SignupActivity, EnterOtpScreenActivity::class.java)
                    intent.putExtra(Appconstant.mobile, edMobileNumber.edInput.text.toString())
                    intent.putExtra(Appconstant.password, edPassword.edInput.text.toString())
                    startActivityForResult(intent, 1)
                }
            }

            override fun onFailure(message: String) {
                profileDialogLayout.visibility = View.GONE

                CommonUtils.showToast(this@SignupActivity, message)

            }

            override fun onError(error: String) {
                profileDialogLayout.visibility = View.GONE

                CommonUtils.showToast(this@SignupActivity, error)
            }

        })
    }
}