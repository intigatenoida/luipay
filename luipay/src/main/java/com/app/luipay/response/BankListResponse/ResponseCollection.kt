package com.app.luipay.response.BankListResponse


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep

@Keep
data class ResponseCollection(
    @SerializedName("BankCode")
    var bankCode: String="",
    @SerializedName("BankImageUrl")
    var bankImageUrl: String="",
    @SerializedName("BankName")
    var bankName: String="",
    @SerializedName("Id")
    var id: Int
)