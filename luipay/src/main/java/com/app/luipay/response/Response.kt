package com.app.luipay.response


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep

@Keep
data class Response(
    @SerializedName("Amount")
    var amount: String
)