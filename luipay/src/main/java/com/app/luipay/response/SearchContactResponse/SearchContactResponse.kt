package com.app.luipay.response.SearchContactResponse


import androidx.annotation.Keep
import com.app.luipay.response.BaseResponse
import com.google.gson.annotations.SerializedName

@Keep
data class SearchContactResponse(

    @SerializedName("Response")
    var response: Any,
    @SerializedName("ResponseCollection")
    var responseCollection: List<ResponseCollection>

) : BaseResponse()