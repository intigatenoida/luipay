package com.app.luipay.response


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep

@Keep
data class WalletBalanceRespons(

    @SerializedName("Response")
    var response: Response,
    @SerializedName("ResponseCollection")
    var responseCollection: List<Any>

):BaseResponse()