package com.app.luipay.response.BankListResponse


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep
import com.app.luipay.response.BaseResponse

@Keep
data class BankListResponse(
    @SerializedName("Response")
    var response: Any,
    @SerializedName("ResponseCollection")
    var responseCollection: List<ResponseCollection>

):BaseResponse()