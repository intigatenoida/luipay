package com.app.luipay.response.SearchContactResponse


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep
import java.io.Serializable

@Keep
data class ResponseCollection(
    @SerializedName("Email")
    var email: String="",
    @SerializedName("Id")
    var id: String="",
    @SerializedName("Name")
    var name: String="",
    @SerializedName("PhoneNo")
    var phoneNo: String="",
    @SerializedName("ProfilePictureUrl")
    var profilePictureUrl: String? = null,
    @SerializedName("StatusId")
    var statusId: Int=0
):Serializable