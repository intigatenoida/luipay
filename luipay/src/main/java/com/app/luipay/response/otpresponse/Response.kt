package com.app.luipay.response.otpresponse


import com.google.gson.annotations.SerializedName

data class Response(
    @SerializedName("AccessToken")
    var accessToken: AccessToken = AccessToken(),

    @SerializedName("Id")
    var id: String = ""
)