package com.app.luipay.response.notification


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep

@Keep
data class Response(
    @SerializedName("CurrentPage")
    var currentPage: Int,
    @SerializedName("NotificationList")
    var notificationList: ArrayList<Notification>,
    @SerializedName("RecordsPerPage")
    var recordsPerPage: Int,
    @SerializedName("TotalPages")
    var totalPages: Int
)