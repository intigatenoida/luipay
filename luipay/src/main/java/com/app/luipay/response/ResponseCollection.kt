package com.app.luipay.response


import com.google.gson.annotations.SerializedName

data class ResponseCollection(
    @SerializedName("AssociatedUserId")
    var associatedUserId: String = "",
    @SerializedName("Id")
    var id: Int = 0,
    @SerializedName("Image")
    var image: String = "",
    @SerializedName("MobileNo")
    var mobileNo: String = "",
    @SerializedName("UserName")
    var userName: String = ""
)