package com.app.luipay.response.notification


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep
import com.app.luipay.response.BaseResponse

@Keep
data class NotificationResposne(
    @SerializedName("Response")
    var response: Response
):BaseResponse()