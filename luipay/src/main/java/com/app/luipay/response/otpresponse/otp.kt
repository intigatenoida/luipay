package com.app.luipay.response.otpresponse


import com.app.luipay.response.BaseResponse
import com.google.gson.annotations.SerializedName

data class otp(

    @SerializedName("Response")
    var response: Response = Response()

): BaseResponse()