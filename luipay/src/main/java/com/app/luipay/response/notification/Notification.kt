package com.app.luipay.response.notification


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep

@Keep
data class Notification(
    @SerializedName("ImageUrl")
    var imageUrl: String?,
    @SerializedName("NotificationId")
    var notificationId: Int?,
    @SerializedName("NotificationMgs")
    var notificationMgs: String?,
    @SerializedName("NotificationTime")
    var notificationTime: String?
)