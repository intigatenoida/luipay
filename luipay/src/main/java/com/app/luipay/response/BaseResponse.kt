package com.app.luipay.response


import com.google.gson.annotations.SerializedName

open class BaseResponse(
    @SerializedName("AppVersion")
    var appVersion: Any = Any(),
    @SerializedName("MessageKey")
    var messageKey: String = "",
    @SerializedName("ResponseStatus")
    var responseStatus: String = ""
)