package com.app.luipay.response


import com.google.gson.annotations.SerializedName

data class RecentTranResponse(

    @SerializedName("Response")
    var response: Any = Any(),
    @SerializedName("ResponseCollection")
    var responseCollection: List<ResponseCollection> = listOf()

): BaseResponse()