package com.app.luipay.prompt

import android.os.Bundle
import androidx.constraintlayout.widget.ConstraintLayout
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.app.luipay.R
import com.app.luipay.utils.interfaces.DialogFragmentClickListener
import kotlinx.android.synthetic.main.prompt_custom.*


class CustomPrompt : BasePrompt() {

    var promptId:Int = 0

    lateinit var dgFrgListener: DialogFragmentClickListener
    lateinit var message: String
    lateinit var positiveText: String
    lateinit var negativeText: String

    companion object {
        fun getInstance(
            promptId:Int,
            message: String,
            positiveText: String,
            negativeText: String,
            dgFrgListener: DialogFragmentClickListener
        ): CustomPrompt {
            val customPrompt = CustomPrompt()
            customPrompt.promptId = promptId
            customPrompt.dgFrgListener = dgFrgListener
            customPrompt.message = message
            customPrompt.positiveText = positiveText
            customPrompt.negativeText = negativeText
            return customPrompt
        }

        const val PROMPT_ID:String = "promptId"
    }

    override fun onStart() {
        setPromptDimensions(0.9f, ConstraintLayout.LayoutParams.WRAP_CONTENT.toFloat())
        super.onStart()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.prompt_custom, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        isCancelable = false
        tv_title.text = message

        if (positiveText.isNotEmpty()) {
            bt_ok.text = positiveText
            bt_ok.setOnClickListener {
                var b = Bundle()
                b.putInt(PROMPT_ID,promptId)
                dgFrgListener.onDialogDataReceived(this,b)
                dgFrgListener.onPositiveButtonClick(dialog!!)
            }
        } else {
            bt_ok.visibility = View.GONE
        }

        if (negativeText.isNotEmpty()) {
            bt_cancel.text = negativeText
            bt_cancel.setOnClickListener {
                dgFrgListener.onNegativeButtonClick(dialog!!)
            }
        } else {
            bt_cancel.visibility = View.GONE
        }
    }
}