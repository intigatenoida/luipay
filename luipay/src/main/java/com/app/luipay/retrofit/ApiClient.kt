package com.app.luipay.retrofit

import android.content.Context
import com.app.luipay.utils.CommonUtils
import com.app.luipay.utils.Prefutils
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class ApiClient {

    companion object {
        var count = 0
        //        val BASE_URL = "http://luipay.oneto11.com/"
        val BASE_URL = "http://qaluipay.oneto11.com/"
        val android_token = "c2bb05c8-edfc-4560-a754-3f2c9748c6c2"

        lateinit var retrofit: Retrofit
        lateinit var retrofit1: Retrofit
        fun getClient(context: Context): Retrofit {

            try {
                val okHttpClient = OkHttpClient.Builder()
                val token = Prefutils.getFromPrefs(context, Prefutils.ACCESS_TOKEN)
                /*if(token?.isNotEmpty()!!) {
                    val interceptor = object : Interceptor {
                        override fun intercept(chain: Interceptor.Chain): Response {
                            val request =
                                chain.request().newBuilder().addHeader(
                                    "AUTHORIZATION","Bearer "+
                                    Prefutils.getFromPrefs(context, Prefutils.ACCESS_TOKEN)!!
                                )
                                    .build()
                            return chain.proceed(request)
                        }

                    }
                    okHttpClient.addInterceptor(interceptor)
                }*/
                val userAgentInterceptor = UserAgentInterceptor(context, okHttpClient);
                okHttpClient.addInterceptor(userAgentInterceptor)
                if (CommonUtils.isDebug()) {
                    val loggingIntercepter = HttpLoggingInterceptor()
                    loggingIntercepter.level = HttpLoggingInterceptor.Level.BODY
                    okHttpClient!!.addInterceptor(loggingIntercepter)
                }
                okHttpClient!!.connectTimeout(15, TimeUnit.MINUTES)
                okHttpClient.readTimeout(15, TimeUnit.MINUTES)
                retrofit = Retrofit.Builder()
                    .addConverterFactory(GsonConverterFactory.create())
                    .baseUrl(BASE_URL).client(okHttpClient.build()).build()
            } catch (e: java.lang.Exception) {
            }
//             }
            return retrofit
        }

        fun getClient(): Retrofit {
//            if (!::retrofit.isInitialized) {
            try {
                val okHttpClient = OkHttpClient.Builder()

                if (CommonUtils.isDebug()) {
                    val loggingIntercepter = HttpLoggingInterceptor()
                    loggingIntercepter.level = HttpLoggingInterceptor.Level.BODY
                    okHttpClient!!.addInterceptor(loggingIntercepter)
                }
                okHttpClient!!.connectTimeout(15, TimeUnit.MINUTES)
                okHttpClient.readTimeout(15, TimeUnit.MINUTES)
                retrofit1 = Retrofit.Builder()
                    .addConverterFactory(GsonConverterFactory.create())
                    .baseUrl(BASE_URL).client(okHttpClient.build()).build()
            } catch (e: java.lang.Exception) {
            }
//             }
            return retrofit1
        }

    }

}