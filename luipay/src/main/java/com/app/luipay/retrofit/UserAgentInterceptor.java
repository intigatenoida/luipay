package com.app.luipay.retrofit;

import android.content.Context;
import android.text.TextUtils;

import com.app.luipay.request.LoginRequest;
import com.app.luipay.response.otpresponse.otp;
import com.app.luipay.utils.CommonUtils;
import com.app.luipay.utils.CustomPassword;
import com.app.luipay.utils.Prefutils;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.concurrent.TimeUnit;

import okhttp3.Authenticator;
import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.Route;
import okhttp3.logging.HttpLoggingInterceptor;
import okio.Buffer;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 *
 */
public class UserAgentInterceptor implements Interceptor, Authenticator {
    private final String TAG = "UserAgentInterceptor";

    private Context context;
    private static final Charset UTF8 = Charset.forName("UTF-8");

    private static boolean isRunning = false;

    //    private int chk = 0;
    boolean chk = true;

    Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public UserAgentInterceptor(Context context, OkHttpClient.Builder builder) {

        this.context = context;
        builder.authenticator(this);
    }

    public Chain chain;

    @Override
    public Response intercept(Chain chain) throws IOException {

        this.chain = chain;
        Request originalRequest = chain.request();
        Request.Builder requestWithUserAgent = originalRequest.newBuilder();

        if (context != null) {
            requestWithUserAgent.removeHeader("AUTHORIZATION");
            requestWithUserAgent.addHeader("AUTHORIZATION", "Bearer " + Prefutils.Companion.getFromPrefs(context, Prefutils.Companion.getACCESS_TOKEN()));
        }


        if (originalRequest != null) {
            RequestBody requestBody = originalRequest.body();
            if (requestBody != null) {


                Buffer buffer = new Buffer();
                requestBody.writeTo(buffer);

                Charset charset = UTF8;
                MediaType contentType = requestBody.contentType();
                if (contentType != null) {
                    charset = contentType.charset(UTF8);
                }

            }
        }
        okhttp3.Request request = requestWithUserAgent
                .build();
        Response response = null;
        try {
            response = chain.proceed(request);
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
        return response;
    }


    @Override
    public Request authenticate(Route route, Response response) throws IOException {

        String token = Prefutils.Companion.getFromPrefs(context, Prefutils.Companion.getACCESS_TOKEN());
        if (!isRunning && !TextUtils.isEmpty(token)) {
            isRunning = true;
            String refreshToken = refreshToken();
            isRunning = false;
            if (refreshToken == null) {
                refreshToken = "";
                return null;
            }

            return response.request().newBuilder()
                    .removeHeader("AUTHORIZATION")
                    .addHeader("AUTHORIZATION", refreshToken)
                    .build();
        } else {
            return null;
        }

    }


    synchronized private String refreshToken() {

        OkHttpClient.Builder httpClient;
        httpClient = new OkHttpClient.Builder();
        httpClient.connectTimeout(15, TimeUnit.MINUTES);
        httpClient.readTimeout(15, TimeUnit.MINUTES);

        // httpClient.addInterceptor( new UserAgentInterceptor(OneAdSdk.getFun1990(context),context));
        if (CommonUtils.Companion.isDebug()) {
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);
            httpClient.addInterceptor(logging);

        }

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ApiClient.Companion.getBASE_URL())
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient.build())
                .build();


        ApiInterface authApiHelper = retrofit.create(ApiInterface.class);

        String token = null;
        try {
            LoginRequest request = new LoginRequest();
            request.setMobileNo(Prefutils.Companion.getFromPrefs(context, Prefutils.MOBILE));
            request.setPassword(Prefutils.Companion.getFromPrefs(context, Prefutils.PASSWORD));
            request.setCountryCodeId(1);
            request.setDeviceNotificationID("");
            request.setLanguageTypeId(1);
            request.setServiceAuthenticationToken(ApiClient.Companion.getAndroid_token());
            Call<otp> call = authApiHelper.loginWithMobileNo(request, "application/json");
            otp response = call.execute().body();
            if (response != null && TextUtils.isEmpty(response.getResponseStatus()) && response.getResponseStatus().equals("1")) {
                Prefutils.Companion.saveToPrefs(context, Prefutils.Companion.getACCESS_TOKEN(), response.getResponse().getAccessToken().getAccessToken());
                token = response.getResponse().getAccessToken().getAccessToken();
            }
        } catch (Exception e) {

        } finally {
            return token;
        }
    }
}

