package com.app.luipay.retrofit

import com.app.luipay.dashboard.model.profile.ProfileResponse
import com.app.luipay.history.responses.HistoryResponse
import com.app.luipay.notifications.reponses.MoneyRequListResponse
import com.app.luipay.notifications.requests.MoneyTransferActionRequest
import com.app.luipay.profile.modals.UpdaetEmailRequest
import com.app.luipay.request.*
import com.app.luipay.response.AppStatusResponse.AppStatusReponse
import com.app.luipay.response.BankListResponse.BankListResponse
import com.app.luipay.response.BaseResponse
import com.app.luipay.response.RecentTranResponse
import com.app.luipay.response.SearchContactResponse.SearchContactResponse
import com.app.luipay.response.WalletBalanceRespons
import com.app.luipay.response.notification.NotificationResposne
import com.app.luipay.response.otpresponse.otp
import com.app.luipay.response.profileImageResponse.ProfilePictureImage
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.http.*

public interface ApiInterface {

    @POST("api/account/LoginWithMobileNo")
    fun loginWithMobileNo(@Body request: LoginRequest,@Header("Content-Type") contentType: String): Call<otp>

    @POST("api/account/UserRegistration")
    fun UserRegistration(@Body request: SignupRequest,@Header("Content-Type") contentType: String): Call<BaseResponse>

    @POST("api/account/ResendOtpForSignUp")
    fun ResendOtpForSignUp(@Body request: ResendotpRequest,@Header("Content-Type") contentType: String): Call<BaseResponse>


    @POST("api/account/CheckSignUpOtp")
    fun CheckSignUpOtp(@Body request: OtpRequest,@Header("Content-Type") contentType: String): Call<otp>

    @Multipart
    @POST("upload/UploadKycDocument")
    fun uploadKyC(
        @Part image: MultipartBody.Part, @Part("LanguageTypeId") LanguageTypeId: RequestBody, @Part(
            "UserIdentityProofId"
        ) UserIdentityProofId: RequestBody, @Part("ServiceAuthenticationToken") ServiceAuthenticationToken: RequestBody
    ): Call<BaseResponse>

    @POST("profile/Setting/GetNotificationList")
    fun GetNotificationList(@Body request: GetNotoficationListRequest, @Header("Content-Type") contentType: String): Call<NotificationResposne>


    @POST("profile/Setting/ChangePassword")
    fun changePassword(@Body appStatusRequest: ChangePasswordRequest, @Header("Content-Type") contentType: String): Call<BaseResponse>


    @POST("profile/Setting/GetGiftCardList")
    fun getGiftList(@Body appStatusRequest: GiftListRequest, @Header("Content-Type") contentType: String): Call<BaseResponse>



    @Multipart
    @POST("upload/UploadProfilePicture")
    fun uploadProfilePic(
        @Part image: MultipartBody.Part, @Part("LanguageTypeId") LanguageTypeId: RequestBody, @Part("ServiceAuthenticationToken") ServiceAuthenticationToken: RequestBody
    ): Call<ProfilePictureImage>


    @POST("profile/Contact/GetMyFriendList")
    fun GetMyFriendList(@Body request : BaseRequest, @Header("Content-Type") contentType: String): Call<BaseResponse>

    @POST("profile/Contact/GetPendingFriendRequestList")
    fun GetPendingFriendRequestList(@Body request : BaseRequest, @Header("Content-Type") contentType: String): Call<BaseResponse>

    @POST("profile/Contact/FriendRequestAction")
    fun FriendRequestAction(@Body request : RequestMoney, @Header("Content-Type") contentType: String): Call<BaseResponse>


    @POST("api/account/GetResponseMessages")
    fun GetResponseMessages(@Body request : BaseRequest, @Header("Content-Type") contentType: String): Call<AppStatusReponse>


    @Multipart
    @POST("bank/AddMoney")
    fun addMoney(@Part file: MultipartBody.Part?, @PartMap partMap: Map<String, @JvmSuppressWildcards RequestBody>): Call<BaseResponse>

    @Multipart
    @POST("bank/AddMoney")
    fun addMoneyWithOutFile(@PartMap partMap: Map<String, @JvmSuppressWildcards RequestBody>): Call<BaseResponse>


    @POST("trans/BankAccount/GetUserCardList")
    fun getUserCardList(@Body appStatusRequest: ServiceAuthenticationToken, @Header("Content-Type") contentType: String): Call<BaseResponse>

    @POST("profile/Dashboard/GetWalletbalance")
    fun getWalletBalance(@Body appStatusRequest: ServiceAuthenticationToken, @Header("Content-Type") contentType: String): Call<WalletBalanceRespons>


    @POST("trans/Request/MoneyRequest")
    fun moneyRequest(@Body appStatusRequest: MoneyRequest, @Header("Content-Type") contentType: String): Call<BaseResponse>

    @POST("profile/Contact/SearchUser")
    fun SearchContact(@Body appStatusRequest: SearchContactRequest, @Header("Content-Type") contentType: String): Call<SearchContactResponse>


    @POST("trans/Transaction/MoneyTransfer")
    fun sendMoney(@Body appStatusRequest: MoneyTransferRequest, @Header("Content-Type") contentType: String): Call<BaseResponse>

    @POST("trans/Request/MoneyRequestAction")
    fun moneyRequestAction(@Body request: MoneyTransferActionRequest, @Header("Content-Type") contentType: String): Call<BaseResponse>

    @POST(" trans/Global/GetBankList")
    fun getBankList(@Body appStatusRequest: ServiceAuthenticationToken, @Header("Content-Type") contentType: String): Call<BankListResponse>

    //-> Get Transaction History ( Dashboard)
    @POST("trans/Transaction/GetTransactionHistory")
    fun getTransactionHistory(@Body appStatusRequest: ServiceAuthenticationToken, @Header("Content-Type") contentType: String): Call<HistoryResponse>


    //--> Get Recent Transaction ( Dashboard)
    @POST("trans/Transaction/GetRecentTransaction")
    fun getRecentTransaction(@Body appStatusRequest: ServiceAuthenticationToken, @Header("Content-Type") contentType: String): Call<RecentTranResponse>


    @POST("trans/Request/GetMoneyRequestList")
    fun getMoneyRequestList(@Body appStatusRequest: ServiceAuthenticationToken, @Header("Content-Type") contentType: String): Call<MoneyRequListResponse>

    //--> Get User Profile Information
    @POST("profile/UserProfile/GetPersonalInformation")
    fun getUserProfileInformation(@Body baseRequest: BaseRequest, @Header("Content-Type") contentType: String): Call<ProfileResponse>


    @POST("profile/Setting/ChangePassword")
    fun ChangePassword(@Body baseRequest: ChangePasswordRequest, @Header("Content-Type") contentType: String): Call<BaseResponse>


    //--> Udpate Email Id
    @POST("profile/UserProfile/SetUserEmailAddress")
    fun setUserEmailID(@Body updaetEmailRequest: UpdaetEmailRequest, @Header("Content-Type") contentType: String): Call<BaseResponse>


}