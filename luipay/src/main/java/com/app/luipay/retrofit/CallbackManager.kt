package com.app.luipay.retrofit

import com.app.luipay.response.BaseResponse
import com.app.luipay.utils.CommonUtils
import com.google.gson.GsonBuilder
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.BufferedReader
import java.io.File
import java.io.FileReader
import java.io.IOException
import java.net.UnknownHostException

abstract class CallbackManager<T> : Callback<T> {

    override fun onFailure(call: Call<T>, t: Throwable) {
        if (call.isCanceled) {
            cancelRequest(call, 0)
        } else {
            if (t is UnknownHostException) {
                onError("Unable to connect to server.")
            } else {

                onError("Unable to connect to server. Try after sometime.")
            }
        }
    }

    override fun onResponse(call: Call<T>, response: Response<T>) {
        if (response.isSuccessful) {
            try {
            val responseNew = response.body() as BaseResponse
            CommonUtils.checkStatusMessage(responseNew.messageKey)


                val jsonObject = readStatusCodeFromFile(responseNew.responseStatus.toInt())
                if (jsonObject != null) {
                    when {
                        responseNew.responseStatus.toInt() == 3 && response.raw().request.url.toString()
                            .contains(
                                "profile/Contact/SearchUser"
                            ) -> {
                            onSuccess(response.body(), "")
                        }
                        jsonObject.getInt("StatusCode") == 1 -> {
                            onSuccess(response.body(), "")
                        }
                        else -> {
                            onFailure(jsonObject.getString("MessageDescription"))
                        }
                    }
                } else {
                    onError("")
                }

            } catch (e: Exception) {
                onSuccess(response.body(), "")
            }
        }
        /*else {
            try {
                val strTemp = response.errorBody()!!.string()
                val responseNew = GsonBuilder().create().fromJson(strTemp, BaseResponse::class.java)
                CommonUtils.checkStatusMessage(responseNew.messageKey)
                val jsonObject = readStatusCodeFromFile(responseNew.responseStatus.toInt())
                if (jsonObject != null) {
                    onFailure(jsonObject.getString("MessageDescription"))
                } else {
                    onFailure(response.message())
                }
            } catch (e: Exception) {
                onFailure(response.message())
            }

        }*/
    }

    @Throws(IOException::class)
    open fun readStatusCodeFromFile(status: Int): JSONObject? {
        var br: BufferedReader? = null
        val file =
            File("data/data/com.app.luipay/responsecode/rescode.text")
        val data = StringBuilder()
        try {
            br = BufferedReader(FileReader(file))
            var line: String?
            while (br.readLine().also { line = it } != null) {
                data.append(line)
                data.append('\n')
            }
        } catch (e: IOException) {
            e.printStackTrace()
        } finally {
            br!!.close()
        }
        return try {
            val jsonObject = JSONObject(data.toString())
            val jsonArray = jsonObject.getJSONArray("ResponseCollection")
            for (i in 0 until jsonArray.length()) {
                val statusCode = jsonArray.getJSONObject(i).getInt("StatusCode")
                if (status == statusCode) {
                    return jsonArray.getJSONObject(i)
                }
            }
            null
        } catch (e: JSONException) {
//            CommonUtils.showLog("error at parsing key", e.message)
            null
        }
    }


    abstract fun onSuccess(any: T?, message: String)
    abstract fun onFailure(message: String)
    abstract fun onError(error: String)

    open fun cancelRequest(call: Call<*>?, errorCode: Int) {

    }
}