package com.app.luipay.notifications

import android.app.Activity
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import com.app.luipay.R
import com.app.luipay.base.BaseFragment
import com.app.luipay.notifications.adapters.NotificationAdapter
import com.google.android.material.tabs.TabLayout
import kotlinx.android.synthetic.main.layout_fragment_notification.view.*


class NotificationFragment : BaseFragment() {

    private lateinit var rootView: View
    private lateinit var adapter: NotificationAdapter

    companion object {
        fun newInstance() = NotificationFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (!::rootView.isInitialized) {
            rootView = inflater.inflate(R.layout.layout_fragment_notification, container, false)
            initUI()
        }
        return rootView
    }

    private fun initUI() {
        rootView.ivBack.setOnClickListener {
            activity!!.finish()
        }

        adapter = NotificationAdapter(context!!, childFragmentManager)
        rootView.vpg_main.adapter = adapter
        rootView.tb_main.setupWithViewPager(rootView.vpg_main)

        perparesTabs()
    }

    private fun perparesTabs() {
        addTab(
            rootView.tb_main.getTabAt(0),
            getString(R.string.requests),
            Color.WHITE
        )

        addTab(
            rootView.tb_main.getTabAt(1),
            getString(R.string.notifications),
            ContextCompat.getColor(context!!,R.color.tab_un_selected)
        )

        rootView.tb_main.addOnTabSelectedListener(object :
            TabLayout.OnTabSelectedListener {
            override fun onTabReselected(p0: TabLayout.Tab?) {

            }

            override fun onTabUnselected(p0: TabLayout.Tab?) {
                changeTabAppearance(p0,ContextCompat.getColor(context!!,R.color.tab_un_selected))
            }

            override fun onTabSelected(p0: TabLayout.Tab?) {
                if (p0 == null) {
                    return
                }
                changeTabAppearance(p0,Color.WHITE)
            }

        })
        wrapTabIndicatorToTitle(rootView.tb_main, 0, 0)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity!!.setResult(Activity.RESULT_CANCELED)
    }

}