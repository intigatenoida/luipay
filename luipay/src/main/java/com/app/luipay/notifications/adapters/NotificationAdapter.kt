package com.app.luipay.notifications.adapters

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.app.luipay.R
import com.app.luipay.notifications.RequestsFragment
import com.app.luipay.notifications.SubNotificationsFragment
import com.app.luipay.utils.SmartFragmentStatePagerAdapter


class NotificationAdapter(fn: FragmentManager) :
    SmartFragmentStatePagerAdapter(fn) {

    private lateinit var context: Context

    constructor(context: Context, fm: FragmentManager) : this(fm) {
        this.context = context
    }

    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> {
                RequestsFragment.newInstance()
            }
            else -> {
                SubNotificationsFragment.newInstance()
            }
        }
    }

    override fun getCount(): Int {
        return 2
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return ""
    }

}