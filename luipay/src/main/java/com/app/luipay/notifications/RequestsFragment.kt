package com.app.luipay.notifications

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.luipay.R
import com.app.luipay.base.BaseFragment
import com.app.luipay.notifications.adapters.RequestsAdapter
import com.app.luipay.notifications.reponses.MoneyRequListResponse
import com.app.luipay.notifications.reponses.MoneyRequestData
import com.app.luipay.notifications.requests.MoneyTransferActionRequest
import com.app.luipay.notifications.vmodels.RequestViewModel
import com.app.luipay.prompt.CustomPrompt
import com.app.luipay.request.ServiceAuthenticationToken
import com.app.luipay.response.BaseResponse
import com.app.luipay.retrofit.ApiClient
import com.app.luipay.retrofit.ApiInterface
import com.app.luipay.retrofit.CallbackManager
import com.app.luipay.utils.Appconstant
import com.app.luipay.utils.CommonUtils
import com.app.luipay.utils.interfaces.DialogFragmentClickListener
import kotlinx.android.synthetic.main.layout_fragment_requests.view.*
import retrofit2.Call


class RequestsFragment : BaseFragment(), RequestsAdapter.Communicator, DialogFragmentClickListener {

    private lateinit var rootView: View
    private lateinit var adapter: RequestsAdapter
    private lateinit var model: RequestViewModel

    private var isProgressConsider: Boolean = false
    private lateinit var call: Call<MoneyRequListResponse>

    private var requestId: Int = 0
    private var flag: String = ""

    companion object {
        fun newInstance() = RequestsFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (!::rootView.isInitialized) {
            rootView = inflater.inflate(R.layout.layout_fragment_requests, container, false)
            initUI()
        }
        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        model = ViewModelProviders.of(this)[RequestViewModel::class.java]
        model.progressLiveData.observe(viewLifecycleOwner, Observer<Boolean> {
            if (isProgressConsider && it) {
                showProcessDialog()
            } else {
                hideProcessDialog()
            }
        })
    }

    override fun onStop() {
        super.onStop()
        hideProcessDialog()
        if (::call.isInitialized) {
            call.cancel()
        }
    }

    override fun onResume() {
        super.onResume()
        if (!::call.isInitialized ||
            !call.isExecuted
        ) {
            isProgressConsider = true
            requestMoneyList()
        }
    }

    private fun initUI() {
        setAdapter()
        rootView.swp_main.setOnRefreshListener {
            isProgressConsider = false
            requestMoneyList()
        }
    }

    private fun setAdapter() {
        val layoutManager = LinearLayoutManager(context!!, LinearLayoutManager.VERTICAL, false)
        rootView.rv_main.layoutManager = layoutManager

        adapter = RequestsAdapter(context!!, this)
        rootView.rv_main.adapter = adapter
    }

    private fun requestMoneyList() {
        if (!isOnline()) {
            CommonUtils.showToast(
                activity!!,
                getString(R.string.turn_on_internet)
            )
            return
        }

        model.toggleProgress(true)

        val helper =
            ApiClient.getClient(activity!!).create(ApiInterface::class.java)
        val request = ServiceAuthenticationToken()
        request.serviceAuthenticationToken = ApiClient.android_token
        call = helper.getMoneyRequestList(request, Appconstant.API_HEADER)
        call.enqueue(object : CallbackManager<MoneyRequListResponse>() {
            override fun onSuccess(any: MoneyRequListResponse?, message: String) {
                model.toggleProgress(false)
                if (rootView.swp_main.isRefreshing) {
                    rootView.swp_main.isRefreshing = false
                }
                if (any?.response == null) {
                    return
                }
                val data = ArrayList<MoneyRequestData>()
                if (!any.response?.receivedList.isNullOrEmpty()) {
                    for (obj in any.response?.receivedList!!) {
                        obj.isReceived = true
                    }
                    data.addAll(any.response!!.receivedList)
                }
                if (!any.response?.sendList.isNullOrEmpty()) {
                    data.addAll(any.response!!.sendList)
                }

                if (data.isNotEmpty()) {
                    rootView.tv_no_history.visibility = GONE
                    adapter.updateData(data)
                } else {
                    rootView.tv_no_history.visibility = VISIBLE
                }

            }

            override fun onFailure(message: String) {
                model.toggleProgress(false)
                if (rootView.swp_main.isRefreshing) {
                    rootView.swp_main.isRefreshing = false
                }
                rootView.tv_no_history.visibility = VISIBLE
            }

            override fun onError(error: String) {
                model.toggleProgress(false)
                if (rootView.swp_main.isRefreshing) {
                    rootView.swp_main.isRefreshing = false
                }
                rootView.tv_no_history.visibility = VISIBLE
            }

            override fun cancelRequest(call: Call<*>?, errorCode: Int) {
                super.cancelRequest(call, errorCode)
                model.toggleProgress(false)
                if (rootView.swp_main.isRefreshing) {
                    rootView.swp_main.isRefreshing = false
                }
                rootView.tv_no_history.visibility = VISIBLE
            }

        })

    }

    private fun updateRequestStatus(requestId: Int, flag: String) {
        if (!isOnline()) {
            CommonUtils.showToast(
                activity!!,
                getString(R.string.turn_on_internet)
            )
            return
        }

        model.toggleProgress(true)

        val helper =
            ApiClient.getClient(activity!!).create(ApiInterface::class.java)
        val request = MoneyTransferActionRequest()
        request.flag = flag
        request.requestId = requestId.toString()
        val call = helper.moneyRequestAction(request, Appconstant.API_HEADER)
        call.enqueue(object : CallbackManager<BaseResponse>() {
            override fun onSuccess(any: BaseResponse?, message: String) {
                model.toggleProgress(false)
                isProgressConsider = false
                requestMoneyList()
            }

            override fun onFailure(message: String) {
                model.toggleProgress(false)
                if (message.isNotEmpty()) {
                    showMessage(message)
                }
            }

            override fun onError(error: String) {
                model.toggleProgress(false)
            }

            override fun cancelRequest(call: Call<*>?, errorCode: Int) {
                super.cancelRequest(call, errorCode)
                model.toggleProgress(false)
            }

        })

    }

    override fun updateStatus(requestId: Int, flag: String) {
        this@RequestsFragment.flag = flag
        this@RequestsFragment.requestId = requestId
        when (flag) {
            "ACCT" -> {
                showPrompt(
                    CustomPrompt.getInstance(
                        1,
                        "You want to approve this request ?",
                        getString(R.string.yes),
                        getString(R.string.no),
                        this@RequestsFragment
                    )
                )
            }

            "RJCT" -> {
                showPrompt(
                    CustomPrompt.getInstance(
                        1,
                        "You want to reject this request ?",
                        getString(R.string.yes),
                        getString(R.string.no),
                        this@RequestsFragment
                    )
                )
            }
        }


    }

    override fun onPositiveButtonClick(dialog: Dialog) {
        dialog.dismiss()
    }

    override fun onNegativeButtonClick(dialog: Dialog) {
        dialog.dismiss()
    }

    override fun onDialogDataReceived(obj: Any, bundle: Bundle?) {
        if (bundle == null) {
            return
        }

        when (obj) {
            is CustomPrompt -> {
                if (requestId > 0 && flag.isNotEmpty()) {
                    updateRequestStatus(requestId, flag)
                }
            }
        }
    }
}
