package com.app.luipay.notifications.reponses

import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName

@Keep
data class MoneyRequestData(
    @SerializedName("Amount")
    val amount: String?,
    @SerializedName("Email")
    val email: String?,
    @SerializedName("Id")
    val id: String?,
    @SerializedName("Memo")
    val memo: String?,
    @SerializedName("Name")
    val name: String?,
    @SerializedName("PhoneNo")
    val phoneNo: String?,
    @SerializedName("ProfilePictureUrl")
    val profilePictureUrl: String?,
    @SerializedName("RejectReason")
    val rejectReason: String?,
    @SerializedName("RequestId")
    val requestId: Int = 0,
    @SerializedName("TransactionDate")
    val transactionDate: String?,
    @SerializedName("TransactionStatus")
    val transactionStatus: String?,
    @SerializedName("TransactionStatusId")
    val transactionStatusId: Int = 0,

    var isReceived:Boolean = false
)