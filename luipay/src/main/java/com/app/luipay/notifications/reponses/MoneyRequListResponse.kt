package com.app.luipay.notifications.reponses

import androidx.annotation.Keep
import com.app.luipay.response.BaseResponse
import com.google.gson.annotations.SerializedName

@Keep
data class MoneyRequListResponse (

    @SerializedName("Response")
    var response:RequestLists? = null

):BaseResponse()