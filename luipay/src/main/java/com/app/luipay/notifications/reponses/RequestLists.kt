package com.app.luipay.notifications.reponses

import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName

@Keep
data class RequestLists(

    @SerializedName("ReceivedMoneyRequest")
    var receivedList: ArrayList<MoneyRequestData> = ArrayList(),

    @SerializedName("SentMoneyRequest")
    var sendList:  ArrayList<MoneyRequestData> = ArrayList()

)