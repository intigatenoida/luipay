package com.app.luipay.notifications.requests

import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName

@Keep
class


MoneyTransferActionRequest {

    @SerializedName("RequestId")
    var requestId:String = ""

    @SerializedName("Flag")
    var flag:String = ""

    @SerializedName("ServiceAuthenticationToken")
    var serviceAuthenticationToken:String = "a40be959-d225-4c10-86e6-04419d83fd26"

}