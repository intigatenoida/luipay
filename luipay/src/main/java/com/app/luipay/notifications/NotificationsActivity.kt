package com.app.luipay.notifications

import android.os.Bundle
import com.app.luipay.R
import com.app.luipay.base.BaseActivity

class NotificationsActivity:BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_notifications)
        LoadFragment(NotificationFragment.newInstance(),"",R.id.mainContainer)
    }

}