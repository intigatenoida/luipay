package com.app.luipay.notifications.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.app.luipay.R
import com.app.luipay.response.notification.Notification
import com.app.luipay.utils.CommonUtils
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.layout_item_requests.view.*
import kotlinx.android.synthetic.main.layout_item_sub_notifications.view.*
import kotlinx.android.synthetic.main.layout_item_sub_notifications.view.tv_data
import kotlinx.android.synthetic.main.layout_item_sub_notifications.view.tv_description
import java.util.ArrayList


class SubNotificationsAdapter() : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private lateinit var context: Context
    private lateinit var layoutInflater: LayoutInflater

    class SubNotificationsViewHolder(val view: View) : RecyclerView.ViewHolder(view)
    var  list : ArrayList<Notification> = arrayListOf()

    constructor(context: Context) : this() {
        this.context = context
        layoutInflater = LayoutInflater.from(context)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view =
            layoutInflater.inflate(
                R.layout.layout_item_sub_notifications,
                parent,
                false
            )
        return SubNotificationsViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        val data = list[position]

        //-- description
        holder.itemView.tv_description.text = data.notificationMgs

        //--> time
        //holder.itemView.tv_data.text = data.notificationTime
        if (!data.notificationTime.isNullOrEmpty()) {
            holder.itemView.tv_data.visibility = View.VISIBLE
            holder.itemView.tv_data.text = CommonUtils.convertToLocalTimeZone(data.notificationTime!!)
        } else {
            holder.itemView.tv_data.visibility = View.INVISIBLE
        }

        //--> Image
        CommonUtils.loadImage(context,data.imageUrl,holder.itemView.img_mai,R.drawable.theme_border)

    }

    fun updatelist(mlist: ArrayList<Notification>) {
        list = mlist
        notifyDataSetChanged()
    }
}