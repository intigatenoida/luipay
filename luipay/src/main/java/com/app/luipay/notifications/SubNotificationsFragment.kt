package com.app.luipay.notifications

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.app.luipay.R
import com.app.luipay.base.BaseFragment
import com.app.luipay.notifications.adapters.SubNotificationsAdapter
import com.app.luipay.request.GetNotoficationListRequest
import com.app.luipay.response.BaseResponse
import com.app.luipay.response.notification.NotificationResposne
import com.app.luipay.retrofit.ApiClient
import com.app.luipay.retrofit.ApiInterface
import com.app.luipay.retrofit.CallbackManager
import com.app.luipay.utils.Appconstant
import com.app.luipay.utils.CommonUtils
import com.app.luipay.utils.Prefutils
import kotlinx.android.synthetic.main.layout_fragment_sub_notification.view.*
import retrofit2.Call


class SubNotificationsFragment : BaseFragment() {

    private lateinit var rootView: View
    private lateinit var adapter: SubNotificationsAdapter

    private var pageNumber: Int = 0
    private var isDataAvailable = true
    private var isLoading = true
    private var call: Call<BaseResponse>? = null

    companion object {
        fun newInstance() = SubNotificationsFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (!::rootView.isInitialized) {
            rootView = inflater.inflate(R.layout.layout_fragment_sub_notification, container, false)
            initUI()
        }
        return rootView
    }

    override fun onResume() {
        super.onResume()
        if (call == null ||
            !call!!.isExecuted
        ) {
            initialHit()
        }
    }

    override fun onStop() {
        super.onStop()
        hideProcessDialog()
        if (call != null) {
            call!!.cancel()
        }
    }

    private fun initUI() {
        setAdapter()
        rootView.swp_main.setOnRefreshListener {
            initialHit()
        }
    }

    private fun initialHit() {
        isDataAvailable = true
        pageNumber = 1
        getNotificationList(true)
    }

    private fun setAdapter() {
        val layoutManager = LinearLayoutManager(context!!, LinearLayoutManager.VERTICAL, false)
        rootView.rv_main.layoutManager = layoutManager

        adapter = SubNotificationsAdapter(context!!)
        rootView.rv_main.adapter = adapter

        rootView.rv_main.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                val lastVisibleItem = layoutManager.findLastVisibleItemPosition()
                val totalItem = layoutManager.itemCount
                val tharseHold = 5
                if (!isLoading &&
                    isDataAvailable &&
                    totalItem < (lastVisibleItem + tharseHold)
                ) {
                    getNotificationList(false)
                }
            }
        })
    }

    private fun getNotificationList(isProgressConsider: Boolean) {

        if (!isOnline()) {
            CommonUtils.showToast(
                activity!!,
                getString(R.string.turn_on_internet)
            )
            return
        }

        if (isProgressConsider) {
            showProcessDialog()
        }
        val helper =
            ApiClient.getClient(activity!!).create(ApiInterface::class.java)
        val request = GetNotoficationListRequest();
        request.CurrentPage = pageNumber
        request.LanguageTypeId = Prefutils.getFromPrefsInt(activity!!, Prefutils.API_LANGUAGE)
        request.ServiceAuthenticationToken = Appconstant.API_KEY
        request.RecordsPerPage = 20
        val call = helper.GetNotificationList(request, Appconstant.API_HEADER)
        call.enqueue(object : CallbackManager<NotificationResposne>() {
            override fun onSuccess(any: NotificationResposne?, message: String) {
                hideProcessDialog()
                if (rootView.swp_main.isRefreshing) {
                    rootView.swp_main.isRefreshing = false
                }

                val res = any as NotificationResposne
                val list = res.response.notificationList
                if (list.isNotEmpty()) {
                    if (list.size < 20) {
                        isDataAvailable = false
                    }
                    adapter.updatelist(list)
                } else {
                    isDataAvailable = false
                    rootView.tv_no_notification.visibility = View.VISIBLE
                }
            }

            override fun onFailure(message: String) {
                rootView.tv_no_notification.visibility = View.VISIBLE
                if (rootView.swp_main.isRefreshing) {
                    rootView.swp_main.isRefreshing = false
                }
                hideProcessDialog()
            }

            override fun onError(error: String) {
                rootView.tv_no_notification.visibility = View.VISIBLE
                if (rootView.swp_main.isRefreshing) {
                    rootView.swp_main.isRefreshing = false
                }
                hideProcessDialog()
            }

            override fun cancelRequest(call: Call<*>?, errorCode: Int) {
                super.cancelRequest(call, errorCode)
                if (rootView.swp_main.isRefreshing) {
                    rootView.swp_main.isRefreshing = false
                }
                hideProcessDialog()
                rootView.tv_no_notification.visibility = View.VISIBLE
            }

        })

    }


}