package com.app.luipay.notifications.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.app.luipay.R
import com.app.luipay.notifications.reponses.MoneyRequestData
import com.app.luipay.utils.CommonUtils
import kotlinx.android.synthetic.main.layout_item_requests.view.*


class RequestsAdapter() : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private lateinit var context: Context
    private lateinit var layoutInflater: LayoutInflater
    private lateinit var listener: Communicator

    private var data: ArrayList<MoneyRequestData> = ArrayList()

    class RequestViewHolder(val view: View) : RecyclerView.ViewHolder(view)

    constructor(context: Context, listener: Communicator) : this() {
        this.context = context
        this.listener = listener
        layoutInflater = LayoutInflater.from(context)
    }

    fun updateData(data: ArrayList<MoneyRequestData>) {
        this.data.clear()
        this.data.addAll(data)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view =
            layoutInflater.inflate(
                R.layout.layout_item_requests,
                parent,
                false
            )
        return RequestViewHolder(view)
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        val temp = data[position]

        //3 pending
        //4 completed
        //5 rejected
        if (temp.isReceived) {
            holder.itemView.tv_description.text =
                "LuiPay user: ${temp.name}\nrequested MVR ${temp.amount}"

            holder.itemView.img_tick.visibility = GONE
            holder.itemView.img_cross.visibility = GONE

            when (temp.transactionStatusId) {

                3 -> {
                    holder.itemView.tv_pending_approval.visibility = GONE
                    holder.itemView.img_tick.visibility = VISIBLE
                    holder.itemView.img_cross.visibility = VISIBLE
                    holder.itemView.img_tick.setOnClickListener {
                        listener.updateStatus(temp.requestId, "ACCT")
                    }
                    holder.itemView.img_cross.setOnClickListener {
                        listener.updateStatus(temp.requestId, "RJCT")
                    }
                }

                5 -> {
                    holder.itemView.tv_pending_approval.visibility = VISIBLE
                    holder.itemView.tv_pending_approval.text = "rejected"
                }

                4 -> {
                    holder.itemView.tv_pending_approval.visibility = VISIBLE
                    holder.itemView.tv_pending_approval.text = "completed"
                }
            }

        } else {
            holder.itemView.tv_description.text =
                "Your: request MVR\n${temp.amount} from Luipay user"

            holder.itemView.img_tick.visibility = GONE
            holder.itemView.img_cross.visibility = GONE
            holder.itemView.tv_pending_approval.visibility = VISIBLE

            when (temp.transactionStatusId) {

                3 -> {
                    holder.itemView.tv_pending_approval.visibility = VISIBLE
                    holder.itemView.tv_pending_approval.text = "pending approval"
                }

                5 -> {
                    holder.itemView.tv_pending_approval.visibility = VISIBLE
                    holder.itemView.tv_pending_approval.text = "rejected"
                }

                4 -> {
                    holder.itemView.tv_pending_approval.visibility = VISIBLE
                    holder.itemView.tv_pending_approval.text = "completed"
                }

            }
        }

        //holder.itemView.tv_data.text = "${temp.transactionDate}"
        if (!temp.transactionDate.isNullOrEmpty()) {
            holder.itemView.tv_data.visibility = VISIBLE
            holder.itemView.tv_data.text = CommonUtils.convertToLocalTimeZone(temp.transactionDate)
        } else {
            holder.itemView.tv_data.visibility = View.INVISIBLE
        }

    }

    interface Communicator {
        fun updateStatus(requestId: Int, flag: String)
    }
}