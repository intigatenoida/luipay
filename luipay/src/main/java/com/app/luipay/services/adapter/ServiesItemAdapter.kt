package com.app.luipay.services.adapter

import android.content.Context
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.app.luipay.R
import com.app.luipay.dashboard.DashboardactionModel
import com.app.luipay.dashboard.onClickDashboard
import kotlinx.android.synthetic.main.item_bill_payment.view.*
import kotlinx.android.synthetic.main.item_service.view.*

class ServiesItemAdapter(
    val context: Context,
    val list: ArrayList<DashboardactionModel>,
    val onClickDashboard: onClickDashboard
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    class ViewHolder(val itemView: View) : RecyclerView.ViewHolder(itemView) {}


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.item_service,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        val data = list[position]
        holder.itemView.ivService.setImageResource(data.drawableId?:0)
        holder.itemView.setOnClickListener {
//            onClickDashboard.onClickDashboard(data.actionType,list)
        }

        if (data.isPadding == true) {

            holder.itemView.ivService.setPaddingRelative(dpToPx(10),dpToPx(0),dpToPx(10),dpToPx(0))

        } else {

            holder.itemView.ivService.setPaddingRelative(dpToPx(0),dpToPx(0),dpToPx(0),dpToPx(0))

        }

    }

    private fun dpToPx(dp: Int): Int {
        val displayMetrics = context!!.resources.displayMetrics
        return Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT))
    }
}