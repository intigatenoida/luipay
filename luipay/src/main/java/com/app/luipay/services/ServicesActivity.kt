package com.app.luipay.services

import android.os.Bundle
import com.app.luipay.R
import com.app.luipay.base.BaseActivity
import com.app.luipay.dashboard.DashboardactionModel
import com.app.luipay.utils.Appconstant

class ServicesActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_services)
        val fragment = ServicesFragment.newInstance(intent?.extras)
        fragment.list =
            intent?.getParcelableArrayListExtra<DashboardactionModel>(Appconstant.SERVICES_LIST)
                    as ArrayList<DashboardactionModel>? ?: arrayListOf()
        LoadFragment(fragment, this@ServicesActivity.javaClass.name, R.id.main_container)
    }

}
