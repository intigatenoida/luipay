package com.app.luipay.services

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.app.luipay.R
import com.app.luipay.base.BaseActivity

class PaymentActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_payment)

        LoadFragment(PaymentFragment.newInstance(),this@PaymentActivity.localClassName,R.id.main_container)
    }
}
