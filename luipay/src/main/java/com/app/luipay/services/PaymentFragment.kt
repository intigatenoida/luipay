package com.app.luipay.services

import android.R.array
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.luipay.R
import com.app.luipay.services.adapter.ServiceAccountAdapter
import com.app.luipay.services.adapter.SpinnerAdapter
import kotlinx.android.synthetic.main.fragment_payment.view.*
import java.util.*


/**
 * A simple [Fragment] subclass.
 */
class PaymentFragment : Fragment() {

    lateinit var rootView: View

    companion object {
        fun newInstance(): PaymentFragment {
            return PaymentFragment()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_payment, container, false)

        //back press
        rootView.ivBack.setOnClickListener {
            activity!!.finish()
        }

        initUI()

        return rootView
    }

    private fun initUI() {
        //setting recyclerview adapterr
        rootView.rcv_servicesadded.layoutManager =
            LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)

        val verticalDecoration = DividerItemDecoration(
            context!!,
            DividerItemDecoration.VERTICAL
        )
        val verticalDivider =
            ContextCompat.getDrawable(context!!, R.drawable.divider_16_transparent)
        verticalDecoration.setDrawable(verticalDivider!!)
        rootView.rcv_servicesadded.addItemDecoration(verticalDecoration)

        rootView.rcv_servicesadded.adapter = ServiceAccountAdapter(activity!!)

        val adapter =
            SpinnerAdapter(activity!!, getData(resources.getStringArray(R.array.servicesProviders)))
        rootView.spn_serviceprovider.adapter = adapter
    }

    private fun getData(stringArray: Array<String>): ArrayList<String> {
        val temp = ArrayList<String>()
        for (text in stringArray) {
            temp.add(text)
        }
        return temp
    }

}
