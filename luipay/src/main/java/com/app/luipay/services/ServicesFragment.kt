package com.app.luipay.services

import android.graphics.Color
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager

import com.app.luipay.R
import com.app.luipay.base.BaseFragment
import com.app.luipay.dashboard.BillPaymentadapter
import com.app.luipay.dashboard.DashboardactionModel
import com.app.luipay.dashboard.onClickDashboard
import com.app.luipay.services.adapter.ServiesItemAdapter
import com.app.luipay.utils.Appconstant
import kotlinx.android.synthetic.main.fragment_services.view.*

/**
 * A simple [Fragment] subclass.
 * Use the [ServicesFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class ServicesFragment : BaseFragment(), onClickDashboard {

    lateinit var rootView: View
    var list: ArrayList<DashboardactionModel> = arrayListOf()

    companion object {
        fun newInstance(bundle: Bundle?): ServicesFragment {
            val servicesFragment = ServicesFragment()
            servicesFragment.arguments = bundle
            return servicesFragment
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_services, container, false)
        initUI()
        return rootView
    }

    private fun initUI() {
        //setting recyclerview adapter
        rootView.rcv_services.layoutManager =
            LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
        rootView.rcv_services.adapter = ServiesItemAdapter(activity!!, list, this)

        //back press
        rootView.ivBack.setOnClickListener {
            activity!!.finish()
        }

        //-- Account No
        rootView.edt_accountNo.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

                if (p0.toString().isNotEmpty()) {
                    rootView.tvbtnAddProcess.setTextColor(Color.parseColor("#272561"))
                    rootView.tvbtnAddProcess.background =
                        ContextCompat.getDrawable(activity!!, R.drawable.enable_25)

                } else {
                    rootView.tvbtnAddProcess.setTextColor(Color.parseColor("#50272561"))
                    rootView.tvbtnAddProcess.background =
                        ContextCompat.getDrawable(activity!!, R.drawable.disable_25)

                }

            }

        })

    }

    override fun onClickDashboard(action: Int?, list: ArrayList<DashboardactionModel>) {

    }
}
