package com.app.luipay.uploadimage

import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Parcelable
import android.provider.MediaStore
import android.util.Log
import com.app.luipay.R

import java.io.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class UploadImageHelper {

    companion object {
        const val tag = "UploadImageHelper"

        const val TEMPORARY_PROFILE_IMAGE = "profileImage.jpg"


        /*give intent of all the app for gallery & camera*/
        fun getPickImageIntent(context: Context, photoUri: Uri): Intent? {
            var chooserIntent: Intent? = null

            var intentList: MutableList<Intent> = ArrayList()

            val pickIntent =
                Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)

            val takePhotoIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            takePhotoIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri)

            intentList = addIntentsToList(context, intentList, pickIntent)
            intentList = addIntentsToList(context, intentList, takePhotoIntent)

            if (intentList.size > 0) {
                chooserIntent = Intent.createChooser(
                    intentList.removeAt(intentList.size - 1),
                    context.getString(R.string.upload_image)
                )
                chooserIntent!!.putExtra(
                    Intent.EXTRA_INITIAL_INTENTS,
                    intentList.toTypedArray<Parcelable>()
                )
            }
            return chooserIntent
        }

        private fun addIntentsToList(
            context: Context,
            list: MutableList<Intent>,
            intent: Intent
        ): MutableList<Intent> {
            val resInfo = context.packageManager.queryIntentActivities(intent, 0)
            for (resolveInfo in resInfo) {
                val packageName = resolveInfo.activityInfo.packageName
                val targetedIntent = Intent(intent)
                targetedIntent.setPackage(packageName)
                list.add(targetedIntent)
            }
            return list
        }

        fun compressImageFile(
            context: Context,
            compressedPath: String,
            shouldOverride: Boolean = true,
            originalUri: Uri
        ): String {
            var scaledBitmap: Bitmap? = null

            try {
                val (hgt, wdt) = ScalingUtils.getImageHgtWdt(context, originalUri, 720f, 1280f)
                try {
                    val bm = getBitmapFromUri(context, originalUri)
                    Log.d(tag, "original bitmap height${bm?.height} width${bm?.width}")
                    Log.d(tag, "Dynamic height$hgt width$wdt")
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                // Part 1: Decode image
                val unscaledBitmap = ScalingUtils.decodeFile(
                    context,
                    originalUri,
                    wdt,
                    hgt,
                    ScalingUtils.Companion.ScalingLogic.FIT
                )
                if (unscaledBitmap != null) {
                    if (!(unscaledBitmap.width <= 800 && unscaledBitmap.height <= 800)) {
                        // Part 2: Scale image
                        scaledBitmap = ScalingUtils.createScaledBitmap(
                            unscaledBitmap,
                            wdt,
                            hgt,
                            ScalingUtils.Companion.ScalingLogic.FIT
                        )
                    } else {
                        scaledBitmap = unscaledBitmap
                    }
                }

                // Store to tmp file
                val mFolder = File("${context.filesDir}/Images")
                if (!mFolder.exists()) {
                    mFolder.mkdir()
                }

                val tmpFile = File(mFolder, "compressed_pan.png")
                if (tmpFile.exists()) {
                    mFolder.delete()
                }
                var fos: FileOutputStream? = null
                try {
                    fos = FileOutputStream(tmpFile)
                    scaledBitmap?.compress(
                        Bitmap.CompressFormat.PNG,
                        ScalingUtils.getImageQualityPercent(tmpFile),
                        fos
                    )
                    fos.flush()
                    fos.close()
                } catch (e: FileNotFoundException) {
                    e.printStackTrace()
                } catch (e: Exception) {
                    e.printStackTrace()
                }

                var compressedPath = ""
                if (tmpFile.exists() && tmpFile.length() > 0) {
                    compressedPath = tmpFile.absolutePath
                    if (shouldOverride) {
                        val srcFile = File(compressedPath)
                        val result = tmpFile.copyTo(srcFile, true)
                        Log.d(tag, "copied file ${result.absolutePath}")
                        Log.d(tag, "Delete temp file ${tmpFile.delete()}")
                    }
                }

                scaledBitmap?.recycle()

                //return if (shouldOverride) path else compressedPath
                return compressedPath
            } catch (e: Throwable) {
                e.printStackTrace()
            }
            return ""
        }

        fun copyFileFromStream(context: Context, uri: Uri, tempFileName: String): File {
            val target = tempFileReturn(context, tempFileName)
            var inputStream: InputStream? = null
            var outputStream: OutputStream? = null
            try {
                outputStream = target.outputStream()
                inputStream = context.contentResolver.openInputStream(uri)
                val buf = ByteArray(1024)
                var len: Int = 0
                while (inputStream!!.read(buf).also({ len = it }) > 0) {
                    outputStream.write(buf, 0, len)
                }
            } catch (e: java.lang.Exception) {

            } finally {
                outputStream?.close()
                inputStream?.close()
            }
            return target
        }


        @Throws(IOException::class)
        fun getBitmapFromUri(
            context: Context,
            uri: Uri,
            options: BitmapFactory.Options? = null
        ): Bitmap? {
            val parcelFileDescriptor = context.contentResolver.openFileDescriptor(uri, "r")
            val fileDescriptor = parcelFileDescriptor?.fileDescriptor
            val image: Bitmap? = if (options != null)
                BitmapFactory.decodeFileDescriptor(fileDescriptor, null, options)
            else
                BitmapFactory.decodeFileDescriptor(fileDescriptor)
            parcelFileDescriptor?.close()
            return image
        }

        fun getTimestampString(): String {
            val date = Calendar.getInstance()
            return SimpleDateFormat("yyyy_MM_dd hh mm ss", Locale.US).format(date.time)
                .replace(" ", "")
        }

        fun tempFileReturn(context: Context, tempFileName: String): File {
            val file = File(context.filesDir, tempFileName)
            if (file.exists()) {
                file.delete()
            }
            file.createNewFile()
            return file
        }

        fun compressImageFile2(
            context: Context,
            path: String,
            shouldOverride: Boolean = true,
            originalUri: Uri,
            maxHeight: Float = 720f,
            maxWidth: Float = 1280f
        ): String {
            var scaledBitmap: Bitmap? = null

            try {
                val (hgt, wdt) = ScalingUtils.getImageHgtWdt(
                    context,
                    originalUri,
                    maxHeight,
                    maxWidth
                )
                try {
                    val bm = getBitmapFromUri(context, originalUri)
                    Log.d(tag, "original bitmap height${bm?.height} width${bm?.width}")
                    Log.d(tag, "Dynamic height$hgt width$wdt")
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                // Part 1: Decode image
                val unscaledBitmap = ScalingUtils.decodeFile(
                    context,
                    originalUri,
                    wdt,
                    hgt,
                    ScalingUtils.Companion.ScalingLogic.FIT
                )
                if (unscaledBitmap != null) {
                    if (!(unscaledBitmap.width <= 1020 && unscaledBitmap.height <= 1280)) {
                        // Part 2: Scale image
                        scaledBitmap = ScalingUtils.createScaledBitmap(
                            unscaledBitmap,
                            wdt,
                            hgt,
                            ScalingUtils.Companion.ScalingLogic.FIT
                        )
                    } else {
                        scaledBitmap = unscaledBitmap
                    }
                }

                // Store to tmp file
                val mFolder = File("${context.filesDir}/Images")
                if (!mFolder.exists()) {
                    mFolder.mkdir()
                }

                val tmpFile = File(mFolder.absolutePath, "compressed.jpg")

                if (!tmpFile.exists()) {
                    tmpFile.createNewFile()
                }

                var fos: FileOutputStream? = null
                try {
                    fos = FileOutputStream(tmpFile)
                    scaledBitmap?.compress(
                        Bitmap.CompressFormat.JPEG,
                        ScalingUtils.getImageQualityPercent(tmpFile),
                        fos
                    )
                    fos.flush()
                    fos.close()
                } catch (e: FileNotFoundException) {
                    e.printStackTrace()
                } catch (e: Exception) {
                    e.printStackTrace()
                }

                var compressedPath = ""
                if (tmpFile.exists() && tmpFile.length() > 0) {
                    compressedPath = tmpFile.absolutePath
                    if (shouldOverride) {
                        val srcFile = File(compressedPath)
                        val result = tmpFile.copyTo(srcFile, true)
                        Log.d(tag, "copied file ${result.absolutePath}")
                        Log.d(tag, "Delete temp file ${tmpFile.delete()}")
                    }
                }

                scaledBitmap?.recycle()

                return if (shouldOverride) path else compressedPath
            } catch (e: Throwable) {
                e.printStackTrace()
            }
            return ""
        }
    }

}

/*fun copyFile(sourceFile: File): File {
            val folder = File(
                Environment.getExternalStorageDirectory(), UploadImageHelper.LOCAL_STORAGE_APP_ROOT_FOLDER +
                        File.separator +
                        UploadImageHelper.LOCAL_STORAGE_APP_UPLOAD_FOLDER
            )
            if (!folder.exists()) {
                folder.mkdirs()
            }
            val target = File(folder, TEMPORARY_PAN_CARD_FILE_NAME)
            if (target.exists()) {
                target.delete()
            }
            return sourceFile.copyTo(target, true)
        }*/