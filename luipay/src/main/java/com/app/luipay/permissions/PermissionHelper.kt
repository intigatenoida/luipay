package com.app.luipay.permissions

import android.Manifest
import android.content.pm.PackageManager
import android.os.Build
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import com.app.luipay.base.BaseFragment

class PermissionHelper {

    companion object {
        val permissions = arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE)
        val cameraPermission = arrayOf(Manifest.permission.CAMERA)

        fun isPermissionsAllowed2(fg: BaseFragment, permissions: Array<String>, shouldRequestIfNotAllowed: Boolean = false, requestCode: Int = -1): Boolean {
            var isGranted = true

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                for (permission in permissions) {
                    isGranted = ContextCompat.checkSelfPermission(fg.context!!, permission) == PackageManager.PERMISSION_GRANTED
                    if (!isGranted)
                        break
                }
            }
            if (!isGranted && shouldRequestIfNotAllowed) {
                if (requestCode.equals(-1))
                    throw RuntimeException("Send request code in third parameter")
                requestRequiredPermissions2(fg, permissions, requestCode)
            }

            return isGranted
        }

        fun requestRequiredPermissions2(fg: BaseFragment, permissions: Array<String>, requestCode: Int) {
            val pendingPermissions: ArrayList<String> = ArrayList()
            permissions.forEachIndexed { index, permission ->
                if (ContextCompat.checkSelfPermission(fg.context!!, permission) == PackageManager.PERMISSION_DENIED)
                    pendingPermissions.add(permission)
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                val array = arrayOfNulls<String>(pendingPermissions.size)
                pendingPermissions.toArray(array)
                fg.requestPermissions(array, requestCode)
            }
        }

        fun isPermissionsAllowed(activity: FragmentActivity, permissions: Array<String>, shouldRequestIfNotAllowed: Boolean = false, requestCode: Int = -1): Boolean {
            var isGranted = true

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                for (permission in permissions) {
                    isGranted = ContextCompat.checkSelfPermission(activity, permission) == PackageManager.PERMISSION_GRANTED
                    if (!isGranted)
                        break
                }
            }
            if (!isGranted && shouldRequestIfNotAllowed) {
                if (requestCode.equals(-1))
                    throw RuntimeException("Send request code in third parameter")
                requestRequiredPermissions(activity, permissions, requestCode)
            }

            return isGranted
        }

        fun requestRequiredPermissions(activity: FragmentActivity, permissions: Array<String>, requestCode: Int) {
            val pendingPermissions: ArrayList<String> = ArrayList()
            permissions.forEachIndexed { index, permission ->
                if (ContextCompat.checkSelfPermission(activity, permission) == PackageManager.PERMISSION_DENIED)
                    pendingPermissions.add(permission)
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                val array = arrayOfNulls<String>(pendingPermissions.size)
                pendingPermissions.toArray(array)
                activity.requestPermissions(array, requestCode)
            }
        }

        fun isAllPermissionsGranted(grantResults: IntArray): Boolean {
            var isGranted = true
            for (grantResult in grantResults) {
                isGranted = grantResult.equals(PackageManager.PERMISSION_GRANTED)
                if (!isGranted)
                    break
            }
            return isGranted
        }
    }
}