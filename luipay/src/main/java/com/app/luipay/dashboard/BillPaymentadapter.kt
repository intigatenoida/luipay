package com.app.luipay.dashboard

import android.content.Context
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.app.luipay.R
import kotlinx.android.synthetic.main.item_bill_payment.view.*

class BillPaymentadapter(
    val context: Context,
    val list: ArrayList<DashboardactionModel>,
    val onClickDashboard: onClickDashboard
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    class ViewHolder(val itemView: View) : RecyclerView.ViewHolder(itemView) {}


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.item_bill_payment,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {

        return list.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        val data = list.get(position)
        holder.itemView.tvName.text = data.actionName
        holder.itemView.ivUser.setImageResource(data.drawableId ?: 0)

        if (data.isPadding == true) {

            //holder.itemView.ivUser.setPaddingRelative(dpToPx(15), dpToPx(0), dpToPx(15), dpToPx(0))
            if (data.isRecharge == true) {
                holder.itemView.ivUser.setPaddingRelative(
                    dpToPx(15),
                    dpToPx(10),
                    dpToPx(15),
                    dpToPx(10)
                )
            } else if (data.isElectric == true) {
                holder.itemView.ivUser.setPaddingRelative(
                    dpToPx(15),
                    dpToPx(10),
                    dpToPx(15),
                    dpToPx(10)
                )
            } else if (data.isMore == true) {
                holder.itemView.ivUser.setPaddingRelative(
                    dpToPx(15),
                    dpToPx(10),
                    dpToPx(15),
                    dpToPx(10)
                )
            } else {
                holder.itemView.ivUser.setPaddingRelative(
                    dpToPx(15),
                    dpToPx(0),
                    dpToPx(15),
                    dpToPx(0)
                )
            }
        } else {

            holder.itemView.ivUser.setPaddingRelative(dpToPx(10), dpToPx(0), dpToPx(10), dpToPx(0))

        }

        holder.itemView.setOnClickListener {
            data.userName = "mahesh"
            onClickDashboard.onClickDashboard(data.actionType, list)
        }

    }

    private fun dpToPx(dp: Int): Int {
        val displayMetrics = context!!.resources.displayMetrics
        return Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT))
    }
}