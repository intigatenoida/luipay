package com.app.luipay.dashboard.model.profile


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep
import com.app.luipay.response.BaseResponse

@Keep
data class ProfileResponse(
    @SerializedName("Response")
    var response: Response

):BaseResponse()