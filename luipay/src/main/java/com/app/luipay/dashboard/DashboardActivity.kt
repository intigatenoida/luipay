package com.app.luipay.dashboard

import android.content.Intent
import android.os.Bundle
import android.view.Gravity
import android.view.View
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.fragment.app.Fragment
import com.app.luipay.R
import com.app.luipay.base.BaseActivity
import com.app.luipay.help.HelpFragment
import com.app.luipay.notifications.NotificationsActivity
import com.app.luipay.payment.ScanCodeFragment
import kotlinx.android.synthetic.main.activity_dashboard.*
import kotlinx.android.synthetic.main.dashboard_header.*


class DashboardActivity : BaseActivity(), NavigationAction {

    lateinit var fragment: Fragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dashboard)

        fragment = HomeFragment()

        LoadFragment(fragment, "", R.id.framecontainer)

        activeBottomView()


        val toogle = ActionBarDrawerToggle(this, drawerLayout, -1, -1)

        drawerLayout.setDrawerListener(toogle)


        ivMenu.setOnClickListener {
            drawerLayout.openDrawer(Gravity.START);
        }

        ivNotification.setOnClickListener {
            val intent = Intent(this@DashboardActivity, NotificationsActivity::class.java)
            startActivity(intent)

            /*val intent = Intent(this@DashboardActivity, HistoryActivity::class.java)
            intent.putExtra("isNotification", true)
            startActivity(intent)*/
        }

        groupPay.setOnClickListener {
            fragment = ScanCodeFragment()
            LoadFragment(fragment, "", R.id.framecontainer)
            activeBottomView()
        }
        groupHome.setOnClickListener {
            fragment = HomeFragment()
            LoadFragment(fragment, "", R.id.framecontainer)
            activeBottomView()
        }

        groupHelp.setOnClickListener {
            fragment = HelpFragment.newInstance()
            LoadFragment(fragment, "", R.id.framecontainer)
            activeBottomView()
        }

    }

    override fun onClickClose() {
        drawerLayout.closeDrawer(Gravity.START);
    }

    override fun onBackPressed() {
        super.onBackPressed()
        if (supportFragmentManager.backStackEntryCount <= 1) {
            finish()
        }
    }

    fun activeBottomView() {
        if (fragment is HomeFragment) {
            viewHelp.visibility = View.GONE
            viewPay.visibility = View.GONE
            viewHome.visibility = View.VISIBLE
        } else if (fragment is ScanCodeFragment) {
            viewHelp.visibility = View.GONE
            viewPay.visibility = View.VISIBLE
            viewHome.visibility = View.GONE
        } else if (fragment is HelpFragment) {
            viewHelp.visibility = View.VISIBLE
            viewPay.visibility = View.GONE
            viewHome.visibility = View.GONE
        }
    }

}