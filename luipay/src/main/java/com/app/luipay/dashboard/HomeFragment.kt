package com.app.luipay.dashboard

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.app.luipay.R
import com.app.luipay.base.BaseFragment
import com.app.luipay.reacharge_pay_bill.ReachargePayBillActivity
import com.app.luipay.request.ServiceAuthenticationToken
import com.app.luipay.response.RecentTranResponse
import com.app.luipay.response.WalletBalanceRespons
import com.app.luipay.retrofit.ApiClient
import com.app.luipay.retrofit.ApiInterface
import com.app.luipay.retrofit.CallbackManager
import com.app.luipay.services.PaymentActivity
import com.app.luipay.utils.Appconstant
import com.app.luipay.utils.CommonUtils
import com.app.luipay.utils.Prefutils
import kotlinx.android.synthetic.main.adv_viewpager.view.*
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.fragment_home.view.*
import java.util.*
import kotlin.collections.ArrayList


class HomeFragment : BaseFragment(), onClickDashboard {
    lateinit var list: ArrayList<DashboardactionModel>
    lateinit var list1: ArrayList<DashboardactionModel>
    lateinit var list2: ArrayList<DashboardactionModel>

    lateinit var timer: Timer
    var pageCount = 0
    lateinit var adapterRecent: RecentUseradapter

    private lateinit var rootView: View
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        rootView = inflater.inflate(R.layout.fragment_home, null)

        list = ArrayList()
        list1 = ArrayList()
        list2 = ArrayList()

        //--> Recent Transaction API Calling
        getRecentTransactions()

        addHeaderAction(rootView)
        addRecentView(rootView)
        addAdsView(rootView)
        addBillPayment(rootView)
        addMakeDonations()
        addGiftCards()

        rootView.tvInvite.setOnClickListener {
            CommonUtils.share(activity!!)
        }
        rootView.imgAmountEye.setOnClickListener {
            if (rootView.txtAmount.visibility == View.GONE) {
                rootView.imgAmountEye.setImageDrawable(
                    ContextCompat.getDrawable(
                        context!!,
                        R.drawable.eye_blue
                    )
                )
                var amount = "00.00"
                if (!TextUtils.isEmpty(
                        Prefutils.getFromPrefs(
                            requireContext(),
                            Prefutils.BALANCE
                        )
                    )
                ) {
                    amount = Prefutils.getFromPrefs(requireContext(), Prefutils.BALANCE)!!
                }
                txtAmount.text = "${amount} ${getString(R.string.mvr)}"
                getWalletBalance()
            } else {
                rootView.txtAmount.visibility = View.GONE
                rootView.imgAmountEye.setImageDrawable(
                    ContextCompat.getDrawable(
                        context!!,
                        R.drawable.eye
                    )
                )
            }
        }

        return rootView
    }

    private fun addGiftCards() {
        val decoration1 = DividerItemDecoration(
            activity,
            DividerItemDecoration.HORIZONTAL
        )
        val divider1 =
            ContextCompat.getDrawable(activity!!, R.drawable.divider_11_by_11)
        decoration1.setDrawable(divider1!!)

        rootView.rv_gift_cards.addItemDecoration(decoration1)
        rootView.rv_gift_cards.layoutManager = GridLayoutManager(activity, 4)

        rootView.rv_gift_cards.adapter = BillPaymentadapter(activity!!, giftCardsList(), this)
    }

    private fun addMakeDonations() {
        val decoration1 = DividerItemDecoration(
            activity,
            DividerItemDecoration.HORIZONTAL
        )
        val divider1 =
            ContextCompat.getDrawable(activity!!, R.drawable.divider_11_by_11)
        decoration1.setDrawable(divider1!!)

        rootView.rv_make_donation.addItemDecoration(decoration1)
        rootView.rv_make_donation.layoutManager = GridLayoutManager(activity, 4)

        rootView.rv_make_donation.adapter = BillPaymentadapter(activity!!, makeDonationList(), this)
    }

    private fun addAdsView(rootView: View) {
        val adsAdapter = AdsViewPageradapter(activity!!)
        rootView.adsViewPager.adapter = adsAdapter
        rootView.adsDot.setupWithViewPager(rootView.adsViewPager, true)
        val timerTask: TimerTask = object : TimerTask() {
            override fun run() {
                if ((pageCount + 1) > 3) {
                    pageCount = 0;
                } else {
                    pageCount++;
                }

                Log.e("*****", "pageCount" + pageCount)
                rootView.adsViewPager.post(Runnable { rootView.adsViewPager.setCurrentItem(pageCount) })
            }
        }
        timer = Timer()
        timer.schedule(timerTask, 5000, 5000)
    }

    private fun addBillPayment(rootView: View) {
        list.clear()

        val decoration1 = DividerItemDecoration(
            activity,
            DividerItemDecoration.HORIZONTAL
        )
        val divider1 =
            ContextCompat.getDrawable(activity!!, R.drawable.divider_11_by_11)
        decoration1.setDrawable(divider1!!)

        rootView.rvBillPayment.addItemDecoration(decoration1)
        rootView.rvBillPayment.layoutManager = GridLayoutManager(activity, 4)

        rootView.rvBillPayment.adapter = BillPaymentadapter(activity!!, getBillPayList(false), this)
    }

    fun makeDonationList(): ArrayList<DashboardactionModel> {
        val list = ArrayList<DashboardactionModel>()

        var info = DashboardactionModel()
        info.actionName = "Cancer Society"
        info.actionType = Appconstant.no
        info.drawableId = R.drawable.ic_cancer
        list.add(info)

        info = DashboardactionModel()
        info.actionName = "ARC"
        info.actionType = Appconstant.no
        info.drawableId = R.drawable.ic_arc
        list.add(info)

        info = DashboardactionModel()
        info.actionName = "Cancer Society"
        info.actionType = Appconstant.no
        info.drawableId = R.drawable.ic_heartofmaldives
        list.add(info)

        return list
    }

    fun giftCardsList(): ArrayList<DashboardactionModel> {
        val list = ArrayList<DashboardactionModel>()

        var info = DashboardactionModel()
        info.actionName = ""
        info.actionType = Appconstant.no
        info.drawableId = R.drawable.ic_xbox
        list.add(info)

        info = DashboardactionModel()
        info.actionName = ""
        info.actionType = Appconstant.no
        info.drawableId = R.drawable.ic_spotify
        list.add(info)

        info = DashboardactionModel()
        info.actionName = ""
        info.actionType = Appconstant.no
        info.drawableId = R.drawable.ic_appstore
        list.add(info)

        info = DashboardactionModel()
        info.actionName = "Less"
        info.actionType = Appconstant.less
        info.drawableId = R.drawable.ic_less
        list.add(info)

        return list
    }

    fun getBillPayList(dropVisible: Boolean): ArrayList<DashboardactionModel> {
        val list = ArrayList<DashboardactionModel>()
        var info = DashboardactionModel()
        info.actionName = "Card Bill"
        info.actionType = Appconstant.recharge
        info.drawableId = R.drawable.cardbill
        info.isPadding = true
        list.add(info)

        info = DashboardactionModel()
        info.actionName = "Recharge"
        info.drawableId = R.drawable.recharge
        info.actionType = Appconstant.recharge
        info.isPadding = true
        info.isRecharge = true
        list.add(info)

        info = DashboardactionModel()
        info.actionName = "Electricity"
        info.actionType = Appconstant.recharge
        info.drawableId = R.drawable.electricity
        info.isPadding = true
        info.isElectric = true
        list.add(info)

        info = DashboardactionModel()
        info.actionName = "Water"
        info.actionType = Appconstant.recharge
        info.drawableId = R.drawable.waterbill
        info.isPadding = true
        list.add(info)

        info = DashboardactionModel()
        info.actionName = "DTH"
        info.actionType = Appconstant.recharge
        info.drawableId = R.drawable.dth
        info.isPadding = true
        list.add(info)

        info = DashboardactionModel()
        info.actionName = "Water"
        info.actionType = Appconstant.recharge
        info.drawableId = R.drawable.waterbill
        info.isPadding = true
        list.add(info)

        info = DashboardactionModel()
        info.actionName = "DTH"
        info.actionType = Appconstant.recharge
        info.drawableId = R.drawable.dth
        info.isPadding = true
        list.add(info)


        if (dropVisible) {
            info = DashboardactionModel()
            info.actionName = "WMSC"
            info.actionType = Appconstant.recharge
            info.drawableId = R.drawable.ic_wmsc
            list.add(info)

            info = DashboardactionModel()
            info.actionName = "STELCO"
            info.actionType = Appconstant.recharge
            info.drawableId = R.drawable.ic_stelco
            list.add(info)

            info = DashboardactionModel()
            info.actionName = "MEDIANET"
            info.actionType = Appconstant.recharge
            info.drawableId = R.drawable.ic_medianet
            list.add(info)

            info = DashboardactionModel()
            info.actionName = "Bill pay"
            info.actionType = Appconstant.recharge
            info.drawableId = R.drawable.ic_ooredoo
            list.add(info)

            info = DashboardactionModel()
            info.actionName = "Bill pay"
            info.actionType = Appconstant.recharge
            info.drawableId = R.drawable.ic_dhiraagu
            list.add(info)

            info = DashboardactionModel()
            info.actionName = "Recharge"
            info.actionType = Appconstant.recharge
            info.drawableId = R.drawable.ic_ooredoo
            list.add(info)

            info = DashboardactionModel()
            info.actionName = "Recharge"
            info.actionType = Appconstant.recharge
            info.drawableId = R.drawable.ic_dhiraagu
            list.add(info)

            info = DashboardactionModel()
            info.actionName = "MALDIVEGAS"
            info.actionType = Appconstant.recharge
            info.drawableId = R.drawable.ic_maldivegas
            list.add(info)

            info = DashboardactionModel()
            info.actionName = "Villa Gas"
            info.actionType = Appconstant.recharge
            info.drawableId = R.drawable.ic_villa
            list.add(info)

            info = DashboardactionModel()
            info.actionName = "Insurance"
            info.actionType = Appconstant.recharge
            info.drawableId = R.drawable.ic_insurance
            list.add(info)

        } else {
            info = DashboardactionModel()
            info.actionName = "More"
            info.actionType = Appconstant.more
            info.drawableId = R.drawable.ic_more
            info.isPadding = true
            info.isRecharge = true
            list.add(info)
        }

        return list
    }

    private fun getWalletBalance() {
        showProcessDialog()
        val token = ServiceAuthenticationToken()
        token.serviceAuthenticationToken = ApiClient.android_token
        val helper = ApiClient.getClient(requireContext()).create(ApiInterface::class.java)
        val call = helper.getWalletBalance(token, "application/json")
        call.enqueue(object : CallbackManager<WalletBalanceRespons>() {
            override fun onSuccess(any: WalletBalanceRespons?, message: String) {
                val response = any as WalletBalanceRespons
                response?.response.amount.let {
                    Prefutils.saveToPrefs(requireContext(), Prefutils.BALANCE, it)
                    rootView.txtAmount.visibility = View.VISIBLE
                    txtAmount.text = "${it} ${getString(R.string.mvr)}"
                }
                hideProcessDialog()
            }

            override fun onFailure(message: String) {
                hideProcessDialog()
            }

            override fun onError(error: String) {
                hideProcessDialog()
            }

        })
    }

    private fun addHeaderAction(rootView: View) {

        list1.clear()

        rootView.rvDashboardAction.layoutManager =
            LinearLayoutManager(activity, RecyclerView.HORIZONTAL, false)

        val decoration = DividerItemDecoration(
            activity,
            DividerItemDecoration.HORIZONTAL
        )
        val divider =
            ContextCompat.getDrawable(activity!!, R.drawable.divider_9)
        decoration.setDrawable(divider!!)
        rootView.rvDashboardAction.addItemDecoration(decoration)


        var info = DashboardactionModel()
        info.actionName = "Add Money"
        info.actionselected = true
        info.drawableId = R.drawable.withdraw
        list1.add(info)

        info = DashboardactionModel()
        info.actionName = "LuiRequest"
        info.drawableId = R.drawable.luirequest

        list1.add(info)

        info = DashboardactionModel()
        info.actionName = "LuiSend"
        info.drawableId = R.drawable.luisend

        list1.add(info)

        info = DashboardactionModel()
        info.actionName = "Link Account"
        info.drawableId = R.drawable.link_account

        list1.add(info)


        rootView.rvDashboardAction.adapter = DashboardAddMoneyAdapter(activity!!, list1)


    }

    private fun addRecentView(rootView: View) {

        list2.clear()


        val decoration1 = DividerItemDecoration(
            activity,
            DividerItemDecoration.HORIZONTAL
        )
        val divider1 =
            ContextCompat.getDrawable(activity!!, R.drawable.divider_11_by_11)
        decoration1.setDrawable(divider1!!)

        rootView.rvRecent.addItemDecoration(decoration1)
        rootView.rvRecent.layoutManager = GridLayoutManager(activity, 4)


//        info = DashboardactionModel()
//        info.actionName = "Dhiraagu"
//        info.actionImage = ContextCompat.getDrawable(activity!!, R.drawable.user1)
//        list2.add(info)
//
//        info = DashboardactionModel()
//        info.actionName = "Dhiraagu"
//        info.actionImage = null
//        list2.add(info)
//
//        info = DashboardactionModel()
//        info.actionName = "Dyna"
//        info.actionImage = ContextCompat.getDrawable(activity!!, R.drawable.user2)
//        list2.add(info)
//
//        info = DashboardactionModel()
//        info.actionName = "Shailesh"
//        info.actionImage = ContextCompat.getDrawable(activity!!, R.drawable.user4)
//        list2.add(info)
//
//        info = DashboardactionModel()
//        info.actionName = "Adriana Li…"
//        info.actionImage = ContextCompat.getDrawable(activity!!, R.drawable.user4)
//        list2.add(info)
//
//        info = DashboardactionModel()
//        info.actionName = "Miranda K…"
//        info.actionImage = ContextCompat.getDrawable(activity!!, R.drawable.user5)
//        list2.add(info)
//
//        info = DashboardactionModel()
//        info.actionName = "Anne"
//        info.actionImage = null
//        list2.add(info)
//
//        info = DashboardactionModel()
//        info.actionName = "Aafreen"
//        info.actionImage = ContextCompat.getDrawable(activity!!, R.drawable.user6)
//        list2.add(info)
//

        adapterRecent = RecentUseradapter(activity!!)
        rootView.rvRecent.adapter = adapterRecent
    }

    override fun onClickDashboard(
        action: Int?,
        list: ArrayList<DashboardactionModel>
    ) {
        if (action == null) {
            return
        }
        when (action) {
            Appconstant.recharge -> {
                startActivity(Intent(activity, ReachargePayBillActivity::class.java))
            }
            Appconstant.more -> {
                rootView.gd_donations.visibility = VISIBLE
                rootView.gd_gift_cards.visibility = VISIBLE
                rootView.rvBillPayment.adapter =
                    BillPaymentadapter(activity!!, getBillPayList(true), this)
            }
            Appconstant.less -> {
                rootView.gd_donations.visibility = GONE
                rootView.gd_gift_cards.visibility = GONE
                rootView.rvBillPayment.adapter =
                    BillPaymentadapter(activity!!, getBillPayList(false), this)
            }
            else -> {
                val intent = Intent(activity, PaymentActivity::class.java)
                list.clear()
                var info = DashboardactionModel()
                info.actionName = "WMSC"
                info.actionType = Appconstant.recharge
                info.drawableId = R.drawable.ic_wmsc
                list.add(info)

                info = DashboardactionModel()
                info.actionName = "STELCO"
                info.actionType = Appconstant.recharge
                info.drawableId = R.drawable.ic_stelco
                list.add(info)

                info = DashboardactionModel()
                info.actionName = "MEDIANET"
                info.actionType = Appconstant.recharge
                info.drawableId = R.drawable.ic_medianet
                list.add(info)

                info = DashboardactionModel()
                info.actionName = "Bill pay"
                info.actionType = Appconstant.recharge
                info.drawableId = R.drawable.ic_ooredoo
                list.add(info)

                info = DashboardactionModel()
                info.actionName = "Bill pay"
                info.actionType = Appconstant.recharge
                info.drawableId = R.drawable.ic_dhiraagu
                list.add(info)

                info = DashboardactionModel()
                info.actionName = "Recharge"
                info.actionType = Appconstant.recharge
                info.drawableId = R.drawable.ic_ooredoo
                list.add(info)

                info = DashboardactionModel()
                info.actionName = "Recharge"
                info.actionType = Appconstant.recharge
                info.drawableId = R.drawable.ic_dhiraagu
                list.add(info)

                info = DashboardactionModel()
                info.actionName = "MALDIVEGAS"
                info.actionType = Appconstant.recharge
                info.drawableId = R.drawable.ic_maldivegas
                list.add(info)

                info = DashboardactionModel()
                info.actionName = "Villa Gas"
                info.actionType = Appconstant.recharge
                info.drawableId = R.drawable.ic_villa
                list.add(info)

                info = DashboardactionModel()
                info.actionName = "Insurance"
                info.actionType = Appconstant.recharge
                info.drawableId = R.drawable.ic_insurance
                list.add(info)
                intent.putExtra(Appconstant.SERVICES_LIST, list)
                startActivity(intent)
            }
        }

    }

    private fun getRecentTransactions() {
        val token = ServiceAuthenticationToken()
        token.serviceAuthenticationToken = ApiClient.android_token
        val helper = ApiClient.getClient(activity!!).create(ApiInterface::class.java)
        val call = helper.getRecentTransaction(token, "application/json")
        call.enqueue(object : CallbackManager<RecentTranResponse>() {
            override fun onSuccess(res: RecentTranResponse?, message: String) {

//                val response = any as BaseResponse


                res?.responseCollection?.let {

                    var info = DashboardactionModel()
                    info.actionName = "History"
                    info.drawableId = R.drawable.history
                    list2.add(info)

                    for (data in it) {
                        info = DashboardactionModel()
                        info.actionName = data.userName
                        info.image = data.image
                        info.id = data.id
                        info.mobileNo = data.mobileNo
                        info.drawableId = R.drawable.history
                        info.associatedUserId = data.associatedUserId
                        list2.add(info)
                    }

                    if (::adapterRecent.isInitialized) {
                        adapterRecent.update(list2)
                    }


                }


            }

            override fun onFailure(message: String) {
                Log.e("aaaa", "aaa")

                var info = DashboardactionModel()
                info.actionName = "History"
                info.drawableId = R.drawable.history
                list2.add(info)

                if (::adapterRecent.isInitialized) {
                    adapterRecent.update(list2)
                }


//                rootView.tv_no_history.visibility = View.VISIBLE
            }

            override fun onError(error: String) {
                Log.e("aaaa", "aaa")

                var info = DashboardactionModel()
                info.actionName = "History"
                info.drawableId = R.drawable.history
                list2.add(info)

                if (::adapterRecent.isInitialized) {
                    adapterRecent.update(list2)
                }
            }
        })
    }


    override fun onDestroy() {
        if (::timer.isInitialized) {
            timer.cancel()
        }
        super.onDestroy()
    }
}