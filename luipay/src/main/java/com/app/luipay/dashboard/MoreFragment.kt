package com.app.luipay.dashboard

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import com.app.luipay.R
import com.app.luipay.base.BaseFragment
import com.app.luipay.dashboard.model.MoreModel
import com.app.luipay.utils.Appconstant

class MoreFragment : BaseFragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var rootView =
            LayoutInflater.from(requireActivity()).inflate(R.layout.fragment_more, container)


        return rootView

    }


    private fun addBillPayment(rootView: View): ArrayList<MoreModel> {

        /*
        val decoration1 = DividerItemDecoration(
            activity,
            DividerItemDecoration.HORIZONTAL
        )
        val divider1 =
            ContextCompat.getDrawable(activity!!, R.drawable.divider_11_by_11)
        decoration1.setDrawable(divider1!!)

        rootView.rvBillPayment.addItemDecoration(decoration1)
        rootView.rvBillPayment.layoutManager = GridLayoutManager(activity, 4)
*/


        var moreList = ArrayList<MoreModel>()
        var dataInfo = MoreModel()
        dataInfo.Name = "luiPay Services"
        dataInfo.type = 1

        moreList.add(dataInfo)


        dataInfo = MoreModel()
        dataInfo.type = 2
        var list = ArrayList<DashboardactionModel>()
        var info = DashboardactionModel()
        info.actionName = "Card Bill"
        info.actionType = Appconstant.recharge
        info.drawableId =  R.drawable.cardbill
        list.add(info)

        info = DashboardactionModel()
        info.actionName = "Recharge"
        info.drawableId = R.drawable.recharge
        info.actionType = Appconstant.recharge
        list.add(info)

        info = DashboardactionModel()
        info.actionName = "Electricity"
        info.actionType = Appconstant.recharge
        info.drawableId =  R.drawable.electricity
        list.add(info)

        info = DashboardactionModel()
        info.actionName = "Water"
        info.actionType = Appconstant.recharge
        info.drawableId = R.drawable.waterbill
        list.add(info)

        info = DashboardactionModel()
        info.actionName = "DTH"
        info.actionType = Appconstant.recharge
        info.drawableId = R.drawable.dth
        list.add(info)

        info = DashboardactionModel()
        info.actionName = "Water"
        info.actionType = Appconstant.recharge
        info.drawableId = R.drawable.waterbill
        list.add(info)

        info = DashboardactionModel()
        info.actionName = "DTH"
        info.actionType = Appconstant.recharge
        info.drawableId = R.drawable.dth
        list.add(info)

        dataInfo.dataList = list
        moreList.add(dataInfo)



        dataInfo = MoreModel()
        dataInfo.Name = "Make Donation"
        dataInfo.type = 1

        moreList.add(dataInfo)


        dataInfo = MoreModel()
        dataInfo.type = 2
        list = ArrayList<DashboardactionModel>()
        info = DashboardactionModel()
        info.actionName = "Card Bill"
        info.actionType = Appconstant.recharge
        info.drawableId = R.drawable.cardbill
        list.add(info)

        info = DashboardactionModel()
        info.actionName = "Recharge"
        info.drawableId =  R.drawable.recharge
        info.actionType = Appconstant.recharge
        list.add(info)


        dataInfo.dataList = list
        moreList.add(dataInfo)


        dataInfo = MoreModel()
        dataInfo.Name = "Make Donation"
        dataInfo.type = 1

        moreList.add(dataInfo)


        dataInfo = MoreModel()
        dataInfo.type = 2
        list = ArrayList<DashboardactionModel>()
        info = DashboardactionModel()
        info.actionName = "Card Bill"
        info.actionType = Appconstant.recharge
        info.drawableId = R.drawable.cardbill
        list.add(info)

        info = DashboardactionModel()
        info.actionName = "Recharge"
        info.drawableId =  R.drawable.recharge
        info.actionType = Appconstant.recharge
        list.add(info)

        info = DashboardactionModel()
        info.actionName = "Recharge"
        info.drawableId = R.drawable.recharge
        info.actionType = Appconstant.recharge
        list.add(info)

        dataInfo.dataList = list
        moreList.add(dataInfo)



        dataInfo = MoreModel()
        dataInfo.Name = "Gift Cards"
        dataInfo.type = 1

        moreList.add(dataInfo)


        dataInfo = MoreModel()
        dataInfo.type = 2
        list = ArrayList<DashboardactionModel>()
        info = DashboardactionModel()
        info.actionName = "Card Bill"
        info.actionType = Appconstant.recharge
        info.drawableId = R.drawable.cardbill
        list.add(info)

        info = DashboardactionModel()
        info.actionName = "Recharge"
        info.drawableId =  R.drawable.recharge
        info.actionType = Appconstant.recharge
        list.add(info)

        info = DashboardactionModel()
        info.actionName = "Recharge"
        info.drawableId = R.drawable.recharge
        info.actionType = Appconstant.recharge
        list.add(info)

        dataInfo.dataList = list
        moreList.add(dataInfo)


        return moreList


//        rootView.rvBillPayment.adapter = BillPaymentadapter(activity!!, list, this)
    }
}