package com.app.luipay.dashboard

import android.graphics.drawable.Drawable
import android.os.Parcel
import android.os.Parcelable
import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import java.io.Serializable

@Parcelize
@Keep
data class DashboardactionModel(

    @SerializedName("AssociatedUserId")
    var associatedUserId: String? = "",
    @SerializedName("Id")
    var id: Int? = 0,
    @SerializedName("Image")
    var image: String? = null,
    @SerializedName("MobileNo")
    var mobileNo: String? = null,
    @SerializedName("UserName")
    var userName: String? = "",

    var actionName: String? = "",
    var drawableId: Int? = 0,
    var actionType: Int? = -1,
    var actionselected: Boolean? = false,
    var isPadding: Boolean? = false,
    var isRecharge: Boolean? = false,
    var isElectric: Boolean? = false,
    var isMore: Boolean? = false
) : Parcelable