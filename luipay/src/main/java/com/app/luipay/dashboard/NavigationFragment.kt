package com.app.luipay.dashboard

import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.app.luipay.DBHandler.DBHelper
import com.app.luipay.R
import com.app.luipay.base.BaseFragment
import com.app.luipay.dashboard.model.profile.ProfileResponse
import com.app.luipay.dashboard.model.profile.Response
import com.app.luipay.login.LoginActivity
import com.app.luipay.request.BaseRequest
import com.app.luipay.retrofit.ApiClient
import com.app.luipay.retrofit.ApiInterface
import com.app.luipay.retrofit.CallbackManager
import com.app.luipay.utils.LuipayToast
import com.app.luipay.utils.Prefutils
import com.app.luipay.utils.custom_listener.CustomAlertDialog
import kotlinx.android.synthetic.main.fragment_navigation.view.*

class NavigationFragment : BaseFragment() {

    lateinit var navigate: NavigationAction
    lateinit var rootView: View

    override fun onAttach(context: Context) {
        super.onAttach(context)
        navigate = context as NavigationAction
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        rootView = inflater.inflate(R.layout.fragment_navigation, null)

        rootView.ivClose.setOnClickListener {
            navigate.onClickClose()
        }

        rootView.tvLogout.setOnClickListener {
            CustomAlertDialog(
                activity,
                getString(R.string.app_name),
                getString(R.string.logout_msg),
                getString(R.string.dialog_ok),
                getString(R.string.dialog_cancel),
                object : CustomAlertDialog.onAlertDialogCustomListener {
                    override fun onSuccessListener(dialog: DialogInterface?) {
                        dialog!!.dismiss()
                        Prefutils.logout(activity!!)
                        DBHelper(activity).clearDatabase()
                        val intent = Intent(activity!!, LoginActivity::class.java)
                        startActivity(intent)
                        activity!!.finish()
                    }

                    override fun onCancelListener() {

                    }

                })

        }


        rootView.clHeader.setOnClickListener { LuipayToast.commingSoon(context) }
        rootView.claccount.setOnClickListener { LuipayToast.commingSoon(context) }
        rootView.clsecurity.setOnClickListener { LuipayToast.commingSoon(context) }
        rootView.clHelp.setOnClickListener { LuipayToast.commingSoon(context) }
        rootView.clHeader.setOnClickListener {
            val intent = Intent(activity!!, QRCodeActivity::class.java)
            startActivity(intent)
        }

        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //-- get User Profile
        getUserProfile()
    }


    private fun getUserProfile() {
        val token = BaseRequest()
        token.ServiceAuthenticationToken = ApiClient.android_token
        token.LanguageTypeId = Prefutils.getFromPrefsInt(activity!!, Prefutils.API_LANGUAGE)
        val helper = ApiClient.getClient(activity!!).create(ApiInterface::class.java)
        val call = helper.getUserProfileInformation(token, "application/json")
        call.enqueue(object : CallbackManager<ProfileResponse>() {
            override fun onSuccess(any: ProfileResponse?, message: String) {
                val apiResponse = any as ProfileResponse
                apiResponse.response.name.let {
                    Prefutils.saveToPrefs(activity!!, Prefutils.NAME, it)
                }
                apiResponse.response.email.let {
                    Prefutils.saveToPrefs(activity!!, Prefutils.EMAIL, it)
                }
                apiResponse.response.gender.let {
                    Prefutils.saveToPrefs(activity!!, Prefutils.GENDER, it)
                }
                apiResponse.response.isKycPending.let {
                    Prefutils.saveToPrefsBoolean(activity!!, Prefutils.ISKYCPENDING, it)
                }
                apiResponse.response.mobileNo.let {
                    Prefutils.saveToPrefs(activity!!, Prefutils.MOBILENO, it)
                }
                apiResponse.response.countryCode.let {
                    Prefutils.saveToPrefs(activity!!, Prefutils.COUNTRYCODE, it)
                }
                apiResponse.response.transactionLimit.let {
                    Prefutils.saveToPrefsInt(activity!!, Prefutils.TRANSACTIONLIMIT, it)
                }
                apiResponse.response.profilePicUrl.let {
                    Prefutils.saveToPrefs(activity!!, Prefutils.PROFILEPICURL, it)
                }
                apiResponse.response.address.let {
                    Prefutils.saveToPrefs(activity!!, Prefutils.ADDRESS, it)
                }
                updateUI(apiResponse.response)
            }

            override fun onFailure(message: String) {

            }

            override fun onError(error: String) {

            }

        })
    }

    private fun updateUI(response: Response) {
        if (response.name.isNotEmpty()) {
            rootView.tvUsername.text = response.name
        }

        if (response.countryCode.isNotEmpty()) {
            if (response.mobileNo.isNotEmpty()) {
                rootView.tvMobile.text = "${response.countryCode} ${response.mobileNo}"
            }
        }


    }
}