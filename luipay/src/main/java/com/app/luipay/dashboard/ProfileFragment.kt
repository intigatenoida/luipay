package com.app.luipay.dashboard

import android.content.Intent
import android.content.pm.PackageManager
import android.content.pm.ResolveInfo
import android.graphics.Bitmap
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.View.VISIBLE
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.fragment.app.Fragment
import com.app.luipay.BuildConfig
import com.app.luipay.R
import com.app.luipay.utils.Prefutils
import com.google.zxing.BarcodeFormat
import com.journeyapps.barcodescanner.BarcodeEncoder
import kotlinx.android.synthetic.main.fragment_profile.view.*
import org.json.JSONObject
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.IOException


/**
 * A simple [Fragment] subclass.
 */
class ProfileFragment : Fragment() {

    lateinit var rootView: View
    lateinit var qrcode: Bitmap

    companion object {
        fun getInstance(): ProfileFragment {
            return ProfileFragment()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_profile, container, false)

        rootView.btn_qrcode.setOnClickListener {
            if (::qrcode.isInitialized) {
                shareQrCode(qrcode)
            }
        }
        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val name = Prefutils.getFromPrefs(activity!!, Prefutils.NAME)
        if (name!!.isNotEmpty()) {
            rootView.txt_userName.text = name
        }

        val mobileNo = Prefutils.getFromPrefs(activity!!, Prefutils.MOBILENO)
        val countryId = Prefutils.getFromPrefs(activity!!, Prefutils.COUNTRYCODE)
        if (mobileNo!!.isNotEmpty()) {
            rootView.txt_mobileNo.text = "$countryId $mobileNo"
        }

        val isKycVerfied = Prefutils.getFromPrefsBoolean(activity!!, Prefutils.ISKYCPENDING)
        if (!isKycVerfied) {
            rootView.txt_kycStatus.text = activity!!.getString(R.string.kyc_verified)
            rootView.txt_kycStatus.setCompoundDrawablesRelativeWithIntrinsicBounds(
                ContextCompat.getDrawable(
                    activity!!,
                    R.drawable.ic_verified
                ), null, null, null
            )
        } else {
            rootView.txt_kycStatus.text = getString(R.string.kyc_pending)
            rootView.txt_kycStatus.setCompoundDrawablesRelativeWithIntrinsicBounds(
                ContextCompat.getDrawable(
                    activity!!,
                    R.drawable.ic_pending
                ), null, null, null
            )

        }

        generateBardCode(getRequiredJSON())
    }

    private fun getRequiredJSON(): String {
        val obj = JSONObject()
        obj.put("ui", Prefutils.getFromPrefs(context!!, Prefutils.USERID) ?: "")
        obj.put("un", Prefutils.getFromPrefs(context!!, Prefutils.NAME) ?: "")
        obj.put("up", Prefutils.getFromPrefs(context!!, Prefutils.PROFILEPICURL) ?: "")
        return obj.toString()
    }

    private fun generateBardCode(content: String) {
        try {
            val barcodeEncoder = BarcodeEncoder()
            val bitmap =
                barcodeEncoder.encodeBitmap(content, BarcodeFormat.QR_CODE, 400, 400)
            qrcode = bitmap
            rootView.profile_barcode.visibility = VISIBLE
            rootView.profile_barcode.setImageBitmap(bitmap)
        } catch (e: Exception) {
        }
    }


    private fun shareQrCode(bitmap: Bitmap) {
        try {
            val share = Intent(Intent.ACTION_SEND)
            share.type = "image/jpeg"
            val bytes = ByteArrayOutputStream()
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes)
            val f =
                File("${context!!.cacheDir}${File.separator}temporary_file.jpg")
            val originalUri = FileProvider.getUriForFile(
                context!!,
                "${BuildConfig.APPLICATION_ID}.provider",
                f
            )
            try {
                f.createNewFile()
                val fo = FileOutputStream(f)
                fo.write(bytes.toByteArray())
            } catch (e: IOException) {
                e.printStackTrace()
            }
            share.putExtra(
                Intent.EXTRA_STREAM,
                originalUri
            )

            val chooser = Intent.createChooser(share, "Share QR Code")
            val resInfoList: List<ResolveInfo> = context!!.packageManager
                .queryIntentActivities(chooser, PackageManager.MATCH_DEFAULT_ONLY)

            for (resolveInfo in resInfoList) {
                val packageName = resolveInfo.activityInfo.packageName
                activity!!.grantUriPermission(
                    packageName,
                    originalUri,
                    Intent.FLAG_GRANT_WRITE_URI_PERMISSION or Intent.FLAG_GRANT_READ_URI_PERMISSION
                )
            }

            startActivity(chooser)
        } catch (e: Exception) {

        }
    }
}
/*val b = bitmap
      val share = Intent(Intent.ACTION_SEND)
      share.type = "image/jpeg"
      val bytes = ByteArrayOutputStream()
      b.compress(Bitmap.CompressFormat.JPEG, 100, bytes)
      val path = MediaStore.Images.Media.insertImage(
          activity!!.contentResolver,
          b, "Title", null
      )
      val imageUri: Uri = Uri.parse(path)
      share.putExtra(Intent.EXTRA_STREAM, imageUri)
      startActivity(Intent.createChooser(share, "Share QR Code"))*/