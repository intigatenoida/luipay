package com.app.luipay.dashboard


import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.app.luipay.R
import com.app.luipay.addmoney.AddMoneyActivity
import com.app.luipay.history.HistoryActivity
import com.app.luipay.money_transfer.MoneyTransferActivity
import com.app.luipay.response.SearchContactResponse.ResponseCollection
import com.app.luipay.utils.Appconstant
import com.app.luipay.utils.CommonUtils
import kotlinx.android.synthetic.main.item_recent.view.*

class RecentUseradapter(val context : Context) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    var list : ArrayList<DashboardactionModel>

    init {
        list= ArrayList()
    }
    class ViewHolder(val itemView: View) : RecyclerView.ViewHolder(itemView) {}


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.item_recent,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {

        return list.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {


        val data=list.get(position)

//        if(data.actionImage!=null){
//            holder.itemView.tvFirstChar.visibility=View.GONE
//            holder.itemView.ivUser.visibility=View.VISIBLE
//            holder.itemView.ivUser.setImageDrawable(data.actionImage)
//        }else{
//            holder.itemView.tvFirstChar.visibility=View.VISIBLE
//            holder.itemView.ivUser.visibility=View.GONE
//        }


        if(position==0){
            holder.itemView.ivUser.setImageResource(data.drawableId?:0)
            holder.itemView.setOnClickListener {
                val intent= Intent(context,HistoryActivity::class.java)
                intent.putExtra("isNotification",false)
                context.startActivity(intent)
            }
        }else {
            CommonUtils.loadImage(context,data.image, holder.itemView.ivUser,R.drawable.ic_user)
            holder.itemView.setOnClickListener {
                val temp = ResponseCollection()
                    temp.name = data.actionName?:""
                    temp.profilePictureUrl = data.image
                    temp.id = data.associatedUserId?:""

                val sendIt = Intent(
                    context!!,
                    AddMoneyActivity::class.java
                )

                val bundle = Bundle()
                bundle.putBoolean(
                    Appconstant.LUI_REQUEST, false
                )
                bundle.putSerializable(Appconstant.NAME, temp)
                sendIt.putExtras(bundle)
                context.startActivity(sendIt)
            }
        }
        holder.itemView.tvUserName.text=data.actionName





    }

    public fun update( list : ArrayList<DashboardactionModel>){
       this.list=list
        notifyDataSetChanged()

    }
}