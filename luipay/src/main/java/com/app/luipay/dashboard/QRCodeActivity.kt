package com.app.luipay.dashboard

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.app.luipay.R
import com.app.luipay.base.BaseActivity
import com.app.luipay.profile.EditProfile
import kotlinx.android.synthetic.main.header_layout_profile.*

class QRCodeActivity : BaseActivity() {

    val REQUEST_CODE_EDITPROFILE = 101
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_q_r_code)
        ivBack.setOnClickListener {
            finish()
        }

        tv_editprofile.visibility = View.VISIBLE
        tv_editprofile.setOnClickListener {
            startActivityForResult(Intent(this@QRCodeActivity, EditProfile::class.java),REQUEST_CODE_EDITPROFILE)
        }
        val qrCodeFragment = ProfileFragment.getInstance()
        LoadFragment(qrCodeFragment,this@QRCodeActivity.localClassName,R.id.main_container)
    }
}
