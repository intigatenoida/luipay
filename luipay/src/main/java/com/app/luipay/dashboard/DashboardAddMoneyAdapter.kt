package com.app.luipay.dashboard

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.app.luipay.R
import com.app.luipay.addmoney.AddedBankActivity
import com.app.luipay.link_account.LinkAccountActivity
import com.app.luipay.money_transfer.MoneyTransferActivity
import com.app.luipay.money_transfer.phonebook.PhonebookActivity
import com.app.luipay.utils.Appconstant
import kotlinx.android.synthetic.main.item_dashboard_top.view.*

class DashboardAddMoneyAdapter(val context: Context, val list: ArrayList<DashboardactionModel>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    class ViewHolder(val itemView: View) : RecyclerView.ViewHolder(itemView) {}


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.item_dashboard_top,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {

        return list.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        val data = list.get(position)
        holder.itemView.ivAction.setImageResource(data.drawableId?:0)
        holder.itemView.tvActionTitle.text = data.actionName

        if (data.actionselected == true) {
            holder.itemView.clMain.background =
                ContextCompat.getDrawable(context, R.drawable.rect_theme_8_dp)
        } else {
            holder.itemView.clMain.setBackgroundColor(Color.parseColor("#ffffff"))
        }

        holder.itemView.clMain.setOnClickListener {

            for (info in list) {
                info.actionselected = false
            }
            data.actionselected = true
            notifyDataSetChanged()


            if (position == 0) {
                val intent = Intent(context, AddedBankActivity::class.java)
                context.startActivity(intent)
            } else if (position == 1) {
                val intent = Intent(context, PhonebookActivity::class.java)
                intent.putExtra(Appconstant.LUI_REQUEST,true)
                context.startActivity(intent)
            } else if (position == 3) {
                val intent = Intent(context, LinkAccountActivity::class.java)
                context.startActivity(intent)
            } else if (position == 2) {
                context.startActivity(Intent(context, PhonebookActivity::class.java).putExtra(Appconstant.LUI_REQUEST,false))

            }

        }


    }
}