package com.app.luipay.dashboard.model.profile


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep

@Keep
data class Response(
    @SerializedName("Address")
    var address: String,
    @SerializedName("CountryCode")
    var countryCode: String,
    @SerializedName("CountryCodeId")
    var countryCodeId: Int,
    @SerializedName("Dob")
    var dob: String,
    @SerializedName("Email")
    var email: String,
    @SerializedName("Gender")
    var gender: String,
    @SerializedName("IsKycPending")
    var isKycPending: Boolean,
    @SerializedName("MobileNo")
    var mobileNo: String,
    @SerializedName("Name")
    var name: String,
    @SerializedName("ProfilePicUrl")
    var profilePicUrl: String,
    @SerializedName("TransactionLimit")
    var transactionLimit: Int
)