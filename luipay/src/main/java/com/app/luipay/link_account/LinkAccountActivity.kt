package com.app.luipay.link_account

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.luipay.DBHandler.BankAccountData
import com.app.luipay.DBHandler.DBHelper
import com.app.luipay.R
import com.app.luipay.addmoney.SelectBankAddMoneyActivity
import com.app.luipay.link_account.card_details.CardDetailsActivity
import com.app.luipay.utils.Appconstant
import kotlinx.android.synthetic.main.activity_link_account.*
import kotlinx.android.synthetic.main.center_title_layout.*

class LinkAccountActivity : AppCompatActivity() {
lateinit var bankAccountListAdapter: BankAccountListAdapter
    private var list = ArrayList<BankAccountData?>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_link_account)
        backBtn.setOnClickListener { finish() }
        titleTv.text=getString(R.string.link_account)
        linkAccountRv.layoutManager=LinearLayoutManager(this)
        bankAccountListAdapter=BankAccountListAdapter(this,list)
        linkAccountRv.adapter=bankAccountListAdapter
        otherBankCl.setOnClickListener {
            val intent = Intent(this@LinkAccountActivity, SelectBankAddMoneyActivity::class.java)
            intent.putExtra(Appconstant.NAME, true)
            startActivityForResult(
                intent, 1000
            ) }
        visaCardLayout.setOnClickListener {
            startActivityForResult(
            Intent(this,
                CardDetailsActivity::class.java
            ), 1000
            ) }
    }

    override fun onResume() {
        super.onResume()
        list.clear()
        list.addAll(DBHelper(this@LinkAccountActivity).getAllData())
        if (list.isEmpty()) {
            selectBankCl.visibility = View.GONE
        } else {
            selectBankCl.visibility = View.VISIBLE
        }
        bankAccountListAdapter.notifyDataSetChanged()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && requestCode == 1000) {
           setResult(Activity.RESULT_OK)
            finish()
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
    }
}
