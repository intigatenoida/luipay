package com.app.luipay.link_account.bank_details

import android.app.Activity
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.SpannableString
import android.text.TextWatcher
import android.text.style.UnderlineSpan
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.app.luipay.DBHandler.BankAccountData
import com.app.luipay.DBHandler.DBHelper
import com.app.luipay.R
import com.app.luipay.utils.Appconstant
import com.app.luipay.utils.CommonUtils
import com.app.luipay.utils.custom_listener.CustomAlertDialog
import kotlinx.android.synthetic.main.activity_bank_details.*
import kotlinx.android.synthetic.main.bank_details_toolbar.*


class BankDetailsActivity : AppCompatActivity(), TextWatcher {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_bank_details)
        edtPassport.addTextChangedListener(this)
        edFullName.addTextChangedListener(this)
        edtPassport.addTextChangedListener(this)
        backBtn.setOnClickListener { finish() }
        val name = intent.getStringExtra(Appconstant.NAME)
        val bankCode = intent.getStringExtra(Appconstant.BANK_CODE)
        val bankURL = intent.getStringExtra(Appconstant.BANK_IMAGE_URL)
        val bankId = intent.getIntExtra(Appconstant.CONTACT_ID, 0)
        val fromEdit = intent.getBooleanExtra(Appconstant.From_edit, false)
        val id = intent.getIntExtra(Appconstant.ID, 0)
        if (fromEdit) {
            updateUI(id)
            txtUnlinkAccount.visibility = View.VISIBLE
            val content = SpannableString(txtUnlinkAccount.text.toString())
            content.setSpan(UnderlineSpan(), 0, content.length, 0)
            txtUnlinkAccount.text = content
        } else {
            txtUnlinkAccount.visibility = View.GONE
        }
        titleTv.text = name
        CommonUtils.loadImage(
            this@BankDetailsActivity,
            bankURL,
            logoImg,
            R.mipmap.ic_launcher_round
        )
        nextBtn.setOnClickListener {

            val data = BankAccountData()
            data.cardNumber = cardNumberEdt.text.toString()
            data.name = edFullName.text.toString()
            data.idPassport = edtPassport.text.toString()
            data.type = Appconstant.BANK_TYPE
            data.bankid = bankId
            data.bankName = name
            data.bankCode = bankCode
            data.bankImageUrl = bankURL
            val response = if (fromEdit) {
                DBHelper(this@BankDetailsActivity).updateCard(data, id.toLong())
            } else {
                DBHelper(this@BankDetailsActivity).InsertCardData(data)
            }
            startActivityForResult(
                Intent(
                    this@BankDetailsActivity,
                    BankDetailStatusActivity::class.java
                ).putExtra(Appconstant.API_KEY, response), 1000
            )
        }
    }

    private fun updateUI(id: Int) {
        val userData = DBHelper(this@BankDetailsActivity).getAllDataByUserId(id)
        cardNumberEdt.setText(userData[0].cardNumber.toString())
        edFullName.setText(userData[0].name.toString())
        edtPassport.setText(userData[0].idPassport.toString())

        txtUnlinkAccount.setOnClickListener {
            CustomAlertDialog(
                this@BankDetailsActivity,
                getString(R.string.app_name),
                getString(R.string.delete_bank_msg),
                getString(R.string.dialog_ok),
                getString(R.string.dialog_cancel),
                object : CustomAlertDialog.onAlertDialogCustomListener {
                    override fun onSuccessListener(dialog: DialogInterface?) {
                        dialog?.dismiss()
                        DBHelper(this@BankDetailsActivity).deleteUsingID(id)
                        setResult(Activity.RESULT_OK)
                        finish()
                    }

                    override fun onCancelListener() {

                    }
                })
        }
    }


    override fun afterTextChanged(s: Editable?) {
        nextBtn.isEnabled =
            (cardNumberEdt.text!!.isNotEmpty() && edFullName.text!!.isNotEmpty() && edtPassport.text!!.isNotEmpty())
    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

    }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && requestCode == 1000) {
            setResult(Activity.RESULT_OK)
            finish()
        }
    }
}
