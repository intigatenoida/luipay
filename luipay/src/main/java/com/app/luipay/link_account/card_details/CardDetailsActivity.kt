package com.app.luipay.link_account.card_details

import android.app.Activity
import android.app.DatePickerDialog
import android.content.DialogInterface
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.text.Editable
import android.text.SpannableString
import android.text.TextWatcher
import android.text.style.UnderlineSpan
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.app.luipay.DBHandler.BankAccountData
import com.app.luipay.DBHandler.DBHelper
import com.app.luipay.R
import com.app.luipay.link_account.bank_details.BankDetailStatusActivity
import com.app.luipay.utils.Appconstant
import com.app.luipay.utils.custom_listener.CustomAlertDialog
import kotlinx.android.synthetic.main.activity_bank_details.*
import kotlinx.android.synthetic.main.activity_card_details.*
import kotlinx.android.synthetic.main.activity_card_details.cardNumberEdt
import kotlinx.android.synthetic.main.activity_card_details.nextBtn
import kotlinx.android.synthetic.main.center_title_layout.*
import java.util.*

class CardDetailsActivity : AppCompatActivity(), TextWatcher {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_card_details)
        backBtn.setOnClickListener { finish() }
        titleTv.text = getString(R.string.visa_master_card)
        cardNumberEdt.addTextChangedListener(this)
        firstNameEdt.addTextChangedListener(this)
        expDateEdt.addTextChangedListener(this)
        cvvEdt.addTextChangedListener(this)
        val fromEdit = intent.getBooleanExtra(Appconstant.From_edit, false)
        val id = intent.getIntExtra(Appconstant.ID, 0)
        if (fromEdit) {
            updateUI(id)
            txtUnlinkCard.visibility = View.VISIBLE
            val content = SpannableString(txtUnlinkCard.text.toString())
            content.setSpan(UnderlineSpan(), 0, content.length, 0)
            txtUnlinkCard.text = content
        } else {
            txtUnlinkCard.visibility = View.GONE
        }
        nextBtn.setOnClickListener {
            val data = BankAccountData()
            data.cardNumber = cardNumberEdt.text.toString()
            data.name = firstNameEdt.text.toString()
            data.idPassport = expDateEdt.text.toString()
            data.cvv=cvvEdt.text.toString()
            data.type = Appconstant.CARD_TYPE
            val response = if (fromEdit) {
                DBHelper(this@CardDetailsActivity).updateCard(data, id.toLong())
            } else {
                DBHelper(this@CardDetailsActivity).InsertCardData(data)
            }
//            val response = DBHelper(this@CardDetailsActivity).InsertCardData(data)

                startActivityForResult(
                    Intent(
                        this@CardDetailsActivity,
                        BankDetailStatusActivity::class.java
                    ).putExtra(Appconstant.API_KEY, response), 1000
                )


        }

        expDateEdt.setOnClickListener {
            val cal = Calendar.getInstance()
            val datePicker = DatePickerDialog(
                this, R.style.my_dialog_theme,
                DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                    expDateEdt.setText(getValue(monthOfYear) + "/" + getValue(year))
                },
                cal.get(Calendar.YEAR),
                cal.get(Calendar.MONTH),
                cal.get(Calendar.DAY_OF_MONTH)
            )
            datePicker.datePicker.minDate = cal.timeInMillis
            datePicker.show()
            /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {

            }*/
        }
    }

    private fun getValue(value: Int): String {
        return if (value < 10) {
            "0$value"
        } else {
            "" + value
        }
    }

    override fun afterTextChanged(s: Editable?) {
        nextBtn.isEnabled =
            cardNumberEdt.text!!.isNotEmpty() && expDateEdt.text!!.isNotEmpty() && firstNameEdt.text!!.isNotEmpty() &&
                    cvvEdt.text!!.isNotEmpty()
    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
    }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
    }


    private fun updateUI(id: Int) {
        var userData = DBHelper(this@CardDetailsActivity).getAllDataByUserId(id)
        cardNumberEdt.setText(userData[0].cardNumber.toString())
        firstNameEdt.setText(userData[0].name.toString())
        expDateEdt.setText(userData[0].idPassport.toString())
        cvvEdt.setText(userData[0].cvv.toString())


        txtUnlinkCard.setOnClickListener {
            CustomAlertDialog(
                this@CardDetailsActivity,
                getString(R.string.app_name),
                getString(R.string.delete_bank_msg),
                getString(R.string.dialog_ok),
                getString(R.string.dialog_cancel),
                object : CustomAlertDialog.onAlertDialogCustomListener {
                    override fun onSuccessListener(dialog: DialogInterface?) {
                        dialog?.dismiss()
                        DBHelper(this@CardDetailsActivity).deleteUsingID(id)
                        setResult(Activity.RESULT_OK)
                        finish()
                    }

                    override fun onCancelListener() {

                    }
                })
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 1000 && resultCode == Activity.RESULT_OK) {
            setResult(Activity.RESULT_OK)
            finish()
        }
    }
}
