package com.app.luipay.link_account.bank_details

import android.app.Activity
import android.os.Bundle
import androidx.core.content.ContextCompat
import com.app.luipay.R
import com.app.luipay.base.BaseActivity
import com.app.luipay.utils.Appconstant
import kotlinx.android.synthetic.main.bank_card_status.*
import kotlinx.android.synthetic.main.center_title_layout.*

class BankDetailStatusActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.bank_card_status)
        backBtn.setOnClickListener { finish() }
        titleTv.text = getString(R.string.link_account_card)
        intent?.let {
//            if (it.getLongExtra(Appconstant.API_KEY, 0) >=0) {
                container.setBackgroundColor(
                    ContextCompat.getColor(
                        this@BankDetailStatusActivity,
                        R.color.success_background_color
                    )
                )
                txtMessage.text = getString(R.string.msg_bank_success)
//            } else {
//                container.setBackgroundColor(
//                    ContextCompat.getColor(
//                        this@BankDetailStatusActivity,
//                        R.color.failed_background_color
//                    )
//                )
//                txtMessage.text = getString(R.string.msg_bank_failed)
//            }

        }
        backBtn.setOnClickListener { onBackPressed() }
    }

    override fun onBackPressed() {
        setResult(Activity.RESULT_OK)
        finish()

    }
}