package com.app.luipay.link_account

import android.content.Context
import android.content.Intent
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.app.luipay.DBHandler.BankAccountData
import com.app.luipay.R
import com.app.luipay.link_account.bank_details.BankDetailsActivity
import com.app.luipay.link_account.card_details.CardDetailsActivity
import com.app.luipay.utils.Appconstant
import com.app.luipay.utils.CommonUtils
import kotlinx.android.synthetic.main.bank_header_layout.view.*
import kotlinx.android.synthetic.main.bank_item_layout.view.*

class BankAccountListAdapter(var context: Context, var list: ArrayList<BankAccountData?>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    val headerLayout = 333
    val itemLayout = 222



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (viewType == 333) {
            val view =
                LayoutInflater.from(context).inflate(R.layout.bank_header_layout, parent, false)
            return HeaderViewHolder(view)
        }
        val view = LayoutInflater.from(context).inflate(R.layout.bank_item_layout, parent, false)
        return ItemViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun getItemViewType(position: Int): Int {
        if (list[position] == null) {
            return headerLayout
        }
        return itemLayout
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is HeaderViewHolder) {
            if (position == 0) {
                holder.itemView.tvHederHeading.text =
                    context.getString(R.string.select_affiliate_bank)
            } else {
                holder.itemView.tvHederHeading.text = context.getString(R.string.another_form)
            }
        } else if (holder is ItemViewHolder) {
            val bankData = list[position]!!
            holder.itemView.bankNameTv.text = list[position]!!.name
            holder.itemView.setOnClickListener {
                var intent1: Intent? = null
                intent1 = if (!TextUtils.isEmpty(bankData.cvv)) {
                    Intent(context, CardDetailsActivity::class.java)
                } else {
                    Intent(context, BankDetailsActivity::class.java)
                }.apply {
                    putExtra(Appconstant.NAME, bankData.bankName)
                    putExtra(Appconstant.CONTACT_ID, bankData.bankid)
                    putExtra(Appconstant.ID, bankData.id)
                    putExtra(Appconstant.BANK_CODE, bankData.bankCode)
                    putExtra(Appconstant.BANK_IMAGE_URL, bankData.bankImageUrl)
                    putExtra(Appconstant.From_edit, true)
                }

                context.startActivity(intent1)
            }
            CommonUtils.loadImage(
                context,
                list[position]!!.bankImageUrl,
                holder.itemView.bankLogoImg,
                R.mipmap.ic_launcher_round
            )
            if (position == itemCount - 1) {
                holder.itemView.divider.visibility = View.GONE
            } else {
                holder.itemView.divider.visibility = View.VISIBLE
            }
        }
    }

    class HeaderViewHolder(view: View) : RecyclerView.ViewHolder(view)
    class ItemViewHolder(view: View) : RecyclerView.ViewHolder(view)

}