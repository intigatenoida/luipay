package com.app.luipay.profile.dialog_fragment

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.app.luipay.R
import com.app.luipay.base.BaseDialogFragment
import kotlinx.android.synthetic.main.layout_dialog_change_profile_pic.view.*


class ChangeImageDialog : BaseDialogFragment() {

    public interface OnImageSelectionListener {
        fun onGallerySelectionListener()
        fun onCameraSelectionListener()
        fun onPDFSelectionListener()
    }

    lateinit var listener: OnImageSelectionListener

    companion object {
        fun getInstance(): ChangeImageDialog {
            val fragment = ChangeImageDialog()
//            val bundle = Bundle()
//            bundle.putBoolean("fromKYC",fromKYC)
//            fragment.arguments=bundle
            return fragment
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        listener = context as OnImageSelectionListener
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val rootView = inflater.inflate(R.layout.layout_dialog_change_profile_pic, null)
        rootView.findViewById<TextView>(R.id.tvLogin).text=getString(R.string.cancel)
        rootView.findViewById<TextView>(R.id.tvLogin).setOnClickListener {
            dialog!!.cancel()
        }

        rootView.findViewById<TextView>(R.id.gallery).setOnClickListener {
            dialog!!.cancel()
            if (::listener.isInitialized) {
                listener.onGallerySelectionListener()
            }
        }
        rootView.findViewById<TextView>(R.id.camera).setOnClickListener {
            dialog!!.cancel()
            if (::listener.isInitialized) {
                listener.onCameraSelectionListener()
            }
        }

            rootView.viewBottom.visibility=View.VISIBLE
            rootView.pdf.visibility=View.VISIBLE
            rootView.pdf.setOnClickListener{
                dialog!!.dismiss()
                if (::listener.isInitialized) {
                    listener.onPDFSelectionListener()
                }
            }


        return rootView
    }
}