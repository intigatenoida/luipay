package com.app.luipay.utils

class Appconstant {

    companion object{
        public val recharge=1
        public val more=2
        public val less=3
        public val no=4
        const val mobile="mobile"
        const val password="password"
        val API_KEY = "A40BE959-D225-4C10-86E6-04419D83FD26"
        val API_HEADER = "application/json"
        val CONTACT_ID="CONTACT_ID"
        val NAME="NAME"
        val LUI_REQUEST="LUI_REQUEST"
        val ID="ID"
        val BANK_NAME="BANK_NAME"
        val BANK_CODE="BANK_CODE"
        val BANK_IMAGE_URL="BANK_IMAGE_URL"
        val BANK_TYPE=1
        val CARD_TYPE=2
        val From_edit="From_edit"
        val DATA="data"
        const val APP_LOCK_ENABLE="APP_LOCK_ENABLE"
        const val APP_LOCK_DISABLE="APP_LOCK_DISABLE"


        //--> Money Transfer
        const val TRANSAMOUNT = "TRANSAMOUNT"
        const val TRANSSTATUS="TRANSSTATUS"
        const val TRANSCODE="TRANSCODE"

        //--> Services
        const val SERVICES_LIST ="SERVICESLIST"

        const val PERMISSION_WRITE_EXTERNAL_STORAGE: Int = 333
        const val PERMISSION_CAMERA: Int = 334
        const val REQUEST_UPLOAD_IMAGE: Int = 4444

        const val TAG_DIALOG = "dialog"
    }


}