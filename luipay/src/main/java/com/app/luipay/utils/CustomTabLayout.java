package com.app.luipay.utils;

import android.content.Context;
import android.util.AttributeSet;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.material.tabs.TabLayout;

public class CustomTabLayout extends TabLayout {
    @NonNull
    private ViewGroup tabStrip;

    public CustomTabLayout(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        tabStrip = (ViewGroup) getChildAt(0);
        tabStrip.getLayoutParams().height = LayoutParams.WRAP_CONTENT;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        // Force the height of this ViewGroup to be the same height of the tabStrip
        setMeasuredDimension(getMeasuredWidth(), tabStrip.getMeasuredHeight());
    }

}
