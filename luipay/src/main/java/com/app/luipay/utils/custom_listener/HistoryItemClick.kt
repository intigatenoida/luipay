package com.app.luipay.utils.custom_listener

import com.app.luipay.history.responses.HistoryData

interface HistoryItemClick {

    fun onItemClick(historyData: HistoryData)
}