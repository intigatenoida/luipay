package com.app.luipay.utils.interfaces

import android.app.Dialog
import android.os.Bundle

interface DialogFragmentClickListener {
    fun onPositiveButtonClick(dialog: Dialog)
    fun onNegativeButtonClick(dialog: Dialog)
    fun onDialogDataReceived(obj:Any,bundle: Bundle?)
}