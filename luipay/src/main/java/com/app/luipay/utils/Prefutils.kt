package com.app.luipay.utils

import android.content.Context
import androidx.preference.PreferenceManager

class Prefutils {

    companion object {
        const val USERID = "userId"
        const val PASSWORD = "PASSWORD"
        const val BALANCE = "BALANCE"
        const val COUNTRY_CODE_API = "COUNTRY_CODE_API"
        const val COUNTRY_CODE = "COUNTRY_CODE"
        const val MOBILE = "MOBILE"
        val ACCESS_TOKEN = "ACCESS_TOKEN"
        val API_LANGUAGE = "API_LANGUAGE"
        val PROFILE = "PROFILE"
        val APP_LOCK="APP_LOCK"
        val APP_LOCK_PIN="APP_LOCK_PIN"

        //-- Profile Section
        const val NAME = "NAME"
        const val ADDRESS = "ADDRESS"
        const val COUNTRYCODE = "COUNTRYCODE"
        const val COUNTRYCODEID = "COUNTRYCODEID"
        const val DOB = "DOB"
        const val GENDER = "GENDER"
        const val EMAIL = "EMAIL"
        const val ISKYCPENDING = "ISKYCPENDING"
        const val MOBILENO = "MOBILENO"
        const val PROFILEPICURL = "PROFILEPICURL"
        const val TRANSACTIONLIMIT = "TRANSACTIONLIMIT"

        //--> Device token
        const val DEVICE_TOKEN ="DEVICETOKEN"


        fun saveToPrefs(
            context: Context?,
            key: String?,
            value: String?
        ): String? {
            val prefs = PreferenceManager
                .getDefaultSharedPreferences(context)
            val editor = prefs.edit()
            editor.putString(key, value)
            editor.apply()
            return key
        }

        fun saveToPrefsInt(
            context: Context?,
            key: String?,
            value: Int
        ): String? {
            val prefs = PreferenceManager
                .getDefaultSharedPreferences(context)
            val editor = prefs.edit()
            editor.putInt(key, value)
            editor.apply()
            return key
        }

        fun saveToPrefsBoolean(
            context: Context?,
            key: String?,
            value: Boolean
        ): String? {
            val prefs = PreferenceManager
                .getDefaultSharedPreferences(context)
            val editor = prefs.edit()
            editor.putBoolean(key, value)
            editor.apply()
            return key
        }

        fun logout(c: Context?) {
            val prefs = PreferenceManager
                .getDefaultSharedPreferences(c)
            val editor = prefs.edit()
            editor.clear()
            editor.apply()
        }

        fun getFromPrefs(
            context: Context?,
            key: String?
        ): String? {
            return if (context != null) {
                val sharedPrefs = PreferenceManager
                    .getDefaultSharedPreferences(context)
                try {
                    sharedPrefs.getString(key, "")
                } catch (e: Exception) {
                    ""
                }
            } else {
                ""
            }
        }

        fun getFromPrefsInt(
            context: Context?,
            key: String?
        ): Int {
            return if (context != null) {
                val sharedPrefs = PreferenceManager
                    .getDefaultSharedPreferences(context)
                try {
                    sharedPrefs.getInt(key, 0)
                } catch (e: Exception) {
                    0
                }
            } else {
                0
            }
        }

        fun getFromPrefsBoolean(
            context: Context?,
            key: String?
        ): Boolean {
            return if (context != null) {
                val sharedPrefs = PreferenceManager
                    .getDefaultSharedPreferences(context)
                try {
                    sharedPrefs.getBoolean(key, false)
                } catch (e: Exception) {
                    false
                }
            } else {
                false
            }
        }


    }

}