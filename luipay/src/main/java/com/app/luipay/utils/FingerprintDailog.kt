package com.app.luipay.utils

import android.content.DialogInterface
import android.os.Bundle
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.app.luipay.R
import com.app.luipay.base.BaseDialogFragment
import kotlinx.android.synthetic.main.layout_dialog_fingerprint.view.*

class FingerprintDailog : BaseDialogFragment() {

    interface onCancelListener {
        fun onCancel()
        fun usePin()
    }


    companion object {
        lateinit var listener: onCancelListener
        fun getInstance(customListener: onCancelListener): FingerprintDailog {
            val fragment = FingerprintDailog()
            listener = customListener
            return fragment
        }
    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val rootView = inflater.inflate(R.layout.layout_dialog_fingerprint, null)

        rootView.tvCancel.setOnClickListener {
            listener?.let {
                listener.onCancel()
            }
        }

        rootView.tvNext.setOnClickListener {
            listener?.let {
                listener.usePin()
            }
        }
        dialog!!.setOnKeyListener(object : DialogInterface.OnKeyListener {
            override fun onKey(dialog: DialogInterface?, keyCode: Int, event: KeyEvent?): Boolean {
                if (keyCode == KeyEvent.KEYCODE_BACK){
                    listener?.let {
                        listener.onCancel()
                    }
                    return true
                }
                return false
            }

        })

        dialog!!.cancel()

        return rootView
    }


}