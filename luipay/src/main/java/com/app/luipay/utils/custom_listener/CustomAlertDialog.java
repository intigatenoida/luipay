package com.app.luipay.utils.custom_listener;

import android.content.Context;
import android.content.DialogInterface;

import android.text.TextUtils;

import androidx.appcompat.app.AlertDialog;

import com.app.luipay.R;


/**
 * Created by ravik on 24-11-2017.
 */

public class CustomAlertDialog {
    public interface onAlertDialogCustomListener{
        public void onSuccessListener(DialogInterface dialog);
        public void onCancelListener();
    }

    public CustomAlertDialog(Context context){
        new CustomAlertDialog(context,"");
    }
    public CustomAlertDialog(Context context, String message){
        new CustomAlertDialog(context,"",message);
    }
    public CustomAlertDialog(Context context, String title, String message){
        new CustomAlertDialog(context,title,message,"","");
    }
    public CustomAlertDialog(Context context, String message, String okButton, onAlertDialogCustomListener lsitener){
        new CustomAlertDialog(context,"",message,okButton,"",lsitener);
    }
    public CustomAlertDialog(Context context, String title, String message, String okButton, String cancekButton){
        new CustomAlertDialog(context,title,message,okButton,cancekButton,null);
    }

    public CustomAlertDialog(Context context, String title, String message, String okButton, String cancekButton, final onAlertDialogCustomListener listener){
        AlertDialog.Builder builder;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M){
            builder=new AlertDialog.Builder(context, R.style.Theme_AppCompat_Light_Dialog_Alert);
        }else{
            builder=new AlertDialog.Builder(context);
        }
        if (!TextUtils.isEmpty(title)){
            builder.setTitle(title);
        }else{
            builder.setTitle(context.getString(R.string.app_name));
        }
        if (!TextUtils.isEmpty(message)){
            builder.setMessage(message);
        }else{
            builder.setMessage("");
        }
        builder.setPositiveButton(TextUtils.isEmpty(okButton) ? context.getString(R.string.dialog_ok) : okButton, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (listener!=null){
                    listener.onSuccessListener(dialog);
                }
            }
        });
        builder.setNegativeButton(TextUtils.isEmpty(cancekButton) ? context.getString(R.string.dialog_cancel) : cancekButton, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (listener!=null){
                    listener.onCancelListener();
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

}
