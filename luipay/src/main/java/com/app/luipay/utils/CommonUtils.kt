package com.app.luipay.utils

import android.R
import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Color
import android.hardware.fingerprint.FingerprintManager
import android.net.Uri
import android.os.Build
import android.provider.Settings
import android.text.TextUtils
import android.util.DisplayMetrics
import android.util.Log
import android.view.WindowManager
import android.widget.ImageView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import com.app.luipay.BuildConfig
import com.app.luipay.request.BaseRequest
import com.app.luipay.response.AppStatusResponse.AppStatusReponse
import com.app.luipay.retrofit.ApiClient
import com.app.luipay.retrofit.ApiInterface
import com.app.luipay.retrofit.CallbackManager
import com.bumptech.glide.Glide
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.messaging.FirebaseMessaging
import com.google.gson.GsonBuilder
import java.io.*
import java.text.DecimalFormat
import java.text.SimpleDateFormat
import java.util.*


class CommonUtils {

    companion object {
         fun showToast(context: Context, value: String) {
            Toast.makeText(context, value, Toast.LENGTH_LONG).show()
        }

        public fun share(context: Context) {
            val shareBody = "Refer Code :"
            val sharingIntent = Intent(Intent.ACTION_SEND)
            sharingIntent.type = "text/plain"
            sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "LuiPay")
            sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody)
            context.startActivity(
                Intent.createChooser(
                    sharingIntent, shareBody
                )
            )
        }

        fun isDebug(): Boolean {
            return BuildConfig.ENABLE_LOG
        }

        fun openSettingDialog(context: Context) {

            var builder = AlertDialog.Builder(context)
            //Uncomment the below code to Set the message and title from the strings.xml file
            //Uncomment the below code to Set the message and title from the strings.xml file
            builder.setMessage("Permission is required for the further process, Please provide the permisson")
                .setTitle("Permission Dialog")
                .setCancelable(false)
                .setPositiveButton("Yes",
                    DialogInterface.OnClickListener { dialog, id ->
                        dialog.cancel()
                        val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
                        val uri: Uri = Uri.fromParts("package", context.getPackageName(), null)
                        intent.data = uri
                        (context as Activity).startActivityForResult(
                            intent,
                            AppPermission.SETTING_CODE
                        )

                    })
                .setNegativeButton("No",
                    DialogInterface.OnClickListener { dialog, id ->
                        //  Action for 'NO' Button
                        dialog.cancel()

                    })
            //Creating dialog box
            //Creating dialog box
            val alert: AlertDialog = builder.create()
            //Setting the title manually
            //Setting the title manually
            alert.show()
            alert.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(Color.parseColor("#ffffff"))
            alert.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(Color.parseColor("#ffffff"))

        }

        fun checkStatusMessage(messageKey: String) {
            val file =
                File("data/data/com.app.luipay/responsecode/statusMessage.text")
            if (file.exists()) {
                val data = StringBuilder()
                var br: BufferedReader? = null
                try {
                    br = BufferedReader(FileReader(file))
                    var line: String?
                    while (br.readLine().also { line = it } != null) {
                        data.append(line)
                        data.append('\n')
                    }
                } catch (e: IOException) {
                    e.printStackTrace()
                } finally {
                    br!!.close()
                }
                if (!data.toString().trim().equals(messageKey, false)) {
                    updateResponseMessage(messageKey)
                }
            } else {
                saveStatusMessage(messageKey)
            }
        }

        private fun updateResponseMessage(messageKey: String) {
            val helper = ApiClient.getClient().create(ApiInterface::class.java)
            val request = BaseRequest()
            request.CountryCodeId = 1
            request.DeviceNotificationID = ""
            request.LanguageTypeId = 1
            request.ServiceAuthenticationToken = ApiClient.android_token

            val call = helper.GetResponseMessages(request, "application/json")

            call.enqueue(object : CallbackManager<AppStatusReponse>() {
                override fun onSuccess(res: AppStatusReponse?, message: String) {
                    val responseData = res as AppStatusReponse
                    responseData.let {
                        createFile(
                            GsonBuilder().disableHtmlEscaping().create().toJson(responseData)
                        )
                    }
                }

                override fun onFailure(message: String) {
                }

                override fun onError(error: String) {

                }

            })
        }

        private fun createFile(responseData: String) {
            var fos: FileWriter? = null
            var isDirectoryCreated = false
            try {
                val directory =
                    File("data/data/com.app.luipay/responsecode")
                if (directory.exists()) {
                    isDirectoryCreated = true
                } else {
                    isDirectoryCreated = directory.mkdir()
                }
                if (isDirectoryCreated) {
                    val file =
                        File("data/data/com.app.luipay/responsecode/rescode.text")
                    if (file.createNewFile()) {
                        fos = FileWriter(file)
                        fos.write(responseData)
                    }
                }
            } catch (fileNotFound: FileNotFoundException) {
            } catch (ioException: IOException) {
                ioException.printStackTrace()
            } finally {
                fos?.close()
            }
        }

        fun saveStatusMessage(messageKey: String) {
            var fos: FileWriter? = null
            var isDirectoryCreated = false
            try {
                val directory =
                    File("data/data/com.app.luipay/responsecode")
                if (directory.exists()) {
                    isDirectoryCreated = true
                } else {
                    isDirectoryCreated = directory.mkdir()
                }
                if (isDirectoryCreated) {
                    val file =
                        File("data/data/com.app.luipay/responsecode/statusMessage.text")
                    if (file.exists()) {
                        file.delete()
                    }
                    if (file.createNewFile()) {
                        fos = FileWriter(file)
                        fos.write(messageKey)
                    }
                }
            } catch (fileNotFound: FileNotFoundException) {
                fileNotFound.printStackTrace()
            } catch (ioException: Exception) {
                ioException.printStackTrace()
            } finally {
                fos?.close()
            }
        }


        fun isBioMetricEnable(context: Context): Boolean {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                try {
                    val fingerprintManager =
                        context.getSystemService(Context.FINGERPRINT_SERVICE) as? FingerprintManager
                    if (fingerprintManager != null) {
                        return fingerprintManager.isHardwareDetected && fingerprintManager.hasEnrolledFingerprints()
                    } else {
                        return false
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                    return false
                }
            } else {
                return false
            }

        }

        fun loadImage(context: Context, url: String?, imageView: ImageView, placeHolder: Int) {
            if (TextUtils.isEmpty(url)) {
                imageView.setImageDrawable(ContextCompat.getDrawable(context, placeHolder))
            } else {
                Glide
                    .with(context)
                    .load(url)
                    .centerCrop()
                    .placeholder(ContextCompat.getDrawable(context, placeHolder))
                    .into(imageView)
            }

        }

        fun isEmailValid(email: String): Boolean {
            val emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+"
            return email.matches(emailPattern.toRegex())
        }

        public fun sendMail(context: Context) {
            val intent = Intent().apply {
                action = Intent.ACTION_SEND
                data = Uri.parse("mailto:")
                type = "message/rfc822"
                putExtra(Intent.EXTRA_EMAIL, "support@luipay.com")
                putExtra(Intent.EXTRA_SUBJECT, "LuiPay Support")
            }

            try {
                if (intent.resolveActivity(context.packageManager) != null) {
                    intent.setPackage("com.google.android.gm")
                    context.startActivity(intent)
                } else {
                }
            } catch (e: java.lang.Exception) {
                context.startActivity(Intent.createChooser(intent, "Send mail..."));
            }
        }

        fun calculateDialogDimensions(
            activity: Activity?,
            dialog: Dialog?,
            width: Float,
            height: Float
        ) {
            if (activity == null || dialog?.window == null) {
                return
            }

            val displayMetrics = DisplayMetrics()
            activity.windowManager.defaultDisplay.getMetrics(displayMetrics)
            val displayWidth = displayMetrics.widthPixels
            val displayHeight = displayMetrics.heightPixels
            val layoutParams = WindowManager.LayoutParams()
            layoutParams.copyFrom(dialog.window!!.attributes)
            // Set the alert dialog window width and height
// Set alert dialog width equal to screen width 90%
// int dialogWindowWidth = (int) (displayWidth * 0.9f);
// Set alert dialog height equal to screen height 90%
// int dialogWindowHeight = (int) (displayHeight * 0.9f);
// Set the width and height for the layout parameters
// This will bet the width and height of alert dialog
            if (width != ConstraintLayout.LayoutParams.WRAP_CONTENT.toFloat() && width != ConstraintLayout.LayoutParams.MATCH_PARENT.toFloat()) { // Set alert dialog width equal to screen width as ratio using width like 0.7f to 70%
                layoutParams.width = (displayWidth * width).toInt()
            } else {
                layoutParams.width = width.toInt()
            }
            if (height != ConstraintLayout.LayoutParams.WRAP_CONTENT.toFloat() && height != ConstraintLayout.LayoutParams.MATCH_PARENT.toFloat()) { // Set alert dialog height equal to screen height as ratio using height like 0.7f to 70%
                layoutParams.height = (displayHeight * height).toInt()
            } else {
                layoutParams.height = height.toInt()
            }
            // Apply the newly created layout parameters to the alert dialog window
            dialog.window!!.attributes = layoutParams
            dialog.window!!.setBackgroundDrawableResource(R.color.transparent)
        }

        fun getTwoDecimal2(amount: Double?): String? {
            var convertedtext = "0.00"
            try {
                convertedtext = String.format("%.2f", amount)
            } catch (e: java.lang.Exception) {
            }
            return convertedtext
        }

        fun adjustDecimal(parameterValue: String?): String? {
            var paramD = 0.0
            val df = DecimalFormat("0.##")
            paramD = try {
                java.lang.Double.valueOf(parameterValue!!)
            } catch (e: java.lang.Exception) {
                0.0
            }
            return df.format(paramD)
        }

        fun getTwoDecimal(amount: Double?): String? {
            var convertedtext = "0.00"
            try {
                val df = DecimalFormat("##.##")
                convertedtext = df.format(amount)
            } catch (e: java.lang.Exception) {

            }
            return convertedtext
        }

        fun getTwoDecimal(amount: String?): String? {
            var convertedtext = "0.00"
            try {
                val df = DecimalFormat("##.##")
                val temp = amount?.toDouble() ?: 0.0
                convertedtext = df.format(temp)
            } catch (e: java.lang.Exception) {

            }
            return convertedtext
        }

        // Used for Notification Time
        fun convertToLocalTimeZone(endtime: String): String {
            val sdf = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.ENGLISH)
            sdf.timeZone = (TimeZone.getTimeZone("UTC"))
            val date = sdf.parse(endtime)
            val df = SimpleDateFormat("hh:mm, MMMM dd/yyyy", Locale.ENGLISH)
            df.timeZone = (TimeZone.getDefault())
            return df.format(date)
        }

        fun onlyDate(endtime: String): String {
            val sdf = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.ENGLISH)
            sdf.timeZone = (TimeZone.getTimeZone("UTC"))
            val date = sdf.parse(endtime)
            val df = SimpleDateFormat("MMMM dd/yyyy", Locale.ENGLISH)
            df.timeZone = (TimeZone.getDefault())
            return df.format(date)
        }

        @JvmStatic
        fun setNotificationDeviceInfo(
            context: Context,
            tokenId: String,
            subscribeTopic: String
        ) {
            if (tokenId.isEmpty()) {
                FirebaseInstanceId.getInstance().instanceId
                    .addOnCompleteListener(OnCompleteListener { task ->
                        if (!task.isSuccessful) {
                            Log.w("DEVICE_TOKEN_TAG", "getInstanceId failed", task.exception)
                            return@OnCompleteListener
                        }

                        // Get new Instance ID token
                        val token = task.result?.token.toString()
                        Prefutils.saveToPrefs(context, Prefutils.DEVICE_TOKEN, token)
                    })
            } else {
            }
            FirebaseMessaging.getInstance().subscribeToTopic(subscribeTopic)
        }
    }


}