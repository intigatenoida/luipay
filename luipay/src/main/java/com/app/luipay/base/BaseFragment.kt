package com.app.luipay.base

import android.app.Activity
import android.content.Context
import android.graphics.Color
import android.net.ConnectivityManager
import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.widget.AppCompatTextView
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import com.app.luipay.R
import com.app.luipay.utils.Appconstant
import com.google.android.material.tabs.TabLayout

open class BaseFragment : Fragment() {

    lateinit var mContext: Context

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mContext = context
    }

    fun showProcessDialog() {
        if (::mContext.isInitialized) {
            (mContext as BaseActivity).showProcessDialog()
        }
    }

    fun hideProcessDialog() {
        if (::mContext.isInitialized) {
            (mContext as BaseActivity).hideProcessDailog()
        }
    }

    fun setFragment(fragment: Fragment, tag: String) {
        val fm = activity!!.supportFragmentManager
        val ft = fm.beginTransaction()
        ft.replace(R.id.mainContainer, fragment)
        ft.addToBackStack(tag)
        ft.commit()
    }


    fun addTab(
        tab: TabLayout.Tab?,
        title: String,
        color: Int = Color.BLACK
    ) {
        if (tab == null) {
            return
        }
        val customTabView: View =
            LayoutInflater.from(activity!!).inflate(R.layout.layout_tab_1, null)
        val tabTitle = customTabView.findViewById<TextView>(R.id.tv_tab_title)
        tabTitle.text = title
        tabTitle.setTextColor(color)
        tab.customView = customTabView
        tab.tag = title
    }

    fun changeTabAppearance(p0: TabLayout.Tab?, color: Int = Color.BLACK) {
        if (p0?.customView == null) {
            return
        }
        val tabTitle = p0.customView!!.findViewById<AppCompatTextView>(R.id.tv_tab_title)
        tabTitle.setTextColor(color)
    }

    fun wrapTabIndicatorToTitle(
        tabLayout: TabLayout,
        externalMargin: Int,
        internalMargin: Int
    ) {
        val tabStrip = tabLayout.getChildAt(0)
        if (tabStrip is ViewGroup) {
            val childCount = tabStrip.childCount
            for (i in 0 until childCount) {
                val tabView = tabStrip.getChildAt(i)
                //set minimum width to 0 for instead for small texts, indicator is not wrapped as expected
                tabView.minimumWidth = 0
                // set padding to 0 for wrapping indicator as title
                tabView.setPadding(0, tabView.paddingTop, 0, tabView.paddingBottom)
                // setting custom margin between tabs
                if (tabView.layoutParams is ViewGroup.MarginLayoutParams) {
                    val layoutParams =
                        tabView.layoutParams as ViewGroup.MarginLayoutParams
                    if (i == 0) { // left
                        settingMargin(layoutParams, externalMargin, internalMargin)
                    } else if (i == childCount - 1) { // right
                        settingMargin(layoutParams, internalMargin, externalMargin)
                    } else { // internal
                        settingMargin(layoutParams, internalMargin, internalMargin)
                    }
                }
            }
            tabLayout.requestLayout()
        }
    }

    private fun settingMargin(
        layoutParams: ViewGroup.MarginLayoutParams,
        start: Int,
        end: Int
    ) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            layoutParams.marginStart = start
            layoutParams.marginEnd = end
            layoutParams.leftMargin = start
            layoutParams.rightMargin = end
        } else {
            layoutParams.leftMargin = start
            layoutParams.rightMargin = end
        }
    }

    open fun isOnline(): Boolean {
        if (activity == null) {
            return false
        }
        val cm =
            activity!!.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val netInfo = cm.activeNetworkInfo
        return netInfo != null && netInfo.isConnectedOrConnecting
    }

    fun showMessage(message: String) {
        Toast.makeText(activity!!, message, Toast.LENGTH_SHORT).show()
    }

    fun showPrompt(dialogFragment: DialogFragment) {
        hideKeyboard()
        dismissPrompt()
        activity!!.supportFragmentManager
            .beginTransaction()
            .add(dialogFragment, Appconstant.TAG_DIALOG)
            .commitAllowingStateLoss()
    }

    fun hideKeyboard() {
        if (activity!!.currentFocus == null) {
            activity!!.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
            return
        }
        val inputMethodManager = activity!!.getSystemService(
            Activity.INPUT_METHOD_SERVICE
        ) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(
            activity!!.currentFocus!!.windowToken, 0
        )
    }

    fun dismissPrompt(): Boolean {
        if (activity == null) {
            return false
        }
        val prev: Fragment? =
            activity!!.supportFragmentManager.findFragmentByTag(Appconstant.TAG_DIALOG)
        if (prev != null) {
            val dialogFragment = prev as DialogFragment
            dialogFragment.dismissAllowingStateLoss()
            return true
        }
        return false
    }
}