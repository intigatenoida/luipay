package com.app.luipay.base

import android.content.Context
import android.net.ConnectivityManager
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment

open class BaseActivity : AppCompatActivity() {

    private lateinit var dialogFragment: ProgressDialog


    public fun LoadFragment(instance: Fragment, tag: String, id: Int) {
        val transition = supportFragmentManager.beginTransaction()
        transition.replace(id, instance, tag)
        transition.commit()
    }

    public fun addFragment(instance: Fragment, tag: String, id: Int) {
        val transition = supportFragmentManager.beginTransaction()
        transition.add(id, instance, tag)
        transition.commit()
    }

    fun showProcessDialog() {
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        val prev = supportFragmentManager.findFragmentByTag("ProcessDialog")
        if (prev != null) {
            val df = prev as ProgressDialog
            df.dismissAllowingStateLoss()
//            fragmentTransaction.remove(prev)
        }
        fragmentTransaction.addToBackStack(null)
        dialogFragment = ProgressDialog.getInstance()
        fragmentTransaction.add(dialogFragment, "ProcessDialog").commitAllowingStateLoss()
//        dialogFragment.show(fragmentTransaction, "ProcessDialog")
    }

    fun hideProcessDailog() {
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        val prev = supportFragmentManager.findFragmentByTag("ProcessDialog")
        if (prev != null) {
            val df = prev as ProgressDialog
            df.dismissAllowingStateLoss()
//            fragmentTransaction.remove(prev)
        }
        if (::dialogFragment.isInitialized) {
            dialogFragment.dismiss()

        }
    }

    open fun isOnline(): Boolean {
        val cm =
            getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val netInfo = cm.activeNetworkInfo
        return netInfo != null && netInfo.isConnectedOrConnecting
    }

    override fun onBackPressed() {
        val count = supportFragmentManager.backStackEntryCount
        if (count == 1) {
            supportFragmentManager.popBackStack()
            super.onBackPressed()
        } else if (count == 0) {
            super.onBackPressed()
        } else {
            supportFragmentManager.popBackStack()
        }
    }

}