package com.app.luipay.base

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.fragment.app.DialogFragment

open class BaseDialogFragment : DialogFragment()  {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(
            DialogFragment.STYLE_NORMAL,
            android.R.style.Theme_Translucent_NoTitleBar_Fullscreen
        )
    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        dialog!!.window!!.setLayout(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.MATCH_PARENT
        )
        dialog!!.setCanceledOnTouchOutside(false)

        dialog!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT));

        dialog!!.setCancelable(false)
        return super.onCreateView(inflater, container, savedInstanceState)
    }

}