package com.app.luipay.base

import android.app.Activity
import android.app.KeyguardManager
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.provider.Settings
import android.text.TextUtils
import com.app.luipay.R
import com.app.luipay.dashboard.DashboardActivity
import com.app.luipay.help.CreateLocalePinConfirmation
import com.app.luipay.login.LoginActivity
import com.app.luipay.request.BaseRequest
import com.app.luipay.response.AppStatusResponse.AppStatusReponse
import com.app.luipay.retrofit.ApiClient
import com.app.luipay.retrofit.ApiInterface
import com.app.luipay.retrofit.CallbackManager
import com.app.luipay.utils.*
import com.app.luipay.utils.custom_listener.CustomAlertDialog
import com.google.gson.GsonBuilder
import java.io.File
import java.io.FileNotFoundException
import java.io.FileWriter
import java.io.IOException


class SplashActivity : BaseActivity(), BiomatricAuthencation.BiomatricAuthencationListener {

    private var isPinClicked: Boolean = false
    private var dialogFragment: FingerprintDailog? = null
    val INTENT_AUTHENTICATE = 1000
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        if (TextUtils.isEmpty(Prefutils.getFromPrefs(this@SplashActivity, Prefutils.APP_LOCK))) {
            Prefutils.saveToPrefs(
                this@SplashActivity,
                Prefutils.APP_LOCK,
                Appconstant.APP_LOCK_ENABLE
            )
        }
    }

    override fun onResume() {
        super.onResume()
        checkResponse()
    }

    private fun checkResponse() {

        try {
            val file = File("data/data/com.app.luipay/responsecode/rescode.text")
            if (!file.exists()) {
                getResponse()
            } else {
                loginViaBioAuthentication()
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun loginViaBioAuthentication() {
        val token = Prefutils.getFromPrefs(this@SplashActivity, Prefutils.ACCESS_TOKEN)
        Handler().postDelayed({
            if (TextUtils.isEmpty(token) || TextUtils.isEmpty(
                    Prefutils.getFromPrefs(
                        this@SplashActivity,
                        Prefutils.PASSWORD
                    )
                )
            ) {
                startActivity(Intent(this, LoginActivity::class.java))
                finish()
            } else {
                enableFingrePrint()
//                    startActivity(Intent(this, DashboardActivity::class.java))
            }

        }, 2000)
    }

    private fun getResponse() {
        showProcessDialog()
        val helper = ApiClient.getClient(this).create(ApiInterface::class.java)
        val request = BaseRequest()
        request.CountryCodeId = 1
        request.DeviceNotificationID = ""
        request.LanguageTypeId = 1
        request.ServiceAuthenticationToken = ApiClient.android_token

        val call = helper.GetResponseMessages(request, "application/json")

        call.enqueue(object : CallbackManager<AppStatusReponse>() {
            override fun onSuccess(res: AppStatusReponse?, message: String) {
                val responseData = res as AppStatusReponse
                responseData.let {
                    createFile(GsonBuilder().disableHtmlEscaping().create().toJson(responseData))
                }

                hideProcessDailog()
            }

            override fun onFailure(message: String) {
                hideProcessDailog()
            }

            override fun onError(error: String) {
                hideProcessDailog()

            }

        })
    }

    private fun createFile(responseData: String) {
        var fos: FileWriter? = null
        var isDirectoryCreated = false
        try {
            val directory =
                File("data/data/com.app.luipay/responsecode")
            if (directory.exists()) {
                isDirectoryCreated = true
            } else {
                isDirectoryCreated = directory.mkdir()
            }
            if (isDirectoryCreated) {
                val file =
                    File("data/data/com.app.luipay/responsecode/rescode.text")
                if (file.createNewFile()) {
                    fos = FileWriter(file)
                    fos.write(responseData)
                }
            }
        } catch (fileNotFound: FileNotFoundException) {
        } catch (ioException: IOException) {
            ioException.printStackTrace()
        } finally {
            fos?.close()
            startActivity(Intent(this, LoginActivity::class.java))
            finish()
        }
    }

    override fun onAuthenticationSucceeded() {
        if (dialogFragment != null) {
            dialogFragment!!.dismiss()
            loginApi()
        }
    }

    private fun loginApi() {
        openDashBoard()
        finish()
        /*if (!isOnline()) {
            CommonUtils.showToast(this, getString(R.string.turn_on_internet))
            return
        }
        showProcessDialog()
        val helper = ApiClient.getClient(this).create(ApiInterface::class.java)

        val request = LoginRequest()
        request.MobileNo = Prefutils.getFromPrefs(this@SplashActivity, Prefutils.MOBILE)!!
        request.Password = Prefutils.getFromPrefs(this@SplashActivity, Prefutils.PASSWORD)!!
        request.CountryCodeId = 1
        request.DeviceNotificationID = ""
        request.LanguageTypeId = 1
        request.ServiceAuthenticationToken = ApiClient.android_token


        val call = helper.loginWithMobileNo(request, "application/json")

        call.enqueue(object : CallbackManager<otp>() {
            override fun onSuccess(res: otp?, message: String) {
                hideProcessDailog()
                if (res?.responseStatus.equals("1")) {
                    Prefutils.saveToPrefs(
                        this@SplashActivity,
                        Prefutils.ACCESS_TOKEN,
                        res?.response?.accessToken?.accessToken
                    )
                    val intent = Intent(this@SplashActivity, DashboardActivity::class.java)
                    startActivity(intent)
                    finish()
                }

            }

            override fun onFailure(message: String) {
                hideProcessDailog()
                CommonUtils.showToast(this@SplashActivity, message)
            }

            override fun onError(error: String) {
                hideProcessDailog()
                CommonUtils.showToast(this@SplashActivity, error)

            }

        })*/
    }

    override fun onAuthenticationError(errorCode: Int) {
        dialogFragment!!.dismiss()
        if (!isPinClicked) {
            finish()
        }
    }

    private fun enableFingrePrint() {
        if (Prefutils.getFromPrefs(
                this@SplashActivity,
                Prefutils.APP_LOCK
            ).equals(Appconstant.APP_LOCK_ENABLE)
        ) {
            if (CommonUtils.isBioMetricEnable(this@SplashActivity)) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    BiomatricAuthencation.initlizeApp(this@SplashActivity)
                    isPinClicked = false
                    openFragmentDialog()
                } else {
                    localPinCheck()
                }
            } else {
                localPinCheck()
            }
        } else {
            openDashBoard()
        }

    }

    private fun localPinCheck() {
        val pin = Prefutils.getFromPrefs(
            this@SplashActivity,
            Prefutils.APP_LOCK_PIN
        )
        if (!TextUtils.isEmpty(pin)
        ) {
            startActivityForResult(
                Intent(
                    this@SplashActivity,
                    CreateLocalePinConfirmation::class.java
                ).putExtra(Appconstant.ID, pin).putExtra(Appconstant.NAME, true), 1000
            )
        } else {
            openDashBoard()
        }
    }

    private fun askAuthencationDialog() {
        val customAlertDialog = CustomAlertDialog(
            this@SplashActivity,
            getString(R.string.app_name),
            getString(R.string.lock_screen_authetiction_smg),
            getString(R.string.dialog_ok),
            getString(R.string.dialog_cancel),
            object : CustomAlertDialog.onAlertDialogCustomListener {
                override fun onSuccessListener(dialog: DialogInterface?) {
                    openFingerprintSetting()
                }

                override fun onCancelListener() {
                    openDashBoard()
                }

            })
    }


    private fun openFingerprintSetting() {
        try {
            val intent = Intent(Settings.ACTION_SECURITY_SETTINGS)
            startActivity(intent)
        } catch (e: Exception) {
            openDashBoard()
        }
    }

    private fun openDashBoard() {
        startActivity(Intent(this, DashboardActivity::class.java))
        finish()
    }

    private fun openFragmentDialog() {
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        val prev = supportFragmentManager.findFragmentByTag("dialog")
        if (prev != null) {
            fragmentTransaction.remove(prev)
        }
        fragmentTransaction.addToBackStack(null)
        dialogFragment = FingerprintDailog.getInstance(listener)
        dialogFragment!!.show(fragmentTransaction, "dialog")
    }

    val listener = object : FingerprintDailog.onCancelListener {
        override fun onCancel() {
            dialogFragment?.dismiss()
            finish()
        }

        override fun usePin() {
            dialogFragment?.dismiss()
            isPinClicked = true
            openPinDiallog()
        }

    }

    private fun openPinDiallog() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val km =
                getSystemService(Context.KEYGUARD_SERVICE) as KeyguardManager
            if (km.isKeyguardSecure) {
                val authIntent = km.createConfirmDeviceCredentialIntent(
                    getString(R.string.app_name),
                    getString(R.string.pin_msg)
                )
                startActivityForResult(authIntent, INTENT_AUTHENTICATE)
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == INTENT_AUTHENTICATE && resultCode == Activity.RESULT_OK) {
            loginApi()
        }
    }

}