package com.app.luipay.base

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

open class BaseViewModel:ViewModel() {

    var progressLiveData = MutableLiveData<Boolean>()

    fun toggleProgress(isProgress:Boolean) {
        progressLiveData.value = isProgress
    }
}