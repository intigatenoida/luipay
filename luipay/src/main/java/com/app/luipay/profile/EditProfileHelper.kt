package com.app.luipay.profile

import android.app.Activity
import android.content.Intent
import android.net.Uri
import androidx.core.content.FileProvider
import com.app.luipay.BuildConfig
import com.app.luipay.R
import com.app.luipay.uploadimage.UploadImageHelper
import com.app.luipay.utils.Appconstant
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_edit_profile.*

class EditProfileHelper(val fg:EditProfileFragment) {

    private var compressedImagePath: String? = null
    private var originalFilePath: String? = null
    private var originalUri: Uri? = null
    private var isCameraSelected = false

    private val disposables = CompositeDisposable()


    fun setImageUri(): Uri {
        val file = UploadImageHelper.tempFileReturn(
            fg.context!!,
            UploadImageHelper.TEMPORARY_PROFILE_IMAGE
        )
        originalUri = FileProvider.getUriForFile(
            fg.context!!,
            "${BuildConfig.APPLICATION_ID}.provider",
            file
        )
        originalFilePath = file.absolutePath
        return originalUri!!
    }

    fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                Appconstant.REQUEST_UPLOAD_IMAGE -> {
                    handleImageResult(data)
                }
            }
        }
    }

    private fun handleImageResult(data: Intent?) {
        fg.showProcessDialog()

        isCameraSelected = false
        val disposableObserver = object : DisposableObserver<String>() {
            override fun onComplete() {
                //fg.hideProcessDialog()
            }

            override fun onNext(t: String) {
                fg.hideProcessDialog()
                Glide.with(fg.context!!)
                    .load(t)
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .skipMemoryCache(true)
                    .placeholder(R.drawable.ic_user)
                    .into(fg.iv_userImg)
            }

            override fun onError(e: Throwable) {
                fg.hideProcessDialog()
            }

        }
        disposables.add(getObserver(data, disposableObserver))
    }

    private fun getObserver(
        data: Intent?,
        disposableObserver: DisposableObserver<String>
    ): DisposableObserver<String> {
        return getObservable(data)
            .subscribeOn(Schedulers.single())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeWith(disposableObserver)
    }

    private fun getObservable(data: Intent?): Observable<String> {
        return Observable.create { emitter ->
            if (data?.data != null) {
                //Photo from gallery
                compressedImagePath = data.data?.path
                val f = UploadImageHelper.copyFileFromStream(
                    fg.context!!,
                    data.data!!,
                    UploadImageHelper.TEMPORARY_PROFILE_IMAGE
                )
                originalUri = Uri.fromFile(f)
                originalFilePath = f.absolutePath
            } else {
                //Photo from Camera
                isCameraSelected = true
                compressedImagePath = originalFilePath ?: ""
            }
            compressedImagePath = UploadImageHelper.compressImageFile(
                fg.context!!,
                compressedImagePath ?: "",
                false,
                originalUri!!
            )
            if (!compressedImagePath.isNullOrEmpty()) {
                emitter.onNext(compressedImagePath ?: "")
                emitter.onComplete()
            } else {
                emitter.onError(Throwable("queryImageUrl is empty"))
            }
        }
    }

    fun onDestroy() {
        if (!disposables?.isDisposed) {
            disposables.dispose()
        }
    }

    fun clearImageParams() {
        compressedImagePath = null
        originalFilePath = null
        originalUri = null
    }
}