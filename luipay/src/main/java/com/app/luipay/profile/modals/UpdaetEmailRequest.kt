package com.app.luipay.profile.modals


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep

@Keep
data class UpdaetEmailRequest(
    @SerializedName("EmailAddress")
    var emailAddress: String ?= null,
    @SerializedName("LanguageTypeId")
    var languageTypeId: Int ?= null,
    @SerializedName("ServiceAuthenticationToken")
    var serviceAuthenticationToken: String ?= null
)