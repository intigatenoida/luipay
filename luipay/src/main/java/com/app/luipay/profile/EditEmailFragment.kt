package com.app.luipay.profile

import android.app.Activity
import android.os.Bundle
import android.text.TextUtils
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.app.luipay.R
import com.app.luipay.base.BaseFragment
import com.app.luipay.profile.modals.UpdaetEmailRequest
import com.app.luipay.request.ServiceAuthenticationToken
import com.app.luipay.response.BaseResponse
import com.app.luipay.response.WalletBalanceRespons
import com.app.luipay.retrofit.ApiClient
import com.app.luipay.retrofit.ApiInterface
import com.app.luipay.retrofit.CallbackManager
import com.app.luipay.utils.CommonUtils
import com.app.luipay.utils.LuipayToast
import com.app.luipay.utils.Prefutils
import kotlinx.android.synthetic.main.fragment_edit_email.view.*
import kotlinx.android.synthetic.main.header_layout_profile.view.*

/**
 * A simple [Fragment] subclass.
 */
class EditEmailFragment : BaseFragment() {


    lateinit var rootView: View

    companion object {
        fun getInstance(): EditEmailFragment {
            return EditEmailFragment()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_edit_email, container, false)

        rootView.tv_editprofile.visibility = View.VISIBLE
        rootView.tv_editprofile.text = "Save"

        rootView.tvMenuName.visibility = View.VISIBLE
        rootView.tvMenuName.text = "Edit Email"


        val email = Prefutils.getFromPrefs(activity!!, Prefutils.EMAIL)

        if (email!!.isNotEmpty()) {
            rootView.et_email_id.setText(email)
        }

        rootView.tv_editprofile.setOnClickListener {
            //-- Api calling
            val email = rootView.et_email_id.text.toString().trim()
            if (!TextUtils.isEmpty(email) && CommonUtils.isEmailValid(email)) {
                updateEmailAddress(email)
            }else{
                LuipayToast.showToast(activity!!,"Please Enter Valid Email Id")
            }
        }

        rootView.ivBack.setOnClickListener {
            activity!!.finish()
        }

        rootView.ivClose.setOnClickListener {
            rootView.et_email_id.setText("")
            rootView.et_email_id.requestFocus()
        }

        return rootView
    }

    private fun updateEmailAddress(email: String) {
        showProcessDialog()
        val token = UpdaetEmailRequest()
        token.serviceAuthenticationToken = ApiClient.android_token
        token.emailAddress = email
        token.languageTypeId = Prefutils.getFromPrefsInt(activity!!, Prefutils.API_LANGUAGE)
        val helper = ApiClient.getClient(requireContext()).create(ApiInterface::class.java)
        val call = helper.setUserEmailID(token, "application/json")
        call.enqueue(object : CallbackManager<BaseResponse>() {
            override fun onSuccess(any: BaseResponse?, message: String) {
                val response = any as BaseResponse
                hideProcessDialog()
                if(response.responseStatus == "1"){
                    Prefutils.saveToPrefs(activity!!,Prefutils.EMAIL,email)
                    activity!!.setResult(Activity.RESULT_OK)
                    activity!!.finish()
                }else{
                    LuipayToast.showToast(activity!!,message)
                }
            }

            override fun onFailure(message: String) {
                hideProcessDialog()
                LuipayToast.showToast(activity!!,message)
            }

            override fun onError(error: String) {
                hideProcessDialog()
                LuipayToast.showToast(activity!!,error)
            }

        })
    }


}
