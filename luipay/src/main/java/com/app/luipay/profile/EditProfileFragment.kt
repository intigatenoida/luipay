package com.app.luipay.profile

import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.app.luipay.R
import com.app.luipay.base.BaseFragment
import com.app.luipay.permissions.PermissionHelper
import com.app.luipay.permissions.PermissionHelper.Companion.permissions
import com.app.luipay.uploadimage.UploadImageHelper
import com.app.luipay.utils.Appconstant
import com.app.luipay.utils.Appconstant.Companion.PERMISSION_WRITE_EXTERNAL_STORAGE
import com.app.luipay.utils.CommonUtils
import com.app.luipay.utils.Prefutils
import kotlinx.android.synthetic.main.activity_money_transfer.*
import kotlinx.android.synthetic.main.fragment_edit_profile.view.*

/**
 * A simple [Fragment] subclass.
 */
class EditProfileFragment : BaseFragment() {

    lateinit var rootView: View
    lateinit var helper: EditProfileHelper

    companion object {
        fun getInstance(): EditProfileFragment {
            return EditProfileFragment()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_edit_profile, container, false)
        init()
        return rootView
    }

    private fun init() {

        //-- Back Press
        rootView.ivBack.setOnClickListener {
            activity!!.finish()
        }

        helper = EditProfileHelper(this)
        val name = Prefutils.getFromPrefs(activity!!, Prefutils.NAME)
        if (name!!.isNotEmpty()) {
            rootView.et_name.setText(name)
        }

        val email = Prefutils.getFromPrefs(activity!!, Prefutils.EMAIL)
        if (email!!.isNotEmpty()) {
            rootView.et_email_id.setText(email)
        }
        rootView.et_email_id.isFocusable = false
        rootView.et_email_id.isClickable = true

        val mobileNo = Prefutils.getFromPrefs(activity!!, Prefutils.MOBILENO)
        if (mobileNo!!.isNotEmpty()) {
            rootView.et_mobileNo.setText(mobileNo)
            rootView.et_mobileNo.isFocusable = false
            rootView.et_mobileNo.isEnabled = false
        }

        rootView.v_profile.setOnClickListener {
            if (PermissionHelper.isPermissionsAllowed2(
                    this,
                    permissions,
                    true,
                    PERMISSION_WRITE_EXTERNAL_STORAGE
                )
            ) {
                helper.clearImageParams()
                chooseImage()
            }
        }
        rootView.et_email_id.setOnClickListener {
            startActivityForResult(Intent(activity!!, EditEmailActivity::class.java), 121)
        }

    }


    private fun chooseImage() {
        this@EditProfileFragment.startActivityForResult(
            UploadImageHelper.getPickImageIntent(
                activity!!,
                helper.setImageUri()
            ),
            Appconstant.REQUEST_UPLOAD_IMAGE
        )
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        helper.onActivityResult(requestCode, resultCode, data)

        if (requestCode == 121 && resultCode == Activity.RESULT_OK) {
            val email = Prefutils.getFromPrefs(activity!!, Prefutils.EMAIL)
            if (email!!.isNotEmpty()) {
                rootView.et_email_id.setText(email)
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == PERMISSION_WRITE_EXTERNAL_STORAGE) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                helper.clearImageParams()
                chooseImage()
            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M &&
                    !shouldShowRequestPermissionRationale(permissions[0])
                ) {
                    CommonUtils.openSettingDialog(context!!)
                }
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        helper.onDestroy()
    }
}
