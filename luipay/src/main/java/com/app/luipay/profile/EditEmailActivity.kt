package com.app.luipay.profile

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.app.luipay.R
import com.app.luipay.base.BaseActivity

class EditEmailActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_email)

        val qrCodeFragment = EditEmailFragment.getInstance()
        LoadFragment(qrCodeFragment,this@EditEmailActivity.localClassName,R.id.main_container)
    }
}
