package com.app.luipay.profile

import android.os.Bundle
import com.app.luipay.R
import com.app.luipay.base.BaseActivity


class EditProfile : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_profile)

        val qrCodeFragment = EditProfileFragment.getInstance()
        LoadFragment(qrCodeFragment,this@EditProfile.localClassName,R.id.main_container)

    }

}
