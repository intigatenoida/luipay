package com.app.luipay.money_transfer

import android.app.Activity
import android.content.*
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import androidx.core.content.ContextCompat
import com.app.luipay.DBHandler.BankAccountData
import com.app.luipay.DBHandler.DBHelper
import com.app.luipay.R
import com.app.luipay.addmoney.SucessActivity
import com.app.luipay.base.BaseActivity
import com.app.luipay.profile.dialog_fragment.ChangeImageDialog
import com.app.luipay.response.BaseResponse
import com.app.luipay.retrofit.ApiClient
import com.app.luipay.retrofit.ApiInterface
import com.app.luipay.retrofit.CallbackManager
import com.app.luipay.utils.*
import kotlinx.android.synthetic.main.activity_money_transfer_new.*
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody


class MoneyTransferActivity : BaseActivity(), ChangeImageDialog.OnImageSelectionListener {

    private var bankId: Int = -1
    private var bankName: String = ""
    private var bankCode: String = ""
    private var bankImageUrl: String = ""

    val SELECT_IMAGE = 100
    val PICK_PDF_CODE = 101

    val BML = "MB"
    val CBL = "CBM01"
    val SBI = "SBI"

    lateinit var bankDataList: ArrayList<BankAccountData>
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_money_transfer_new)
        ivBack.setOnClickListener { finish() }
        btnClaimAmount2.setOnClickListener {
            if (isValidated()) {
                uploadDocumentWithOutFile()
//                selectedPdf?.let { uploadDocument(it) }
            }
//                startActivityForResult(Intent(this, TranxStatusActivity::class.java), 1000)
        }

        bankId = intent.getIntExtra(Appconstant.CONTACT_ID, -1)
        bankName = intent?.getStringExtra(Appconstant.BANK_NAME) ?: ""
        bankCode = intent?.getStringExtra(Appconstant.BANK_CODE) ?: ""
        bankImageUrl = intent?.getStringExtra(Appconstant.BANK_IMAGE_URL) ?: ""

        when (bankCode) {
            BML -> {
                tvbtnopenBML.text = getString(R.string.open_bml_app)
            }
            CBL -> {
                tvbtnopenBML.text = getString(R.string.open_cbm_app)
            }
            SBI -> {
                tvbtnopenBML.text = getString(R.string.open_sbi_app)
            }
            else -> {
                tvbtnopenBML.text = getString(R.string.open_bml_app)
            }
        }

        btnClaimAmount.setOnClickListener {
            if (isValidated1()) {
                openImageChooser()
            }
        }

        tvAccountNumber.setOnClickListener {
            val clipboard: ClipboardManager =
                getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
            val clip = ClipData.newPlainText("label", tvAccountNumber.text.toString())
            clipboard.setPrimaryClip(clip)
            CommonUtils.showToast(this@MoneyTransferActivity, "Account Number copy to clipboard")
        }

        tvbtnopenBML.setOnClickListener {
            when (bankCode) {
                BML -> {
                    openPlayStore("mv.com.bml.mib", "market://details?id=mv.com.bml.mib")
                }
                CBL -> {
                    openPlayStore(
                        "com.cbcmaldives.banking.mobile.android.ui.activities.maldives",
                        "com.cbcmaldives.banking.mobile.android.ui.activities.maldives"
                    )
                }
                SBI -> {
                    openPlayStore(
                        "com.sbi.lotusintouch",
                        "market://details?id=com.sbi.lotusintouch"
                    )
                }
                else -> {
                    openPlayStore("mv.com.bml.mib", "market://details?id=mv.com.bml.mib")
                }
            }
        }

        edAmount1.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

                if (p0.toString().isNotEmpty()) {
                    btnClaimAmount.setTextColor(Color.parseColor("#272561"))
                    view2.setBackgroundColor(Color.parseColor("#ffffff"))
                    btnClaimAmount.background =
                        ContextCompat.getDrawable(this@MoneyTransferActivity, R.drawable.enable_25)

                } else {
                    view2.setBackgroundColor(Color.parseColor("#50ffffff"))
                    btnClaimAmount.setTextColor(Color.parseColor("#50272561"))
                    btnClaimAmount.background =
                        ContextCompat.getDrawable(this@MoneyTransferActivity, R.drawable.disable_25)

                }

            }

        })
        edAmount.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                enableDiableButton()
            }

        })

        edtTransactionId.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                enableDiableButton()
            }

        })


        if (bankImageUrl.isNotEmpty()) {
            userToImg_1.visibility = VISIBLE
            userToImg_place.visibility = GONE
            CommonUtils.loadImage(
                this@MoneyTransferActivity,
                bankImageUrl,
                userToImg_1,
                R.drawable.ic_bank
            )
        } else {
            userToImg_1.visibility = GONE
            userToImg_place.visibility = VISIBLE
        }
        setSpinner()
        /* edtFullName.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
             override fun onNothingSelected(parent: AdapterView<*>?) {
                 CommonUtils.showToast(this@MoneyTransferActivity,"No data found")
             }

             override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                 CommonUtils.showToast(this@MoneyTransferActivity,"Data found")
             }
         }*/
        /*      edtFullName. setOnClickListener(object : View.OnClickListener {
                  override fun onClick(v: View?) {
                      if (edtFullName.getSelectedItem() == null) { // user selected nothing...
                         CommonUtils.showToast(this@MoneyTransferActivity,"no item found")
                      }
                  }
             })*/
    }


    private fun enableDiableButton() {
        if (edtTransactionId.text.toString().isNotEmpty() && edAmount.text.toString().isNotEmpty()) {
            btnClaimAmount2.setTextColor(Color.parseColor("#272561"))
            view1.setBackgroundColor(Color.parseColor("#ffffff"))
            btnClaimAmount2.background =
                ContextCompat.getDrawable(this@MoneyTransferActivity, R.drawable.enable_25)

        } else {
            view1.setBackgroundColor(Color.parseColor("#50ffffff"))
            btnClaimAmount2.setTextColor(Color.parseColor("#50272561"))
            btnClaimAmount2.background =
                ContextCompat.getDrawable(this@MoneyTransferActivity, R.drawable.disable_25)

        }
    }

    private fun openPlayStore(mpackage: String, market: String) {
        try {
            val launchIntent =
                packageManager.getLaunchIntentForPackage(mpackage)
            startActivity(launchIntent)
        } catch (e: Exception) {
            try {
                startActivity(
                    Intent(
                        Intent.ACTION_VIEW,
                        Uri.parse(market)
                    )
                )
            } catch (anfe: ActivityNotFoundException) {
                startActivity(
                    Intent(
                        Intent.ACTION_VIEW,
                        Uri.parse("https://play.google.com/store/apps/details?id=$mpackage")
                    )
                )
            }
        }
    }

    private fun setSpinner() {
        bankDataList = DBHelper(this@MoneyTransferActivity).getAllDataByBankId(bankId)
        if (bankDataList.isNotEmpty()) {
            val userNameList = ArrayList<String>()
            for (i in 0 until bankDataList.size) {
                userNameList.add(bankDataList[i].name)
            }
            edtFullName.adapter = SpinnerAdapter(
                this, userNameList
            )
            bottomLayoutGroup.visibility = View.VISIBLE

        } else {

            bottomLayoutGroup.visibility = View.GONE
            /* EmptyId.setOnClickListener {
                 startActivityForResult(
                     Intent(
                         this@MoneyTransferActivity,
                         LinkAccountActivity::class.java
                     ), 2000
                 )
             }*/
        }
    }

    private fun isValidated(): Boolean {
        if (TextUtils.isEmpty(edAmount.text.toString().trim())) {
            LuipayToast.showToast(
                this@MoneyTransferActivity,
                getString(R.string.please_enter_amount)
            )
            return false
        }

        if (TextUtils.isEmpty(edtTransactionId.text.toString().trim())) {
            LuipayToast.showToast(
                this@MoneyTransferActivity,
                getString(R.string.please_enter_Transaction_Id)
            )
            return false
        }


        return true
    }


    private fun isValidated1(): Boolean {
        if (TextUtils.isEmpty(edAmount1.text.toString().trim())) {
            LuipayToast.showToast(this@MoneyTransferActivity, "Please Enter Amount")
            return false
        }

        return true
    }

//    fun genTransactionId(): Int {
//        val r = Random(System.currentTimeMillis())
//        return (1 + r.nextInt(2)) * 10000 + r.nextInt(10000)
//    }

    private fun openImageChooser() {
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        val prev = supportFragmentManager.findFragmentByTag("dialog")
        if (prev != null) {
            fragmentTransaction.remove(prev)
        }
        fragmentTransaction.addToBackStack(null)
        val dialogFragment = ChangeImageDialog.getInstance()
        dialogFragment.show(fragmentTransaction, "dialog")
    }

    private fun uploadDocument(fileUri: Uri) {
        showProcessDialog()
        var body: MultipartBody.Part? = null
        val refillAMount = edAmount1.text.toString().trim()
        val memo = ""
        fileUri?.let {
            body = prepareFilePart("photo", fileUri)
        }

        val RefillAmount: RequestBody = createPartFromString(refillAMount)
        val Memo: RequestBody = createPartFromString(memo)
        val Pin: RequestBody = createPartFromString(
            Prefutils.getFromPrefs(
                this@MoneyTransferActivity,
                Prefutils.PASSWORD
            )!!
        )
        val ServiceAuthenticationToken: RequestBody = createPartFromString(ApiClient.android_token)
        val language: RequestBody = createPartFromString(
            "1"
        )
        val tempBankId = createPartFromString(bankId.toString())

        val map: HashMap<String, RequestBody> = HashMap()
        map["ServiceAuthenticationToken"] = ServiceAuthenticationToken

        map["RefillAmount"] = RefillAmount

        map["Memo"] = Memo
        map["Pin"] = Pin
        map["LanguageTypeId"] = language
        map["BankId"] = tempBankId
        val helper = ApiClient.getClient(this).create(ApiInterface::class.java)
        val call = helper.addMoney(body, map)
        call.enqueue(object : CallbackManager<BaseResponse>() {
            override fun onSuccess(any: BaseResponse?, message: String) {
                hideProcessDailog()
                val intent = Intent(this@MoneyTransferActivity, SucessActivity::class.java)
                intent.putExtra(Appconstant.TRANSAMOUNT, refillAMount)
                intent.putExtra(Appconstant.TRANSCODE, "121200")
                intent.putExtra(Appconstant.TRANSSTATUS, true)
                startActivityForResult(intent, 1)
            }

            override fun onFailure(message: String) {
                hideProcessDailog()
                LuipayToast.showToast(this@MoneyTransferActivity, message)
            }

            override fun onError(error: String) {
                hideProcessDailog()
                LuipayToast.showToast(this@MoneyTransferActivity, error)
            }
        })
    }


    private fun uploadDocumentWithOutFile() {
        showProcessDialog()

        val refillAMount = edAmount.text.toString().trim()
        val memo = ""

        val RefillAmount: RequestBody = createPartFromString(refillAMount)

        val transactionID: RequestBody =
            createPartFromString(edtTransactionId.text.toString().trim())
        val Memo: RequestBody = createPartFromString(memo)
        val Pin: RequestBody = createPartFromString(
            Prefutils.getFromPrefs(
                this@MoneyTransferActivity,
                Prefutils.PASSWORD
            )!!
        )
        val ServiceAuthenticationToken: RequestBody = createPartFromString(ApiClient.android_token)
        val language: RequestBody = createPartFromString(
            "1"
        )

        val map: HashMap<String, RequestBody> = HashMap()
        map["ServiceAuthenticationToken"] = ServiceAuthenticationToken
        map["TransactionId"] = transactionID
        map["RefillAmount"] = RefillAmount
        map["Memo"] = Memo
        map["Pin"] = Pin
        map["LanguageTypeId"] = language
        val helper = ApiClient.getClient(this).create(ApiInterface::class.java)
        val call = helper.addMoneyWithOutFile(map)
        call.enqueue(object : CallbackManager<BaseResponse>() {
            override fun onSuccess(any: BaseResponse?, message: String) {
                hideProcessDailog()
                val intent = Intent(this@MoneyTransferActivity, SucessActivity::class.java)
                intent.putExtra(Appconstant.TRANSAMOUNT, refillAMount)
                intent.putExtra(Appconstant.TRANSCODE, edtTransactionId.text.toString().trim())
                intent.putExtra(Appconstant.TRANSSTATUS, true)
                startActivityForResult(intent, 1)
            }

            override fun onFailure(message: String) {
                hideProcessDailog()
                LuipayToast.showToast(this@MoneyTransferActivity, message)
            }

            override fun onError(error: String) {
                hideProcessDailog()
                LuipayToast.showToast(this@MoneyTransferActivity, error)
            }
        })
    }


    fun createPartFromString(s: String): RequestBody {
        return RequestBody.create(
            "multipart/form-data".toMediaTypeOrNull(), s
        )
    }

    private fun prepareFilePart(partName: String, fileUri: Uri): MultipartBody.Part {
        val file = FileUtils.from(this, fileUri)
        val requestFile: RequestBody = RequestBody.create(
            contentResolver.getType(fileUri!!)!!.toMediaTypeOrNull(),
            file
        )

        return MultipartBody.Part.createFormData("File", file.getName(), requestFile)

    }
//    MediaType.parse(getContentResolver().getType(fileUri)),


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == PICK_PDF_CODE && resultCode == RESULT_OK && data != null) {
            var selectedPdf: Uri? = data.data
            selectedPdf?.let {
                uploadDocument(selectedPdf)
                CommonUtils.showToast(
                    this@MoneyTransferActivity,
                    getString(R.string.pdf_file_selected)
                )
            }
        } else if (requestCode == SELECT_IMAGE && resultCode == RESULT_OK && data != null) {
            var selectedPdf: Uri? = data.data
            selectedPdf?.let {
                uploadDocument(selectedPdf)
                CommonUtils.showToast(
                    this@MoneyTransferActivity,
                    getString(R.string.image_selected)
                )
            }

        } else if (resultCode == Activity.RESULT_OK && requestCode == 1000) {
            setResult(Activity.RESULT_OK)
            finish()
        } else if (resultCode == Activity.RESULT_OK && requestCode == 2000) {
//            setSpinner()
        } else if (requestCode == 1 && resultCode == Activity.RESULT_OK) {
            setResult(Activity.RESULT_OK)
            finish()
        }
    }

    override fun onGallerySelectionListener() {
        val intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), SELECT_IMAGE)
    }

    override fun onCameraSelectionListener() {
        if (AppPermission.checkAppPermission(
                this@MoneyTransferActivity,
                AppPermission.CAMERA_CODE
            )
        ) {
            onGallerySelectionListener()
        }
    }

    override fun onPDFSelectionListener() {
        val browseStorage = Intent(Intent.ACTION_GET_CONTENT)
        browseStorage.type = "application/pdf"
        browseStorage.addCategory(Intent.CATEGORY_OPENABLE)
        startActivityForResult(Intent.createChooser(browseStorage, "Select PDF"), PICK_PDF_CODE)
    }

}
