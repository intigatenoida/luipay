package com.app.luipay.money_transfer.phonebook

import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.database.Cursor
import android.os.Bundle
import android.provider.ContactsContract
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.luipay.R
import com.app.luipay.addmoney.AddMoneyActivity
import com.app.luipay.base.BaseActivity
import com.app.luipay.request.SearchContactRequest
import com.app.luipay.response.SearchContactResponse.ResponseCollection
import com.app.luipay.response.SearchContactResponse.SearchContactResponse
import com.app.luipay.retrofit.ApiClient
import com.app.luipay.retrofit.ApiInterface
import com.app.luipay.retrofit.CallbackManager
import com.app.luipay.utils.AppPermission
import com.app.luipay.utils.Appconstant
import com.app.luipay.utils.CommonUtils
import kotlinx.android.synthetic.main.activity_phonebook.*
import kotlinx.android.synthetic.main.center_title_layout.*
import retrofit2.Call


class PhonebookActivity : BaseActivity(), PhonebookAdapter.onNumberClickListener {
    var filteredList = ArrayList<ResponseCollection>()
    var contactList = ArrayList<ResponseCollection>()
    var fromSend: Boolean = false
    lateinit var adapter: PhonebookAdapter

    lateinit var call: Call<SearchContactResponse>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_phonebook)
        backBtn.setOnClickListener { finish() }
        titleTv.text = getString(R.string.phonebook)
        adapter = PhonebookAdapter(this, this)
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = adapter
        fromSend = intent.getBooleanExtra(Appconstant.LUI_REQUEST, false)

        edSearch.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                filteredList.clear()
                if (p0.toString().length > 2) {
                    for (data in contactList) {
                        if (data.name.contains(
                                p0.toString(),
                                ignoreCase = true
                            ) || data.phoneNo.contains(p0.toString(), ignoreCase = true)
                        ) {
                            filteredList.add(data)
                        }
                    }
                    if (filteredList.isEmpty()) {
                        if (::call.isInitialized) {
                            call.cancel()
                        }
                        searchApi(false, edSearch.text.toString(), false, false)
                    }
                } else {
                    filteredList.addAll(contactList)
                }

                if (::adapter.isInitialized) {
                    adapter.updateList(filteredList)
                }
            }

        })

/*        edSearch.setOnEditorActionListener(object : TextView.OnEditorActionListener{
            override fun onEditorAction(p0: TextView?, p1: Int, p2: KeyEvent?): Boolean {
                if (p2 != null && p2.getKeyCode() == KeyEvent.KEYCODE_ENTER
                    || p1 == EditorInfo.IME_ACTION_SEARCH
                ) {
                    startActivity(Intent(this@PhonebookActivity, MoneyTransferActivity::class.java))
                    return true
                }
                return false
            }
        })*/


        checkContactPermission()
    }

    override fun onStop() {
        super.onStop()
        if (::call.isInitialized) {
            call.cancel()
        }
    }

    private fun searchApi(
        moveToNextScreen: Boolean,
        phoneNo: String,
        isCLick: Boolean,
        isProgressConsider: Boolean
    ) {
        if (isProgressConsider) {
            showProcessDialog()
        }
        val request = SearchContactRequest()
        request.languageTypeId = 1
        request.search = phoneNo
        request.serviceAuthenticationToken = ApiClient.android_token
        val helper = ApiClient.getClient(this).create(ApiInterface::class.java)
        call = helper.SearchContact(request, Appconstant.API_HEADER)
        call.enqueue(object : CallbackManager<SearchContactResponse>() {
            override fun onSuccess(any: SearchContactResponse?, message: String) {
                hideProcessDailog()
                val response = any as SearchContactResponse
                if (response != null && response.responseCollection != null && response.responseCollection.isNotEmpty()) {
                    if (moveToNextScreen) {
                        openNextScreen(response.responseCollection[0])
                    } else {
                        filteredList.addAll(response.responseCollection)
                        if (::adapter.isInitialized) {
                            adapter.updateList(filteredList)
                        }
                    }
                } else {
                    if (isCLick) {
                        CommonUtils.showToast(this@PhonebookActivity, "User not registered")
                    } else {
                        CommonUtils.showToast(this@PhonebookActivity, "User not found")
                    }

                }
            }

            override fun onFailure(message: String) {
                hideProcessDailog()
            }

            override fun onError(error: String) {
                hideProcessDailog()
            }

        })
    }

    private fun checkContactPermission() {
        if (AppPermission.checkAppPermission(
                this@PhonebookActivity,
                AppPermission.CONTACT_CODE

            )
        ) {
            getContactListFromDevice()
        }
    }

    private fun getContactListFromDevice() {
        var lastNumber = ""
        val cursor: Cursor? = contentResolver.query(
            ContactsContract.CommonDataKinds.Phone.CONTENT_URI, arrayOf(
                ContactsContract.Contacts._ID,
                ContactsContract.Contacts.DISPLAY_NAME,
                ContactsContract.CommonDataKinds.Phone.NUMBER,
                ContactsContract.RawContacts.ACCOUNT_TYPE
            ), null,
            null, ContactsContract.Contacts.DISPLAY_NAME + " ASC"
        )
        cursor?.let {
            if (cursor.count > 0) {

                while (cursor.moveToNext()) {
                    val model = ResponseCollection()
                    var number =
                        cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER))
                    if (number.contains("+91")) {
                        number = number.removePrefix("+91").trim()
                    }
                    if (number.contains("+880")) {
                        number = number.removePrefix("+880").trim()
                    }

                    if (lastNumber.equals(number.trim())) {
                        continue
                    } else {
                        model.id = ""
                        model.name =
                            cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME))
//                        model.profilePictureUrl =
//                            cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.PHOTO_URI))

                        model.phoneNo = number
                        contactList.add(model)
                        lastNumber = number
                    }

                }
            }
        }
        if (::adapter.isInitialized) {
            adapter.updateList(contactList)
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if ((requestCode == AppPermission.CONTACT_CODE)) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                getContactListFromDevice()
            } else {
                AppPermission.CheckForNeverAsk(this, AppPermission.CONTACT_CODE)
            }
        }
    }

    override fun onNumberClick(data: ResponseCollection) {
        if (TextUtils.isEmpty(data.id)) {
            searchApi(true, data.phoneNo, true, true)
        } else {
            openNextScreen(data)
        }
    }

    private fun openNextScreen(data: ResponseCollection) {
        val intent = Intent(
            this@PhonebookActivity,
            AddMoneyActivity::class.java
        )

        val bundle = Bundle()
        bundle.putBoolean(
            Appconstant.LUI_REQUEST, fromSend
        )
        bundle.putSerializable(Appconstant.NAME, data)
        intent.putExtras(bundle)
        startActivityForResult(
            intent, 1000
        )
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 1000 && resultCode == Activity.RESULT_OK) {
            finish()
        }
    }
}
