package com.app.luipay.money_transfer.phonebook

import android.app.Activity
import android.os.Bundle
import com.app.luipay.R
import com.app.luipay.base.BaseActivity
import com.app.luipay.utils.Appconstant
import kotlinx.android.synthetic.main.activity_status_luirequest.*

class PhoneBookStatusActivity : BaseActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_status_luirequest)

        name.text = intent?.getStringExtra(Appconstant.NAME)?:""
        ivBack.setOnClickListener {
            onBackPressed()
        }
    }

    override fun onBackPressed() {
        setResult(Activity.RESULT_OK)
        finish()
    }

}