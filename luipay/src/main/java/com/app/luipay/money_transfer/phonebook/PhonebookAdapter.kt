package com.app.luipay.money_transfer.phonebook

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.app.luipay.R
import com.app.luipay.response.SearchContactResponse.ResponseCollection
import kotlinx.android.synthetic.main.phonebook_item.view.*

class PhonebookAdapter(var context: Context,private  val listener:onNumberClickListener) :
    RecyclerView.Adapter<PhonebookAdapter.ViewHolder>() {
    var list: ArrayList<ResponseCollection>

    interface onNumberClickListener{
        fun onNumberClick(data:ResponseCollection)
    }

    init {
        list=ArrayList()
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.phonebook_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val data=list.get(position)
        holder.itemView.nameTV.text=data.name
        holder.itemView.mobileNumberTV.text = data.phoneNo

        holder.itemView.parentLayout.setOnClickListener {
            listener.onNumberClick(data)
        }
    }

    fun updateList(contactList: ArrayList<ResponseCollection>) {
        list.clear()
        list.addAll(contactList)
        notifyDataSetChanged()
    }
}