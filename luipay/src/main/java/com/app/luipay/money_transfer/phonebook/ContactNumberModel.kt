package com.app.luipay.money_transfer.phonebook

import android.os.Parcel
import android.os.Parcelable

class ContactNumberModel() : Parcelable {

    var id: String = ""
    var name: String = ""
    var number: String = ""
    var photouri: String = ""
    var isSelected: Boolean = false
    var amount:String="tk"

    constructor(parcel: Parcel) : this() {
        id = parcel.readString()!!
        name = parcel.readString()!!
        number = parcel.readString()!!
        photouri = parcel.readString()!!
        amount= parcel.readString()!!
        isSelected = parcel.readByte() != 0.toByte()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(name)
        parcel.writeString(number)
        parcel.writeString(photouri)
        parcel.writeString(amount)
        parcel.writeByte(if (isSelected) 1 else 0)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ContactNumberModel> {
        override fun createFromParcel(parcel: Parcel): ContactNumberModel {
            return ContactNumberModel(parcel)
        }

        override fun newArray(size: Int): Array<ContactNumberModel?> {
            return arrayOfNulls(size)
        }
    }


}