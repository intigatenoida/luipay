package com.app.luipay.help

import android.os.Bundle
import android.text.InputType
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.view.View
import android.view.inputmethod.EditorInfo
import com.app.luipay.R
import com.app.luipay.base.BaseActivity
import com.app.luipay.request.ChangePasswordRequest
import com.app.luipay.response.BaseResponse
import com.app.luipay.retrofit.ApiClient
import com.app.luipay.retrofit.ApiInterface
import com.app.luipay.retrofit.CallbackManager
import com.app.luipay.utils.CommonUtils
import com.app.luipay.utils.CustomPassword
import com.app.luipay.utils.Prefutils
import kotlinx.android.synthetic.main.activity_change_password.*
import kotlinx.android.synthetic.main.activity_change_password.btnLogin
import kotlinx.android.synthetic.main.button_view.view.*
import kotlinx.android.synthetic.main.center_title_layout.*
import kotlinx.android.synthetic.main.edittext_view.view.*


class ChangePassword : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_change_password)

        titleTv.text = getString(R.string.change_pass)

        backBtn.setOnClickListener {
            finish()
        }

        edOldPass.tvLabel.text = getString(R.string.old_pass)
        edOldConfirm.tvLabel.text = getString(R.string.confirm_old)
        edNewPassword.tvLabel.text = getString(R.string.new_pass)

        edOldPass.edInput.imeOptions = EditorInfo.IME_ACTION_NEXT
        edOldConfirm.edInput.imeOptions = EditorInfo.IME_ACTION_NEXT
        edNewPassword.edInput.imeOptions = EditorInfo.IME_ACTION_DONE

        edOldPass.edInput.inputType = InputType.TYPE_CLASS_TEXT or
                InputType.TYPE_TEXT_VARIATION_PASSWORD
        edOldConfirm.edInput.inputType = InputType.TYPE_CLASS_TEXT or
                InputType.TYPE_TEXT_VARIATION_PASSWORD
        edNewPassword.edInput.inputType = InputType.TYPE_CLASS_TEXT or
                InputType.TYPE_TEXT_VARIATION_PASSWORD

        edOldPass.img_eye.visibility = View.VISIBLE
        edOldPass.img_eye.setImageResource(R.drawable.ic_show)
        edOldPass.edInput.transformationMethod = PasswordTransformationMethod.getInstance()
        edOldPass.img_eye.setOnClickListener {
            if (edOldPass.edInput.transformationMethod.equals(PasswordTransformationMethod.getInstance())) {
                edOldPass.img_eye.setImageResource(R.drawable.ic_hide)
                edOldPass.edInput.transformationMethod =
                    HideReturnsTransformationMethod.getInstance()
            } else {
                edOldPass.img_eye.setImageResource(R.drawable.ic_show)
                edOldPass.edInput.transformationMethod = PasswordTransformationMethod.getInstance()
            }
            edOldPass.edInput.setSelection(edOldPass.edInput.text?.toString()?.length ?: 0)
        }

        edOldConfirm.img_eye.visibility = View.VISIBLE
        edOldConfirm.img_eye.setImageResource(R.drawable.ic_show)
        edOldConfirm.edInput.transformationMethod = PasswordTransformationMethod.getInstance()
        edOldConfirm.img_eye.setOnClickListener {
            if (edOldConfirm.edInput.transformationMethod.equals(PasswordTransformationMethod.getInstance())) {
                edOldConfirm.img_eye.setImageResource(R.drawable.ic_hide)
                edOldConfirm.edInput.transformationMethod =
                    HideReturnsTransformationMethod.getInstance()
            } else {
                edOldConfirm.img_eye.setImageResource(R.drawable.ic_show)
                edOldConfirm.edInput.transformationMethod = PasswordTransformationMethod.getInstance()
            }
            edOldConfirm.edInput.setSelection(edOldPass.edInput.text?.toString()?.length ?: 0)
        }

        edNewPassword.img_eye.visibility = View.VISIBLE
        edNewPassword.img_eye.setImageResource(R.drawable.ic_show)
        edNewPassword.edInput.transformationMethod = PasswordTransformationMethod.getInstance()
        edNewPassword.img_eye.setOnClickListener {
            if (edNewPassword.edInput.transformationMethod.equals(PasswordTransformationMethod.getInstance())) {
                edNewPassword.img_eye.setImageResource(R.drawable.ic_hide)
                edNewPassword.edInput.transformationMethod =
                    HideReturnsTransformationMethod.getInstance()
            } else {
                edNewPassword.img_eye.setImageResource(R.drawable.ic_show)
                edNewPassword.edInput.transformationMethod = PasswordTransformationMethod.getInstance()
            }
            edNewPassword.edInput.setSelection(edOldPass.edInput.text?.toString()?.length ?: 0)
        }

        btnLogin.tvLogin.text = getString(R.string.change_pass)

        btnLogin.tvLogin.setOnClickListener {
            val oldPass = edOldPass.edInput.text.toString()
            val oldConfirm = edOldConfirm.edInput.text.toString()
            val newPass = edNewPassword.edInput.text.toString()

            if (oldPass.isNotBlank()) {
                if (oldConfirm.isNotEmpty()) {

                    if (newPass.isNotEmpty()) {
                        if (newPass.equals(oldConfirm)) {
                            ChangePassword(oldPass, newPass)
                        } else {
                            CommonUtils.showToast(this, "Password must be same")

                        }
                    } else {
                        CommonUtils.showToast(this, "Confirm New Password")

                    }
                } else {
                    CommonUtils.showToast(this, "Enter New Password")

                }
            } else {
                CommonUtils.showToast(this, "Enter Old Password")
            }
        }

    }

    private fun ChangePassword(old: String, new: String) {

        showProcessDialog()
        val token = ChangePasswordRequest()
        token.ServiceAuthenticationToken = ApiClient.android_token
        token.LanguageTypeId = 1
        token.NewPassword = CustomPassword.encrypt(new)
        token.OldPassword = CustomPassword.encrypt(old)


        val helper = ApiClient.getClient(this).create(ApiInterface::class.java)
        val call = helper.ChangePassword(token, "application/json")
        call.enqueue(object : CallbackManager<BaseResponse>() {
            override fun onSuccess(any: BaseResponse?, message: String) {
                val response = any as BaseResponse
                hideProcessDailog()
                savePasswordLocaly(token.NewPassword)
                showToast(getString(R.string.password_change_successfully))
                finish()
            }

            override fun onFailure(message: String) {
                showToast(message)
                hideProcessDailog()
            }

            override fun onError(error: String) {
                hideProcessDailog()

            }

        })
    }

    private fun savePasswordLocaly(newPassword: String?) {
        Prefutils.saveToPrefs(this@ChangePassword, Prefutils.PASSWORD, newPassword)
    }

    private fun showToast(message: String) {
        CommonUtils.showToast(this, message)
    }
}