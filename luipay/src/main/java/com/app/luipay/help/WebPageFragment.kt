package com.app.luipay.help

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebSettings
import android.webkit.WebViewClient
import com.app.luipay.R
import com.app.luipay.base.BaseFragment
import com.app.luipay.utils.Appconstant
import kotlinx.android.synthetic.main.center_title_layout.view.*
import kotlinx.android.synthetic.main.fragment_web_page.view.*


class WebPageFragment : BaseFragment() {

    companion object {
        fun newInstance(bundle: Bundle?): WebPageFragment {
            val fg = WebPageFragment()
            fg.arguments = bundle
            return fg
        }
    }

    private lateinit var rootView: View

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (!::rootView.isInitialized) {
            rootView = inflater.inflate(R.layout.fragment_web_page, container, false)
            iniUi()
        }
        return rootView
    }

    private fun iniUi() {

        rootView.titleTv.text = arguments?.getString(Appconstant.NAME) ?: ""

        rootView.backBtn.setOnClickListener {
            activity!!.finish()
        }

        val webSetting: WebSettings = rootView.wv_page.settings
        webSetting.builtInZoomControls = true
        webSetting.javaScriptEnabled = true

        rootView.wv_page.webViewClient = WebViewClient()
        rootView.wv_page.loadUrl("file:///android_asset/temp.html")
    }

}