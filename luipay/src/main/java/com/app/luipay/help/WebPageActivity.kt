package com.app.luipay.help

import android.os.Bundle
import com.app.luipay.R
import com.app.luipay.base.BaseActivity

class WebPageActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_notifications)
        LoadFragment(WebPageFragment.newInstance(intent?.extras),"",R.id.mainContainer)
    }

}