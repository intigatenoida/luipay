package com.app.luipay.help

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import com.app.luipay.R
import com.app.luipay.base.BaseActivity
import com.app.luipay.utils.Appconstant
import com.google.android.material.bottomsheet.BottomSheetDialog
import kotlinx.android.synthetic.main.activity_create_pin.*
import kotlinx.android.synthetic.main.bottom_confirmation.*
import kotlinx.android.synthetic.main.center_title_layout.*

class CreateLocalePinAcitivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_pin)
        backBtn.setOnClickListener {
            setResult(Activity.RESULT_CANCELED)
            finish()
        }
        titleTv.text = getString(R.string.create_pin)

        otpView.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                if (s.toString().length == 4) {
                    startActivityForResult(
                        Intent(
                            this@CreateLocalePinAcitivity,
                            CreateLocalePinConfirmation::class.java
                        ).putExtra(Appconstant.ID, s.toString()), 1000
                    )
                }

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 1000 && resultCode == Activity.RESULT_OK) {
            setResult(Activity.RESULT_OK)
            finish()
        }
    }


}