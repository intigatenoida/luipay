package com.app.luipay.help

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.app.luipay.R
import com.app.luipay.base.BaseFragment
import com.app.luipay.utils.Appconstant
import com.app.luipay.utils.CommonUtils
import kotlinx.android.synthetic.main.fragment_help.view.*


class HelpFragment : BaseFragment() {

    companion object {
        fun newInstance() = HelpFragment()
    }

    private lateinit var rootView: View

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (!::rootView.isInitialized) {
            rootView = inflater.inflate(R.layout.fragment_help, container, false)
            iniUi()
        }
        return rootView
    }

    private fun iniUi() {
        rootView.tv_change_password.setOnClickListener {
            startActivity(
                Intent(activity!!, ChangePassword::class.java).putExtra(
                    Appconstant.NAME,
                    getString(R.string.change_password)
                )
            )
        }

        rootView.tv_manage_app_lock.setOnClickListener {
            startActivity(
                Intent(activity!!, ManageAppLockActivity::class.java).putExtra(
                    Appconstant.NAME,
                    getString(R.string.manage_app_lock)
                )
            )
        }

        rootView.tv_saved_payment.setOnClickListener {
            startActivity(
                Intent(activity!!, SelectPaymentDetails::class.java).putExtra(
                    Appconstant.NAME,
                    getString(R.string.saved_payments_details)
                )
            )
        }

        rootView.tv_privacy_policy.setOnClickListener {
            startActivity(
                Intent(activity!!, WebPageActivity::class.java).putExtra(
                    Appconstant.NAME,
                    getString(R.string.privacy_policy)
                )
            )
        }

        rootView.tv_about_us.setOnClickListener {
            startActivity(
                Intent(activity!!, WebPageActivity::class.java).putExtra(
                    Appconstant.NAME,
                    getString(R.string.about_us)
                )
            )
        }

        rootView.tv_terms.setOnClickListener {
            startActivity(
                Intent(activity!!, WebPageActivity::class.java).putExtra(
                    Appconstant.NAME,
                    getString(R.string.terms_conditions)
                )
            )
        }

        rootView.tv_contact.setOnClickListener {

            try {
                CommonUtils.sendMail(requireContext())
            } catch (e: Exception) {
            }
        }

    }
}