package com.app.luipay.help

import androidx.annotation.Keep
import com.app.luipay.request.BaseRequest
import com.google.gson.annotations.SerializedName

@Keep
class ChangePasswordRequest  : BaseRequest(){

    @SerializedName("OldPassword")
    var OldPassword:String = ""

    @SerializedName("NewPassword")
    var NewPassword:String = ""

}