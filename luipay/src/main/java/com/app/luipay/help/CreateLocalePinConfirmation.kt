package com.app.luipay.help

import android.app.Activity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import com.app.luipay.R
import com.app.luipay.base.BaseActivity
import com.app.luipay.utils.Appconstant
import com.app.luipay.utils.CommonUtils
import com.app.luipay.utils.Prefutils
import kotlinx.android.synthetic.main.activity_create_pin.*
import kotlinx.android.synthetic.main.center_title_layout.*

class CreateLocalePinConfirmation : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_pin)
        backBtn.setOnClickListener {
            setResult(Activity.RESULT_CANCELED)
            finish()
        }
        if (intent?.getBooleanExtra(Appconstant.NAME,false) == true){
            titleTv.text = getString(R.string.confirmation_pin)
        }else{
            titleTv.text = getString(R.string.create_confirmation_pin)
        }

        val pin = intent?.getStringExtra(Appconstant.ID)
        txtTitle.text=getString(R.string.confimation_pin)
        otpView.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                if (s.toString().length == 4) {
                    if (pin.equals(s.toString())) {
                        Prefutils.saveToPrefs(
                            this@CreateLocalePinConfirmation,
                            Prefutils.APP_LOCK_PIN,
                            pin
                        )
                            setResult(Activity.RESULT_OK)
                            finish()


                    } else {
                        CommonUtils.showToast(this@CreateLocalePinConfirmation, "Pin not match")
                    }
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

        })
    }
}