package com.app.luipay.help

import android.os.Bundle
import android.view.View.GONE
import android.view.View.VISIBLE
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.luipay.DBHandler.BankAccountData
import com.app.luipay.DBHandler.DBHelper
import com.app.luipay.R
import com.app.luipay.base.BaseActivity
import com.app.luipay.link_account.BankAccountListAdapter
import kotlinx.android.synthetic.main.activity_payment_details.*
import kotlinx.android.synthetic.main.center_title_layout.*

class SelectPaymentDetails : BaseActivity() {

    var bankData = ArrayList<BankAccountData?>()
    lateinit var bankAccountListAdapter: BankAccountListAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_payment_details)

        titleTv.text = getString(R.string.saved_payment_details)

        backBtn.setOnClickListener {
            finish()
        }
        manageUI()
    }

    override fun onResume() {
        super.onResume()
        getData()
    }

    private fun manageUI() {
        rv.layoutManager = LinearLayoutManager(this)
        bankAccountListAdapter = BankAccountListAdapter(this, bankData)

        rv.adapter = bankAccountListAdapter
    }

    private fun getData() {
        bankData.clear()
        bankData.addAll(DBHelper(this@SelectPaymentDetails).allData)
        if (bankData.isNotEmpty() && ::bankAccountListAdapter.isInitialized) {
            tv_no_history.visibility = GONE
            rv.visibility = VISIBLE
            bankAccountListAdapter.notifyDataSetChanged()
        } else {
            tv_no_history.visibility = VISIBLE
            rv.visibility = GONE
        }
    }


}