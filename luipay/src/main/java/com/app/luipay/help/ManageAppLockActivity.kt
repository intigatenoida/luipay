package com.app.luipay.help

import android.app.Activity
import android.app.KeyguardManager
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.text.TextUtils
import com.app.luipay.R
import com.app.luipay.base.BaseActivity
import com.app.luipay.utils.*
import com.google.android.material.bottomsheet.BottomSheetDialog
import kotlinx.android.synthetic.main.bottom_confirmation.*
import kotlinx.android.synthetic.main.center_title_layout.*
import kotlinx.android.synthetic.main.layout_activity_manage_app_lock.*

class ManageAppLockActivity : BaseActivity(), BiomatricAuthencation.BiomatricAuthencationListener {

    private var isUsePinClicked: Boolean = false
    private var dialogFragment: FingerprintDailog? = null
    var isLocallyCheck = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.layout_activity_manage_app_lock)
        backBtn.setOnClickListener { finish() }
        titleTv.text = getString(R.string.app_lock_password)
        chkMngLock.isChecked =
            Prefutils.getFromPrefs(this@ManageAppLockActivity, Prefutils.APP_LOCK)
                .equals(Appconstant.APP_LOCK_ENABLE)
        manageSwitchListener()
    }

    private fun manageSwitchListener() {
        chkMngLock.setOnCheckedChangeListener { buttonView, isChecked ->
            isUsePinClicked = false
            isLocallyCheck = isChecked
            enableFingrePrint()
        }
    }


    private fun enableFingrePrint() {
        if (CommonUtils.isBioMetricEnable(this@ManageAppLockActivity)) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                openFragmentDialog()
                BiomatricAuthencation.initlizeApp(this@ManageAppLockActivity)
            } else {
                askAuthencationDialog()
            }
        } else {
            askAuthencationDialog()
        }
    }

    private fun askAuthencationDialog() {
        val pin = Prefutils.getFromPrefs(this@ManageAppLockActivity, Prefutils.APP_LOCK_PIN)
        if (TextUtils.isEmpty(pin)) {
            startActivityForResult(
                Intent(
                    this@ManageAppLockActivity,
                    CreateLocalePinAcitivity::class.java
                ), 1000
            )
        } else {
            startActivityForResult(
                Intent(
                    this@ManageAppLockActivity,
                    CreateLocalePinConfirmation::class.java
                ).putExtra(Appconstant.ID, pin).putExtra(Appconstant.NAME, true), 1000
            )
        }

    }


    private fun openFragmentDialog() {
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        val prev = supportFragmentManager.findFragmentByTag("dialog")
        if (prev != null) {
            fragmentTransaction.remove(prev)
        }
        fragmentTransaction.addToBackStack(null)
        dialogFragment = FingerprintDailog.getInstance(listener)
        dialogFragment!!.show(fragmentTransaction, "dialog")
    }


    private fun openPinDiallog() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val km =
                getSystemService(Context.KEYGUARD_SERVICE) as KeyguardManager
            if (km.isKeyguardSecure) {
                val authIntent = km.createConfirmDeviceCredentialIntent(
                    getString(R.string.app_name),
                    getString(R.string.pin_msg)
                )
                startActivityForResult(authIntent, 1000)
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 1000 && resultCode == Activity.RESULT_OK) {
            Prefutils.saveToPrefs(
                this@ManageAppLockActivity,
                Prefutils.APP_LOCK,
                if (isLocallyCheck) {
                    Appconstant.APP_LOCK_ENABLE
                } else {
                    Appconstant.APP_LOCK_DISABLE
                }
            )

            openBottomNavigation()
        } else {
            chkMngLock.setOnCheckedChangeListener(null)
            chkMngLock.isChecked = !isLocallyCheck
            manageSwitchListener()
        }
    }

    override fun onAuthenticationSucceeded() {
        Prefutils.saveToPrefs(
            this@ManageAppLockActivity,
            Prefutils.APP_LOCK,
            if (isLocallyCheck) {
                Appconstant.APP_LOCK_ENABLE
            } else {
                Appconstant.APP_LOCK_DISABLE
            }
        )
        if (isLocallyCheck) {
            dialogFragment?.let { it.dismiss() }
            openBottomNavigation()
        }
    }

    private fun openBottomNavigation() {
        if (isLocallyCheck) {
            showConfirmationDialog()
        } else {
            finish()
        }
    }

    val listener = object : FingerprintDailog.onCancelListener {
        override fun onCancel() {
            dialogFragment?.dismiss()
            dialogFragment = null
            chkMngLock.setOnCheckedChangeListener(null)
            chkMngLock.isChecked = !isLocallyCheck
            manageSwitchListener()
        }

        override fun usePin() {
            isUsePinClicked = true
            dialogFragment?.dismiss()
            dialogFragment = null
            openPinDiallog()

        }

    }

    override fun onAuthenticationError(errorCode: Int) {
        if (!isUsePinClicked) {
            chkMngLock.setOnCheckedChangeListener(null)
            chkMngLock.isChecked = !isLocallyCheck
            manageSwitchListener()
        }
    }

    fun showConfirmationDialog() {
        val dialog = BottomSheetDialog(this, R.style.Theme_Dialog)
        dialog.setContentView(R.layout.bottom_confirmation)
        dialog.setCancelable(false)
        dialog.ivClose.setOnClickListener {
            dialog.dismiss()
            finish()
        }

        dialog.btn_proceed.setOnClickListener {
            dialog.dismiss()
            finish()
        }
        dialog.show()
    }

}