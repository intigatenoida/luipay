package com.app.luipay.firebase

import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.media.RingtoneManager
import android.net.Uri
import android.os.Build
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat
import com.app.luipay.R
import com.app.luipay.notifications.NotificationsActivity
import com.app.luipay.utils.CommonUtils
import com.app.luipay.utils.Prefutils
import com.google.firebase.messaging.FirebaseMessaging
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import kotlin.random.Random


class MyFireBaseMessagingService : FirebaseMessagingService() {

    val TAG = "MyFBMessagingService"

    val SUBSCRIBE_TOPIC = "LuiPayAnnouncement"

    override fun onNewToken(token: String) {
        Log.d(TAG, "Refreshed token: $token")
        if(token.isEmpty()){
            CommonUtils.setNotificationDeviceInfo(this,token,SUBSCRIBE_TOPIC)
        }else{
            Prefutils.saveToPrefs(this, Prefutils.DEVICE_TOKEN, token)
        }
        FirebaseMessaging.getInstance().subscribeToTopic(SUBSCRIBE_TOPIC)
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        super.onMessageReceived(remoteMessage)
        Log.d(TAG, remoteMessage.data.toString())

        val notificationSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)

        val mintent = Intent(this, NotificationsActivity::class.java)

        val pendingIntent = PendingIntent.getActivity(
            this, 0, mintent,
            PendingIntent.FLAG_ONE_SHOT
        )
        val notificationManager =
            getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        val notificationID = Random.nextInt(3000)

        sendNotification(
            remoteMessage.data["title"],
            remoteMessage.data["message"],
            notificationSoundUri,
            notificationManager,
            pendingIntent,
            notificationID
        )
    }

    private fun sendNotification(
        title: String?,
        message: String?,
        notificationSoundUri: Uri?,
        notificationManager: NotificationManager,
        pendingIntent: PendingIntent?,
        notificationID: Int
    ) {
        val largeIcon = BitmapFactory.decodeResource(
            resources,
            R.mipmap.ic_launcher
        )
        val notificationBuilder =
            NotificationCompat.Builder(
                this,
                getString(R.string.default_notification_channel_id)
            )
                .setLargeIcon(largeIcon)
                .setContentTitle(title)
                .setContentText(message)
                .setSmallIcon(R.mipmap.ic_launcher_round)
                .setStyle(NotificationCompat.BigTextStyle().bigText(title))
                .setStyle(NotificationCompat.BigTextStyle().bigText(message))
                .setAutoCancel(true)
                .setSound(notificationSoundUri)
                .setContentIntent(pendingIntent)

        //Set notification color to match your app color template
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            notificationBuilder.color = ContextCompat.getColor(this, R.color.colorAccent)
        }
        notificationManager.notify(notificationID, notificationBuilder.build())
    }

}