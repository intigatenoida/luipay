package com.app.luipay.history

import android.os.Bundle
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.luipay.R
import com.app.luipay.base.BaseActivity
import com.app.luipay.history.adapter.HistoryAdapter
import com.app.luipay.request.GetNotoficationListRequest
import com.app.luipay.response.BaseResponse
import com.app.luipay.retrofit.ApiClient
import com.app.luipay.retrofit.ApiInterface
import com.app.luipay.retrofit.CallbackManager
import com.app.luipay.utils.Appconstant
import com.app.luipay.utils.CommonUtils
import com.app.luipay.utils.Prefutils


class HistoryActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_transaction)
        LoadFragment(HistoryFragment.newInstance(), "", R.id.main_container)
    }

    
}