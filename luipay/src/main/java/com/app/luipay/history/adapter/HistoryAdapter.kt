package com.app.luipay.history.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.View.*
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.app.luipay.R
import com.app.luipay.history.responses.HistoryData
import com.app.luipay.utils.CommonUtils
import com.app.luipay.utils.custom_listener.HistoryItemClick
import kotlinx.android.synthetic.main.item_history.view.*
import kotlin.math.abs

class HistoryAdapter(private val mContext: Context, private val listener: HistoryItemClick) :
    RecyclerView.Adapter<HistoryAdapter.HistoryViewHolder>() {

    class HistoryViewHolder(item: View) : RecyclerView.ViewHolder(item)

    private var data: MutableList<HistoryData> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HistoryViewHolder {
        return HistoryViewHolder(
            LayoutInflater.from(mContext).inflate(
                R.layout.item_history,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: HistoryViewHolder, position: Int) {

        val temp = data[position]

        holder.itemView.title.text = temp.bankName ?: ""

        if (!temp.bankImage.isNullOrEmpty()) {
            holder.itemView.imgIcon.visibility = VISIBLE
            CommonUtils.loadImage(
                mContext,
                temp.bankImage ?: "",
                holder.itemView.imgIcon,
                R.drawable.temp_bank
            )
        } else {
            holder.itemView.imgIcon.visibility = INVISIBLE
        }

        if (temp.bankName.isNullOrEmpty() &&
            temp.bankImage.isNullOrEmpty()
        ) {
            holder.itemView.ln_bank.visibility = GONE
        } else {
            holder.itemView.ln_bank.visibility = VISIBLE
        }

        if (!temp.amount.isNullOrEmpty()) {
            var value = temp.amount.toDouble()
            value = abs(value)
            holder.itemView.amount.text = "${CommonUtils.getTwoDecimal2(value)} MVR"
        }

        if (!temp.transactionDate.isNullOrEmpty()) {
            holder.itemView.tv_date.visibility = VISIBLE
            holder.itemView.tv_date.text = CommonUtils.convertToLocalTimeZone(temp.transactionDate)
        } else {
            holder.itemView.tv_date.visibility = GONE
        }

        holder.itemView.transfer.text = temp.transactionType ?: ""
        //holder.itemView.tv_description.text = "${temp.userName ?: ""}\n${temp.mobileNo ?: ""}"

        if (!temp.transactionStatus.isNullOrEmpty()) {
            holder.itemView.status.text = temp.transactionStatus ?: ""
            if (temp.transactionStatus.equals("Completed")) {
                holder.itemView.status.setTextColor(
                    ContextCompat.getColor(
                        mContext,
                        R.color.green_status_color
                    )
                )
            } else {
                holder.itemView.status.setTextColor(
                    ContextCompat.getColor(
                        mContext,
                        R.color.failed_background_color_2
                    )
                )
            }
        } else {
            holder.itemView.status.text = ""
        }

        holder.itemView.container.setOnClickListener {
            listener?.let { it.onItemClick(temp) }
        }
    }

    fun updateData(historyList: MutableList<HistoryData>) {
        data.clear()
        data.addAll(historyList)
        notifyDataSetChanged()
    }


}