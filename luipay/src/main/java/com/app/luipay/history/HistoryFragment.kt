package com.app.luipay.history

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.luipay.R
import com.app.luipay.base.BaseFragment
import com.app.luipay.base.BaseViewModel
import com.app.luipay.history.adapter.HistoryAdapter
import com.app.luipay.history.responses.HistoryData
import com.app.luipay.history.responses.HistoryResponse
import com.app.luipay.request.ServiceAuthenticationToken
import com.app.luipay.retrofit.ApiClient
import com.app.luipay.retrofit.ApiInterface
import com.app.luipay.retrofit.CallbackManager
import com.app.luipay.utils.Appconstant
import com.app.luipay.utils.CommonUtils
import com.app.luipay.utils.custom_listener.HistoryItemClick
import kotlinx.android.synthetic.main.center_title_layout.view.*
import kotlinx.android.synthetic.main.layout_fragment_history.view.*
import retrofit2.Call


class HistoryFragment : BaseFragment(), HistoryItemClick {

    private lateinit var rootView: View
    private lateinit var adapter: HistoryAdapter
    private lateinit var model: BaseViewModel

    private var isProgressConsider: Boolean = false
    private lateinit var call: Call<HistoryResponse>

    companion object {
        fun newInstance() = HistoryFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (!::rootView.isInitialized) {
            rootView = inflater.inflate(R.layout.layout_fragment_history, container, false)
            initUI()
        }
        return rootView
    }

    private fun initUI() {
        model = ViewModelProviders.of(this)[BaseViewModel::class.java]
        setAdapter()

        rootView.backBtn.setOnClickListener {
            activity!!.finish()
        }

        rootView.titleTv.text = getString(R.string.history)

        isProgressConsider = true
        requestRecentTransaction()

        rootView.swp_main.setOnRefreshListener {
            isProgressConsider = false
            requestRecentTransaction()
        }

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        model.progressLiveData.observe(viewLifecycleOwner, Observer<Boolean> {
            if (isProgressConsider && it) {
                showProcessDialog()
            } else {
                hideProcessDialog()
            }
        })
    }

    override fun onStop() {
        super.onStop()
        hideProcessDialog()
        if (::call.isInitialized) {
            call.cancel()
        }
    }

    private fun setAdapter() {
        val layoutManager = LinearLayoutManager(context!!, LinearLayoutManager.VERTICAL, false)
        rootView.rcyHistory.layoutManager = layoutManager
        val verticalDecoration = DividerItemDecoration(
            context!!,
            DividerItemDecoration.VERTICAL
        )
        val verticalDivider =
            ContextCompat.getDrawable(context!!, R.drawable.divider_16)
        verticalDecoration.setDrawable(verticalDivider!!)
        rootView.rcyHistory.addItemDecoration(verticalDecoration)
        adapter = HistoryAdapter(context!!, this)
        rootView.rcyHistory.adapter = adapter
    }

    private fun requestRecentTransaction() {
        if (!isOnline()) {
            CommonUtils.showToast(
                activity!!,
                getString(R.string.turn_on_internet)
            )
            return
        }

        model.toggleProgress(true)

        val token = ServiceAuthenticationToken()
        token.serviceAuthenticationToken = ApiClient.android_token
        val helper = ApiClient.getClient(context!!).create(ApiInterface::class.java)
        call = helper.getTransactionHistory(token, "application/json")
        call.enqueue(object : CallbackManager<HistoryResponse>() {
            override fun onSuccess(any: HistoryResponse?, message: String) {
                model.toggleProgress(false)
                if (rootView.swp_main.isRefreshing) {
                    rootView.swp_main.isRefreshing = false
                }
                if (any?.historyList == null) {
                    return
                }

                if (any.historyList.isNotEmpty()) {
                    rootView.tv_no_history.visibility = GONE
                    adapter.updateData(any.historyList)
                } else {
                    rootView.tv_no_history.visibility = VISIBLE
                }

            }

            override fun onFailure(message: String) {
                rootView.tv_no_history.visibility = VISIBLE
                model.toggleProgress(false)
                if (rootView.swp_main.isRefreshing) {
                    rootView.swp_main.isRefreshing = false
                }
            }

            override fun onError(error: String) {
                rootView.tv_no_history.visibility = VISIBLE
                model.toggleProgress(false)
                if (rootView.swp_main.isRefreshing) {
                    rootView.swp_main.isRefreshing = false
                }
            }

            override fun cancelRequest(call: Call<*>?, errorCode: Int) {
                super.cancelRequest(call, errorCode)
                model.toggleProgress(false)
                if (rootView.swp_main.isRefreshing) {
                    rootView.swp_main.isRefreshing = false
                }
            }

        })
    }

    override fun onItemClick(historyData: HistoryData) {
        this@HistoryFragment.startActivity(
            Intent(
                context!!,
                HistoryDetailActivity::class.java
            ).putExtra(Appconstant.DATA, historyData)
        )
    }
}