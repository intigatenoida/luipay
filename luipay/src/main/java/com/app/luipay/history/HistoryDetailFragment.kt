package com.app.luipay.history

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.luipay.R
import com.app.luipay.base.BaseFragment
import com.app.luipay.history.adapter.HistoryDetailDataModel
import com.app.luipay.history.adapter.HistoryDetailItemAdapter
import com.app.luipay.history.responses.HistoryData
import com.app.luipay.utils.Appconstant
import com.app.luipay.utils.CommonUtils
import kotlinx.android.synthetic.main.center_title_layout.view.*
import kotlinx.android.synthetic.main.item_history.view.*
import kotlinx.android.synthetic.main.layout_fragment_history_details.view.*
import kotlinx.android.synthetic.main.layout_fragment_history_details.view.amount
import kotlinx.android.synthetic.main.layout_fragment_history_details.view.imgIcon
import kotlinx.android.synthetic.main.layout_fragment_history_details.view.status
import kotlinx.android.synthetic.main.layout_fragment_history_details.view.title
import kotlin.math.abs

class HistoryDetailFragment : BaseFragment() {

    private lateinit var rootView: View
    private var transactionData: HistoryData? = null

    companion object {
        fun newInstance(bundle: Bundle?): HistoryDetailFragment {
            val fg = HistoryDetailFragment()
            fg.arguments = bundle
            return fg
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (!::rootView.isInitialized) {
            rootView = inflater.inflate(R.layout.layout_fragment_history_details, container, false)
            initUI()
        }
        return rootView
    }

    private fun initUI() {
        rootView.header.titleTv.text = "Transaction Details"
        rootView.backBtn.setOnClickListener {
            activity!!.finish()
        }

        transactionData = arguments?.getParcelable(Appconstant.DATA)
        if (transactionData?.amount.isNullOrEmpty()) {
            rootView.amount.text = ""
        } else {
            var value = transactionData!!.amount!!.toDouble()
            value = abs(value)
            rootView.amount.text = "${CommonUtils.getTwoDecimal2(value)}"
        }

        if (transactionData?.transactionStatus.equals("Completed")) {
            rootView.greenGroup.setBackgroundColor(
                ContextCompat.getColor(
                    context!!,
                    R.color.green_shade_1_100per
                )
            )
            rootView.status.visibility = VISIBLE
        } else {
            rootView.greenGroup.setBackgroundColor(
                ContextCompat.getColor(
                    context!!,
                    R.color.failed_background_color
                )
            )
            rootView.status.visibility = GONE
        }

        if (transactionData?.transactionDate.isNullOrEmpty()) {
            rootView.date.visibility = GONE
        } else {
            rootView.date.visibility = VISIBLE
            rootView.date.text = CommonUtils.onlyDate(transactionData!!.transactionDate!!)
        }

        if (transactionData?.bankName.isNullOrEmpty()) {
            rootView.imgIcon.visibility = GONE
            rootView.title.visibility = GONE
        } else {
            rootView.imgIcon.visibility = VISIBLE
            rootView.title.visibility = VISIBLE
            rootView.title.text = "Top up your wallet from ${transactionData?.bankName ?: ""}"
        }

        setAdapter()
    }

    private fun setAdapter() {
        rootView.detailRcy.layoutManager = LinearLayoutManager(context!!)

        val adapter = HistoryDetailItemAdapter()
        val data = mutableListOf<HistoryDetailDataModel>()
        if (!transactionData?.fund.isNullOrEmpty()) {
            data.add(HistoryDetailDataModel("Fund", transactionData?.fund ?: ""))
        }

        if (!transactionData?.transactionId.isNullOrEmpty()) {
            data.add(HistoryDetailDataModel("Trading Code", transactionData?.transactionId ?: ""))
        }

        if (!transactionData?.transactionDate.isNullOrEmpty()) {
            val time = CommonUtils.convertToLocalTimeZone(transactionData?.transactionDate ?: "")
            data.add(HistoryDetailDataModel("Time", time))
        }

        if (!transactionData?.transactionType.isNullOrEmpty()) {

            if (transactionData!!.transactionType!!.equals("Transfer", true)) {

                if (!transactionData?.amount.isNullOrEmpty()) {

                    val value = transactionData!!.amount!!.toDouble()
                    if (value <= 0) {
                        //send
                        //some person requested from us
                        //Transfer money to Alex Hartman
                        rootView.imgIcon.visibility = VISIBLE
                        rootView.imgIcon.setImageResource(R.drawable.ic_transfer_money)
                        rootView.title.visibility = VISIBLE
                        rootView.title.text =
                            "Transfer money to ${transactionData?.associatedUserName ?: ""}"


                        if (!transactionData?.associatedUserName.isNullOrEmpty()) {
                            data.add(
                                HistoryDetailDataModel(
                                    "Receiver",
                                    transactionData?.associatedUserName ?: ""
                                )
                            )
                        }

                        if (!transactionData?.associatedMobileNo.isNullOrEmpty()) {
                            data.add(
                                HistoryDetailDataModel(
                                    "Phone Number",
                                    transactionData?.associatedMobileNo ?: ""
                                )
                            )
                        }

                        if (!transactionData?.userName.isNullOrEmpty()) {
                            data.add(
                                HistoryDetailDataModel(
                                    "Sender",
                                    transactionData?.userName ?: ""
                                )
                            )
                        }

                        if (!transactionData?.mobileNo.isNullOrEmpty()) {
                            data.add(
                                HistoryDetailDataModel(
                                    "Phone Number",
                                    transactionData?.mobileNo ?: ""
                                )
                            )
                        }

                    } else {
                        //if someone transfer to us (without reuest) - associated
                        //request money from someone - associated
                        //Receive money from Alex Hartman

                        rootView.imgIcon.visibility = VISIBLE
                        rootView.imgIcon.setImageResource(R.drawable.ic_received_money)
                        rootView.title.visibility = VISIBLE
                        rootView.title.text =
                            "Receive money from ${transactionData?.associatedUserName ?: ""}"


                        if (!transactionData?.userName.isNullOrEmpty()) {
                            data.add(
                                HistoryDetailDataModel(
                                    "Receiver",
                                    transactionData?.userName ?: ""
                                )
                            )
                        }

                        if (!transactionData?.mobileNo.isNullOrEmpty()) {
                            data.add(
                                HistoryDetailDataModel(
                                    "Phone Number",
                                    transactionData?.mobileNo ?: ""
                                )
                            )
                        }

                        if (!transactionData?.associatedUserName.isNullOrEmpty()) {
                            data.add(
                                HistoryDetailDataModel(
                                    "Sender",
                                    transactionData?.associatedUserName ?: ""
                                )
                            )
                        }

                        if (!transactionData?.associatedMobileNo.isNullOrEmpty()) {
                            data.add(
                                HistoryDetailDataModel(
                                    "Phone Number",
                                    transactionData?.associatedMobileNo ?: ""
                                )
                            )
                        }

                    }

                }

            } else if (transactionData!!.transactionType!!.equals("Refill", true)) {


                rootView.imgIcon.visibility = VISIBLE
                rootView.imgIcon.setImageResource(R.drawable.ic_top_up)
                rootView.title.visibility = VISIBLE
                rootView.title.text = "Top up your wallet from ${transactionData?.bankName ?: ""}"

                //refill success
                rootView.status.text = getString(R.string.successful_transaction)
                rootView.status.setCompoundDrawablesRelativeWithIntrinsicBounds(0,0,R.drawable.check,0)
                rootView.greenGroup.setBackgroundColor(ContextCompat.getColor(context!!,R.color.green_shade_1_100per))

                //refill reject with reason
                /*rootView.status.text = getString(R.string.cancel_transaction)
                rootView.status.setCompoundDrawablesRelativeWithIntrinsicBounds(0,0,R.drawable.ic_canceled,0)
                rootView.greenGroup.setBackgroundColor(ContextCompat.getColor(context!!,R.color.red_1))
                rootView.title.text = "${transactionData?.rejectReason ?: ""}"*/


                if (!transactionData?.userName.isNullOrEmpty()) {
                    data.add(HistoryDetailDataModel("Receiver", transactionData?.userName ?: ""))
                }

                if (!transactionData?.mobileNo.isNullOrEmpty()) {
                    data.add(
                        HistoryDetailDataModel(
                            "Phone Number",
                            transactionData?.mobileNo ?: ""
                        )
                    )
                }
            }

            data.add(
                HistoryDetailDataModel(
                    "Transaction Type",
                    transactionData?.transactionType ?: ""
                )
            )
        }

        if (!transactionData?.amount.isNullOrEmpty()) {
            var value = transactionData!!.amount!!.toDouble()
            value = abs(value)
            data.add(
                HistoryDetailDataModel(
                    "Amount of money",
                    "${CommonUtils.getTwoDecimal2(value)} MVR"
                )
            )
        }

        if (!transactionData?.memo.isNullOrEmpty()) {
            data.add(HistoryDetailDataModel("Message", transactionData?.memo ?: ""))
        }

        adapter.updateData(data)
        rootView.detailRcy.adapter = adapter

    }

}