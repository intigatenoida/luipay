package com.app.luipay.history.responses

import android.os.Parcelable
import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize


@Parcelize
@Keep
data class HistoryData(
    @SerializedName("Amount")
    val amount: String?,
    @SerializedName("AssociatedUserId")
    val associatedUserId: String?,
    @SerializedName("BalanceAmount")
    val balanceAmount: String?,
    @SerializedName("BankImage")
    val bankImage: String?,
    @SerializedName("BankName")
    val bankName: String?,
    @SerializedName("Id")
    val id: Int,
    @SerializedName("Image")
    val image: String?,
    @SerializedName("Memo")
    val memo: String?,
    @SerializedName("MobileNo")
    val mobileNo: String?,
    @SerializedName("RejectReason")
    val rejectReason: String?,
    @SerializedName("TransactionDate")
    val transactionDate: String?,
    @SerializedName("TransactionId")
    val transactionId: String?,
    @SerializedName("TransactionStatus")
    val transactionStatus: String?,
    @SerializedName("TransactionType")
    val transactionType: String?,
    @SerializedName("UserName")
    val userName: String?,
    @SerializedName("AssociatedUserName")
    val associatedUserName: String?,
    @SerializedName("AssociatedMobileNo")
    val associatedMobileNo: String?,
    @SerializedName("Fund")
    val fund: String?
):Parcelable