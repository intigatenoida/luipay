package com.app.luipay.history.adapter

data class HistoryDetailDataModel (

    var title: String = "",

    var value:String = ""
)