package com.app.luipay.history

import android.os.Bundle
import com.app.luipay.R
import com.app.luipay.base.BaseActivity

class HistoryDetailActivity : BaseActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.tranaction_detail)
        LoadFragment(HistoryDetailFragment.newInstance(intent?.extras), "", R.id.main_container)
    }
}