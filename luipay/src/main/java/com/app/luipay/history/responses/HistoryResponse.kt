package com.app.luipay.history.responses

import androidx.annotation.Keep
import com.app.luipay.response.BaseResponse
import com.google.gson.annotations.SerializedName

@Keep
data class HistoryResponse(

    @SerializedName("ResponseCollection")
    var historyList:MutableList<HistoryData> = mutableListOf()

):BaseResponse()