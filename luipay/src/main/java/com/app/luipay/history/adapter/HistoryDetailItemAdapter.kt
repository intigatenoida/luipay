package com.app.luipay.history.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.app.luipay.R
import kotlinx.android.synthetic.main.item_tranasaction_detail.view.*

class HistoryDetailItemAdapter : RecyclerView.Adapter<HistoryDetailItemAdapter.ViewHolder>() {

    class ViewHolder(item: View) : RecyclerView.ViewHolder(item)

    private var data:MutableList<HistoryDetailDataModel> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_tranasaction_detail,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.itemView.title.text = data[position].title
        holder.itemView.detail.text = data[position].value
    }

    fun updateData(data: MutableList<HistoryDetailDataModel>) {
        this.data = data
        notifyDataSetChanged()
    }
}