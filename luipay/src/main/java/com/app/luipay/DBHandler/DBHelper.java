package com.app.luipay.DBHandler;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.text.TextUtils;

import java.util.ArrayList;


public class DBHelper extends SQLiteOpenHelper {


    private static final int DATABASE_VERSION = 1;

    private static final String DATABASE_NAME = "LuiPay";


    private static final String TABLE_RAW_DATA = "CARD_DATA";

    private static final String KEY_ID = "Id";
    private static final String KEY_CARD_NUMBER = "number";
    private static final String KEY_NAME = "Name";
    private static final String KEY_PASSPORT = "passport";
    private static final String KEY_CVV = "cvv";
    private static final String KEY_TYPE = "type";
    private static final String KEY_BANK_NAME = "Bank_name";
    private static final String KEY_BANK_Code = "Bank_code";
    private static final String KEY_BANK_URL = "Bank_url";
    private static final String KEY_BANK_ID = "Bank_id";

    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        String CREATE_RAW_DATA_TABLE = "CREATE TABLE  " + TABLE_RAW_DATA + "("
                + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL ,"
                + KEY_NAME + " TEXT NOT NULL,"
                + KEY_CARD_NUMBER + " TEXT NOT NULL,"
                + KEY_CVV + " TEXT,"
                + KEY_PASSPORT + " TEXT NOT NULL,"
                + KEY_TYPE + " INTEGER NOT NULL,"
                + KEY_BANK_ID + " INTEGER ,"
                + KEY_BANK_NAME + " TEXT,"
                + KEY_BANK_URL + " TEXT,"
                + KEY_BANK_Code + " TEXT" + ")";
        db.execSQL(CREATE_RAW_DATA_TABLE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_RAW_DATA);
        onCreate(db);
    }


    /**
     * Insert new Rawdata
     *
     * @param rawdata
     * @return
     */
    public long InsertCardData(BankAccountData rawdata) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(KEY_NAME, rawdata.getName());
        values.put(KEY_CARD_NUMBER, rawdata.getCardNumber());
        values.put(KEY_PASSPORT, rawdata.getIdPassport());
        values.put(KEY_CVV, TextUtils.isEmpty(rawdata.getCvv()) ? "" : rawdata.getCvv());
        values.put(KEY_TYPE, rawdata.getType());
        values.put(KEY_BANK_ID, rawdata.getBankid());
        values.put(KEY_BANK_NAME, TextUtils.isEmpty(rawdata.getBankName()) ? "" : rawdata.getBankName());
        values.put(KEY_BANK_Code, TextUtils.isEmpty(rawdata.getBankCode()) ? "" : rawdata.getBankCode());
        values.put(KEY_BANK_URL, TextUtils.isEmpty(rawdata.getBankImageUrl()) ? "" : rawdata.getBankImageUrl());
        long r = db.insert(TABLE_RAW_DATA, null, values);
        db.close();
        return r;
    }


    public ArrayList<BankAccountData> getAllData() {
        ArrayList<BankAccountData> dataList = new ArrayList<>();
        SQLiteDatabase db = null;
        try {
            db = this.getWritableDatabase();
            String select = "SELECT * FROM " + TABLE_RAW_DATA;
            Cursor mCursor = db.rawQuery(select, null);
            if (mCursor.getCount() > 0) {
                if (mCursor.moveToFirst()) {
                    do {
                        BankAccountData data = new BankAccountData();
                        data.setName(mCursor.getString(mCursor.getColumnIndex(KEY_NAME)));
                        data.setId(mCursor.getInt(mCursor.getColumnIndex(KEY_ID)));
                        data.setCardNumber(mCursor.getString(mCursor.getColumnIndex(KEY_CARD_NUMBER)));
                        data.setCvv(mCursor.getString(mCursor.getColumnIndex(KEY_CVV)));
                        data.setIdPassport(mCursor.getString(mCursor.getColumnIndex(KEY_PASSPORT)));
                        data.setType(mCursor.getInt(mCursor.getColumnIndex(KEY_TYPE)));
                        data.setBankName(mCursor.getString(mCursor.getColumnIndex(KEY_BANK_NAME)));
                        data.setBankCode(mCursor.getString(mCursor.getColumnIndex(KEY_BANK_Code)));
                        data.setBankImageUrl(mCursor.getString(mCursor.getColumnIndex(KEY_BANK_URL)));
                        data.setBankid(mCursor.getInt(mCursor.getColumnIndex(KEY_BANK_ID)));
                        dataList.add(data);
                    } while (mCursor.moveToNext());
                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.close();
            return dataList;
        }
    }


    public ArrayList<BankAccountData> getAllDataByBankId(int bankId) {
        ArrayList<BankAccountData> dataList = new ArrayList<>();
        SQLiteDatabase db = null;
        try {
            db = this.getWritableDatabase();
            String select = "SELECT * FROM " + TABLE_RAW_DATA + " WHERE " + KEY_BANK_ID + " = '" + bankId + "'";
            ;
            Cursor mCursor = db.rawQuery(select, null);
            if (mCursor.getCount() > 0) {
                if (mCursor.moveToFirst()) {
                    do {
                        BankAccountData data = new BankAccountData();
                        data.setName(mCursor.getString(mCursor.getColumnIndex(KEY_NAME)));
                        data.setId(mCursor.getInt(mCursor.getColumnIndex(KEY_ID)));
                        data.setCardNumber(mCursor.getString(mCursor.getColumnIndex(KEY_CARD_NUMBER)));
                        data.setCvv(mCursor.getString(mCursor.getColumnIndex(KEY_CVV)));
                        data.setIdPassport(mCursor.getString(mCursor.getColumnIndex(KEY_PASSPORT)));
                        data.setType(mCursor.getInt(mCursor.getColumnIndex(KEY_TYPE)));
                        data.setBankName(mCursor.getString(mCursor.getColumnIndex(KEY_BANK_NAME)));
                        data.setBankCode(mCursor.getString(mCursor.getColumnIndex(KEY_BANK_Code)));
                        data.setBankImageUrl(mCursor.getString(mCursor.getColumnIndex(KEY_BANK_URL)));
                        data.setBankid(mCursor.getInt(mCursor.getColumnIndex(KEY_BANK_ID)));
                        dataList.add(data);
                    } while (mCursor.moveToNext());
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.close();
            return dataList;
        }
    }


    public ArrayList<BankAccountData> getAllDataByUserId(int userid) {
        ArrayList<BankAccountData> dataList = new ArrayList<>();
        SQLiteDatabase db = null;
        try {
            db = this.getWritableDatabase();
            String select = "SELECT * FROM " + TABLE_RAW_DATA + " WHERE " + KEY_ID + " = '" + userid + "'";
            Cursor mCursor = db.rawQuery(select, null);
            if (mCursor.getCount() > 0) {
                if (mCursor.moveToFirst()) {
                    do {
                        BankAccountData data = new BankAccountData();
                        data.setName(mCursor.getString(mCursor.getColumnIndex(KEY_NAME)));
                        data.setId(mCursor.getInt(mCursor.getColumnIndex(KEY_ID)));
                        data.setCardNumber(mCursor.getString(mCursor.getColumnIndex(KEY_CARD_NUMBER)));
                        data.setCvv(mCursor.getString(mCursor.getColumnIndex(KEY_CVV)));
                        data.setIdPassport(mCursor.getString(mCursor.getColumnIndex(KEY_PASSPORT)));
                        data.setType(mCursor.getInt(mCursor.getColumnIndex(KEY_TYPE)));
                        data.setBankName(mCursor.getString(mCursor.getColumnIndex(KEY_BANK_NAME)));
                        data.setBankCode(mCursor.getString(mCursor.getColumnIndex(KEY_BANK_Code)));
                        data.setBankImageUrl(mCursor.getString(mCursor.getColumnIndex(KEY_BANK_URL)));
                        data.setBankid(mCursor.getInt(mCursor.getColumnIndex(KEY_BANK_ID)));
                        dataList.add(data);
                    } while (mCursor.moveToNext());
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.close();
            return dataList;
        }
    }

    public long updateCard(BankAccountData rawdata, long id) {
        long updateId = 0;
        try {
            SQLiteDatabase db = getReadableDatabase();
            ContentValues contentValues = new ContentValues();
            contentValues.put(KEY_NAME, rawdata.getName());
            contentValues.put(KEY_CARD_NUMBER, rawdata.getCardNumber());
            contentValues.put(KEY_PASSPORT, rawdata.getIdPassport());
            updateId = db.update(TABLE_RAW_DATA, contentValues, KEY_ID + "=" + id, null);
            return updateId;
        } catch (SQLiteException e) {
            e.printStackTrace();
            return 0;
        }

    }

    public void clearDatabase() {
        this.getWritableDatabase().execSQL("DELETE FROM " + TABLE_RAW_DATA);
    }

    public void deleteUsingID(int id) {
        getWritableDatabase().execSQL("delete from " + TABLE_RAW_DATA + " where " + KEY_ID + "= '" + id + "'");
    }

}

